
% clear CH4 CH4_avg IRF_CH4 M I DC
clear all
%% load data file
[IRFFiles,DataFolder] = uigetfile('*.*','Please select FLIm iRF data file',...
    'M:\Ben\V4 vs V5 CH4','MultiSelect', 'on');
CH = loadBinary(fullfile(DataFolder,IRFFiles),'double',0);
NumOfWaveform = size(CH,1);
CH_raw_avg = sum(CH)/NumOfWaveform;
% plot(CH_avg)
% hold on

% CH_BG = loadBinary(fullfile(DataFolder,strcat(IRFFiles,' BKGND')),'double',0);
% NumOfWaveform = size(CH_BG,1);
% CH_BG_avg = sum(CH_BG)/NumOfWaveform;
% plot(CH_BG_avg);
% CH_avg = CH_raw_avg-CH_BG_avg;
CH_avg = CH_raw_avg;
% plot(CH_avg);

%% substrct DC value from iRF
DC = mean(CH_avg(1,1500:2000));
CH_avg = CH_avg-DC;
% plot(CH_avg)
%% 
[M I] = max(CH_avg);
IRF_CH = CH_avg(I-25:I+224)/M;
IRF_CH = IRF_CH';
plot(IRF_CH);
%% save to file
% cd('M:\Tissue Engineering Project\Data\IRFs')
% fid=fopen('Laser.txt','w');
% fprintf(fid, '%12.6E \t', size(IRF_CH'));
% fprintf(fid, '%12.6E \t', IRF_CH);
% fclose(fid);
% plot(avg_WF);
% hold on
% hold off