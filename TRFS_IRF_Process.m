%% scrpit to process TRFS IRF
close all
clear all

%% Load TRFS data file
[File,dirpath] = uigetfile('*','Select TRFS Data Files (.txt)');
% TRFS_IRF=loadBinary(fullfile(dirpath,File),'double',0);
TRFS_IRF=load(fullfile(dirpath,File));
plot(TRFS_IRF(:,1))
% get number of waveforms
no_waveform = size(TRFS_IRF,2);
% average waveforms
avg_IRF = sum(TRFS_IRF,2)/no_waveform;
plot(avg_IRF)
%% calculate DC value
DC = mean(avg_IRF(2500:4000));
% remove DC
avg_IRF = (avg_IRF-DC);
plot(avg_IRF)
%% chop IRF
[~,I]= min(avg_IRF);
IRF = avg_IRF(I-100:I+399);
% normalize IRF
IRF = IRF/abs(min(IRF));
plot(IRF)
% cd to IRF folder on server
cd('M:\Tissue Engineering Project\Data\IRFs')
save('TRFS_V4_TE002_Scope_20160509_1000avg.dat','IRF', '-ascii')