close all
clear all
%% load data
cd('M:\Tissue Engineering Project')
%  load('M:\Tissue Engineering Project 2015\Data\20151211 Collagen gel day 0\FLIM\2015_12_11_gel1 6mgperml\Result\map.mat')
[Files,dirpath]=uigetfile('*.mat','Select result file(s)' ); 
dirpath
load([dirpath Files]);

%% step up plotting axis
ystep = 0.1;
xstep = 0.1;
x_length = size(LT1,1);
y_length = size(LT1,2);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);

% int_range = [0 20];
% LT_range = [0 7];

%% Intensity ratio plot
% SNR mask
M1 = ones(size(INT1));
M2 = ones(size(INT2));
M3 = ones(size(INT3));
M4 = ones(size(INT4));
M1(SNR1<25) = 0;
M2(SNR2<25) = 0;
M3(SNR3<25) = 0;
M4(SNR4<25) = 0;
M1(INT1<1) = 0;
M2(INT2<1) = 0;
M3(INT3<1) = 0;
M4(INT4<1) = 0;

int_ratio_ch1 = INT1./(INT1+INT2+INT3).*M1;
int_ratio_ch2 = INT2./(INT1+INT2+INT3).*M2;
% int_ratio_ch3 = INT3./(INT1+INT2);

figure('name','Intensity ratio images.');
colormap jet
% subplot(1,2,1);
imagesc(x,y,int_ratio_ch1',[0.6 1]);
axis equal
title('CH1')
colorbar
% subplot(1,2,2);
figure
imagesc(x,y,int_ratio_ch2',[0 1]);
axis equal
title('CH2')
colorbar
% subplot(1,3,3);
% imagesc(x,y,int_ratio_ch3',[0 1]);
% axis equal
% title('CH3/CH123')
% cd('M:\Tissue Engineering Project 2015\Data\20151120\FLIM\Intensity Ratio')