close all
clear all
% cd to your folder and rename all folder to start with 'Sample'
path('C:\Users\XZ\OneDrive\UC Davis\Projects\Programming\Matlab Code',path);

cd('M:\Tissue Engineering Project 2015\Data\20151012BP\FLIM\All Result');
sample_name_list = dir('1_*.mat');
no_files = length(sample_name_list);

%% start plotting
% h=figure;
% colormap jet
for m=1:no_files
    h=figure;
    colormap jet
    file_name = sample_name_list(m).name;
    [pathstr,name,ext] = fileparts(file_name);
    load(file_name);
%     figure
    %set pixels that has a negative intensity to 0 and normalize intensity
    %so that it can be used with intensity weighted lifetime calculation
    ystep = 0.2;
    xstep = 0.05;
    x_length = size(LT1,1);
    y_length = size(LT1,2);
    x = linspace(0,x_length*xstep,x_length);
    y = linspace(0,y_length*ystep,y_length);
    
    %% Intensity ratio plot
    % SNR mask
    M1 = ones(size(INT1));
    M2 = ones(size(INT2));
    M3 = ones(size(INT3));
    M4 = ones(size(INT4));
    M1(SNR1<25) = 0;
    M2(SNR2<25) = 0;
    M3(SNR3<25) = 0;
    M4(SNR4<25) = 0;
%     M1(INT1<1) = 0;
%     M2(INT2<1) = 0;
%     M3(INT3<1) = 0;
%     M4(INT4<1) = 0;
    
    int_ratio_ch1 = INT1./(INT1+INT2+INT3).*M1;
    int_ratio_ch2 = INT2./(INT1+INT2+INT3).*M2;
    % int_ratio_ch3 = INT3./(INT1+INT2);
    
%     figure('name','Intensity ratio images.');
%     colormap jet
    k=2*m;
    subplot(1,2,1);
    imagesc(x,y,int_ratio_ch1',[0.4 0.8]);
    axis equal
    title('CH1')
    if m == 1
        ylabel('Endo')
    elseif m == 2
        ylabel('Nonendo')
    end
    colorbar
    
    subplot(1,2,2);
    imagesc(x,y,int_ratio_ch2',[0 0.4]);
    axis equal
    title('CH2')
    
    colorbar
    drawnow
    
    %     savefig(name);
end
% print([name '.jpeg'],'-djpeg','-r300')