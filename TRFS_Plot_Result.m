%scrtpt to plot TRFS result, writen by Xiangnan 02/17/2016
close all
clear all

%% chose files
Root = 'C:\Users\XZ\Desktop\Data\20160408_collagen_GAG_Study\DeCon_0.916';
[TRFSFiles,DataFolder] = uigetfile('*.mat*','Please select FLIm data file',...
    Root,'MultiSelect', 'on');

%% Load one file to get data size, Initialize Data matrix
load(fullfile(DataFolder,TRFSFiles{1}));
% [pathstr,name,ext] = fileparts(TRFSFiles{1});
% sample_name = name;

NumOfFiles = length(TRFSFiles);
try
SpecLength = length(avg_spec_int);
catch
    SpecLength = length(spec_int);
end
avg_int_data_all = zeros(SpecLength,NumOfFiles);
avg_lt_data_all = zeros(SpecLength,NumOfFiles);
gain_data_all = zeros(SpecLength,NumOfFiles);
norm_spec_int = zeros(SpecLength,NumOfFiles);
avg_int_std_data_all = zeros(SpecLength,NumOfFiles);
avg_lt_std_data_all = zeros(SpecLength,NumOfFiles);
% legend_vector = zeros(1,NumOfFiles);
% plotStyle = {'.-k','.-b','.-r','.-g','.-y'};
plotStyle={'-k.','-g.', '-r.','-b.','-mx','-.ys','-.kd', '-.b^','-.g>','-.rp',}; % with marker
% creat matrix with all data
%%

for i = 1:NumOfFiles
    load(fullfile(DataFolder,TRFSFiles{i}));
    [pathstr,name,ext] = fileparts(TRFSFiles{i});
    sample_name{i} = name;
    
    lambda_int = lambda;
    lambda_lt = lambda;
    try
    avg_int_data_all(:,i) = avg_spec_int;
    catch
          avg_int_data_all(:,i) = spec_int;
    end  
    try
    avg_int_std_data_all(:,i) = avg_spec_int_std;
    catch
            avg_int_std_data_all(:,i) =avg_int_std_data_all(:,i);
    end
    try
    avg_lt_data_all(:,i) = avg_lt;
    catch
    avg_lt_data_all(:,i) = lifet_avg;
    end
    try
    avg_lt_std_data_all(:,i) = avg_lt_std;
    catch
            avg_lt_std_data_all(:,i) = avg_lt_std_data_all(:,i);
    end
    
%     gain_data_all(:,i) = gain(:,1);
    avg_norm_spec_int_all(:,i) = norm_spec_int;
    avg_norm_spec_int_std_all(:,i) = norm_spec_int_std;
%     legendInfo{i} = ['Sample ' num2str(i)];
end
%% plot lt
set(0,'DefaultLineMarkerSize',12);

hh=figure;
movegui(hh,'northwest');
for i=1:1:NumOfFiles
h{i}=plot_with_transparent_errorbars(lambda_lt,avg_lt_data_all(:,i),avg_lt_std_data_all(:,i),plotStyle{i},0.3);
if i==1
   legend_vector=h{i}; 
else
legend_vector=[legend_vector h{i}];
end
hold on
end
hold off
ll=legend(legend_vector,sample_name,'Interpreter', 'none','Location','southeast');

% ll=legend([h{1} h{2} h{3} h{4}],{'20 ','80','20','80'});
% ll=legend([h{1} h{2} h{3} h{4}],{'Sample 1','Sample 2','Sample 3','Sample 4'},'Location','southeast');
% ll=legend([h{1} h{2} h{3} h{4} h{5}],{'OM1','OM2','OM3','OM4', 'OMAcell'});
% ll=legend([h{1} h{2} h{3} h{4} h{5}],{'OM1','OM2','OM3','OM4','OMAcell'});
% ll=legend([h{1} h{2}],{'GM','OM'});
title('Average Lifetime')
xlim([360 590])
set(gca,'XTick',[360:20:590])
ylim([0 9])
set(gca,'YTick',[0:0.5:9])
xlabel('Wavelength(nm)')
ylabel('Lifetime(ns)')
set(gca,'FontSize',14);
% savefig('Lifetime.fig');
% cd('C:\Users\XZ\Desktop\Box Sync\20160408_collagen_GAG_study\GAG')
% savefig(hh, strcat(sample_name, '_LT'))
%% plot INT
hh=figure;
movegui(hh,'northeast');
for i=1:1:NumOfFiles
h{i}=plot_with_transparent_errorbars(lambda_lt,avg_norm_spec_int_all(:,i),avg_norm_spec_int_std_all(:,i),plotStyle{i},0.3);
if i==1
   legend_vector=h{i}; 
else
legend_vector=[legend_vector h{i}];
end
hold on
end
hold off
% ll=legend([h{1} h{2} h{3} h{4}],{'20','80','20','80'});
ll=legend(legend_vector,sample_name,'Interpreter', 'none');
% ll=legend([h{1} h{2}],{'GM','OM'});
title('Normalized Intensity')
xlabel('Wavelength(nm)')
ylabel('Normalized Intensity(a.u.)')
xlim([360 590])
ylim([0 1.2])
set(gca,'XTick',[360:20:590])
set(gca,'FontSize',14);
% savefig('NormlizedSpectrum.fig');
% savefig(hh,strcat(sample_name, '_INT'))
% plot(lt_data_all);
% name = 1:NumOfFiles;
% name = num2cell(name);
% legend(legendInfo)
% figure
% plot(norm_spec_int_all);
% plot_with_transparent_errorbars(lambda_int,norm_spec_int_all,norm_spec_int_std_all,'yo-',0.3);
% legend(legendInfo)

