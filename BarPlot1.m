
Data = CH2(2:5,2:4)
Data = cell2mat(CH2(2:5,2:4))
error=cell2mat(CH2(6:9,2:4))
barwitherr(error',Data')
colormap jet
legend('GM Static','OM Static','GM Bioreactor','OM Bioreactor')
title('Channel 2 lifetime.')
ylabel('Lifetime')
xlabel('days')
ax = gca;
ax.XTickLabel={'1','7','14'};