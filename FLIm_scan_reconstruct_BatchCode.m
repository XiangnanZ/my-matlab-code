% FILM Reconstruct Batch Process
close all
clear all
% cd to your folder and rename all folder to start with 'Sample'
cd('C:\Users\XZ\Desktop\Data\FLIM\20160323 Adult_calf carotid study FLIM');
sample_list = dir('20160324*');

numoffolder = length(sample_list);

mes = 'The folders are: \n';
for k=1:numoffolder
mes=[mes sample_list(k).name '\n'];
end

mes = sprintf(mes);

questdlg(mes);
% foldernames = sample_list(1);
for k=1:numoffolder
    cd(sample_list(k).name);
    cd('Result');
    filelist = dir('*DeCon.mat');
    n_lines = length(filelist);
    for j=1:n_lines;
    filenames{j} = filelist(j).name;
    end
    
    load(filenames{1});
    % define parameters
    n_avg = 64;
    DAQtime = 58;
    basegain = 1800;
    n_pix = size(BG_scaled,2);
%     n_lines = length(Files);
    s = 0.74; % threshold for saturation
    
    Time = (0:n_pix-1)*DAQtime/1000; % acq time points for a frame
    
    %% initialize
    R1 = zeros(n_pix,n_lines);
    R2 = zeros(n_pix,n_lines);
    R3 = zeros(n_pix,n_lines);
    R4 = zeros(n_pix,n_lines);
    
    LT1 = zeros(n_pix,n_lines);
    LT2 = zeros(n_pix,n_lines);
    LT3 = zeros(n_pix,n_lines);
    LT4 = zeros(n_pix,n_lines);
    
    INT1 = zeros(n_pix,n_lines);
    INT2 = zeros(n_pix,n_lines);
    INT3 = zeros(n_pix,n_lines);
    INT4 = zeros(n_pix,n_lines);
    
    SNR1 = zeros(n_pix,n_lines);
    SNR2 = zeros(n_pix,n_lines);
    SNR3 = zeros(n_pix,n_lines);
    SNR4 = zeros(n_pix,n_lines);
    
    % all zero matrix for SNR mask
    M1 = zeros(n_pix,n_lines);
    M2 = zeros(n_pix,n_lines);
    M3 = zeros(n_pix,n_lines);
    M4 = zeros(n_pix,n_lines);
    % creat SNR mask
    % M_SNR1 = zeros(n_pix,n_lines);
    % M_SNR2 = zeros(n_pix,n_lines);
    % M_SNR3 = zeros(n_pix,n_lines);
    % M_SNR4 = zeros(n_pix,n_lines);
    
    %% load each frame
    for i = 1:n_lines
        i
        
        load(filenames{i});
        raw1 = out{1}{4};
        raw2 = out{2}{4};
        raw3 = out{3}{4};
        raw4 = out{4}{4};
        
        R1(:,i) = lifet_avg{1};
        R2(:,i) = lifet_avg{2};
        R3(:,i) = lifet_avg{3};
        R4(:,i) = lifet_avg{4};
        
        % mask for saturation
        %     mask1 = prod(~double(raw1>=s));
        %     mask2 = prod(~double(raw2>=s));
        %     mask3 = prod(~double(raw3>=s));
        %     mask4 = prod(~double(raw4>=s));
        
        % time stamps
        T = timeStamp;
        % interpolate values and time
        INT1(:,i) = interp1(T,spec_int{1},Time);
        INT2(:,i) = interp1(T,spec_int{2},Time);
        INT3(:,i) = interp1(T,spec_int{3},Time);
        INT4(:,i) = interp1(T,spec_int{4},Time);
        
        LT1(:,i) = interp1(T,lifet_avg{1},Time);
        LT2(:,i) = interp1(T,lifet_avg{2},Time);
        LT3(:,i) = interp1(T,lifet_avg{3},Time);
        LT4(:,i) = interp1(T,lifet_avg{4},Time);
        
        
        
        SNR1(:,i) = interp1(T,SNR{1},Time);
        SNR2(:,i) = interp1(T,SNR{2},Time);
        SNR3(:,i) = interp1(T,SNR{3},Time);
        SNR4(:,i) = interp1(T,SNR{4},Time);
        
        %     % creat mask from saturation
        %     M1(:,i) = im2bw(interp1(T,mask1,Time));
        %     M2(:,i) = im2bw(interp1(T,mask2,Time));
        %     M3(:,i) = im2bw(interp1(T,mask3,Time));
        %     M4(:,i) = im2bw(interp1(T,mask4,Time));
        
        
        
        
    end
    %SNR mask
    M1(SNR1>25) = 1;
    M2(SNR2>25) = 1;
    M3(SNR3>25) = 1;
    M4(SNR4>25) = 1;
    %total mask
    %    M1 = M1.*M_SNR1;
    %    M2 = M2.*M_SNR2;
    %    M3 = M3.*M_SNR3;
    %    M4 = M4.*M_SNR4;
    
    
%     cd(dirpath);
    save([sample_list(k).name '_map.mat']);
%     figure
%     imagesc(LT1, [0 6]);
%     colorbar;
%     figure
%     imagesc(INT1);
%     colorbar;
    cd('..')
    cd ..
end