
% clear CH4 CH4_avg IRF_CH4 M I DC
% clear all
%% load data file
[TRFSFiles,DataFolder] = uigetfile('*.*','Please select FLIm data file',...
    'C:\Users\XZ\Desktop\20160407_TE002 TNI5185_TRFS_iRF','MultiSelect', 'on');
CH = loadBinary(fullfile(DataFolder,TRFSFiles),'double',0);
% CH = CH(2501:5000,:);
NumOfWaveform = size(CH,1);
CH_avg = sum(CH)/NumOfWaveform;
plot(CH_avg)
%% substrct DC value from iRF
DC = mean(CH_avg(1,1500:3500));
CH_avg = CH_avg-DC;
plot(CH_avg)
%% 
[M I] = max(CH_avg);
IRF_CH = CH_avg(I-50:I+249)/M;
plot(IRF_CH);
%% save to file
cd('M:\Tissue Engineering Project\Data\IRFs')
fid=fopen('Laser.txt','w');
fprintf(fid, '%12.6E \t', size(IRF_CH'));
fprintf(fid, '%12.6E \t', IRF_CH);
fclose(fid);
% plot(avg_WF);
% hold on
% hold off