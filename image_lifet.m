function h=image_lifet(x,y,lifet,spec_map,w)

imagesc(x,y,lifet); 

if max(spec_map(:))>1||min(spec_map(:))<0||w>1||w<0
    
    warndlg('Map and w must have values between 0 and 1');
    return;
    
end

green=cat(3,zeros(size(lifet)),zeros(size(lifet)),zeros(size(lifet)));
hold on 
h=image(x,y,green+0.1);
% set(h,'alphadata',(1-spec_map)*w);
spec_map_norm = spec_map./max(spec_map(:));
threshold = spec_map_norm>w;

spec_map_norm(~threshold) = spec_map_norm(~threshold)./w;
spec_map_norm(threshold) = 1;

set(h,'AlphaData',1-spec_map_norm)

hold off

% assignin('base','h_cim',h);

end