% %% load data
% load('map.mat')
close all
clear all
clc
%load map.dat from image reconstruction

[Files,dirpath]=uigetfile('*.mat','Select result file(s)' ); 

load([dirpath Files]);
% %% 
% %set pixels that has a negative intensity to 0 and normalize intensity 
% %so that it can be used with intensity wrighted lifetime calculation
% INT1(INT1<0)=0;
% INT1_norm = INT1/max(INT1(:));
% INT2(INT2<0)=0;
% INT2_norm = INT2/max(INT2(:));
% INT3(INT3<0)=0;
% INT3_norm = INT3/max(INT3(:));

%% image size 
mask=ones(size(INT1));
mask(LT1<5)=0;
mask(LT1>6.5)=0;
% step up plotting axis
ystep = 0.1;
xstep = 0.1;
x_length = size(LT1,1);
y_length = size(LT1,2);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);

% CH1_int_range = [0 50];
% CH2_int_range = [0 10];
% CH3_int_range = [0 3];

int_range = [0 15];%intensity range
lif_range = [5 7];%life time range

% plot lifetime and calculate mean lifetime from ROI
%diable if inly image is needed.
figure
% imagesc(LT1.*mask, lif_range);
imagesc(INT1.*mask);
[point1,point2] = ginput(2);
col_1 = floor(point1(1));
col_2 = floor(point1(2));
row_1 = floor(point2(1));
row_2 = floor(point2(2));
if col_1<1
   col_1=1;
end
if col_2<1
   col_2=1;
end
if row_1<1
   row_1=1;
end
if row_2<1
   row_2=1;
end
ROICH1 = LT1(row_1:row_2,col_1:col_2).*mask(row_1:row_2,col_1:col_2);
% non_zero = nnz(ROICH1);
ROI_data = ROICH1(ROICH1~=0);
life_avg_CH1 = nanmean(ROI_data)
life_std_CH1 = nanstd(ROI_data)

ROICH2 = LT2(row_1:row_2,col_1:col_2);
life_avg_CH2 = nanmean(ROICH2(ROICH1~=0))
life_std_CH2 = nanstd(ROICH2(ROICH1~=0))

% fileID = fopen('M:\Tissue Engineering Project 2015\Data\20151120\FLIM\Average_LT.txt','a');
% fprintf(fileID,'cell2_nonendo\t%5.4f\t%5.4f\t%5.4f\t%5.4f\n',life_avg_CH1,life_std_CH1,life_avg_CH2,life_std_CH2);
% fclose(fileID);
% %% plot all data in the figure.
% figure('position', [100, 100, 1500, 1200]);
% subplot(3,3,1);
% imagesc(y,x,INT1, int_range);
% colorbar
% axis image
% Ytick=[0 2.5 5];
% subplot(3,3,2);
% imagesc(y,x,INT2, int_range);
% colorbar
% axis image
% subplot(3,3,3);
% imagesc(y,x,INT3, int_range);
% colorbar
% axis image
% subplot(3,3,4);
% imagesc(y,x,LT1, lif_range);
% colorbar
% axis image
% subplot(3,3,5);
% imagesc(y,x,LT2, lif_range);
% colorbar
% axis image
% subplot(3,3,6);
% imagesc(y,x,LT3, lif_range);
% colorbar
% axis image
% 
% %intensity weighted lifetime function, return is a figure;
% subplot(3,3,7);
% image_lifet(y,x,LT1,INT1_norm,1);
% caxis(lif_range);
% colorbar
% axis image
% subplot(3,3,8);
% image_lifet(y,x,LT2,INT2_norm,1);
% caxis(lif_range);
% colorbar
% axis image
% subplot(3,3,9);
% image_lifet(y,x,LT3,INT3_norm,1);
% caxis(lif_range);
% colorbar
% axis image
% cd(path(1:39));
% fig_name  = strcat(path(38:39),'.fig')
% savefig(fig_name);