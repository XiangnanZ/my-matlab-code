%Convert single TRFS data file
close all
clear all

[filename,dirpath]=uigetfile('*','Select TRFS data file(s)','C:\Users\XZ\Desktop\Data\20160509NewTRFSCalibration\20160509Test' ); 
cd(dirpath)
[pathstr,name,ext] = fileparts(filename) ;
data = load(strcat(dirpath,'\',name,'.txt'));

fid = fopen(strcat(name,'_convert'),'w+');
n = size(data,1);
m = size(data,2);
for idx = 1:m
    fwrite(fid,1,'int32','l');
    fwrite(fid,n,'int32','l');
    fwrite(fid,data(:,idx),'double','l');
end
fclose(fid);
% read wavelength list and extract gain
header = load(strcat(dirpath,'\',name,'_header_value.txt'));
wavelength = header(13:end)';
TimeResolution = header(2)*1e9;
numAvg = header(6);
k = length(wavelength);

% NameScan = textscan(folderName,'%s %s %d %d','delimiter', '_');
% Subject = NameScan{2}{1};
% Gain = NameScan{3};
% Index = NameScan{4};
% GainVoltage = double(Gain).*ones(size(wavelength));

GainVoltage = load(strcat(dirpath,'\',name,'_HV.txt'));
ts = [wavelength GainVoltage'];

cd(dirpath)

fid = fopen(strcat(name,'_convert_header'),'w+');
% SystemID
fwrite(fid,3,'int32','l');
% InputRange
fwrite(fid,1,'double','l');
% Offset
fwrite(fid,0,'double','l');
% TimeResolution
fwrite(fid,TimeResolution,'double','l');
% BW
fwrite(fid,1.5,'double','l');
% numAvg
fwrite(fid,numAvg,'int32','l');
% n
fwrite(fid,2,'int32','l');
% m
fwrite(fid,k,'int32','l');
% ts
fwrite(fid,ts,'double','l');

fclose(fid);