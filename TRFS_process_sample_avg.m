
%script to calculate average across different samples
clear all
close all

%% chose files
[TRFSFiles,DataFolder] = uigetfile('*.mat*','Please select FLIm data file',...
    'C:\Users\XZ\Desktop\Data\20160504 Coll gel Ribose study DAY 7\TRFS','MultiSelect', 'on');

load(fullfile(DataFolder,TRFSFiles{1}));
[pathstr,name,ext] = fileparts(TRFSFiles{1});
sample_name = name;
%% creat matrix with all data
lambda = lambda;

NumOfFiles = length(TRFSFiles);
SpecLength = length(lambda);
avg_lt_std_all = zeros(SpecLength,NumOfFiles);
avg_lt_all = zeros(SpecLength,NumOfFiles);
norm_spec_int_all = zeros(SpecLength,NumOfFiles);
norm_spec_int_std_all = zeros(SpecLength,NumOfFiles);
avg_spec_int_all = zeros(SpecLength,NumOfFiles);
avg_spec_int_std_all = zeros(SpecLength,NumOfFiles);

%%
for i = 1:NumOfFiles
    load(fullfile(DataFolder,TRFSFiles{i}));
    avg_lt_std_all(:,i) = avg_lt_std;
    avg_lt_all(:,i) = avg_lt;
    norm_spec_int_all(:,i) = norm_spec_int;
    norm_spec_int_std_all(:,i) = norm_spec_int_std;
    avg_spec_int_all(:,i) = avg_spec_int;
    avg_spec_int_std_all(:,i) = avg_spec_int_std;
end

s_avg_lt = mean(avg_lt_all,2);
avg_lt_std_all = avg_lt_std_all.^2;
s_avg_lt_std = sqrt(sum(avg_lt_std_all,2))/NumOfFiles;
s_norm_spec_int=mean(norm_spec_int_all,2);
norm_spec_int_std_all = norm_spec_int_std_all.^2;
s_norm_spec_int_std=sqrt(sum(norm_spec_int_std_all,2))/NumOfFiles;
s_avg_spec_int = mean(avg_spec_int_all,2);
avg_spec_int_std_all = avg_spec_int_std_all.^2;
s_avg_spec_int_std = sqrt(sum(avg_spec_int_std_all,2))/NumOfFiles;

h=figure;
movegui(h,'northeast');
h3=plot_with_transparent_errorbars(lambda,s_avg_lt,s_avg_lt_std,'k*-',0.3);
ylim([0 10]);
% save group data
prompt = 'Please input sample or group name.';
answer = inputdlg(prompt);

if isempty(answer{1})
    h = warndlg('No input is recognized');
else

cd(DataFolder)
% answer{1}=sample_name;
save([answer{1} '_all_avg'],'lambda','s_avg_lt','s_avg_lt_std','s_norm_spec_int','s_norm_spec_int_std');
h = msgbox(strcat({'The average of '},answer{1},' is successfully saved'),'Success');
end