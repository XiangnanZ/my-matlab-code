%script to claculate TRFS average and std
clear all
close all

%% chose files
folderpath = 'C:\Users\XZ\Desktop\Data\20160516 Cartilage construct HA_LOX_LINK TRFS _all\DeCon alpha0.9';
[TRFSFiles,DataFolder] = uigetfile('*Sample_4*.mat*','Please select FLIm data file',...
    folderpath,'MultiSelect', 'on');

%% Load one file to get data size, Initialize Data matrix
try
load(fullfile(DataFolder,TRFSFiles{1}));
NumOfFiles = length(TRFSFiles);
catch
    load(fullfile(DataFolder,TRFSFiles));
    NumOfFiles = 1;
end

try    
[pathstr,name,ext] = fileparts(TRFSFiles{1});
catch
    [pathstr,name,ext] = fileparts(TRFSFiles);
end
sample_name = name(4:12);
%% creat matrix with all data
lambda_int = lambda;
lambda_lt = lambda;


SpecLength = length(spec_int);
int_data_all = zeros(SpecLength,NumOfFiles);
lt_data_all = zeros(SpecLength,NumOfFiles);
gain_data_all = zeros(SpecLength,NumOfFiles);
norm_spec_int = zeros(SpecLength,NumOfFiles);
SNR_all = zeros(SpecLength,NumOfFiles);
%%
for i = 1:NumOfFiles
    if NumOfFiles==1
        load(fullfile(DataFolder,TRFSFiles));
    else
    load(fullfile(DataFolder,TRFSFiles{i}));
    end
    int_data_all(:,i) = spec_int;
    lt_data_all(:,i) = lifet_avg;
    gain_data_all(:,i) = gain(:,1);
    norm_spec_int_all(:,i) = spec_int/max(spec_int(:));
    SNR_all(:,i) = SNR;
end

h=figure;
movegui(h,'northwest');
plot(lambda_int,norm_spec_int_all);
ylim([-0.1 1.1]);
h=figure;
plot(lambda_lt,lt_data_all);
ylim([0 10]);
movegui(h,'north');
h=figure;
plot(lambda,SNR_all);
movegui(h,'south');
%% calculate average
avg_spec_int = mean(int_data_all,2);
avg_spec_int_std = std(int_data_all,[],2);
norm_spec_int = mean(norm_spec_int_all,2);
norm_spec_int_std = std(norm_spec_int_all,[],2);
avg_lt =  mean(lt_data_all,2);
avg_lt_std = std(lt_data_all,[],2);
min_SNR = min(SNR_all,[],2); % minimum value at each wavelength across all measurement
%% filter out NaN

% nan_position = ~(isnan(avg_spec_int)|isnan(avg_spec_int_std));
% lambda_int = lambda_int(nan_position);
% avg_spec_int = avg_spec_int(nan_position);
% avg_spec_int_std = avg_spec_int_std(nan_position);

% nan_position = ~(isnan(avg_lt)|isnan(avg_lt_std));
% lambda_lt = lambda_lt(nan_position);
% avg_lt = avg_lt(nan_position);
% avg_lt_std = avg_lt_std(nan_position);

%% plot group data
% Plot result
% figure
% plot_with_transparent_errorbars(lambda_int,avg_spec_int,avg_spec_int_std,'b-',0.3);
% plot_with_transparent_errorbars(lambda_int,norm_spec_int,norm_spec_int_std,'yo-',0.3);
h=figure;
movegui(h,'northeast');
h3=plot_with_transparent_errorbars(lambda,avg_lt,avg_lt_std,'k*-',0.3);
ylim([0 10]);
% save group data
prompt = 'Please input sample or group name.';
answer = inputdlg(prompt);

if isempty(answer{1})
    h = warndlg('No input is recognized');
else
save_folder = 'C:\Users\XZ\Desktop\Data\20160516 Cartilage construct HA_LOX_LINK TRFS _all\DeCon alpha0.9';
cd(DataFolder)
answer{1}=sample_name;
save([answer{1} '_all_avg'],'lambda','avg_lt','avg_lt_std','avg_spec_int','avg_spec_int_std','norm_spec_int','norm_spec_int_std','int_data_all','lt_data_all','SNR');
uiwait(msgbox(strcat({'The average of '},answer{1},' is successfully saved'),'Success','modal'));
end
close all
