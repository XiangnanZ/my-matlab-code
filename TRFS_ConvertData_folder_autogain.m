masterfolder = uigetdir(pwd,'Please input the master folder path');
cd(masterfolder);
folder_list = dir(masterfolder);

folder_list = folder_list(3:end);
numFolder = length(folder_list);


%%

for fileidx = 1:numFolder

folderName = folder_list(fileidx).name;
data = load(strcat(folderName,'\',folderName,'.txt'));

fid = fopen(strcat(folderName,'_convert'),'w+');
n = size(data,1);
m = size(data,2);
for idx = 1:m
    fwrite(fid,1,'int32','l');
    fwrite(fid,n,'int32','l');
    fwrite(fid,data(:,idx),'double','l');
end
fclose(fid);
% read wavelength list and extract gain
header = load(strcat(folderName,'\',folderName,'_header_value.txt'));
wavelength = header(13:end)';
TimeResolution = header(2)*1e9;
numAvg = header(6);
k = length(wavelength);

% NameScan = textscan(folderName,'%s %s %d %d','delimiter', '_');
% Subject = NameScan{2}{1};
% Gain = NameScan{3};
% Index = NameScan{4};
% GainVoltage = double(Gain).*ones(size(wavelength));

GainVoltage = load(strcat(folderName,'\',folderName,'_HV.txt'));
% GainVoltage = ones(size(wavelength))*2100;
% GainVoltage = GainVoltage';
ts = [wavelength GainVoltage'];

fid = fopen(strcat(folderName,'_convert_header'),'w+');
% SystemID
fwrite(fid,3,'int32','l');
% InputRange
fwrite(fid,1,'double','l');
% Offset
fwrite(fid,0,'double','l');
% TimeResolution
fwrite(fid,TimeResolution,'double','l');
% BW
fwrite(fid,1.5,'double','l');
% numAvg
fwrite(fid,numAvg,'int32','l');
% n
fwrite(fid,2,'int32','l');
% m
fwrite(fid,k,'int32','l');
% ts
fwrite(fid,ts,'double','l');

fclose(fid);

end
cd('../../../')