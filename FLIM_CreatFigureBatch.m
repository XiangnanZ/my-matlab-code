close all
clear all
% cd to your folder and rename all folder to start with 'Sample'
path('C:\Users\jeffzhou\OneDrive\UC Davis\Projects\Programming\Matlab Code',path);

cd('C:\Users\XZ\Desktop\Box Sync\20160307Study_Debika\20160310SD14\FLIM');
sample_name_list = dir('*.mat');
no_files = length(sample_name_list);
% h=figure;
for i=1:no_files
    file_name = sample_name_list(i).name;
    [pathstr,name,ext] = fileparts(file_name);
    load(file_name);
    
    figure
    %set pixels that has a negative intensity to 0 and normalize intensity
    %so that it can be used with intensity weighted lifetime calculation
    INT1(INT1<0)=0;
    INT1_norm = INT1/max(INT1(:));
    INT2(INT2<0)=0;
    INT2_norm = INT2/max(INT2(:));
    INT3(INT3<0)=0;
    INT3_norm = INT3/max(INT3(:));
    
    %% use mask
    M1 = zeros(size(INT1));
    M2 = zeros(size(INT2));
    M3 = zeros(size(INT3));
    M4 = zeros(size(INT4));
    M1(SNR1>25) = 1;
    M2(SNR2>25) = 1;
    M3(SNR3>25) = 1;
    M4(SNR4>25) = 1;
    
    
    
    INT1 = INT1.*M1;
    INT2 = INT2.*M2;
    INT3 = INT3.*M3;
    INT4 = INT4.*M4;
    LT1 = LT1.*M1;
    LT2 = LT2.*M2;
    LT3 = LT3.*M3;
    LT4 = LT4.*M4;
    
    %% step up plotting axis
    ystep = 0.1;
    xstep = 0.1;
    x_length = size(LT1,1);
    y_length = size(LT1,2);
    x = linspace(0,x_length*xstep,x_length);
    y = linspace(0,y_length*ystep,y_length);
    
    CH1_int_range = [0 3];
    CH2_int_range = [0 3];
    CH3_int_range = [0 3];
    LT_range = [3 5];
    
    %% plot all data in the figure.
%     h=figure;
    % title('2.5mg/ml Sample 1.')
    colormap jet
    title('ASB_2_endo.')
    subplot(3,3,1);
    imagesc(x,y,INT1', CH1_int_range);
    colorbar
    axis equal
    % Ytick=[0 2.5 5];
    title('Ch 1 Intensity')
    xlabel('mm')
    ylabel('mm')
    subplot(3,3,2);
    imagesc(x,y,INT2', CH2_int_range);
    colorbar
    axis equal
    title('Ch 2 Intensity')
    xlabel('mm')
    ylabel('mm')
    subplot(3,3,3);
    imagesc(x,y,INT3', CH3_int_range);
    colorbar
    axis equal
    title('Ch 3 Intensity')
    xlabel('mm')
    ylabel('mm')
    subplot(3,3,4);
    imagesc(x,y,LT1', LT_range);
    colorbar
    axis equal
    title('Ch 1 Lifetime')
    xlabel('mm')
    ylabel('mm')
    subplot(3,3,5);
    imagesc(x,y,LT2', LT_range);
    colorbar
    axis equal
    title('Ch 2 Lifetime')
    xlabel('mm')
    ylabel('mm')
    subplot(3,3,6);
    imagesc(x,y,LT3', LT_range);
    colorbar
    axis equal
    title('Ch 3 Lifetime')
    xlabel('mm')
    ylabel('mm')
    
    %% intensity weighted lifetime function, return is a figure;
    subplot(3,3,7);
    image_lifet(x,y,LT1',INT1_norm',1);
    caxis(LT_range);
    colorbar
    axis equal
    title('Ch 1 Intensity weighted Lifetime')
    xlabel('mm')
    ylabel('mm')
    subplot(3,3,8);
    image_lifet(x,y,LT2',INT2_norm',1);
    caxis(LT_range);
    colorbar
    axis equal
    title('Ch 2 Intensity weighted Lifetime')
    xlabel('mm')
    ylabel('mm')
    subplot(3,3,9);
    image_lifet(x,y,LT3',INT3_norm',1);
    caxis(LT_range);
    colorbar
    axis equal
    title('Ch 3 Intensity weighted Lifetime')
    xlabel('mm')
    ylabel('mm')
    drawnow
    %% print to JPEG
    savefig(name)
%     print([name '.jpeg'],'-djpeg','-r300')
%     savefig(name);
end
