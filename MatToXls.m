%code to convert mat to xls

[Files,DataFolder] = uigetfile('*.mat','Please select FLIm data file',...
    'M:\Tissue Engineering Project\Data\20160504 Coll gel Ribose study DAY 7\TRFS\DeCon_Result_all with SNR','MultiSelect', 'on');

cd(DataFolder)

if iscell(Files)
for i=1:length(Files)
      load(Files{i}); % 
      data = [lambda avg_lt avg_lt_std norm_spec_int norm_spec_int_std SNR];
      header = {'Wavelength(nm)','lifetime(ns)','lifetime standard deviation','normalized spectrum','normalized spectrum standard deviation','Signal to noise ratio'};
      [pathstr,name,ext] = fileparts(Files{i});
      % save data to excel sheet    
      xlswrite([name '.xlsx'],header,1);
      xlswrite([name '.xlsx'],data,1,'A2');
     
      
end
else
    load(Files{i}); %
    data = [lambda avg_lt avg_lt_std norm_spec_int norm_spec_int_std SNR];
      header = {'Wavelength(nm)','lifetime(ns)','lifetime standard deviation','normalized spectrum','normalized spectrum standard deviation','Signal to noise ratio'};
    [pathstr,name,ext] = fileparts(Files{i});
    % save data to excel sheet       
    xlswrite([name '.xlsx'],header,1);
    xlswrite([name '.xlsx'],data,1,'A2');
    
end
    
