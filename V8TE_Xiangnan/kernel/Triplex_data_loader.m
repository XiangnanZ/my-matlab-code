function [out1, flagvalue] = Triplex_data_loader(spec_raw, frame_length, n_tbin, handles)

spec_dc=0;

out1={};

if or(isstruct(spec_raw), isstruct(spec_dc))
    
    warndlg('Expect a matrix from input file!');
    flagvalue = 17;
    return;
    
end

if ne(mod(size(spec_raw, 1), frame_length), 0)
    
    warndlg('Frame length is not consistent with the data file!');
    flagvalue = 17;
    return;
    
end

spec_all = reshape(spec_raw, frame_length, []);

peak_left=round(n_tbin*0.1);

c_max = getappdata(handles.figure1, 'TRIPLEXPeaks');

if isempty(c_max)
    
    BGPos = load('PeaksPos.dat');
    
    h1 = figure;
    set(gcf, 'Position', [239 74 1382 896]);
    spec_ttemp = min(spec_all, [], 2);
    plot(spec_ttemp);
    if ne(BGPos(2, 1), 0)
        
        hold on
        plot(BGPos(2, 1), spec_ttemp(BGPos(2, 1)), 'ro', 'MarkerSize', 10);
        hold off
        
    end
    
    if ne(BGPos(2, 2), 0)
        
        hold on
        plot(BGPos(2, 2), spec_ttemp(BGPos(2, 2)), 'ro', 'MarkerSize', 10);
        hold off
        
    end
    
    if ne(BGPos(2, 3), 0)
        
        hold on
        if le(BGPos(2, 3), length(spec_ttemp))
            
            plot(BGPos(2, 3), spec_ttemp(BGPos(2, 3)), 'ro', 'MarkerSize', 10);
            
        else
            
            plot((length(spec_ttemp) - 10), spec_ttemp(end - 10), 'ro', 'MarkerSize', 10);
            BGPos(2, 3) = length(spec_ttemp) - 10;
            
        end            
            
        hold off
        
    end
    
    if ne(BGPos(2, 4), 0)
        
        hold on
        if le(BGPos(2, 4), length(spec_ttemp))
            
            plot(BGPos(2, 4), spec_ttemp(BGPos(2, 4)), 'ro', 'MarkerSize', 10);
            
        else
            
            plot((length(spec_ttemp) - 10), spec_ttemp(end - 10), 'ro', 'MarkerSize', 10);
            BGPos(2, 4) = length(spec_ttemp) - 10;
            
        end            
         
        hold off
        
    end
   
    dlg_title = 'Please enter the peaks of your signal';
    options.Resize='on';
    options.WindowStyle='normal';
    def1 = {num2str(BGPos(2, 1)) num2str(BGPos(2, 2)) num2str(BGPos(2, 3)) num2str(BGPos(2, 4))};
    c_max = inputdlg({'Ch1' 'Ch2' 'Ch3' 'Ch4'}, dlg_title, [1, length(dlg_title) + 30], def1, options);    
    
    if isempty(c_max)
        
        flagvalue = 17;
        waitfor(msgbox('The peak selection was canceled'));
        close(h1);
        return
        
    end
    
    c_max = str2double(c_max)';
    
    if isequal(max(c_max), 0)
        
        flagvalue = 17;
        waitfor(msgbox('The peak selection was wrong'));
        close(h1);
        return        
    
    end
    
    BGPos(2, :) = [c_max 0 0];
    save('PeaksPos.dat', 'BGPos', '-ascii');
    
    close(h1);
    
    setappdata(handles.figure1, 'TRIPLEXPeaks', c_max);
    
end

if ne(c_max(1), 0)
    
    spec1=-spec_all(c_max(1)-peak_left:c_max(1)+n_tbin-peak_left-1,:);    
    
else
    
    spec1 = [];
    
end

if ne(c_max(2), 0)
    
    spec2=-spec_all(c_max(2)-peak_left:c_max(2)+n_tbin-peak_left-1,:);
   
else
    
    spec2 = [];
    
end

if ne(c_max(3), 0)
    
    spec3=-spec_all(c_max(3)-peak_left:c_max(3)+n_tbin-peak_left-1,:);
    
else
    
    spec3 = [];
    
end

if ne(c_max(4), 0)
    
    spec4=-spec_all(c_max(4)-peak_left:c_max(4)+n_tbin-peak_left-1,:);
    
else
    
    spec4 = [];
    
end

out1={spec1,spec2,spec3,spec4};

flagvalue = 16;

