function out=spec_laser_align(spec,laser,K,n_align_fst,shif_range,shif_c)
% K - order used for alignment
% n_align_fst - number of pulses used for alignment (with highest intensity)
% shif_range - window for shifting laser
% shif_c - initial guess of laser shift 

M=size(spec,1);
% K=8;
alpha=alpha_up(M,K);

n_align=min(n_align_fst,size(spec,2));%first sn_align pulses for alignment

L=Laguerre(M,K,alpha);
% L=[L,ones(length(L),1),];%bkgd fitting

[max_spec,max_spec_I]=sort(max(spec),'descend');
spec_t=spec(:,max_spec_I(1:n_align));%temporary spec matrix for alignment

%====find the shift range=============================
if ~exist('shif_range','var')||isempty(shif_range)
   
    shif_range=20;
    
end

if ~exist('shif_c','var')||isempty(shif_c)

    [~,I_laser]=min(laser<max(laser)*0.9);
    [~,I_spec]=min(spec_t(:,1)<max_spec(1)*0.9);
    
    shif_c=I_spec-I_laser;
    
end
    
shif=shif_c-shif_range/2:shif_c+shif_range/2;
%====================================================

err_shif=zeros(1,length(shif));

for si=1:length(shif)
    
    laser_temp=circshift(laser,[shif(si),0]);
    vv=filter(laser_temp,1,L);

    cc=vv\spec_t;
%     h=L*cc;
    spec_t_fitted=vv*cc;

    err_shif(si)=norm(spec_t-spec_t_fitted,'fro');

end

[~,I_min_err_shif]=min(err_shif);
% h1=figure;hold on;plot(shif,err_shif,'LineWidth',2);plot(shif(I_min_err_shif),min_err_shif,'rx','LineWidth',3,'MarkerSize',16);

out_temp=shif(I_min_err_shif);

if I_min_err_shif<=5
    
%     close(h1);
    out_temp=spec_laser_align(spec,laser,K,n_align_fst,shif_range,shif_c-shif_range/2);
    
elseif I_min_err_shif>=length(err_shif)-4
    
%     close(h1);
    out_temp=spec_laser_align(spec,laser,K,n_align_fst,shif_range,shif_c+shif_range/2);
    
end

out=out_temp;
% laser_temp=circshift(laser,[shif(I_min_err_shif),0]);
% vv=filter(laser_temp,1,L);