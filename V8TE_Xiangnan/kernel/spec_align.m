function spec_new=spec_align(spec,theta,spec_or)

spec_norm=spec./repmat(max(spec),size(spec,1),1);%normalized to max
% spec_norm=spec./repmat(mean(spec.*(spec>=0.0011),1),length(spec),1);%normalized to mean

[r_max,c_max]=find(spec==max(spec(:)));%location of the peak value

threhold=theta;

y_all=0;width=round(0.25*size(spec,1)/2)*2;

while mean(sum(y_all))/width<=0.6
y_all=double(spec_norm(max(r_max(1)-width/2,1):r_max(1)+width/2,:)>=(threhold));

% y_all=double(spec(r_max(1)-40:r_max(1)+60,:)>=0.004);%threshold on absolute signal

width=round(width*(1-0.05)/2)*2;

end

% y_all=double(spec_norm(1:140,:)>=(max(spec_norm(:))*threhold));

[r0,c0]=find(y_all==0);
[r1,c1]=find(y_all==1);

X0=[ones(length(c0),1),c0,c0.^2];
X1=[ones(length(c1),1),c1,c1.^2];
Y0=r0;
Y1=r1;

% f=@(a)-sum(1./(1+exp((Y0-X0*a))))+sum(1./(1+exp((Y1-X1*a))));%no weighted

w=max(spec)./max(max(spec));
% w=10.^(SNR/20);
f=@(a)-sum(w(c0)'./(1+exp((Y0-X0*a))))+sum(w(c1)'./(1+exp((Y1-X1*a))));%weighted

options=optimset('TolX',1e-12,'TolFun',1e-12,'MaxFunEvals',1000, 'Diagnostics', 'off', 'Display', 'off');
% poly_coeff=fminunc(f,[10,-0.1,0.001]',options);
% poly_coeff=fminsearch(f,[40,-0.005,0.0000001]',options);
% poly_coeff=fmincon(f,[10,-0.1,0.001]',[],[],[],[],[-Inf,-Inf,1e-12]',[Inf,0,Inf],[],options);
n_w=size(spec,2);
poly_coeff=fmincon(f,[10,-0.1,0.001]',[zeros(n_w,1),ones(n_w,1),2*(1:n_w)'],zeros(n_w,1),[],[],[-Inf,-Inf,1e-12]',[Inf,0,Inf],[],options);


wl_ind=1:size(spec,2);
poly_fitted=poly_coeff(1)+wl_ind*poly_coeff(2)+wl_ind.^2*poly_coeff(3);
% figure;imagesc((y_all));hold on;plot(wl_ind,poly_fitted);
% figure;plot((-poly_fitted+poly_fitted(1))*50);

edge_ind=round(poly_fitted);
% figure;plot(edge_ind-edge_ind(1),'o');

shifs=edge_ind-edge_ind(1);
spec_new=zeros(size(spec));

for i=1:size(spec_or,2)
    
    if eq(i, 1)
        
        spec_new(:,i)=circshift(spec_or(:,i),-shifs(i));
        
    else
        
        spec_new(:,i)=circshift(spec_or(:,i),-shifs(i-1));
        
    end
    
end