function [lifet_avg, spec_int, cc, alpha, test_out, out, alpha_ind_out, record_ind_out, record_out] = TRFS_main_script(spec, laser, dt, bw, lambda, record, dt_interp, n, alpha, alpha_ind, record_ind, handles)

n_tbin = size(spec, 1);
n_tbin_laser = size(laser, 1);

if ne(dt, dt_interp)
    
    spec=interp1(0:n_tbin-1,spec,0:dt_interp/dt:n_tbin-1);
    laser=interp1(0:n_tbin_laser-1,laser,0:dt_interp/dt:n_tbin_laser-1)';

    dt = dt_interp;
    
end

if eq(record_ind, 0)
    
    if lt((record * dt), 40)
        
        record = round(40/dt);
        str = ['The truncation length you provided is smaller than 40 ns. Switching to ' num2str(record) ' pts'];
        set(handles.edit3, 'string', num2str(record));
        waitfor(msgbox(str));
        record_ind_out = 1;
        
    elseif gt((record * dt), 200)
        
        record = round(200/dt);
        str = ['The truncation length you provided is much larger than 200 ns. Switching to ' num2str(record) ' pts'];
        set(handles.edit3, 'string', num2str(record));
        waitfor(msgbox(str));
        record_ind_out = 1;
        
    else
        
        record_ind_out = 1;
        
    end
    
else
    
    record_ind_out = record_ind;
    
end

record_out = record;

[~, pos] = max(mean(spec(:, 3:end), 2));

% 10% pro-peak length
if le(pos - (0.15 * record) + record, size(spec, 1))
    
    spec = spec((pos - (0.15 * record)):(pos - (0.15 * record) + record), :);
    
else
    
    spec = spec((pos - (0.15 * record)):end, :);
    
end

% Start processing...
% Output is the average lifetime over wavelengths/pixels
[lifet_avg, spec_int, cc, alpha, test_out, out, alpha_ind_out] = decon_LG(spec, laser, dt, bw, [1, length(lambda)], n, alpha, 'TRFS', alpha_ind, handles);