function TRIPLEX_plot_raw_decon_Results(handles, wavenum, pointnum, measnum)

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRIPLEXresultsData');

try
    
    timeStep = out{pointnum}.timeDiscr;
    
catch err
    
    timeStep = out{pointnum}.timeDiscr_IntRes;
    
    spec = out{pointnum}.spec(:, measnum);
    noise = mean(spec((end - 100):end, :));
    spec = bsxfun(@minus, spec, noise);
    try
        Gain = out{pointnum}.gain_list(measnum, 1);
    catch err
        Gain = 0;
    end
    try
        BG_scaled = out{pointnum}.BG_scaled(:, measnum);
    catch err
        BG_scaled = [];
    end
    
    nf = findobj('Name','Signal_Gain'); % all graphical objects
    
    if isempty(nf)
        
        nf = figure('Name','Signal_Gain');
        
    end
    
    figure(nf);
    set(gcf, 'Position', [84 528 560 420]);
    
    plot(spec, 'LineWidth', 3);
    if ~isempty(BG_scaled)
        
        hold on
        plot(BG_scaled, 'r', 'LineWidth', 2);
        plot(spec - BG_scaled, 'g', 'LineWidth', 2);
        hold off
        ylim([-1 0.1])
        legend({'Raw Signal' 'Scaled Background' 'Residual'}, 'Location', 'SouthEast');
        
    else
        
        ylim([-1 0.1])
        legend({'Raw Signal'}, 'Location', 'SouthEast');
        
    end
    title(['Gain: ' num2str(ceil(Gain))])
    
end

if ~isempty(out{pointnum}.SNR{wavenum})
    
    SNR = out{pointnum}.SNR{wavenum}(measnum);
    lifet = out{pointnum}.lifet_avg{wavenum}(measnum);
    intIntensity = out{pointnum}.spec_int{wavenum}(measnum);
    
else
    
    str = ['Measured Voltage for CH' num2str(wavenum) ' for point No' num2str(pointnum) ' and repetition No' num2str(measnum - 1) ' with SNR = na dB'];
    set(handles.Title1, 'String', str);
    axes(handles.axes1);
    cla reset;
    hold off
    axis on
    box on    
    axes(handles.axes2);
    cla reset;
    legend off
    hold off
    axis on
    box on
    axes(handles.axes3);
    cla reset;
    legend off
    hold off
    axis on
    box on
    axes(handles.axes4);
    cla reset;
    legend off
    hold off
    axis on
    box on
    return
        
end

str = ['Measured Voltage for CH' num2str(wavenum) ' for point No' num2str(pointnum) ' and repetition No' num2str(measnum - 1) ' with SNR = ' num2str(round(10 * SNR) / 10) 'dB, LT ' num2str(round(10 * lifet) / 10) ' ns and int_I ' num2str(round(10 * intIntensity) / 10)];

set(handles.Title1, 'String', str);

out_spec1 = out{pointnum}.out{wavenum}{3};
out_spec2 = out{pointnum}.out{wavenum}{4};
out_spec3 = out{pointnum}.out{wavenum}{6};
out_spec3 = out_spec3./max(out_spec3);
out_spec3 = out_spec3.*max(out_spec2(:, measnum));
out_spec4 = out{pointnum}.out{wavenum}{2};

axes(handles.axes1)
x_values = (timeStep * [1:size(out_spec1, 1)]);
x_values2 = (timeStep * [1:size(out_spec3, 1)]);

plot(x_values, out_spec1(:, measnum), 'LineWidth', 2)
hold on
plot(x_values, out_spec2(:, measnum), 'r-')
plot(x_values2, out_spec3, 'g-')
hold off

xlabel('Time (ns)');
ylabel('Absolute Intensity (V)');
xlim([0 max(x_values)])

inputNames = getappdata(prevHandles.figure1, 'TRIPLEXrawDataNames');
prefixes = inputNames.prefices;

legend({prefixes{pointnum}}, 'Interpreter', 'none');

axes(handles.axes2)
diff_out_spec = (out_spec2(:, measnum) - out_spec1(:, measnum))./max(out_spec2(:, measnum));
plot(x_values, diff_out_spec, 'm', 'LineWidth', 2)
ylabel('Norm. Residuals (a.u.)');
try
    ylim([-max(max(diff_out_spec), abs(min(diff_out_spec))) max(max(diff_out_spec), abs(min(diff_out_spec)))]);
end
xlim([0 max(x_values)])
box on

axes(handles.axes4)
diff_out_spec = out_spec2(:, measnum) - out_spec1(:, measnum);
diff_out_spec = xcorr(diff_out_spec, (size(diff_out_spec, 1) - 1), 'coeff');
plot(x_values, diff_out_spec(size(x_values, 2):end), 'm', 'LineWidth', 2)

bounds(1) = 2 / sqrt(size(x_values, 2));
bounds(2) = -bounds(1);

hold on
plot(x_values, (bounds(1) * ones(size(x_values))), 'k--')
plot(x_values, (bounds(2) * ones(size(x_values))), 'k--')
hold off
ylabel('Res. Autocorrelation (a.u.)');
try
    ylim([-max(max(diff_out_spec), abs(min(diff_out_spec))) max(max(diff_out_spec), abs(min(diff_out_spec)))]);
end
xlim([0 max(x_values)])
box on

axes(handles.axes3)
plot(x_values, out_spec4(:, measnum)./max(out_spec4(:, measnum)), 'm', 'LineWidth', 2)

ylabel('Norm. Decay (a.u.)');
ylim([0 1])
xlim([0 max(x_values)])
box on
set(gca, 'XTick', [])