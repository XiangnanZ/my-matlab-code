function flagvalue = loadiIRF(handles)

axes(handles.axes1);
cla reset;
box on
hold off

axes(handles.axes2);
cla reset;
box on
legend off
hold off

set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');

dataInfo = getappdata(handles.figure1, 'outputNames');
[dataPath, ~] = fileparts(dataInfo.filenames{1});

[filename, foldername] = uigetfile('.dat', 'Please Select the iIRF File', dataPath);

if ~foldername
    
    flagvalue = 5;
    return;
    
end

full_path = fullfile(foldername, filename);
iIRF = - load(full_path);
iIRF = iIRF - mean(iIRF((end - 100):end));

setappdata(handles.figure1, 'iIRF', iIRF);

flagvalue = 6;