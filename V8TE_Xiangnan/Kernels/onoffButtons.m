function onoffButtons(handles, flagvalue)

if eq(flagvalue, 0)
    % Before show the GUI
    
    % TRFS Preprocessing Block
    set(handles.pushbutton1, 'enable', 'on');
    set(handles.pushbutton2, 'enable', 'off');
    set(handles.pushbutton3, 'enable', 'off');
    set(handles.pushbutton4, 'enable', 'off');
    set(handles.pushbutton4, 'String', 'TRFS Preprocess');
    set(handles.pushbutton5, 'enable', 'off');
    set(handles.edit1, 'enable', 'off');
    set(handles.edit2, 'enable', 'off');
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.radiobutton1, 'enable', 'off');
    set(handles.radiobutton2, 'enable', 'off');
    set(handles.radiobutton3, 'enable', 'off');
    set(handles.radiobutton4, 'enable', 'off');
    set(handles.radiobutton5, 'enable', 'off');
    set(handles.text3, 'String', 'Number of Spectra: 0');
    setappdata(handles.figure1, 'outputNames', []);
    setappdata(handles.figure1, 'iIRF', []);
    setappdata(handles.figure1, 'Wavelength_Calib', [])        
    setappdata(handles.figure1, 'rawOutputData', []);
    setappdata(handles.figure1, 'BlackPlot', []);    
    setappdata(handles.figure1, 'FlagType', []);
    setappdata(handles.figure1, 'FlagTypeAxis1', []);
    setappdata(handles.figure1, 'FlagTypeAxis2', []);
    setappdata(handles.figure1, 'sliderFlag', []);     
    
    % TRFS Processing Block
    set(handles.pushbutton6, 'enable', 'off');
    set(handles.pushbutton7, 'enable', 'off');
    set(handles.pushbutton8, 'enable', 'off');
    set(handles.edit3, 'enable', 'off');
    set(handles.edit4, 'enable', 'off');
    set(handles.edit5, 'enable', 'off');
    set(handles.edit6, 'enable', 'off');
    set(handles.edit7, 'enable', 'off');
    set(handles.radiobutton6, 'enable', 'off');
    set(handles.radiobutton7, 'enable', 'off');
    set(handles.radiobutton8, 'enable', 'off');
    set(handles.radiobutton9, 'enable', 'off');
    set(handles.radiobutton10, 'enable', 'off');
    set(handles.popupmenu1, 'enable', 'off');
    set(handles.popupmenu2, 'enable', 'off');
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);    
    setappdata(handles.figure1, 'TRFS_Data', []);
    setappdata(handles.figure1, 'BlackPlot1', []);
    setappdata(handles.figure1, 'BlackPlot2', []);
    setappdata(handles.figure1, 'dataType', []);
    set(handles.text27, 'String', 'SNR<=20dB = 0');
    set(handles.text29, 'String', 'SNR>=60dB = 0');
    set(handles.text28, 'String', '20dB<=SNR<=60dB = 0');
    
    % TRIPLEX Processing Block
    set(handles.pushbutton9, 'enable', 'on');
    set(handles.pushbutton10, 'enable', 'off');
    set(handles.pushbutton11, 'enable', 'off');
    set(handles.pushbutton11, 'String', 'TRIPLEX Process');
    set(handles.pushbutton12, 'enable', 'off');
    set(handles.pushbutton13, 'enable', 'off');
    set(handles.edit8, 'enable', 'off');
    set(handles.edit9, 'enable', 'off');
    set(handles.edit10, 'enable', 'off');
    set(handles.edit11, 'enable', 'off');
    set(handles.edit12, 'enable', 'off');
    set(handles.edit13, 'enable', 'off');
    set(handles.edit20, 'enable', 'off');
    set(handles.edit21, 'enable', 'off');
    set(handles.popupmenu3, 'enable', 'off');
    set(handles.popupmenu4, 'enable', 'off');
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    set(handles.radiobutton13, 'enable', 'off');
    set(handles.radiobutton14, 'enable', 'off');
    set(handles.text15, 'String', 'Number of Points: 0');
    setappdata(handles.figure1, 'TRIPLEXrawDataNames', []);
    set(handles.checkbox1, 'enable', 'off');
    set(handles.checkbox2, 'enable', 'off');
    set(handles.checkbox3, 'enable', 'off');
    set(handles.checkbox4, 'enable', 'off');
    set(handles.checkbox1, 'Value', 1);
    set(handles.checkbox2, 'Value', 1);
    set(handles.checkbox3, 'Value', 1);
    set(handles.checkbox4, 'Value', 1);
     
    % Label block
    set(handles.pushbutton14, 'enable', 'off');
    set(handles.radiobutton11, 'enable', 'off');
    set(handles.radiobutton12, 'enable', 'off');

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
 
elseif eq(flagvalue, 1)
    % Before running the Load TRFS Data Button    
    % TRFS Preprocessing Block
    set(handles.pushbutton2, 'enable', 'off');
    set(handles.pushbutton3, 'enable', 'off');
    set(handles.pushbutton4, 'enable', 'off');
    set(handles.pushbutton4, 'String', 'TRFS Preprocess');
    set(handles.pushbutton5, 'enable', 'off');
    set(handles.edit1, 'enable', 'off');
    set(handles.edit2, 'enable', 'off');
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.radiobutton1, 'enable', 'off');
    set(handles.radiobutton2, 'enable', 'off');
    set(handles.radiobutton3, 'enable', 'off');
    set(handles.radiobutton4, 'enable', 'off');
    set(handles.radiobutton5, 'enable', 'off');
    set(handles.text3, 'String', 'Number of Spectra: 0');
    setappdata(handles.figure1, 'outputNames', []);
    setappdata(handles.figure1, 'iIRF', []);
    setappdata(handles.figure1, 'Wavelength_Calib', [])        
    setappdata(handles.figure1, 'rawOutputData', []);
    setappdata(handles.figure1, 'BlackPlot', []);    
    setappdata(handles.figure1, 'FlagType', []);
    setappdata(handles.figure1, 'FlagTypeAxis1', []);
    setappdata(handles.figure1, 'FlagTypeAxis2', []);
    setappdata(handles.figure1, 'sliderFlag', []);
    setappdata(handles.figure1, 'dataType', []);
    
    % TRFS Processing Block
    set(handles.pushbutton6, 'enable', 'off');
    set(handles.pushbutton7, 'enable', 'off');
    set(handles.pushbutton8, 'enable', 'off');
    set(handles.edit3, 'enable', 'off');
    set(handles.edit4, 'enable', 'off');
    set(handles.edit5, 'enable', 'off');
    set(handles.edit6, 'enable', 'off');
    set(handles.edit7, 'enable', 'off');
    set(handles.radiobutton6, 'enable', 'off');
    set(handles.radiobutton7, 'enable', 'off');
    set(handles.radiobutton8, 'enable', 'off');
    set(handles.radiobutton9, 'enable', 'off');
    set(handles.radiobutton10, 'enable', 'off');
    set(handles.popupmenu1, 'enable', 'off');
    set(handles.popupmenu2, 'enable', 'off');
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);
    setappdata(handles.figure1, 'TRFS_Data', []);
    setappdata(handles.figure1, 'BlackPlot1', []);
    setappdata(handles.figure1, 'BlackPlot2', []);
    set(handles.text27, 'String', 'SNR<=20dB = 0');
    set(handles.text29, 'String', 'SNR>=60dB = 0');
    set(handles.text28, 'String', '20dB<=SNR<=60dB = 0');
    
    % Label block
    set(handles.pushbutton14, 'enable', 'off');
    set(handles.radiobutton11, 'enable', 'off');
    set(handles.radiobutton12, 'enable', 'off');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 2)
    % After running the Load TRFS Data Button    
    % TRFS Preprocessing Block
    set(handles.pushbutton2, 'enable', 'on');
    set(handles.pushbutton3, 'enable', 'on');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

elseif eq(flagvalue, 3)
    % Before running the Load Spec. Calib. Button
    
    % TRFS Preprocessing Block
    set(handles.pushbutton3, 'enable', 'on');
    set(handles.pushbutton4, 'enable', 'off');
    set(handles.pushbutton4, 'String', 'TRFS Preprocess');
    set(handles.pushbutton5, 'enable', 'off');
    set(handles.edit1, 'enable', 'off');
    set(handles.edit2, 'enable', 'off');
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.radiobutton1, 'enable', 'off');
    set(handles.radiobutton2, 'enable', 'off');
    set(handles.radiobutton3, 'enable', 'off');
    set(handles.radiobutton4, 'enable', 'off');
    set(handles.radiobutton5, 'enable', 'off');
    setappdata(handles.figure1, 'Wavelength_Calib', [])        
    setappdata(handles.figure1, 'rawOutputData', []);
    setappdata(handles.figure1, 'BlackPlot', []);    
    setappdata(handles.figure1, 'FlagType', []);
    setappdata(handles.figure1, 'FlagTypeAxis1', []);
    setappdata(handles.figure1, 'FlagTypeAxis2', []);
    setappdata(handles.figure1, 'sliderFlag', []);
            
    % TRFS Processing Block
    set(handles.pushbutton6, 'enable', 'off');
    set(handles.pushbutton7, 'enable', 'off');
    set(handles.pushbutton8, 'enable', 'off');
    set(handles.edit3, 'enable', 'off');
    set(handles.edit4, 'enable', 'off');
    set(handles.edit5, 'enable', 'off');
    set(handles.edit6, 'enable', 'off');
    set(handles.edit7, 'enable', 'off');
    set(handles.radiobutton6, 'enable', 'off');
    set(handles.radiobutton7, 'enable', 'off');
    set(handles.radiobutton8, 'enable', 'off');
    set(handles.radiobutton9, 'enable', 'off');
    set(handles.radiobutton10, 'enable', 'off');
    set(handles.popupmenu1, 'enable', 'off');
    set(handles.popupmenu2, 'enable', 'off');
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);
    setappdata(handles.figure1, 'TRFS_Data', []);
    setappdata(handles.figure1, 'BlackPlot1', []);
    setappdata(handles.figure1, 'BlackPlot2', []);
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 4)
    % After running the Load Spec. Calib. Button
    
    % TRFS Preprocessing Block
    set(handles.pushbutton3, 'enable', 'on');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 5)
    % Before running the Load iIRF Button
    
    % TRFS Preprocessing Block
    set(handles.pushbutton4, 'enable', 'off');
    set(handles.pushbutton4, 'String', 'TRFS Preprocess');
    set(handles.pushbutton5, 'enable', 'off');
    set(handles.edit1, 'enable', 'off');
    set(handles.edit2, 'enable', 'off');
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.radiobutton1, 'enable', 'off');
    set(handles.radiobutton2, 'enable', 'off');
    set(handles.radiobutton3, 'enable', 'off');
    set(handles.radiobutton4, 'enable', 'off');
    set(handles.radiobutton5, 'enable', 'off');
    setappdata(handles.figure1, 'iIRF', []);
    setappdata(handles.figure1, 'rawOutputData', []);
    setappdata(handles.figure1, 'BlackPlot', []);    
    setappdata(handles.figure1, 'FlagType', []);
    setappdata(handles.figure1, 'FlagTypeAxis1', []);
    setappdata(handles.figure1, 'FlagTypeAxis2', []);
    setappdata(handles.figure1, 'sliderFlag', []);
        
    % TRFS Processing Block
    set(handles.pushbutton6, 'enable', 'off');
    set(handles.pushbutton7, 'enable', 'off');
    set(handles.pushbutton8, 'enable', 'off');
    set(handles.edit3, 'enable', 'off');
    set(handles.edit4, 'enable', 'off');
    set(handles.edit5, 'enable', 'off');
    set(handles.edit6, 'enable', 'off');
    set(handles.edit7, 'enable', 'off');
    set(handles.radiobutton6, 'enable', 'off');
    set(handles.radiobutton7, 'enable', 'off');
    set(handles.radiobutton8, 'enable', 'off');
    set(handles.radiobutton9, 'enable', 'off');
    set(handles.radiobutton10, 'enable', 'off');
    set(handles.popupmenu1, 'enable', 'off');
    set(handles.popupmenu2, 'enable', 'off');
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);
    setappdata(handles.figure1, 'TRFS_Data', []);
    setappdata(handles.figure1, 'BlackPlot1', []);
    setappdata(handles.figure1, 'BlackPlot2', []);
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 6)
    % After running the Load iIRF Button
    
    % TRFS Preprocessing Block
    set(handles.pushbutton4, 'enable', 'on');
    set(handles.radiobutton1, 'enable', 'on');
    set(handles.radiobutton2, 'enable', 'on');
    set(handles.radiobutton3, 'enable', 'on');
    set(handles.radiobutton4, 'enable', 'on');
    set(handles.radiobutton5, 'enable', 'on');
    datatype = getappdata(handles.figure1, 'dataType');
    if eq(datatype, 1)
        set(handles.edit1, 'enable', 'on');
    else
        set(handles.edit1, 'enable', 'off');
    end
    set(handles.edit2, 'enable', 'on');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 7)
    % Before running the TRFS Preprocess Button
    
    % TRFS Preprocessing Block
    set(handles.pushbutton1, 'enable', 'off');
    set(handles.pushbutton2, 'enable', 'off');
    set(handles.pushbutton3, 'enable', 'off');
    set(handles.pushbutton4, 'enable', 'off');
    set(handles.pushbutton4, 'String', 'Running...');
    set(handles.pushbutton5, 'enable', 'off');
    set(handles.edit1, 'enable', 'off');
    set(handles.edit2, 'enable', 'off');
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.radiobutton1, 'enable', 'off');
    set(handles.radiobutton2, 'enable', 'off');
    set(handles.radiobutton3, 'enable', 'off');
    set(handles.radiobutton4, 'enable', 'off');
    set(handles.radiobutton5, 'enable', 'off');
    setappdata(handles.figure1, 'rawOutputData', []);
    setappdata(handles.figure1, 'BlackPlot', []);    
    setappdata(handles.figure1, 'FlagType', []);
    setappdata(handles.figure1, 'FlagTypeAxis1', []);
    setappdata(handles.figure1, 'FlagTypeAxis2', []);
    setappdata(handles.figure1, 'sliderFlag', []);
    
    % TRFS Processing Block
    set(handles.pushbutton6, 'enable', 'off');
    set(handles.pushbutton7, 'enable', 'off');
    set(handles.pushbutton8, 'enable', 'off');
    set(handles.edit3, 'enable', 'off');
    set(handles.edit4, 'enable', 'off');
    set(handles.edit5, 'enable', 'off');
    set(handles.edit6, 'enable', 'off');
    set(handles.edit7, 'enable', 'off');
    set(handles.radiobutton6, 'enable', 'off');
    set(handles.radiobutton7, 'enable', 'off');
    set(handles.radiobutton8, 'enable', 'off');
    set(handles.radiobutton9, 'enable', 'off');
    set(handles.radiobutton10, 'enable', 'off');
    set(handles.popupmenu1, 'enable', 'off');
    set(handles.popupmenu2, 'enable', 'off');
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);
    setappdata(handles.figure1, 'TRFS_Data', []);
    setappdata(handles.figure1, 'BlackPlot1', []);
    setappdata(handles.figure1, 'BlackPlot2', []);
    set(handles.text27, 'String', 'SNR<=20dB = 0');
    set(handles.text29, 'String', 'SNR>=60dB = 0');
    set(handles.text28, 'String', '20dB<=SNR<=60dB = 0');
     
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 8)
    % After running the TRFS Preprocess Button    
    set(handles.pushbutton5, 'enable', 'on');
    set(handles.pushbutton4, 'String', 'TRFS Preprocess');
    set(handles.pushbutton4, 'enable', 'on');
    set(handles.pushbutton1, 'enable', 'on');
    set(handles.pushbutton2, 'enable', 'on');      
    set(handles.pushbutton3, 'enable', 'on');
    datatype = getappdata(handles.figure1, 'dataType');
    if eq(datatype, 1)
        set(handles.edit1, 'enable', 'on');
    else
        set(handles.edit1, 'enable', 'off');
    end
    set(handles.edit2, 'enable', 'on');
    set(handles.radiobutton1, 'enable', 'on');
    set(handles.radiobutton2, 'enable', 'on');
    set(handles.radiobutton3, 'enable', 'on');
    set(handles.radiobutton4, 'enable', 'on');
    set(handles.radiobutton5, 'enable', 'on');
    
    % TRFS Processing
    set(handles.pushbutton6, 'enable', 'on');
    set(handles.edit3, 'enable', 'on');
    set(handles.edit4, 'enable', 'on');
    set(handles.edit5, 'enable', 'on');
    set(handles.edit6, 'enable', 'on');
    set(handles.edit7, 'enable', 'on');
    set(handles.radiobutton6, 'enable', 'on');
    set(handles.radiobutton7, 'enable', 'on');
    set(handles.radiobutton8, 'enable', 'on');
    set(handles.radiobutton9, 'enable', 'on');
    set(handles.radiobutton10, 'enable', 'on');
    
    % -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 9)
    % Before running the TRFS Process Button
    
    % TRFS Processing
    set(handles.pushbutton6, 'enable', 'off');
    set(handles.pushbutton6, 'String', 'Running...');
    set(handles.pushbutton7, 'enable', 'off');
    set(handles.pushbutton8, 'enable', 'off');
    set(handles.edit3, 'enable', 'off');
    set(handles.edit5, 'enable', 'off');
    set(handles.edit4, 'enable', 'off');
    set(handles.edit6, 'enable', 'off');
    set(handles.edit7, 'enable', 'off');
    set(handles.radiobutton6, 'enable', 'off');
    set(handles.radiobutton7, 'enable', 'off');
    set(handles.radiobutton8, 'enable', 'off');
    set(handles.radiobutton9, 'enable', 'off');
    set(handles.radiobutton10, 'enable', 'off');
    setappdata(handles.figure1, 'TRFS_Data', []);
    set(handles.popupmenu1, 'enable', 'off');
    set(handles.popupmenu2, 'enable', 'off');
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);    
    set(handles.text27, 'String', 'SNR<=20dB = 0');
    set(handles.text29, 'String', 'SNR>=60dB = 0');
    set(handles.text28, 'String', '20dB<=SNR<=60dB = 0');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 10)
    % After running the TRFS Process Button
    
    set(handles.pushbutton6, 'enable', 'on');
    set(handles.pushbutton6, 'String', 'TRFS Process');
    set(handles.pushbutton7, 'enable', 'on');
    set(handles.pushbutton8, 'enable', 'on');
    set(handles.edit3, 'enable', 'on');
    set(handles.edit5, 'enable', 'on');
    set(handles.edit4, 'enable', 'on');
    set(handles.edit6, 'enable', 'on');
    set(handles.edit7, 'enable', 'on');
    set(handles.radiobutton6, 'enable', 'on');
    set(handles.radiobutton7, 'enable', 'on');
    set(handles.radiobutton8, 'enable', 'on');
    set(handles.radiobutton9, 'enable', 'on');
    set(handles.radiobutton10, 'enable', 'on');
    set(handles.popupmenu1, 'enable', 'on');
    set(handles.popupmenu2, 'enable', 'on');   
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

elseif eq(flagvalue, 11)
    % Before running the Load TRIPLEX Data Button
        
    set(handles.pushbutton9, 'enable', 'on');
    set(handles.pushbutton10, 'enable', 'off');
    set(handles.pushbutton11, 'enable', 'off');
    set(handles.pushbutton12, 'enable', 'off');
    set(handles.pushbutton13, 'enable', 'off');
    set(handles.edit8, 'enable', 'off');
    set(handles.edit9, 'enable', 'off');
    set(handles.edit10, 'enable', 'off');
    set(handles.edit11, 'enable', 'off');
    set(handles.edit12, 'enable', 'off');
    set(handles.edit13, 'enable', 'off');
    set(handles.edit20, 'enable', 'off');
    set(handles.edit21, 'enable', 'off');
    set(handles.popupmenu3, 'enable', 'off');
    set(handles.popupmenu4, 'enable', 'off');
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    set(handles.radiobutton13, 'enable', 'off');
    set(handles.radiobutton14, 'enable', 'off');
    set(handles.text15, 'String', 'Number of Points: 0');
    setappdata(handles.figure1, 'TRIPLEXrawDataNames', []);
    setappdata(handles.figure1, 'TRIPLEXresultsData', []);
    setappdata(handles.figure1, 'TRIPLEXiIRF', []);
    setappdata(handles.figure1, 'BlackPlot1TRIPLEX', []);
    setappdata(handles.figure1, 'BlackPlot2TRIPLEX', []);
    setappdata(handles.figure1, 'TRIPLEXDataType', []);
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.text27, 'String', 'SNR<=20dB = 0');
    set(handles.text29, 'String', 'SNR>=60dB = 0');
    set(handles.text28, 'String', '20dB<=SNR<=60dB = 0');
    set(handles.checkbox1, 'enable', 'off');
    set(handles.checkbox2, 'enable', 'off');
    set(handles.checkbox3, 'enable', 'off');
    set(handles.checkbox4, 'enable', 'off');
    set(handles.checkbox1, 'Value', 1);
    set(handles.checkbox2, 'Value', 1);
    set(handles.checkbox3, 'Value', 1);
    set(handles.checkbox4, 'Value', 1);
    
    % Label block
    set(handles.pushbutton14, 'enable', 'off');
    set(handles.radiobutton11, 'enable', 'off');
    set(handles.radiobutton12, 'enable', 'off');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 12)
    % After running the Load TRIPLEX Data Button
    
    set(handles.pushbutton10, 'enable', 'on');  
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 13)
    % Before running the Load TRIPLEX iIRF
    
    set(handles.pushbutton9, 'enable', 'on');
    set(handles.pushbutton10, 'enable', 'on');
    set(handles.pushbutton11, 'enable', 'off');
    set(handles.pushbutton12, 'enable', 'off');
    set(handles.pushbutton13, 'enable', 'off');
    set(handles.edit8, 'enable', 'off');
    set(handles.edit9, 'enable', 'off');
    set(handles.edit10, 'enable', 'off');
    set(handles.edit11, 'enable', 'off');
    set(handles.edit12, 'enable', 'off');
    set(handles.edit13, 'enable', 'off');
    set(handles.edit20, 'enable', 'off');
    set(handles.edit21, 'enable', 'off');
    set(handles.popupmenu3, 'enable', 'off');
    set(handles.popupmenu4, 'enable', 'off');
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    set(handles.radiobutton13, 'enable', 'off');
    set(handles.radiobutton14, 'enable', 'off');
    setappdata(handles.figure1, 'TRIPLEXresultsData', []);
    setappdata(handles.figure1, 'TRIPLEXiIRF', []);
    set(handles.pushbutton11, 'String', 'TRIPLEX Process');
    setappdata(handles.figure1, 'BlackPlot1TRIPLEX', []);
    setappdata(handles.figure1, 'BlackPlot2TRIPLEX', []);
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.text27, 'String', 'SNR<=20dB = 0');
    set(handles.text29, 'String', 'SNR>=60dB = 0');
    set(handles.text28, 'String', '20dB<=SNR<=60dB = 0');
    set(handles.checkbox1, 'enable', 'off');
    set(handles.checkbox2, 'enable', 'off');
    set(handles.checkbox3, 'enable', 'off');
    set(handles.checkbox4, 'enable', 'off');
    set(handles.checkbox1, 'Value', 1);
    set(handles.checkbox2, 'Value', 1);
    set(handles.checkbox3, 'Value', 1);
    set(handles.checkbox4, 'Value', 1);
       
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 14)
    % After running the Load TRIPLEX iIRF
    
    set(handles.pushbutton11, 'enable', 'on');
    set(handles.edit8, 'enable', 'on');
    set(handles.edit9, 'enable', 'on');
    set(handles.edit10, 'enable', 'on');
    set(handles.edit11, 'enable', 'on');
    set(handles.edit12, 'enable', 'on');
    set(handles.edit13, 'enable', 'on');
    set(handles.edit20, 'enable', 'on');
    set(handles.edit21, 'enable', 'on');
    set(handles.radiobutton13, 'enable', 'on');
    set(handles.radiobutton14, 'enable', 'on');
    set(handles.checkbox1, 'enable', 'off');
    set(handles.checkbox2, 'enable', 'off');
    set(handles.checkbox3, 'enable', 'off');
    set(handles.checkbox4, 'enable', 'off');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 15)
    
    % Before running the TRIPLEX Process Button
    
    set(handles.pushbutton9, 'enable', 'off');
    set(handles.pushbutton11, 'String', 'Running...');
    set(handles.pushbutton10, 'enable', 'off');
    set(handles.pushbutton11, 'enable', 'off');
    set(handles.pushbutton12, 'enable', 'off');
    set(handles.pushbutton13, 'enable', 'off');    
    set(handles.edit8, 'enable', 'off');
    set(handles.edit9, 'enable', 'off');
    set(handles.edit10, 'enable', 'off');
    set(handles.edit11, 'enable', 'off');
    set(handles.edit12, 'enable', 'off');
    set(handles.edit13, 'enable', 'off');
    set(handles.edit20, 'enable', 'off');
    set(handles.edit21, 'enable', 'off');
    set(handles.popupmenu3, 'enable', 'off');
    set(handles.popupmenu4, 'enable', 'off');    
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    set(handles.radiobutton13, 'enable', 'off');
    set(handles.radiobutton14, 'enable', 'off');
    setappdata(handles.figure1, 'TRIPLEXresultsData', []);
    setappdata(handles.figure1, 'BlackPlot1TRIPLEX', []);
    setappdata(handles.figure1, 'BlackPlot2TRIPLEX', []);
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.text27, 'String', 'SNR<=20dB = 0');
    set(handles.text29, 'String', 'SNR>=60dB = 0');
    set(handles.text28, 'String', '20dB<=SNR<=60dB = 0');
    set(handles.checkbox1, 'enable', 'off');
    set(handles.checkbox2, 'enable', 'off');
    set(handles.checkbox3, 'enable', 'off');
    set(handles.checkbox4, 'enable', 'off');
    set(handles.checkbox1, 'Value', 1);
    set(handles.checkbox2, 'Value', 1);
    set(handles.checkbox3, 'Value', 1);
    set(handles.checkbox4, 'Value', 1);
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 16)    
    % After running the TRIPLEX Process Button with success
    
    set(handles.pushbutton9, 'enable', 'on');
    set(handles.pushbutton11, 'String', 'TRIPLEX Process');
    set(handles.pushbutton10, 'enable', 'on');
    set(handles.pushbutton11, 'enable', 'on');    
    set(handles.pushbutton12, 'enable', 'on');
    set(handles.pushbutton13, 'enable', 'on');
    set(handles.edit8, 'enable', 'on');
    set(handles.edit9, 'enable', 'on');
    set(handles.edit10, 'enable', 'on');
    set(handles.edit11, 'enable', 'on');
    set(handles.edit12, 'enable', 'on');
    set(handles.edit13, 'enable', 'on');
    set(handles.edit20, 'enable', 'on');
    set(handles.edit21, 'enable', 'on');
    set(handles.popupmenu3, 'enable', 'on');
    set(handles.popupmenu4, 'enable', 'on');
    set(handles.radiobutton13, 'enable', 'on');
    set(handles.radiobutton14, 'enable', 'on');
    set(handles.checkbox1, 'enable', 'on');
    set(handles.checkbox2, 'enable', 'on');
    set(handles.checkbox3, 'enable', 'on');
    set(handles.checkbox4, 'enable', 'on');
    
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    
elseif eq(flagvalue, 17)
    % After running the TRIPLEX Process Button with failure
    
    set(handles.pushbutton9, 'enable', 'on');
    set(handles.pushbutton11, 'String', 'TRIPLEX Process');
    set(handles.pushbutton10, 'enable', 'on');
    set(handles.pushbutton11, 'enable', 'on');        
    set(handles.edit8, 'enable', 'on');
    set(handles.edit9, 'enable', 'on');
    set(handles.edit10, 'enable', 'on');
    set(handles.edit11, 'enable', 'on');
    set(handles.edit12, 'enable', 'on');
    set(handles.edit13, 'enable', 'on');
    set(handles.edit20, 'enable', 'on');
    set(handles.edit21, 'enable', 'on');
    set(handles.radiobutton13, 'enable', 'on');
    set(handles.radiobutton14, 'enable', 'on');
    setappdata(handles.figure1, 'TRIPLEXresultsData', []);
    setappdata(handles.figure1, 'BlackPlot1TRIPLEX', []);
    setappdata(handles.figure1, 'BlackPlot2TRIPLEX', []);
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    set(handles.checkbox1, 'enable', 'off');
    set(handles.checkbox2, 'enable', 'off');
    set(handles.checkbox3, 'enable', 'off');
    set(handles.checkbox4, 'enable', 'off');
    
end