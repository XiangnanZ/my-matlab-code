function flagvalue = TRIPLEXProc(handles)

axes(handles.axes1);
cla reset;
hold off
axis on
box on

axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on

set(handles.Title1, 'String', 'Average Lifetime');
set(handles.Title2, 'String', 'Integrated Intensity');

pause(.01)

n = str2double(get(handles.edit13, 'String')); % Laguerre order
alpha = get(handles.edit10, 'String'); % alpha mode

if ~strcmp(alpha, 'auto')
    
    alpha = str2double(alpha);
    
end

n_tbin = str2double(get(handles.edit12, 'String')); % Number of sampling points in the truncated spec

dt_interp = str2double(get(handles.edit9, 'String'));

laser = getappdata(handles.figure1, 'TRIPLEXiIRF'); % System iIRF

% shif = 0; % laser shift

outputNames = getappdata(handles.figure1, 'TRIPLEXrawDataNames');
flagSlider = getappdata(handles.figure1, 'sliderFlag');
prefixes = outputNames.prefixes;
gain_list = outputNames.gain_values;

if gt(size(outputNames.filenames, 1), 1)
    
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'on');
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(outputNames.filenames, 1), 'SliderStep', [(1 / (size(outputNames.filenames, 1) - 1)) (1 / (size(outputNames.filenames, 1) - 1))]);
    flagSlider = 3;
    
end

lifet_avg = cell(size(outputNames.filenames));
spec_int = cell(size(outputNames.filenames));
cc = cell(size(outputNames.filenames));
alpha_out = cell(size(outputNames.filenames));
test = cell(size(outputNames.filenames));
out = cell(size(outputNames.filenames));
SNR = cell(size(outputNames.filenames));
BG_scaled = cell(size(outputNames.filenames));

if eq(get(handles.radiobutton12, 'Value'), 1)
    
    col = jet(size(outputNames.filenames, 1));
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(outputNames.Group);
        col0 = jet(size(outputNames.filenames, 1));
        cmin = min(groupnum);
        cmax = max(groupnum);
        idx = min(size(outputNames.filenames, 1), round((size(outputNames.filenames, 1) - 1) * (groupnum - cmin) / (cmax - cmin)) + 1);
        col = col0(idx, :);
        
    catch err
        
        col = jet(size(outputNames.filenames, 1));
        
    end
    
end

TRIPLEXDataType = getappdata(handles.figure1, 'TRIPLEXDataType');
setappdata(handles.figure1, 'TRIPLEXPeaks', []);
alpha_ind = 0;

kk = 0;
setappdata(handles.figure1, 'BGrem_flag', 0);
for i = 1:size(outputNames.filenames, 1)
    
    if eq(flagSlider, 3)
        
        set(handles.slider2, 'Value', i);
        
    end
    
    [specHeader, spec] = DataLoader(outputNames.filenames{i});
    dt = specHeader.TimeResolution;
    bw = specHeader.BW;
    
    spec = spec';
    noise = mean(spec((end - 100):end, :));
    spec = bsxfun(@minus, spec, noise);
    
    dataInd = detectSaturation(handles, spec);
    spec(:, dataInd) = 0;
    gainCor = 0;
    
    while ~kk
        quest1 = questdlg('Do you want to remove background fluorescence and/or adjust for the gain?',...
            'Well?', 'Remove BG and Gain Adj', 'Gain Adj Only', 'Nothing', 'Remove BG and Gain Adj');
        foldername_BG = [];
        if strcmp(quest1, 'Remove BG and Gain Adj')
            
            [BG_path, ~, ~] = fileparts(outputNames.filenames{i});
            [filename_BG, foldername_BG] = uigetfile('*.*', 'Please Select the Background Data from TRFS', BG_path);
            if ~foldername_BG
                
                flagvalue = 0;
                return
                
            end
            
            kk = 1;
            
        else
            
            kk = 1;
            
        end
        
    end
    
    if foldername_BG
        
        [spec_gainNorm, gainCor, BG_scaled{i}] = removeBG_TRIPLEX(filename_BG, foldername_BG, spec, handles);
        
        if isempty(spec_gainNorm)
            
            set(handles.pushbutton11, 'String', 'TRIPLEX Process');
            set(handles.text15, 'String', 'Number of Spectra: 0');
            waitfor(msgbox('Something went wrong'));
            flagvalue = 0;
            return
            
        end
        
    else
        
        if strcmp(quest1, 'Gain Adj Only')
            
            spec_gainNorm = spec;
            gainCor = 1;
            
        else
            
            spec_gainNorm = spec;
            
        end
        
    end
    
    warning off
    [lifet_avg{i}, ~, cc{i}, alpha_out{i}, test{i}, out{i}, SNR{i}, flagvalue, alpha_ind_out] = Triplex_main_script(spec_gainNorm', laser, dt, bw, n_tbin, n, alpha, dt_interp, handles, alpha_ind);
    
    if or(eq(flagvalue, 17), eq(flagvalue, 13))
        
        waitfor(msgbox('Deconvolution failed'));
        set(handles.pushbutton11, 'String', 'TRIPLEX Process');
        return
        
    end
    
    if eq(gainCor, 1)
        
        BGPos = load('PeaksPos.dat');
        
        gain_run = gain_list{i}(:, 1);
        gainfactor = BGPos(3, 4) - gain_run;
        gainfactor = gainfactor / 100;
        gainfactor = 3.^gainfactor;
        gain_list{i} = [gain_list{i} (BGPos(3, 4) * ones(size(gain_run)))];
        
    else
        
        gain_list{i} = [gain_list{i} gain_list{i}];
        
    end
    
    if ~isempty(out{i}{1})
        
        if eq(gainCor, 1)
            
            out{i}{1}{4} = bsxfun(@times, out{i}{1}{4}, gainfactor');
            out{i}{1}{3} = bsxfun(@times, out{i}{1}{3}, gainfactor');
            
        end
        
        spec_int{i}{1} = trapz(out{i}{1}{3})'.*dt_interp;
        
        spec_int{i}{1}(spec_int{i}{1} == 0) = nan;
        
    else
        
        spec_int{i}{1} = [];
        
    end
    
    if ~isempty(out{i}{2})
        
        if eq(gainCor, 1)
            
            out{i}{2}{4} = bsxfun(@times, out{i}{2}{4}, gainfactor');
            out{i}{2}{3} = bsxfun(@times, out{i}{2}{3}, gainfactor');
            
        end
        
        spec_int{i}{2} = trapz(out{i}{2}{3})'.*dt_interp;
        
        spec_int{i}{2}(spec_int{i}{2} == 0) = nan;
        
    else
        
        spec_int{i}{2} = [];
        
    end
    
    if ~isempty(out{i}{3})
        
        if eq(gainCor, 1)
            
            out{i}{3}{4} = bsxfun(@times, out{i}{3}{4}, gainfactor');
            out{i}{3}{3} = bsxfun(@times, out{i}{3}{3}, gainfactor');
            
        end
        
        spec_int{i}{3} = trapz(out{i}{3}{3})'.*dt_interp;
        
        spec_int{i}{3}(spec_int{i}{3} == 0) = nan;
        
    else
        
        spec_int{i}{3} = [];
        
    end
    
    if ~isempty(out{i}{4})
        
        if eq(gainCor, 1)
            
            out{i}{4}{4} = bsxfun(@times, out{i}{4}{4}, gainfactor');
            out{i}{4}{3} = bsxfun(@times, out{i}{4}{3}, gainfactor');
            
        end
        
        spec_int{i}{4} = trapz(out{i}{4}{3})'.*dt_interp;
        
        spec_int{i}{4}(spec_int{i}{4} == 0) = nan;
        
    else
        
        spec_int{i}{4} = [];
        
    end    
    
    alpha_ind = alpha_ind_out;
    
    if eq(get(handles.radiobutton13, 'Value'), 1)
        
        if ~isempty(lifet_avg{i}{1})
            
            y_axis1_mean(1) = nanmean(lifet_avg{i}{1});
            y_axis1_std(1) = nanstd(lifet_avg{i}{1});
            
            y_axis2_mean(1) = nanmean(spec_int{i}{1});
            y_axis2_std(1) = nanstd(spec_int{i}{1});
            
        else
            
            y_axis1_mean(1) = NaN;
            y_axis1_std(1) = NaN;
            
            y_axis2_mean(1) = NaN;
            y_axis2_std(1) = NaN;
            
        end
        
        if ~isempty(lifet_avg{i}{2})
            
            y_axis1_mean(2) = nanmean(lifet_avg{i}{2});
            y_axis1_std(2) = nanstd(lifet_avg{i}{2});
            
            y_axis2_mean(2) = nanmean(spec_int{i}{2});
            y_axis2_std(2) = nanstd(spec_int{i}{2});
            
        else
            
            y_axis1_mean(2) = NaN;
            y_axis1_std(2) = NaN;
            
            y_axis2_mean(2) = NaN;
            y_axis2_std(2) = NaN;
            
        end
        
         if ~isempty(lifet_avg{i}{3})
            
            y_axis1_mean(3) = nanmean(lifet_avg{i}{3});
            y_axis1_std(3) = nanstd(lifet_avg{i}{3});
            
            y_axis2_mean(3) = nanmean(spec_int{i}{3});
            y_axis2_std(3) = nanstd(spec_int{i}{3});
            
        else
            
            y_axis1_mean(3) = NaN;
            y_axis1_std(3) = NaN;
            
            y_axis2_mean(3) = NaN;
            y_axis2_std(3) = NaN;
            
         end
        
          if ~isempty(lifet_avg{i}{4})
            
            y_axis1_mean(4) = nanmean(lifet_avg{i}{4});
            y_axis1_std(4) = nanstd(lifet_avg{i}{4});
            
            y_axis2_mean(4) = nanmean(spec_int{i}{4});
            y_axis2_std(4) = nanstd(spec_int{i}{4});
            
        else
            
            y_axis1_mean(4) = NaN;
            y_axis1_std(4) = NaN;
            
            y_axis2_mean(4) = NaN;
            y_axis2_std(4) = NaN;
            
          end
          
          x_axis_values = {'Ch1' 'Ch2' 'Ch3' 'Ch4'};
          
          axes(handles.axes1);
          hold on
          errorbar(1:4, [y_axis1_mean(1) y_axis1_mean(2) y_axis1_mean(3) y_axis1_mean(4)], [y_axis1_std(1) y_axis1_std(2) y_axis1_std(3) y_axis1_std(4)], 'b.', 'MarkerSize', 15, 'color', col(i,:));
          plot(1:4, y_axis1_mean, 'b--', 'color', col(i, :));
          xlabel('Channels')
          ylabel('Average Lifetime (ns)')
          ylim([0 12]);
          set(gca, 'XTick', [1:4]);
          set(gca, 'XTickLabel', x_axis_values);
          hold off
          
          axes(handles.axes2);
          hold on
          errorbar(1:4, [y_axis2_mean(1) y_axis2_mean(2) y_axis2_mean(3) y_axis2_mean(4)], [y_axis2_std(1) y_axis2_std(2) y_axis2_std(3) y_axis2_std(4)], 'b.', 'MarkerSize', 15, 'color', col(i,:));
          plot(1:4, y_axis2_mean, 'b--', 'color', col(i, :));
          xlabel('Channels')
          ylabel('Integrated Intensity (a.u.)')
          ylim([0 35]);
          set(gca, 'XTick', [1:4]);
          set(gca, 'XTickLabel', x_axis_values);
          hold off
          
    else
        
         if ~isempty(lifet_avg{i}{1})
            
            y_axis1_1 = lifet_avg{i}{1};                       
            y_axis2_1 = spec_int{i}{1};
             
        else
            
            y_axis1_1 = NaN;            
            y_axis2_1 = NaN;
            
        end
        
        if ~isempty(lifet_avg{i}{2})
            
            y_axis1_2 = lifet_avg{i}{2};                       
            y_axis2_2 = spec_int{i}{2};
             
        else
            
            y_axis1_2 = NaN;            
            y_axis2_2 = NaN;
            
        end
        
         if ~isempty(lifet_avg{i}{3})
            
            y_axis1_3 = lifet_avg{i}{3};                       
            y_axis2_3 = spec_int{i}{3};
             
        else
            
            y_axis1_3 = NaN;            
            y_axis2_3 = NaN;
            
        end
        
         if ~isempty(lifet_avg{i}{4})
            
            y_axis1_4 = lifet_avg{i}{4};                       
            y_axis2_4 = spec_int{i}{4};
             
        else
            
            y_axis1_4 = NaN;            
            y_axis2_4 = NaN;
            
         end
         
         if ne(max(size(y_axis1_1)), 1)
             
             x_axis_values = (1:size(y_axis1_1, 1))';
             
         elseif ne(max(size(y_axis1_2)), 1)
             
             x_axis_values = (1:size(y_axis1_2, 1))';
             
         elseif ne(max(size(y_axis1_3)), 1)
             
             x_axis_values = (1:size(y_axis1_3, 1))';
             
         elseif ne(max(size(y_axis1_4)), 1)
             
             x_axis_values = (1:size(y_axis1_4, 1))';             
         
         end
             
         x_axis_values = x_axis_values - 1;
         
         if ne(max(size(y_axis1_1)), 1)
             
             SNR_0_1 = SNR{i}{1};
             SNR1 = 50 * ones(size(SNR_0_1, 1), 1);
             SNR1(SNR_0_1 <= 20) = 20;
             SNR1(SNR_0_1 >= 60) = 100;
             
         else
             
             y_axis1_1 = NaN * ones(size(x_axis_values));
             y_axis2_1 = NaN * ones(size(x_axis_values));
             SNR1 = ones(size(x_axis_values));
             
         end
             
         if ne(max(size(y_axis1_2)), 1)
             
             SNR_0_2 = SNR{i}{2};
             SNR2 = 50 * ones(size(SNR_0_2, 1), 1);
             SNR2(SNR_0_2 <= 20) = 20;
             SNR2(SNR_0_2 >= 60) = 100;
             
         else
             
             y_axis1_2 = NaN * ones(size(x_axis_values));
             y_axis2_2 = NaN * ones(size(x_axis_values));
             SNR2 = ones(size(x_axis_values));
             
         end
         
         if ne(max(size(y_axis1_3)), 1)
             
             SNR_0_3 = SNR{i}{3};
             SNR3 = 50 * ones(size(SNR_0_3, 1), 1);
             SNR3(SNR_0_3 <= 20) = 20;
             SNR3(SNR_0_3 >= 60) = 100;
             
         else
             
             y_axis1_3 = NaN * ones(size(x_axis_values));
             y_axis2_3 = NaN * ones(size(x_axis_values));
             SNR3 = ones(size(x_axis_values));
             
         end
         
         if ne(max(size(y_axis1_4)), 1)
             
             SNR_0_4 = SNR{i}{4};
             SNR4 = 50 * ones(size(SNR_0_4, 1), 1);
             SNR4(SNR_0_4 <= 20) = 20;
             SNR4(SNR_0_4 >= 60) = 100;
             
         else
             
             y_axis1_4 = NaN * ones(size(x_axis_values));
             y_axis2_4 = NaN * ones(size(x_axis_values));
             SNR4 = ones(size(x_axis_values));
             
         end
         
         axes(handles.axes1);
         hold on
         plot(x_axis_values, y_axis1_1, 'b-', 'color', col(i,:));
         plot(x_axis_values, y_axis1_2, 'b--', 'color', col(i,:));
         plot(x_axis_values, y_axis1_3, 'b-.', 'color', col(i,:));
         plot(x_axis_values, y_axis1_4, 'b-o', 'color', col(i,:));
         xlabel('Point Indices (pts)')
         ylabel('Average Lifetime (ns)')
         ylim([0 12]);
         hold off
         
         axes(handles.axes2);
         hold on
         plot(x_axis_values, y_axis2_1, 'b-', 'color', col(i,:));
         plot(x_axis_values, y_axis2_2, 'b--', 'color', col(i,:));
         plot(x_axis_values, y_axis2_3, 'b-.', 'color', col(i,:));
         plot(x_axis_values, y_axis2_4, 'b-o', 'color', col(i,:));
         xlabel('Point Indices (pts)')
         ylabel('Integrated Intensity (a.u.)')
         ylim([0 35]);
         hold off
         
    end
    
    pause(.01)
    
end

SNR01 = cell2mat(SNR{i});
less20dB = find(le(SNR01, 20));
str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 1))];
set(handles.text27, 'String', str_less20dB);

great60dB = find(ge(SNR01, 60));
str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 1))];
set(handles.text29, 'String', str_great60dB);

str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR01(:), 1) - size(great60dB, 1) - size(less20dB, 1))];
set(handles.text28, 'String', str_between2060dB);

axes(handles.axes2);
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{i}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(outputNames.Group);
        legend({[num2str(groupnum(i)) '_' prefixes{i}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{i}}, 'Interpreter', 'none');
        
    end
    
end

axes(handles.axes2);
A = cell(size(spec_int, 1), 1);
x_num_points = zeros(size(spec_int, 1), 1);
for j = 1:size(A, 1)
    
    A{j} = cell2mat(spec_int{j});
    x_num_points(j) = size(A{j}, 1) - 1;
    
end

if eq(get(handles.radiobutton13, 'Value'), 1)
    
    try
        
        ylim([0 (max(mean(cell2mat(A'))) + max(std(cell2mat(A'))))]);
        
    catch err
        
        ydata01 = get(gca, 'Children');
        ydata1 = get(ydata01, 'YData');
        ylim([0 max(cellfun(@nanmax, ydata1))])
        
    end
    clear A
    
else
    
    ylim([0 max(max(cell2mat(A)))]);
    clear A
    
end

if eq(get(handles.radiobutton13, 'Value'), 1)
    
    axes(handles.axes1);
    hold on
    h1(1) = errorbar(1:4, [y_axis1_mean(1) y_axis1_mean(2) y_axis1_mean(3) y_axis1_mean(4)], [y_axis1_std(1) y_axis1_std(2) y_axis1_std(3) y_axis1_std(4)], 'k.', 'MarkerSize', 20);
    h1(2) = plot(1:4, y_axis1_mean, 'k--', 'LineWidth', 3);
    hold off
    
    axes(handles.axes2);
    hold on
    h2(1) = errorbar(1:4, [y_axis2_mean(1) y_axis2_mean(2) y_axis2_mean(3) y_axis2_mean(4)], [y_axis2_std(1) y_axis2_std(2) y_axis2_std(3) y_axis2_std(4)], 'k.', 'MarkerSize', 20);
    h2(2) = plot(1:4, y_axis2_mean, 'k--', 'LineWidth', 3);
    hold off
    
    setappdata(handles.figure1, 'TypeTRIPLEX', 1);
    
else
    
    axes(handles.axes1);
    hold on
    h1(1) = plot(x_axis_values, y_axis1_1, 'k-');
    h1(2) = plot(x_axis_values, y_axis1_2, 'k--');
    h1(3) = plot(x_axis_values, y_axis1_3, 'k-.');
    h1(4) = plot(x_axis_values, y_axis1_4, 'k-o');
    legend({'CH1' 'CH2' 'CH3' 'CH4'});
    h1(5) = scatter(x_axis_values, y_axis1_1, SNR1, 'k', 'fill');
    h1(6) = scatter(x_axis_values, y_axis1_2, SNR2, 'k', 'fill');
    h1(7) = scatter(x_axis_values, y_axis1_3, SNR3, 'k', 'fill');
    h1(8) = scatter(x_axis_values, y_axis1_4, SNR4, 'k', 'fill');
    xlim([0 max(x_num_points)]);
    hold off
    
    axes(handles.axes2);
    hold on
    h2(1) = plot(x_axis_values, y_axis2_1, 'k-');
    h2(2) = plot(x_axis_values, y_axis2_2, 'k--');
    h2(3) = plot(x_axis_values, y_axis2_3, 'k-.');
    h2(4) = plot(x_axis_values, y_axis2_4, 'k-o');    
    h2(5) = scatter(x_axis_values, y_axis2_1, SNR1, 'k', 'fill');
    h2(6) = scatter(x_axis_values, y_axis2_2, SNR2, 'k', 'fill');
    h2(7) = scatter(x_axis_values, y_axis2_3, SNR3, 'k', 'fill');
    h2(8) = scatter(x_axis_values, y_axis2_4, SNR4, 'k', 'fill');
    xlim([0 max(x_num_points)]);
    hold off
    
    setappdata(handles.figure1, 'TypeTRIPLEX', 2);
    
end

setappdata(handles.figure1, 'sliderFlag', flagSlider);
setappdata(handles.figure1, 'BlackPlot1TRIPLEX', h1);
setappdata(handles.figure1, 'BlackPlot2TRIPLEX', h2);

if eq(flagSlider, 3)
    
    set(handles.slider2, 'enable', 'on');
    
end

TRIPLEX_out.lifet_avg = lifet_avg;
TRIPLEX_out.spec_int = spec_int;
TRIPLEX_out.cc = cc;
TRIPLEX_out.alpha_out = alpha_out;
TRIPLEX_out.test = test;
TRIPLEX_out.out = out;
TRIPLEX_out.SNR = SNR;
TRIPLEX_out.gain_list = gain_list;
TRIPLEX_out.BG_scaled = BG_scaled;

setappdata(handles.figure1, 'TRIPLEXresultsData', TRIPLEX_out);

flagvalue = 16;