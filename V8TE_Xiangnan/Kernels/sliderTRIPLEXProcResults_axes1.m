function sliderTRIPLEXProcResults_axes1(handles, ind)

popupmenu3_Value = get(handles.popupmenu3, 'Value');
if ne(popupmenu3_Value, 1)
    
    L_num = get(handles.popupmenu3, 'String');
    L_num = L_num{popupmenu3_Value};
    L_num1 = str2double(regexp(L_num, '\d+', 'match'));
    
end

inputData = getappdata(handles.figure1, 'outputResults');
inputNames = getappdata(handles.figure1, 'outputNames');
groupnums = inputNames.groupnums;

un_groupnums = groupnums;

if eq(get(handles.checkbox4, 'Value'), 1)
    
    un_groupnums = unique(groupnums);
    
end

y_axis1_mean_all = cell(size(inputNames.filenames, 1), 1);
y_axis1_mean = cell(size(inputNames.filenames, 1), 1);
y_axis1_std = cell(size(inputNames.filenames, 1), 1);

for i = 1:size(inputNames.filenames, 1)
    
    axes(handles.axes1);
    if eq(popupmenu3_Value, 1)
        
        % Get the nanmean values of lifetime
        % -----------------------------------------------------------------------------------------------------
        
        if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
            
            yaxis1_value = inputData{i}.lifet_avg;
            
            if ~isempty(yaxis1_value{1})
                
                y_axis1_mean{i}(1) = nanmean(yaxis1_value{1});
                y_axis1_std{i}(1) = nanstd(yaxis1_value{1});
                
            else
                
                y_axis1_mean{i}(1) = NaN;
                y_axis1_std{i}(1) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_mean{i}(2) = nanmean(yaxis1_value{2});
                y_axis1_std{i}(2) = nanstd(yaxis1_value{2});
                
            else
                
                y_axis1_mean{i}(2) = NaN;
                y_axis1_std{i}(2) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_mean{i}(3) = nanmean(yaxis1_value{3});
                y_axis1_std{i}(3) = nanstd(yaxis1_value{3});
                
            else
                
                y_axis1_mean{i}(3) = NaN;
                y_axis1_std{i}(3) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_mean{i}(4) = nanmean(yaxis1_value{4});
                y_axis1_std{i}(4) = nanstd(yaxis1_value{4});
                
            else
                
                y_axis1_mean{i}(4) = NaN;
                y_axis1_std{i}(4) = NaN;
                
            end
            
            % Else get the points
            % -----------------------------------------------------------------------------------------------------
            
        else
            
            yaxis1_value = inputData{ind}.lifet_avg;
            SNR = inputData{ind}.SNR;    
            
            if ~isempty(yaxis1_value{1})
                
                y_axis1_1 = yaxis1_value{1};
                
            else
                
                y_axis1_1 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_2 = yaxis1_value{2};
                
            else
                
                y_axis1_2 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_3 = yaxis1_value{3};
                
            else
                
                y_axis1_3 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_4 = yaxis1_value{4};
                
            else
                
                y_axis1_4 = NaN;
                
            end
            
            if ne(max(size(y_axis1_1)), 1)
                
                x_axis_values = (1:size(y_axis1_1, 1))';
                
            elseif ne(max(size(y_axis1_2)), 1)
                
                x_axis_values = (1:size(y_axis1_2, 1))';
                
            elseif ne(max(size(y_axis1_3)), 1)
                
                x_axis_values = (1:size(y_axis1_3, 1))';
                
            elseif ne(max(size(y_axis1_4)), 1)
                
                x_axis_values = (1:size(y_axis1_4, 1))';
                
            end
            
            x_axis_values = x_axis_values - 1;
            
            if ne(max(size(y_axis1_1)), 1)
                
                SNR_0_1 = SNR{1};
                SNR1 = 50 * ones(size(SNR_0_1, 1), 1);
                SNR1(SNR_0_1 <= 20) = 20;
                SNR1(SNR_0_1 >= 60) = 100;
                
            else
                
                y_axis1_1 = NaN * ones(size(x_axis_values));
                SNR1 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_2)), 1)
                
                SNR_0_2 = SNR{2};
                SNR2 = 50 * ones(size(SNR_0_2, 1), 1);
                SNR2(SNR_0_2 <= 20) = 20;
                SNR2(SNR_0_2 >= 60) = 100;
                
            else
                
                y_axis1_2 = NaN * ones(size(x_axis_values));
                SNR2 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_3)), 1)
                
                SNR_0_3 = SNR{3};
                SNR3 = 50 * ones(size(SNR_0_3, 1), 1);
                SNR3(SNR_0_3 <= 20) = 20;
                SNR3(SNR_0_3 >= 60) = 100;
                
            else
                
                y_axis1_3 = NaN * ones(size(x_axis_values));
                SNR3 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_4)), 1)
                
                SNR_0_4 = SNR{4};
                SNR4 = 50 * ones(size(SNR_0_4, 1), 1);
                SNR4(SNR_0_4 <= 20) = 20;
                SNR4(SNR_0_4 >= 60) = 100;
                
            else
                
                y_axis1_4 = NaN * ones(size(x_axis_values));
                SNR4 = ones(size(x_axis_values));
                
            end
            
        end
        
    else
        
        % Get Laguerre
        % -----------------------------------------------------------------------------------------------------
        
        if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
            
            yaxis1_value = inputData{i}.Laguerre_coeffs;
            
            if ~isempty(yaxis1_value{1})
                
                y_axis1_mean{i}(1) = nanmean(yaxis1_value{1}{L_num1});
                y_axis1_std{i}(1) = nanstd(yaxis1_value{1}{L_num1});
                
            else
                
                y_axis1_mean{i}(1) = NaN;
                y_axis1_std{i}(1) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_mean{i}(2) = nanmean(yaxis1_value{2}{L_num1});
                y_axis1_std{i}(2) = nanstd(yaxis1_value{2}{L_num1});
                
            else
                
                y_axis1_mean{i}(2) = NaN;
                y_axis1_std{i}(2) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_mean{i}(3) = nanmean(yaxis1_value{3}{L_num1});
                y_axis1_std{i}(3) = nanstd(yaxis1_value{3}{L_num1});
                
            else
                
                y_axis1_mean{i}(3) = NaN;
                y_axis1_std{i}(3) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_mean{i}(4) = nanmean(yaxis1_value{4}{L_num1});
                y_axis1_std{i}(4) = nanstd(yaxis1_value{4}{L_num1});
                
            else
                
                y_axis1_mean{i}(4) = NaN;
                y_axis1_std{i}(4) = NaN;
                
            end
            
        else
            
            % Else get the Laguerres
            % -----------------------------------------------------------------------------------------------------
            
            yaxis1_value = inputData{ind}.Laguerre_coeffs;
            SNR = inputData{ind}.SNR;
                
            if ~isempty(yaxis1_value{1})
                
                y_axis1_1 = yaxis1_value{1}{L_num1};
                
            else
                
                y_axis1_1 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_2 = yaxis1_value{2}{L_num1};
                
            else
                
                y_axis1_2 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_3 = yaxis1_value{3}{L_num1};
                
            else
                
                y_axis1_3 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_4 = yaxis1_value{4}{L_num1};
                
            else
                
                y_axis1_4 = NaN;
                
            end
            
            if ne(max(size(y_axis1_1)), 1)
                
                x_axis_values = (1:size(y_axis1_1, 2))';
                
            elseif ne(max(size(y_axis1_2)), 1)
                
                x_axis_values = (1:size(y_axis1_2, 2))';
                
            elseif ne(max(size(y_axis1_3)), 1)
                
                x_axis_values = (1:size(y_axis1_3, 2))';
                
            elseif ne(max(size(y_axis1_4)), 1)
                
                x_axis_values = (1:size(y_axis1_4, 2))';
                
            end
            
            x_axis_values = x_axis_values - 1;
            
            if ne(max(size(y_axis1_1)), 1)
                
                SNR_0_1 = SNR{1};
                SNR1 = 50 * ones(size(SNR_0_1, 1), 1);
                SNR1(SNR_0_1 <= 20) = 20;
                SNR1(SNR_0_1 >= 60) = 100;
                
            else
                
                y_axis1_1 = NaN * ones(size(x_axis_values))';
                SNR1 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_2)), 1)
                
                SNR_0_2 = SNR{2};
                SNR2 = 50 * ones(size(SNR_0_2, 1), 1);
                SNR2(SNR_0_2 <= 20) = 20;
                SNR2(SNR_0_2 >= 60) = 100;
                
            else
                
                y_axis1_2 = NaN * ones(size(x_axis_values))';
                SNR2 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_3)), 1)
                
                SNR_0_3 = SNR{3};
                SNR3 = 50 * ones(size(SNR_0_3, 1), 1);
                SNR3(SNR_0_3 <= 20) = 20;
                SNR3(SNR_0_3 >= 60) = 100;
                
            else
                
                y_axis1_3 = NaN * ones(size(x_axis_values))';
                SNR3 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_4)), 1)
                
                SNR_0_4 = SNR{4};
                SNR4 = 50 * ones(size(SNR_0_4, 1), 1);
                SNR4(SNR_0_4 <= 20) = 20;
                SNR4(SNR_0_4 >= 60) = 100;
                
            else
                
                y_axis1_4 = NaN * ones(size(x_axis_values))';
                SNR4 = ones(size(x_axis_values));
                
            end
            
        end
        
    end
    
    SNR_index = ones(1, 4);
    
    if eq(get(handles.checkbox5, 'Value'), 0)
        
        if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
            
            y_axis1_mean{i}(1) = NaN;
            y_axis1_std{i}(1) = NaN;
            
        else
            
            y_axis1_1 = NaN * ones(size(y_axis1_1));
            
        end
        
        SNR_index(1) = 0;
        
    end
    
    if eq(get(handles.checkbox6, 'Value'), 0)
        
        if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
            
            y_axis1_mean{i}(2) = NaN;
            y_axis1_std{i}(2) = NaN;
            
        else
            
            y_axis1_2 = NaN * ones(size(y_axis1_2));
            
        end
        
        SNR_index(2) = 0;
        
    end
    
    if eq(get(handles.checkbox7, 'Value'), 0)
        
        if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
            
            y_axis1_mean{i}(3) = NaN;
            y_axis1_std{i}(3) = NaN;
            
        else
            
            y_axis1_3 = NaN * ones(size(y_axis1_3));
            
        end
        
        SNR_index(3) = 0;
        
    end
    
    if eq(get(handles.checkbox8, 'Value'), 0)
        
        if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
            
            y_axis1_mean{i}(4) = NaN;
            y_axis1_std{i}(4) = NaN;
            
        else
            
            y_axis1_4 = NaN * ones(size(y_axis1_4));
            
        end
        
        SNR_index(4) = 0;
        
    end
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        if eq(get(handles.checkbox4, 'Value'), 1)
            
            y_axis1_mean_all{i, 1} = y_axis1_mean{i};
            
        end
       
    end 
    
end

A = cell(size(inputData, 1), 1);
x_num_points = zeros(size(inputData, 1), 1);
for j = 1:size(A, 1)
    
    A{j} = cell2mat(inputData{j}.lifet_avg');
    x_num_points(j) = size(A{j}, 1) - 1;
    
end
clear A

axes(handles.axes1);
h1 = getappdata(handles.figure1, 'BlackPlot');
delete(h1);

if eq(get(handles.checkbox4, 'Value'), 0)
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        hold on
        h1(1) = errorbar(1:4, [y_axis1_mean{ind}(1) y_axis1_mean{ind}(2) y_axis1_mean{ind}(3) y_axis1_mean{ind}(4)], [y_axis1_std{ind}(1) y_axis1_std{ind}(2) y_axis1_std{ind}(3) y_axis1_std{ind}(4)], 'k.', 'MarkerSize', 20);
        h1(2) = plot(1:4, y_axis1_mean{ind}, 'k--', 'LineWidth', 3);
        hold off
        
    else
        
        hold on
        h1(1) = plot(x_axis_values, y_axis1_1, 'k-');
        h1(2) = plot(x_axis_values, y_axis1_2, 'k--');
        h1(3) = plot(x_axis_values, y_axis1_3, 'k-.');
        h1(4) = plot(x_axis_values, y_axis1_4, 'k-o');
        legend({'CH1' 'CH2' 'CH3' 'CH4'});
        h1(5) = scatter(x_axis_values, y_axis1_1, SNR1, 'k', 'fill');
        h1(6) = scatter(x_axis_values, y_axis1_2, SNR2, 'k', 'fill');
        h1(7) = scatter(x_axis_values, y_axis1_3, SNR3, 'k', 'fill');
        h1(8) = scatter(x_axis_values, y_axis1_4, SNR4, 'k', 'fill');
        hold off
        
    end
    
    SNR01 = inputData{ind}.SNR;
    SNR01 = cell2mat(SNR01(logical(SNR_index)));
    less20dB = find(le(SNR01, 20));
    str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 1))];
    set(handles.text28, 'String', str_less20dB);
    
    great60dB = find(ge(SNR01, 60));
    str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 1))];
    set(handles.text30, 'String', str_great60dB);
    
    str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR01(:), 1) - size(great60dB, 1) - size(less20dB, 1))];
    set(handles.text29, 'String', str_between2060dB);
    
else
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        for j = ind
            
            f_un_labels = find(eq(groupnums, un_groupnums(j)));
            
            y_axis1_mean_all_1  = cell2mat( y_axis1_mean_all(f_un_labels));
            y_axis1_mean_all_mean = nanmean(y_axis1_mean_all_1);
            y_axis1_mean_all_std = nanstd(y_axis1_mean_all_1);
            
            if eq(size(y_axis1_mean_all_mean, 2), 1)
                
                y_axis1_mean_all_mean = y_axis1_mean_all_1;
                y_axis1_mean_all_std = [0 0 0 0];
                
            end
            
        end
        
        hold on
        h1(1) = errorbar(1:4, y_axis1_mean_all_mean, y_axis1_mean_all_std, 'k.', 'MarkerSize', 20);
        h1(2) = plot(1:4, y_axis1_mean_all_mean, 'k--', 'LineWidth', 3);
        hold off
        
        set(handles.text28, 'String', 'SNR<=20dB = 0');
        set(handles.text29, 'String', 'SNR>=60dB = 0');
        set(handles.text30, 'String', '20dB<=SNR<=60dB = 0');
        
    else
        
        hold on
        h1(1) = plot(x_axis_values, y_axis1_1, 'k-');
        h1(2) = plot(x_axis_values, y_axis1_2, 'k--');
        h1(3) = plot(x_axis_values, y_axis1_3, 'k-.');
        h1(4) = plot(x_axis_values, y_axis1_4, 'k-o');
        legend({'CH1' 'CH2' 'CH3' 'CH4'});
        h1(5) = scatter(x_axis_values, y_axis1_1, SNR1, 'k', 'fill');
        h1(6) = scatter(x_axis_values, y_axis1_2, SNR2, 'k', 'fill');
        h1(7) = scatter(x_axis_values, y_axis1_3, SNR3, 'k', 'fill');
        h1(8) = scatter(x_axis_values, y_axis1_4, SNR4, 'k', 'fill');
        hold off
        
        SNR01 = inputData{ind}.SNR;
        SNR01 = cell2mat(SNR01(logical(SNR_index)));
        less20dB = find(le(SNR01, 20));
        str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 1))];
        set(handles.text28, 'String', str_less20dB);
        
        great60dB = find(ge(SNR01, 60));
        str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 1))];
        set(handles.text30, 'String', str_great60dB);
        
        str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR01(:), 1) - size(great60dB, 1) - size(less20dB, 1))];
        set(handles.text29, 'String', str_between2060dB);
        
    end
    
end

setappdata(handles.figure1, 'BlackPlot', h1);