function TRFSProc(handles)

axes(handles.axes1);
cla reset;
hold off
axis on
box on

axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on

pause(.01)

record = str2double(get(handles.edit3, 'String'));
dt_interp = str2double(get(handles.edit4, 'String'));
n = str2double(get(handles.edit6, 'String'));
alpha = get(handles.edit7, 'String');

if ~strcmp(alpha, 'auto')
    
    alpha = str2double(alpha);
    
end

rawOutputData = getappdata(handles.figure1, 'rawOutputData');
spec = rawOutputData.raw_Intensity;
laser = getappdata(handles.figure1, 'iIRF')';
if ne(size(laser, 2), 1)
    laser = laser';
end
lambda = rawOutputData.wavelengths_list;
inputNames = getappdata(handles.figure1, 'outputNames');
prefixes = inputNames.prefixes;
dt = inputNames.timeResolution;
bw = inputNames.bw;

lifet_avg = cell(size(spec));
spec_int = cell(size(spec));
cc = cell(size(spec));
alpha_out = cell(size(spec));
test = cell(size(spec));
out = cell(size(spec));
SNR = cell(size(spec));

warning off
flagSlider = getappdata(handles.figure1, 'sliderFlag');
flagType = 0;

if eq(get(handles.radiobutton12, 'Value'), 1)
    
    col = jet(size(spec, 1));
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        col0 = jet(size(spec, 1));
        cmin = min(groupnum);
        cmax = max(groupnum);
        idx = min(size(spec, 1), round((size(spec, 1) - 1) * (groupnum - cmin) / (cmax - cmin)) + 1);
        col = col0(idx, :);
        
    catch err
        
        col = jet(size(spec, 1));
        
    end
    
end

if gt(size(spec, 1), 1)
    
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'on');
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(spec, 1), 'SliderStep', [(1 / (size(spec, 1) - 1)) (1 / (size(spec, 1) - 1))]);
    flagSlider = 2;
    
end

set(handles.Title1, 'String', 'Average Lifetime');
set(handles.Title2, 'String', 'Integrated Intensity');

alpha_ind = 0;
record_ind = 0;
for i = 1:size(spec, 1)
        
    if eq(flagSlider, 2)
        
        set(handles.slider2, 'Value', i);
        
    end
    
    warning('off', 'all');
    [lifet_avg{i}, ~, cc{i}, alpha_out{i}, test{i}, out{i}, alpha_ind_out, record_ind_out, record_out] = TRFS_main_script(spec{i}, laser(:, 1), dt{i}, bw{i}, lambda{i}', record, dt_interp, n, alpha, alpha_ind, record_ind, handles);
    
    spec_raw = out{i}{4};
    fIRF_noise = spec_raw;
    [~, v] = max(mean(fIRF_noise, 2));
    aa = fIRF_noise(v, :) - mean(fIRF_noise((end - 100):end, :));
    bb = std(fIRF_noise((end - 100):end, :));
    SNR{i} = 20*log10(aa./bb);
    SNR{i}(aa < bb) = 0; 
    
    lifet_avg{i}(SNR{i} == 0) = NaN;
    
    spec_int{i} = trapz(out{i}{3}).*(str2double(get(handles.edit4, 'String')));
    
    alpha_ind = alpha_ind_out;
    record_ind = record_ind_out;
    record = record_out;
    wave_step = min(abs(unique(diff(lambda{i}))));    
    axis(handles.axes2);
    
    if eq(get(handles.radiobutton9, 'Value'), 1)
        
        flagType = 1;
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            axes(handles.axes1);
            hold on
            plot(lambda{i}, lifet_avg{i}, 'color', col(i, :))      
            xlabel('Wavelengths (nm)');
            ylabel('Lifetime (ns)')
            ylim([0 12]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
            axes(handles.axes2);
            hold on
            plot(lambda{i}, spec_int{i}./norm(spec_int{i}), 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Normalized Intensity (a.u.)')
            ylim([0 1]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i});
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i}, pp);
            y1 = fnval(yy, xx);
            
            axes(handles.axes1);setappdata(handles.figure1, 'FlagTypeAxis1', flagType);
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Lifetime (ns)')
            ylim([0 12]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i}./norm(spec_int{i}));
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i}./norm(spec_int{i}), pp);
            y1 = fnval(yy, xx);
            
            axes(handles.axes2);
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Normalized Intensity (a.u.)')
            ylim([0 1]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
        end
        
    elseif eq(get(handles.radiobutton10, 'Value'), 1)
        
        flagType = 2;
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            axes(handles.axes1);
            hold on
            plot(lambda{i}, lifet_avg{i}, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Lifetime (ns)')
            ylim([0 12]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
            axes(handles.axes2);
            hold on
            plot(lambda{i}, spec_int{i}, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Absolute Intensity (a.u.)')
            ylim([0 25]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i});
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i}, pp);
            y1 = fnval(yy, xx);
            
            axes(handles.axes1);
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Lifetime (ns)')
            ylim([0 12]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i});
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i}, pp);
            y1 = fnval(yy, xx);
            
            axes(handles.axes2);
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Absolute Intensity (a.u.)')
            ylim([0 25]);
            xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))])
            hold off
            
        end
        
    end
    
    pause(.01)
    
end

axes(handles.axes1);
if and(lt(max(cell2mat(lifet_avg')), 15), gt(max(cell2mat(lifet_avg')), 12))
    
    ylim([0 max(cell2mat(lifet_avg'))])
    
else
    
    ylim([0 12])
    
end

axes(handles.axes2);
if eq(get(handles.radiobutton10, 'Value'), 1)
    
    ylim([0 max(cell2mat(spec_int'))])
    
elseif eq(get(handles.radiobutton9, 'Value'), 1)
    
    ydata01 = get(gca, 'Children');
    ydata1 = get(ydata01, 'YData');
    if iscell(ydata1)
        
        ylim([0 max(cellfun(@nanmax, ydata1))])
        
    else
        
        ylim([0 nanmax(ydata1)])
        
    end
    
end

if eq(get(handles.radiobutton9, 'Value'), 1)
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        axes(handles.axes1);
        hold on
        h1(1) = plot(lambda{i}, lifet_avg{i}, 'k-', 'LineWidth', 3);
        hold off
        
        axes(handles.axes2);
        hold on
        h2(1) = plot(lambda{i}, spec_int{i}./norm(spec_int{i}), 'k-', 'LineWidth', 3);
        hold off
        
    elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
        
        xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i});
        yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i}, pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes1);
        hold on
        h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            SNR_0 = SNR{i};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h1(2) = scatter(lambda{i}, lifet_avg{i}, SNR_1, col(i, :), 'fill');
            clear SNR_0 SNR_1;
            
        end
        
        hold off
        
        xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i}./norm(spec_int{i}));
        yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i}./norm(spec_int{i}), pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes2);
        hold on
        h2(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{i};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h2(2) = scatter(lambda{i}, spec_int{i}./norm(spec_int{i}), SNR_1, col(i, :), 'fill');
            clear SNR_0 SNR_1
            
        end
        
        hold off
        
    end
    
elseif eq(get(handles.radiobutton10, 'Value'), 1)
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        axes(handles.axes1);
        hold on
        h1(1) = plot(unique(lambda{i}), lifet_avg{i}, 'k-', 'LineWidth', 3);
        hold off
        
        axes(handles.axes2);
        hold on
        h2(1) = plot(unique(lambda{i}), spec_int{i}, 'k-', 'LineWidth', 3);
        hold off
        
    elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
        
        xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i});
        yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), lifet_avg{i}, pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes1);
        hold on
        h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{i};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h1(2) = scatter(lambda{i}, lifet_avg{i}, SNR_1, col(i, :), 'fill');
            clear SNR_1 SNR_0
            
        end
        
        hold off
        
        xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i});
        yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), spec_int{i}, pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes2);
        hold on
        h2(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{i};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h2(2) = scatter(lambda{i}, spec_int{i}, SNR_1, col(i, :), 'fill');
            clear SNR_0 SNR_1
            
        end
        
        hold off
        
    end
    
end

setappdata(handles.figure1, 'BlackPlot1', h1);
setappdata(handles.figure1, 'BlackPlot2', h2);
setappdata(handles.figure1, 'FlagTypeAxis1', flagType);
setappdata(handles.figure1, 'FlagTypeAxis2', flagType);

if eq(flagSlider, 2)
    
    set(handles.slider2, 'enable', 'on');
    
end

setappdata(handles.figure1, 'sliderFlag', flagSlider);

less20dB = find(le(SNR{i}, 20));
str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 2))];
set(handles.text27, 'String', str_less20dB);

great60dB = find(ge(SNR{i}, 60));
str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 2))];
set(handles.text29, 'String', str_great60dB);

str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR{i}, 2) - size(great60dB, 2) - size(less20dB, 2))];
set(handles.text28, 'String', str_between2060dB);

TRFS_out.lifet_avg = lifet_avg;
TRFS_out.spec_int = spec_int;
TRFS_out.cc = cc;
TRFS_out.alpha_out = alpha_out;
TRFS_out.test = test;
TRFS_out.out = out;
TRFS_out.SNR = SNR;

setappdata(handles.figure1, 'TRFS_Data', TRFS_out);

axes(handles.axes2);
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{i}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        legend({[num2str(groupnum(i)) '_' prefixes{i}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{i}}, 'Interpreter', 'none');
        
    end
    
end