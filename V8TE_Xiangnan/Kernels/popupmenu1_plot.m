function popupmenu1_plot(handles)

axes(handles.axes1);
cla reset;
hold off
axis on
box on

inputData = getappdata(handles.figure1, 'TRFS_Data');
rawOutputData = getappdata(handles.figure1, 'rawOutputData');
lambda = rawOutputData.wavelengths_list;
SNR = inputData.SNR;

xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))]);

popupmenu1_Value = get(handles.popupmenu1, 'Value');
if ne(popupmenu1_Value, 1)
    
    L_num = get(handles.popupmenu1, 'String');
    L_num = L_num{popupmenu1_Value};
    L_num = str2double(regexp(L_num, '\d+', 'match'));
    
end

warning off
flagSlider = getappdata(handles.figure1, 'sliderFlag');

if eq(get(handles.radiobutton9, 'Value'), 1)
    
    flagtype = 1;
    
elseif eq(get(handles.radiobutton10, 'Value'), 1)
    
    flagtype = 2;
    
end

setappdata(handles.figure1, 'FlagTypeAxis1', flagtype);

if eq(get(handles.radiobutton12, 'Value'), 1)
    
    col = jet(size(inputData.lifet_avg, 1));
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        inputNames = getappdata(handles.figure1, 'outputNames');
        groupnum = str2double(inputNames.Group);
        col0 = jet(size(inputData.lifet_avg, 1));
        cmin = min(groupnum);
        cmax = max(groupnum);
        idx = min(size(inputData.lifet_avg, 1), round((size(inputData.lifet_avg, 1) - 1) * (groupnum - cmin) / (cmax - cmin)) + 1);
        col = col0(idx, :);
        
    catch err
        
        col = jet(size(inputData.lifet_avg, 1));
        
    end
    
end

if gt(size(inputData.lifet_avg, 1), 1)
    
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'on');
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(inputData.lifet_avg, 1), 'SliderStep', [(1 / (size(inputData.lifet_avg, 1) - 1)) (1 / (size(inputData.lifet_avg, 1) - 1))]);
    flagSlider = 2;
    
end

if eq(popupmenu1_Value, 1)
    
    for i = 1:size(inputData.lifet_avg, 1)
        
        wave_step = min(abs(unique(diff(lambda{i}))));
        
        if eq(flagSlider, 2)
            
            set(handles.slider2, 'Value', i);
            
        end
        
        yaxis_values = inputData.lifet_avg{i};
%         yaxis_values = yaxis_values(isfinite(yaxis_values));
        set(handles.Title1, 'String', 'Average Lifetime');
        
        if eq(get(handles.radiobutton9, 'Value'), 1)
            
            if eq(get(handles.radiobutton6, 'Value'), 1)
                
                hold on
                plot(lambda{i}, yaxis_values, 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Lifetime (ns)')
                ylim([0 12]);
                hold off
                
            elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
                
                xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
                
                % 2.6 Apply spline interpolation on the plane x-y
                [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values);
                yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values, pp);
                y1 = fnval(yy, xx);
                
                hold on
                plot(xx, y1, 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Lifetime (ns)')
                ylim([0 12]);
                hold off
                
            end
            
        elseif eq(get(handles.radiobutton10, 'Value'), 1)
                        
            if eq(get(handles.radiobutton6, 'Value'), 1)
                
                hold on
                plot(lambda{i}, yaxis_values, 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Lifetime (ns)')
                ylim([0 12]);
                hold off
                
            elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
                
                xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
                
                % 2.6 Apply spline interpolation on the plane x-y
                [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values);
                yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values, pp);
                y1 = fnval(yy, xx);
                
                hold on
                plot(xx, y1, 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Lifetime (ns)')
                ylim([0 12]);
                hold off
                
            end
            
        end
        
        pause(.01)
        
    end
    
    if and(lt(max(cell2mat(inputData.lifet_avg')), 15), gt(max(cell2mat(inputData.lifet_avg')), 12))
        
        ylim([0 max(cell2mat(inputData.lifet_avg'))])
                
    else
        
        ylim([0 12])
        
    end
    
    if eq(get(handles.radiobutton9, 'Value'), 1)
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            hold on
            h1(1) = plot(lambda{i}, inputData.lifet_avg{i}, 'k-', 'LineWidth', 3);
            h1(2) = h1(1);
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.lifet_avg{i});
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.lifet_avg{i}, pp);
            y1 = fnval(yy, xx);
            
            hold on
            h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
            
            if eq(get(handles.radiobutton8, 'Value'), 1)
                
                SNR_0 = SNR{i};
                SNR_1 = 50 * ones(size(SNR_0, 2), 1);
                SNR_1(SNR_0 <= 20) = 20;
                SNR_1(SNR_0 >= 60) = 100;
                h1(2) = scatter(lambda{i}, inputData.lifet_avg{i}, SNR_1, col(i, :), 'fill');
                clear SNR_1 SNR_0
                
            end
            
            hold off
            
        end
        
    elseif eq(get(handles.radiobutton10, 'Value'), 1)
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            hold on
            h1(1) = plot(unique(lambda{i}), inputData.lifet_avg{i}, 'k-', 'LineWidth', 3);
            h1(2) = h1(1);
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.lifet_avg{i});
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.lifet_avg{i}, pp);
            y1 = fnval(yy, xx);
            
            hold on
            h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
            
            if eq(get(handles.radiobutton8, 'Value'), 1)
                
                SNR_0 = SNR{i};
                SNR_1 = 50 * ones(size(SNR_0, 2), 1);
                SNR_1(SNR_0 <= 20) = 20;
                SNR_1(SNR_0 >= 60) = 100;
                h1(2) = scatter(lambda{i}, inputData.lifet_avg{i}, SNR_1, col(i, :), 'fill');
                clear SNR_0 SNR_1
                
            end
            
            hold off
            
        end
        
    end
    
    setappdata(handles.figure1, 'BlackPlot1', h1);
    
    if eq(flagSlider, 2)
        
        set(handles.slider2, 'enable', 'on');
        
    end
    
    setappdata(handles.figure1, 'sliderFlag', flagSlider);
    
elseif gt(popupmenu1_Value, 1)
    
    for i = 1:size(inputData.lifet_avg, 1)
        
        wave_step = min(abs(unique(diff(lambda{i}))));
        
        if eq(flagSlider, 2)
            
            set(handles.slider2, 'Value', i);
            
        end
        
        yaxis_values = inputData.cc{i};
        yaxis_values = yaxis_values{L_num};
%         yaxis_values = yaxis_values(isfinite(yaxis_values));
        
        if eq(get(handles.radiobutton9, 'Value'), 1)
            
            yaxis_values = yaxis_values./norm(yaxis_values);
            
        end
        
        set(handles.Title1, 'String', ['Laguerre Coeffs ', num2str(L_num)]);
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            hold on
            plot(lambda{i}, yaxis_values, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Laguerre Coeffs (a.u.)')
            ylim([0 1]);
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values);
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values, pp);
            y1 = fnval(yy, xx);
            
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Laguerre Coeffs (a.u.)')
            ylim([0 2]);
            hold off
            
        end
        
        pause(.01)
        
    end
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        hold on
        h1(1) = plot(unique(lambda{i}), yaxis_values, 'k-', 'LineWidth', 3);
        h1(2) = h1(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
        
        xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values);
        yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values, pp);
        y1 = fnval(yy, xx);
        
        hold on
        h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{i};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h1(2) = scatter(lambda{i}, yaxis_values, SNR_1, col(i, :), 'fill');
            clear SNR_1 SNR_0
            
        end
        
        hold off
        
    end
    
    setappdata(handles.figure1, 'BlackPlot1', h1);
    
    if eq(flagSlider, 2)
        
        set(handles.slider2, 'enable', 'on');
        
    end
    
    setappdata(handles.figure1, 'sliderFlag', flagSlider);  
    axis tight
    
end