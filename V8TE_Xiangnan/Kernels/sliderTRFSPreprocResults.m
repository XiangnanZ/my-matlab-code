function sliderTRFSPreprocResults(handles, ind)

PB_flags = getappdata(handles.figure1, 'PB_flags');
set(handles.radiobutton4, 'Value', PB_flags(1));
set(handles.radiobutton5, 'Value', PB_flags(2));

% Initialization
inputNames = getappdata(handles.figure1, 'outputNames_TRFS');
prefixes = inputNames.prefices;
groupnums = inputNames.groupnums;
waveNum_Step = inputNames.waveNum_Step;

inputTRFSData = getappdata(handles.figure1, 'outputResults_TRFS');
wave_step = waveNum_Step(:, 2);

intIntensity = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));
wavelengths = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));

for i = 1:size(groupnums, 1)
    
    intIntensity(1:size(inputTRFSData{i}.intIntensity, 1), i) = inputTRFSData{i}.intIntensity;
    wavelengths(1:size(inputTRFSData{i}. wavelengths_list, 1), i) = inputTRFSData{i}. wavelengths_list;
        
end

if eq(get(handles.radiobutton4, 'Value'), 1)
     
     for i = 1:size(intIntensity, 2)
         
         intIntensity(:, i) = intIntensity(:, i)./norm(intIntensity(isfinite(intIntensity(:, i)), i));
         
     end
     
end

if eq(get(handles.checkbox1, 'Value'), 1)
    
    un_groupnums = unique(groupnums);
    
    intIntensity0 = NaN * ones(max(waveNum_Step(:, 1)), size(un_groupnums, 1));
    wavelengths0 = NaN * ones(max(waveNum_Step(:, 1)), size(un_groupnums, 1));
    std_intIntensity = intIntensity0;
    
    for i = 1:size(un_groupnums, 1)
        
        f_un_labels = find(eq(groupnums, un_groupnums(i)));
        
        intIntensity0(:, i) = nanmean(intIntensity(:, f_un_labels), 2);
        wavelengths0(:, i) = nanmean(wavelengths(:, f_un_labels), 2);
        std_intIntensity(:, i) = nanstd(intIntensity(:, f_un_labels), [], 2);
        
    end
    
    intIntensity = intIntensity0;
    wavelengths = wavelengths0;
    
end

i = ind;
h = getappdata(handles.figure1, 'BlackPlot');
delete(h);

if or(eq(get(handles.radiobutton4, 'Value'), 1), eq(get(handles.radiobutton5, 'Value'), 1))
    
    x_axes1_values = unique(wavelengths(isfinite(wavelengths(:, i)), i));
    y_axes1_values = intIntensity(isfinite(intIntensity(:, i)), i);
    axes(handles.axes1);
    
    if eq(get(handles.radiobutton1, 'Value'), 1)
        
        hold on        
        if eq(get(handles.checkbox1, 'Value'), 1)
            
            h(1) = errorbar(x_axes1_values, y_axes1_values, std_intIntensity(isfinite(std_intIntensity(:, i)), i), 'k-', 'LineWidth', 3);
                        
        else
            
            h(1) = plot(x_axes1_values, y_axes1_values, 'k-', 'LineWidth', 3);
                        
        end
        h(2) = h(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
        
        xx = linspace(nanmin(wavelengths(:, i)), nanmax(wavelengths(:, i)), (nanmax(wavelengths(:, i)) - nanmin(wavelengths(:, i))));
        
        % 2.6 Apply spline interpolation on the plane x-y
        if eq(get(handles.checkbox1, 'Value'), 1)
            
            wave_step_ind = find(eq(groupnums, un_groupnums(i)));
            
        else
            
            wave_step_ind = i;
            
        end
        [~, pp] = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i));
        yy = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i), pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes1);
        hold on
        h(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton3, 'Value'), 1)
            
            if eq(get(handles.checkbox1, 'Value'), 1)
                
                h(2) = errorbar(unique(wavelengths(isfinite(wavelengths(:, i)), i)), intIntensity(isfinite(intIntensity(:, i)), i), std_intIntensity(isfinite(std_intIntensity(:, i)), i), 'k.', 'LineWidth', 3);
                
            else
                
                h(2) = plot(unique(wavelengths(isfinite(wavelengths(:, i)), i)), intIntensity(isfinite(intIntensity(:, i)), i), 'k.', 'MarkerSize', 15);
                
            end
            
        else
            
            h(2) = h(1);
            
        end
        
        hold off
        
    end
    
end

setappdata(handles.figure1, 'BlackPlot', h);

if eq(get(handles.checkbox1, 'Value'), 0)
    
    legend({prefixes{ind}}, 'Interpreter', 'none');
    
else
    
    legend({num2str(un_groupnums(ind))}, 'Interpreter', 'none');
    
end