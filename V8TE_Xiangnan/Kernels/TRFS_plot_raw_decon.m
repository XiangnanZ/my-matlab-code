function TRFS_plot_raw_decon(handles, wavenum, pointnum)

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRFS_Data');

timeStep = str2double(get(prevHandles.edit4, 'string'));

% Plot Raw
raw_Data = getappdata(prevHandles.figure1, 'rawOutputData');
raw_intensity = raw_Data.trunc_Intensity{pointnum};
gain_list = raw_Data.gain_list{pointnum};
BG_scaled = raw_Data.BG_scaled{pointnum};

nf = findobj('Name','Signal_Gain'); % all graphical objects

if isempty(nf)
    
    nf = figure('Name','Signal_Gain');
    
end

figure(nf);
set(gcf, 'Position', [84 528 560 420]);

plot(-raw_intensity(:, wavenum), 'LineWidth', 3);
if ~isempty(BG_scaled)
    
    hold on
    plot(-BG_scaled(:, wavenum), 'r', 'LineWidth', 2);
    plot(-(raw_intensity(:, wavenum) - BG_scaled(:, wavenum)), 'g', 'LineWidth', 2);
    hold off
    ylim([-1 0.1])
    legend({'Raw Signal' 'Scaled Background' 'Residual'}, 'Location', 'SouthEast');
    
else
    
    ylim([-1 0.1])
    legend({'Raw Signal'}, 'Location', 'SouthEast');
        
end

title(['Signal Gain: ' num2str(round(gain_list(wavenum, 1)))])

rawOutputData = getappdata(prevHandles.figure1, 'rawOutputData');
lambda = rawOutputData.wavelengths_list;
SNR = out.SNR;
lifet = out.lifet_avg;
intIntensity = out.spec_int;

str = ['Measured Voltage of point No ' num2str(pointnum) ' at ' num2str(lambda{pointnum}(wavenum)) ' nm with SNR ' num2str(round(10 * SNR{pointnum}(wavenum)) / 10) 'dB, LT ' num2str(round(10 * lifet{pointnum}(wavenum)) / 10) ' ns and int_I ' num2str(round(10 * intIntensity{pointnum}(wavenum)) / 10)];

set(handles.Title1, 'String', str);

out_spec1 = out.out{pointnum}{3};
out_spec2 = out.out{pointnum}{4};
out_spec3 = out.out{pointnum}{2};
out_spec4 = out.out{pointnum}{6};
out_spec4 = out_spec4./max(out_spec4);
out_spec4 = out_spec4.*max(out_spec2(:, wavenum));

axes(handles.axes2)
x_values = (timeStep * [1:size(out_spec1, 1)]);

plot(x_values, out_spec1(:, wavenum), 'LineWidth', 2)
hold on
plot(x_values, out_spec2(:, wavenum), 'r-')
if gt(size(out_spec4, 1), size(x_values, 2))
    
    plot(x_values, out_spec4(1:size(x_values, 2)), 'g-')
    
else
    
    plot(x_values(1:size(out_spec4, 1)), out_spec4, 'g-')
    
end
hold off

xlabel('Time (ns)');
ylabel('Absolute Intensity (V)');
xlim([0 max(x_values)])
box on

inputNames = getappdata(prevHandles.figure1, 'outputNames');
prefixes = inputNames.prefixes;

legend({prefixes{pointnum}}, 'Interpreter', 'none');

axes(handles.axes1)
diff_out_spec = (out_spec2(:, wavenum) - out_spec1(:, wavenum))./ max(out_spec2(:, wavenum));
plot(x_values, diff_out_spec, 'm', 'LineWidth', 2)

ylabel('Norm. Residuals (a.u.)');
ylim([-max(max(diff_out_spec), abs(min(diff_out_spec))) max(max(diff_out_spec), abs(min(diff_out_spec)))]);
xlim([0 max(x_values)])
box on
% set(gca, 'XTick', [])

axes(handles.axes4)
diff_out_spec = out_spec2(:, wavenum) - out_spec1(:, wavenum);
diff_out_spec = xcorr(diff_out_spec, (size(diff_out_spec, 1) - 1), 'coeff');
plot(x_values, diff_out_spec(size(x_values, 2):end), 'm', 'LineWidth', 2)

bounds(1) = 2 / sqrt(size(x_values, 2));
bounds(2) = -bounds(1);
hold on
plot(x_values, (bounds(1) * ones(size(x_values))), 'k--')
plot(x_values, (bounds(2) * ones(size(x_values))), 'k--')
hold off

ylabel('Res. Autocorrelation (a.u.)');
ylim([-max(max(diff_out_spec), abs(min(diff_out_spec))) max(max(diff_out_spec), abs(min(diff_out_spec)))]);
xlim([0 max(x_values)])
box on

axes(handles.axes3)
plot(x_values, out_spec3(:, wavenum)./max(out_spec3(:, wavenum)), 'm', 'LineWidth', 2)

ylabel('Norm. Decay (a.u.)');
ylim([0 1])
xlim([0 max(x_values)])
box on
% set(gca, 'XTick', [])