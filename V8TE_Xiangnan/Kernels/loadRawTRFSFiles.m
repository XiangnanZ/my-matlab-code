function output = loadRawTRFSFiles(foldername, nameListing)

% 3. Remove all the '_hdr.dat' files
find_hdr = strfind(nameListing, '_hdr.dat');
nameListing(~cellfun('isempty', find_hdr)) = [];

% 4. Keep only the '.dat' files
find_dat = strfind(nameListing, '.dat');
nameListing(cellfun('isempty', find_dat)) = [];

% 5. Find the '_CH' part of the filenames and extract wavelenths, suffixes
% and prefixes.
find_CH = strfind(nameListing, '_CH');
nameListing(cellfun('isempty', find_CH)) = [];
find_CH(cellfun('isempty', find_CH)) = [];

if ne(size(nameListing, 2), 0)
    
    % wavelengths = [wavelengths flag]
    wavelengths = zeros(size(nameListing, 2), 3);
    filenames = cell(size(nameListing, 2), 1);
    flag_value = 1;
    
    for i = 1:size(find_CH, 2)
        
        str = cell2mat(nameListing(i));
        filenames{i, 1} = fullfile(foldername, str);
        
        if gt(i, 1)
            
            d = str2double(str((find_CH{i} - 3):(find_CH{i} - 1))) - wavelengths((i - 1), 1);
            
            if lt(d, 0)
                
                flag_value = flag_value + 1;
                
            end
            
        end
        
        wavelengths(i, 1) = str2double(str((find_CH{i} - 3):(find_CH{i} - 1)));
        wavelengths(i, 2) = flag_value;
        wavelengths(i, 3) = str2double(str((find_CH{i} - 8):(find_CH{i} - 5)));
        
    end
    
    prefixes = cell(flag_value, 1);
    
    for i = 1:flag_value
        
        t = find(eq(wavelengths(:, 2), i));
        str = cell2mat(nameListing(t(1)));
        prefixes{i, 1} = str(1:(find_CH{t(1)} - 5));
        
    end
    
    str = cell2mat(nameListing(1));
    suffix = strcat(str(find_CH{1}:(find_CH{1} + 3)), '.mat');
    
    output.wavelengths = wavelengths;
    output.suffix = suffix;
    output.filenames = filenames;
    output.prefixes = prefixes;
    
else
    
    output = [];
    
end