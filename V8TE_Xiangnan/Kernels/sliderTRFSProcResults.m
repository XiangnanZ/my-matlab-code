function sliderTRFSProcResults(handles, ind)

PB_flags = getappdata(handles.figure1, 'PB_flags_proc');
set(handles.radiobutton9, 'Value', PB_flags(1));
set(handles.radiobutton10, 'Value', PB_flags(2));

% Initialization
inputNames = getappdata(handles.figure1, 'outputNames_TRFSproc');
prefixes = inputNames.prefices;
groupnums = inputNames.groupnums;
waveNum_Step = inputNames.waveNum_Step;

% Colormap
if ne(unique(groupnums), 0)
    
    col = jet(max(groupnums));
    
else
    
    col = jet(size(groupnums, 1));
    
end

inputTRFSData = getappdata(handles.figure1, 'outputResults_TRFSproc');
wave_step = waveNum_Step(:, 2);

intIntensity = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));
lifet = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));
SNR = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));
wavelengths = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));

popupmenu2_Value = get(handles.popupmenu2, 'Value');
if ne(popupmenu2_Value, 1)
    
    L_num0 = get(handles.popupmenu2, 'String');
    L_num0 = L_num0{popupmenu2_Value};
    L_num2 = str2double(regexp(L_num0, '\d+', 'match'));
    
    set(handles.Title2, 'String', L_num0);
    axes(handles.axes2);
    ylabel('Laguerre Coeffs (a.u.)')
    
elseif eq(popupmenu2_Value, 1)
    
    set(handles.Title2, 'String', 'Integrated Intensity');
    axes(handles.axes2);
    ylabel('Normalized Intensity (a.u.)')    
    
end

popupmenu1_Value = get(handles.popupmenu1, 'Value');
if ne(popupmenu1_Value, 1)
    
    L_num0 = get(handles.popupmenu1, 'String');
    L_num0 = L_num0{popupmenu1_Value};
    L_num1 = str2double(regexp(L_num0, '\d+', 'match'));
    
    set(handles.Title1, 'String', L_num0);
    axes(handles.axes1);
    ylabel('Laguerre Coeffs (a.u.)')
    
elseif eq(popupmenu1_Value, 1)
    
    set(handles.Title1, 'String', 'Average Lifetime');
    axes(handles.axes1);
    ylabel('Lifetime (ns)')
        
end

for i = 1:size(groupnums, 1)
    
    if ne(popupmenu2_Value, 1)
        
        intIntensity(1:size(inputTRFSData{i}.Laguerre_coeffs{L_num2}, 2), i) = inputTRFSData{i}.Laguerre_coeffs{L_num2}';
        
    elseif eq(popupmenu2_Value, 1)
        
        intIntensity(1:size(inputTRFSData{i}.spec_int, 1), i) = inputTRFSData{i}.spec_int;
        
    end
    
    if ne(popupmenu1_Value, 1)
        
        lifet(1:size(inputTRFSData{i}.Laguerre_coeffs{L_num1}, 2), i) = inputTRFSData{i}.Laguerre_coeffs{L_num1}';
        
    elseif eq(popupmenu1_Value, 1)
        
        lifet(1:size(inputTRFSData{i}.lifet_avg, 1), i) = inputTRFSData{i}.lifet_avg;
        
    end
    
    wavelengths(1:size(inputTRFSData{i}.lambda, 1), i) = inputTRFSData{i}.lambda;
    SNR(1:size(inputTRFSData{i}.SNR, 1), i) = inputTRFSData{i}.SNR;
        
end

if eq(get(handles.radiobutton9, 'Value'), 1)
    
    for i = 1:size(intIntensity, 2)
         
         intIntensity(:, i) = intIntensity(:, i)./norm(intIntensity(isfinite(intIntensity(:, i)), i));
         if gt(popupmenu1_Value, 1)
             
             lifet(:, i) = lifet(:, i)./norm(lifet(isfinite(lifet(:, i)), i));
             
         end
         
     end
    
end

if eq(get(handles.checkbox3, 'Value'), 1)
    
    intIntensity0 = intIntensity./lifet;
    lifet0 = lifet./intIntensity;
    
    intIntensity = intIntensity0;
    lifet = lifet0;
    
end

un_groupnums = groupnums;

if eq(get(handles.checkbox2, 'Value'), 1)
    
    un_groupnums = unique(groupnums);
    
    intIntensity0 = NaN * ones(max(waveNum_Step(:, 1)), size(un_groupnums, 1));
    wavelengths0 = NaN * ones(max(waveNum_Step(:, 1)), size(un_groupnums, 1));
    std_intIntensity = intIntensity0;
    lifet0 = intIntensity0;
    std_lifet = intIntensity0;
    
    for i = 1:size(un_groupnums, 1)
        
        f_un_labels = find(eq(groupnums, un_groupnums(i)));
        
        intIntensity0(:, i) = nanmean(intIntensity(:, f_un_labels), 2);
        wavelengths0(:, i) = nanmean(wavelengths(:, f_un_labels), 2);
        std_intIntensity(:, i) = nanstd(intIntensity(:, f_un_labels), [], 2);
        
        lifet0(:, i) = nanmean(lifet(:, f_un_labels), 2);
        std_lifet(:, i) = nanstd(lifet(:, f_un_labels), [], 2);
        
    end
    
    intIntensity = intIntensity0;
    wavelengths = wavelengths0;
    lifet = lifet0;
    
end

i = ind;
h = getappdata(handles.figure1, 'BlackPlot');
delete(h);
h1 = getappdata(handles.figure1, 'BlackPlot1');
delete(h1);

if or(eq(get(handles.radiobutton9, 'Value'), 1), eq(get(handles.radiobutton10, 'Value'), 1))
    
%     x_axes1_values = unique(wavelengths(isfinite(wavelengths(:, i)), i));
    x_axes1_values = unique(wavelengths(:, i));
%     y_axes1_values = intIntensity(isfinite(intIntensity(:, i)), i);
    y_axes1_values = intIntensity(:, i);
    axes(handles.axes2);
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        hold on        
        if eq(get(handles.checkbox2, 'Value'), 1)
            
%             h1(1) = errorbar(x_axes1_values, y_axes1_values, std_intIntensity(isfinite(std_intIntensity(:, i)), i), 'k-', 'LineWidth', 3);
            h1(1) = errorbar(x_axes1_values, y_axes1_values, std_intIntensity(:, i), 'k-', 'LineWidth', 3);
            
        else
            
            h1(1) = plot(x_axes1_values, y_axes1_values, 'k-', 'LineWidth', 3);
            
        end
        h1(2) = h1(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton7, 'Value'), 1), eq(get(handles.radiobutton8, 'Value'), 1))
        
        xx = linspace(nanmin(wavelengths(:, i)), nanmax(wavelengths(:, i)), (nanmax(wavelengths(:, i)) - nanmin(wavelengths(:, i))));
        
        % 2.6 Apply spline interpolation on the plane x-y
        if eq(get(handles.checkbox2, 'Value'), 1)
            
            wave_step_ind = find(eq(groupnums, un_groupnums(i)));
            
        else
            
            wave_step_ind = i;
            
        end
        [~, pp] = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i));
        yy = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i), pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes2);
        hold on
        h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            if eq(get(handles.checkbox2, 'Value'), 1)
                
                h1(2) = errorbar(unique(wavelengths(isfinite(wavelengths(:, i)), i)), intIntensity(isfinite(intIntensity(:, i)), i), std_intIntensity(isfinite(std_intIntensity(:, i)), i), 'k.', 'LineWidth', 3);
                
            else
                
                SNR_0 = SNR(isfinite(SNR(:, i)), i);
                SNR_1 = 50 * ones(size(SNR_0, 1), 1);
                SNR_1(SNR_0 <= 20) = 20;
                SNR_1(SNR_0 >= 60) = 100;
                h1(2) = scatter(unique(wavelengths(isfinite(wavelengths(:, i)), i)), intIntensity(isfinite(intIntensity(:, i)), i), SNR_1, col(un_groupnums(i), :), 'fill');
                clear SNR_0 SNR_1;
                
            end
            
        else
            
            h1(2) = h1(1);
            
        end
        
        hold off
        
    end
    
end

if or(eq(get(handles.radiobutton9, 'Value'), 1), eq(get(handles.radiobutton10, 'Value'), 1))
    
%     x_axes1_values = unique(wavelengths(isfinite(wavelengths(:, i)), i));
    x_axes1_values = unique(wavelengths(:, i));
%     y_axes1_values = lifet(isfinite(lifet(:, i)), i);
    y_axes1_values = lifet(:, i);
    axes(handles.axes1);
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        hold on        
        if eq(get(handles.checkbox2, 'Value'), 1)
            
%             h(1) = errorbar(x_axes1_values, y_axes1_values, std_lifet(isfinite(std_lifet(:, i)), i), 'k-', 'LineWidth', 3);
            h(1) = errorbar(x_axes1_values, y_axes1_values, std_lifet(:, i), 'k-', 'LineWidth', 3);
            
        else
            
            h(1) = plot(x_axes1_values, y_axes1_values, 'k-', 'LineWidth', 3);
            
        end
        h(2) = h(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton7, 'Value'), 1), eq(get(handles.radiobutton8, 'Value'), 1))
        
        xx = linspace(nanmin(wavelengths(:, i)), nanmax(wavelengths(:, i)), (nanmax(wavelengths(:, i)) - nanmin(wavelengths(:, i))));
        
        % 2.6 Apply spline interpolation on the plane x-y
        if eq(get(handles.checkbox2, 'Value'), 1)
            
            wave_step_ind = find(eq(groupnums, un_groupnums(i)));
            
        else
            
            wave_step_ind = i;
            
        end
        [~, pp] = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), lifet(:, i));
        yy = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), lifet(:, i), pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes1);
        hold on
        h(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            if eq(get(handles.checkbox2, 'Value'), 1)
                
                h(2) = errorbar(unique(wavelengths(isfinite(wavelengths(:, i)), i)), lifet(isfinite(lifet(:, i)), i), std_lifet(isfinite(std_lifet(:, i)), i), 'k.', 'LineWidth', 3);
                
            else
                
                SNR_0 = SNR(isfinite(SNR(:, i)), i);
                SNR_1 = 50 * ones(size(SNR_0, 1), 1);
                SNR_1(SNR_0 <= 20) = 20;
                SNR_1(SNR_0 >= 60) = 100;
                h(2) = scatter(unique(wavelengths(isfinite(wavelengths(:, i)), i)), lifet(isfinite(lifet(:, i)), i), SNR_1, col(un_groupnums(i), :), 'fill');
                clear SNR_0 SNR_1;
                
            end
            
        else
            
            h(2) = h(1);
            
        end
        
        hold off
        
    end
    
end

setappdata(handles.figure1, 'BlackPlot', h);
setappdata(handles.figure1, 'BlackPlot1', h1);
    
if eq(get(handles.checkbox2, 'Value'), 0)
    
    legend({prefixes{ind}}, 'Interpreter', 'none');
    less20dB = find(le(SNR(isfinite(SNR(:, ind)), ind), 20));
    str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 1))];
    set(handles.text28, 'String', str_less20dB);
    
    great60dB = find(ge(SNR(isfinite(SNR(:, ind)), ind), 60));
    str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 1))];
    set(handles.text30, 'String', str_great60dB);
    
    str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR(isfinite(SNR(:, ind)), ind), 1) - size(great60dB, 1) - size(less20dB, 1))];
    set(handles.text29, 'String', str_between2060dB);
    
else
    
    legend({num2str(un_groupnums(ind))}, 'Interpreter', 'none');
    
end