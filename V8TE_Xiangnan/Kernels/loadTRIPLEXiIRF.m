function flagvalue = loadTRIPLEXiIRF(handles)

axes(handles.axes1);
cla reset;
box on
hold off

axes(handles.axes2);
cla reset;
box on
legend off
hold off

set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');

dataInfo = getappdata(handles.figure1, 'TRIPLEXrawDataNames');
dataPath = 'M:\Tissue Engineering Project 2015\Data\IRFs';
% [dataPath, ~] = fileparts(dataInfo.filenames{1});

[filename, foldername] = uigetfile('.mat', 'Please Select the TRIPLEX iIRF File', dataPath);

if ~foldername
    
    flagvalue = 13;
    return;
    
end

full_path = fullfile(foldername, filename);
iIRF_in = load(full_path);
fName = fieldnames(iIRF_in);
iIRF = getfield(iIRF_in, char(fName));

setappdata(handles.figure1, 'TRIPLEXiIRF', iIRF);

flagvalue = 14;

