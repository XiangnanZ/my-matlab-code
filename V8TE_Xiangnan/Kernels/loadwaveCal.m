function flagvalue = loadwaveCal(handles)

axes(handles.axes1);
cla reset;
axis on
box on
hold off

axes(handles.axes2);
cla reset;
axis on
box on
legend off
hold off

set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');

dataInfo = getappdata(handles.figure1, 'outputNames');
[dataPath, ~] = fileparts(dataInfo.filenames{1});

[filename, foldername] = uigetfile('.mat', 'Please Select the Wavelength Calibration File', dataPath);
if ~foldername
    
    flagvalue = 3;
    return
    
end

full_path = fullfile(foldername, filename);
wavelength_calib = load(full_path);
fName = fieldnames(wavelength_calib);
wavelength_calib = getfield(wavelength_calib, char(fName));

setappdata(handles.figure1, 'Wavelength_Calib', wavelength_calib);
flagvalue = 4;