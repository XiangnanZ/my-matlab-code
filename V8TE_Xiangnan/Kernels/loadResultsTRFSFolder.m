function [output, outputResults] = loadResultsTRFSFolder(foldername)

% 1. Read the folder contents
dirListing = dir(foldername);
nameListing = {dirListing.name};

% 2. Find and exclude folders within the folder
find_Dirs = [dirListing.isdir];
nameListing(find_Dirs) = [];

% 3. Keep only the '_TRFS_PreProc' files
find_dat = strfind(nameListing, '_TRFS_DeCon');
nameListing(cellfun('isempty', find_dat)) = [];

% 4. Create filenames and groupnums
if ne(size(nameListing, 2), 0)
    
    groupnums = zeros(size(nameListing, 2), 1);
    filenames = cell(size(nameListing, 2), 1);
    prefices = cell(size(nameListing, 2), 1);
    waveNum_Step = zeros(size(nameListing, 2), 2);
    
    outputResults = cell(size(nameListing, 2), 1);
    
    for i = 1:size(nameListing, 2)
        
        str = cell2mat(nameListing(i));
        prefFind = strfind(str, '.mat');
        filenames{i, 1} = fullfile(foldername, str);
        prefices{i, 1} = str(1:(prefFind - 1));
        groupnums(i, 1) = str2double(str(1:2));
        
        outputResults{i, 1} = load(filenames{i, 1});
        
        waveNum_Step(i, 1) = size(outputResults{i, 1}.lambda, 1);
        waveNum_Step(i, 2) = min(diff(outputResults{i, 1}.lambda, 1));
        
    end
    
    output.groupnums = groupnums;
    output.filenames = filenames;
    output.prefices = prefices;
    output.waveNum_Step = waveNum_Step;
    
else
    
    output = [];
    outputResults = [];
    
end