function output = loadRawTRFSFolder_Davis(foldername, handles)

% 1. Read the folder contents
dirListing = dir(foldername);
nameFolderListing = {dirListing.name};

% 2. List folders and subfolders
find_dots = ismember(nameFolderListing, {'.', '..'});
nameFolderListing(find_dots) = [];
nameFolderListing = nameFolderListing(:);

nameListing_full_1 = cell(size(nameFolderListing, 1), 1);
nameListing_full_2 = cell(size(nameFolderListing, 1), 1);

for i = 1:size(nameFolderListing, 1)
    
    tempFolder = dir(fullfile(foldername, nameFolderListing{i}));
    nameListing = {tempFolder.name};
    
    % 2. Find and exclude folders within the folder
    find_Dirs = [tempFolder.isdir];
    nameListing(find_Dirs) = [];
    
    % 3. Keep only the '.txt' files
    find_txt = strfind(nameListing, '.txt');
    nameListing(cellfun('isempty', find_txt)) = [];
    
    % 4. Remove all the '_ch2.txt' files
    find_hdr = strfind(nameListing, '_ch2.txt');
    nameListing(~cellfun('isempty', find_hdr)) = [];
    
    % 5. Remove all the '_header.txt' files
    find_hdr = strfind(nameListing, '_header.txt');
    nameListing(~cellfun('isempty', find_hdr)) = [];
    
    % 6. Remove all the '_header.txt' files
    find_hdr = strfind(nameListing, '_value.txt');
    nameListing(~cellfun('isempty', find_hdr)) = [];
    
    if ~isempty(nameListing(:))
        
        nameListing_full_1{i} = nameListing(:);
        nameListing_full_2{i} = fullfile(foldername, nameFolderListing{i});
        
    end
    
end

nameListing_full_1 = nameListing_full_1(~cellfun(@isempty, nameListing_full_1));
nameListing_full_2 = nameListing_full_2(~cellfun(@isempty, nameListing_full_2));

nameListing_full = cat(2, nameListing_full_1, nameListing_full_2);

if ne(size(nameListing_full, 1), 0)
    
    % wavelengths = [wavelengths flag]
    wavelengths = cell(size(nameListing_full, 1), 1);
    filenames = cell(size(nameListing_full, 1), 1);
    find_txt = cell(size(nameListing_full, 1), 1);
    
    for i = 1:size(nameListing_full, 1)
        
        find_txt0 = strfind(nameListing_full{i, 1}, '.txt');
        find_txt{i} = find_txt0{1};
        
        str = cell2mat(nameListing_full{i, 1});
        filenames{i, 1} = fullfile(foldername, nameFolderListing{i}, str);
        
        wavelengths_filename = strcat(str(1:(find_txt0{1} - 1)), '_header_value.txt');
        wavelengths_filename = load(fullfile(foldername, nameFolderListing{i}, wavelengths_filename));
        
        set(handles.edit2, 'string', num2str(round((wavelengths_filename(2)) * 10^12)));
        set(handles.edit1, 'string', num2str(round(wavelengths_filename(3))));
        
        wavelengths{i}(:, 1) = round(wavelengths_filename(13:end))';
        wavelengths{i}(:, 2) = i;
        
    end
    
    wavelengths = cell2mat(wavelengths);
    
    prefixes = cell(size(find_txt, 1), 1);
    
    for i = 1:size(find_txt, 1)
        
        str = cell2mat(nameListing_full{i, 1});
        prefixes{i, 1} = str(1:(find_txt{i} - 1));
        
    end
    
    suffix = strcat('.mat');
    
    output.wavelengths = wavelengths;
    output.suffix = suffix;
    output.filenames = filenames;
    output.prefixes = prefixes;
    
else
    
    output = [];
    
end