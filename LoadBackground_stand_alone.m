% close all
% clear all


[Files,dirpath]=uigetfile('M:\Tissue Engineering Project 2015\Data\*','Select data file(s)', 'MultiSelect','off' ); 
filepath = [dirpath Files];
    fid = fopen(filepath,'r');
    if fid < 0
        error('Cannot open background file "%s"!',filepath);
    else
        head = fread(fid,[1 3],'int32');
        Background = fread(fid,[1 head(2)],'double');
        fclose(fid);
    end

    plot(Background);
