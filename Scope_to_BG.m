%Code to convert scope data to GUI bakground data
% you will need LoadBinary function to run this.
%load scope data
% close all
% clear all 

[Files,dirpath]=uigetfile('*','Select result file(s)' ); 
[pathstr,name,ext] = fileparts(Files);
scope= loadBinary([dirpath Files],'double',0);

scope_mean = mean(scope);
plot(scope_mean);

% datasize = size(scope);
% header = [datasize datasize];
% data = scope;

% cd(dirpath)
% fileID = fopen([name '_BG_convert'],'w');
% fwrite(fileID,header,'int32');
% fwrite(fileID,data,'double');
% fclose(fileID);


