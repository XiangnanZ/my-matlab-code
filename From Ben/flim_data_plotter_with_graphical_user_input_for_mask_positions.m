%% Ben Sherlock
%% 07/03/2016 

%% SCRIPT NEEDS FUNCTION "circ_mask.m" TO RUN

close all
clear all
%% Load data stored in the map file
f=dir('*FLIM MAP.mat*');
load(f.name);
filename=f.name;
filename=filename(1:end-8)
%% Set pixels that have a negative intensity to 0 and normalize intensity 
%% so that it can be used with intensity weighted lifetime calculation
INT1(INT1<0)=0;
INT1_norm = INT1/max(INT1(:));
INT2(INT2<0)=0;
INT2_norm = INT2/max(INT2(:));
INT3(INT3<0)=0;
INT3_norm = INT3/max(INT3(:));

%% use mask to get rid of points with SNR<25
M1 = zeros(size(INT1));
M2 = zeros(size(INT2));
M3 = zeros(size(INT3));
M4 = zeros(size(INT4));
M1(SNR1>25) = 1;
M2(SNR2>25) = 1;
M3(SNR3>25) = 1;
M4(SNR4>25) = 1;



INT1 = INT1.*M1;
INT2 = INT2.*M2;
INT3 = INT3.*M3;
INT4 = INT4.*M4;
LT1 = LT1.*M1;
LT2 = LT2.*M2;
LT3 = LT3.*M3;
LT4 = LT4.*M4;

%% Set up plotting axis
ystep = 0.1;
xstep = 0.1;
x_length = size(LT1,1);
y_length = size(LT1,2);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);

caxmax=[LT1 LT2 LT3];
caxmax=max(caxmax(:)+1);
caxmax=6;
%% Set up plotting axis
ystep = 0.1;
xstep = 0.1;
x_length = size(LT1,1);
y_length = size(LT1,2);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);


%% Set up mask used to select region within which all pixel values will be averaged

s1=size(LT1',1);
s2=size(LT1',2);

%% In this case there are four samples in each image so we need four masks
figure(1)
img=LT1';
img(isnan(img))=0;


imagesc(x,y,img);
colorbar
caxis([0 caxmax]);
axis([0 max(x) 0 max(y)])

%%-----THIS IS THE PART WHERE INPUTS FROM THE MOUSE CLICKS ON THE FIGURE
%%-----ARE USED AS INPUTS FOR THE POSITIONS OF THE MASKS AND WHITE CIRCLES

[xshift yshift]=ginput(4);
%% The x and y shifts are used to control the centre positions of the circular masks
%% Enter in percentage values for where you want the ROIs to be

xshift1=xshift(1)/x(end)*100;
yshift1=yshift(1)/y(end)*100;

xshift2=xshift(2)/x(end)*100;
yshift2=yshift(2)/y(end)*100;

xshift3=xshift(3)/x(end)*100;
yshift3=yshift(3)/y(end)*100;

xshift4=xshift(4)/x(end)*100;
yshift4=yshift(4)/y(end)*100;


% Rads represents the radius in pixels of the individual circular masks
rads=25;

m1=circ_mask(s1,s2,(s1.*yshift1*0.01),(s2.*xshift1*0.01),rads);
m2=circ_mask(s1,s2,(s1.*yshift2*0.01),(s2.*xshift2*0.01),rads);
m3=circ_mask(s1,s2,(s1.*yshift3*0.01),(s2.*xshift3*0.01),rads);
m4=circ_mask(s1,s2,(s1.*yshift4*0.01),(s2.*xshift4*0.01),rads);

mask=m1+m2+m3+m4;
%figure(89)
%imagesc(mask)
%%
figh=figure(1);
set(figh, 'Position', [100, 100, 1400, 800]);
%[left bottom width height]
subplot(2,1,1)
colormap jet

%% Set variable **img** equal to the image of interest eg lifetime map from ch1 
img=LT1';
img(isnan(img))=0;


imagesc(x,y,img);
colorbar
caxis([0 caxmax]);
axis([0 max(x) 0 max(y)])
title('Channel 1 Lifetime')
set(gca,'Ydir','normal')
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Don't include any zero pixels in the averages
img1=img(m1);
lt_avg1=mean(img1(img1~=0));
lt_std1=std(img1(img1~=0));

img2=img(m2);
lt_avg2=mean(img2(img2~=0));
lt_std2=std(img2(img2~=0));

img3=img(m3);
lt_avg3=mean(img3(img3~=0));
lt_std3=std(img3(img3~=0));

img4=img(m4);
lt_avg4=mean(img4(img4~=0));
lt_std4=std(img4(img4~=0));


%%%%%--------------DRAW CIRCULAR ROIs ON FLIM MAP----------------%%%%%%

col1={'w'};
col2={'w'};
r=rads*0.2;
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
hold off

str1 = strcat('LT avg = ',num2str(round(lt_avg1,2)),' +/-',num2str(round(lt_std1,2)),' ns');
dim1 = [0.15 0.62 0.6 0.3];
annotation('textbox',dim1,'String',str1,'FitBoxToText','on','BackgroundColor','white');
str2 = strcat('LT avg = ',num2str(round(lt_avg2,2)),' +/-',num2str(round(lt_std2,2)),' ns');
dim2= [0.31 0.62 0.6 0.3];
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','BackgroundColor','white');
str3 = strcat('LT avg = ',num2str(round(lt_avg3,2)),' +/-',num2str(round(lt_std3,2)),' ns');
dim3= [0.51 0.62 0.6 0.3];
annotation('textbox',dim3,'String',str3,'FitBoxToText','on','BackgroundColor','white');
str4 = strcat('LT avg = ',num2str(round(lt_avg4,2)),' +/-',num2str(round(lt_std4,2)),' ns');
dim4= [0.7 0.62 0.6 0.3];
annotation('textbox',dim4,'String',str4,'FitBoxToText','on','BackgroundColor','white');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ch1_LT=[round(lt_avg1,2) round(lt_avg2,2) round(lt_avg3,2) round(lt_avg4,2)];
csvwrite(strcat(filename,'_CH1_LT.dat'),Ch1_LT)

Ch1_LT_STDEV=[round(lt_std1,2) round(lt_std2,2)  round(lt_std3,2) round(lt_std4,2)];
csvwrite(strcat(filename,'_CH1_LT_STDEV.dat'),Ch1_LT_STDEV);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,1,2)

img=LT2';
img(isnan(img))=0;
imagesc(x,y',img);
colorbar
caxis([0 caxmax+1]);
axis([0 max(x) 0 max(y)])

hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Don't include any zero pixels in the averages
img1=img(m1);
lt_avg1=mean(img1(img1~=0));
lt_std1=std(img1(img1~=0));

img2=img(m2);
lt_avg2=mean(img2(img2~=0));
lt_std2=std(img2(img2~=0));

img3=img(m3);
lt_avg3=mean(img3(img3~=0));
lt_std3=std(img3(img3~=0));

img4=img(m4);
lt_avg4=mean(img4(img4~=0));
lt_std4=std(img4(img4~=0));

%%%%%--------------DRAW CIRCULAR ROIs ON FLIM----------------%%%%%%

col1={'w'};
col2={'w'};
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
hold off

str1 = strcat('LT avg = ',num2str(round(lt_avg1,2)),' +/-',num2str(round(lt_std1,2)),' ns');
dim1 = [0.15 0.15 0.6 0.3];
annotation('textbox',dim1,'String',str1,'FitBoxToText','on','BackgroundColor','white');
str2 = strcat('LT avg = ',num2str(round(lt_avg2,2)),' +/-',num2str(round(lt_std2,2)),' ns');
dim2= [0.31 0.15 0.6 0.3];
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','BackgroundColor','white');
str3 = strcat('LT avg = ',num2str(round(lt_avg3,2)),' +/-',num2str(round(lt_std3,2)),' ns');
dim3= [0.51 0.15 0.6 0.3];
annotation('textbox',dim3,'String',str3,'FitBoxToText','on','BackgroundColor','white');
str4 = strcat('LT avg = ',num2str(round(lt_avg4,2)),' +/-',num2str(round(lt_std4,2)),' ns');
dim4= [0.7 0.15 0.6 0.3];
annotation('textbox',dim4,'String',str4,'FitBoxToText','on','BackgroundColor','white');

axis([0 max(x) 0 max(y)])
title('Channel 2 Lifetime')
xlabel('mm')
ylabel('mm')
set(gca,'Ydir','normal')
hold off

Ch2_LT=[round(lt_avg1,2) round(lt_avg2,2) round(lt_avg3,2) round(lt_avg4,2)];
csvwrite(strcat(filename,'_CH2_LT.dat'),Ch2_LT);

Ch2_LT_STDEV=[round(lt_std1,2) round(lt_std2,2)  round(lt_std3,2) round(lt_std4,2)];
csvwrite(strcat(filename,'_CH2_LT_STDEV.dat'),Ch2_LT_STDEV);

savefig(strcat(filename,'_Ch1 LT & Ch2 LT'));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figh=figure(2);
set(figh, 'Position', [100, 100, 1400, 800]);
%[left bottom width height]
subplot(2,1,1)
colormap jet

%% Set variable **img** equal to the image of interest eg lifetime map from ch1 
img=LT3';
img(isnan(img))=0;


imagesc(x,y,img);
colorbar
caxis([0 caxmax]);
axis([0 max(x) 0 max(y)])
title('Channel 3 Lifetime')
set(gca,'Ydir','normal')
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Don't include any zero pixels in the averages
img1=img(m1);
lt_avg1=mean(img1(img1~=0));
lt_std1=std(img1(img1~=0));

img2=img(m2);
lt_avg2=mean(img2(img2~=0));
lt_std2=std(img2(img2~=0));

img3=img(m3);
lt_avg3=mean(img3(img3~=0));
lt_std3=std(img3(img3~=0));

img4=img(m4);
lt_avg4=mean(img4(img4~=0));
lt_std4=std(img4(img4~=0));

%%%%%--------------DRAW CIRCULAR ROIs ON FLIM----------------%%%%%%

col1={'w'};
col2={'w'};
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
hold off

str1 = strcat('LT avg = ',num2str(round(lt_avg1,2)),' +/-',num2str(round(lt_std1,2)),' ns');
dim1 = [0.15 0.62 0.6 0.3];
annotation('textbox',dim1,'String',str1,'FitBoxToText','on','BackgroundColor','white');
str2 = strcat('LT avg = ',num2str(round(lt_avg2,2)),' +/-',num2str(round(lt_std2,2)),' ns');
dim2= [0.31 0.62 0.6 0.3];
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','BackgroundColor','white');
str3 = strcat('LT avg = ',num2str(round(lt_avg3,2)),' +/-',num2str(round(lt_std3,2)),' ns');
dim3= [0.51 0.62 0.6 0.3];
annotation('textbox',dim3,'String',str3,'FitBoxToText','on','BackgroundColor','white');
str4 = strcat('LT avg = ',num2str(round(lt_avg4,2)),' +/-',num2str(round(lt_std4,2)),' ns');
dim4= [0.7 0.62 0.6 0.3];
annotation('textbox',dim4,'String',str4,'FitBoxToText','on','BackgroundColor','white');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
Ch3_LT=[round(lt_avg1,2) round(lt_avg2,2) round(lt_avg3,2) round(lt_avg4,2)];
csvwrite(strcat(filename,'_CH3_LT.dat'),Ch3_LT)

Ch3_LT_STDEV=[round(lt_std1,2) round(lt_std2,2)  round(lt_std3,2) round(lt_std4,2)];
csvwrite(strcat(filename,'_CH3_LT_STDEV.dat'),Ch3_LT_STDEV);

%%------Use intensity weighted lifetime to aid visualisation----%%%%%%%%
subplot(2,1,2)
colormap jet

%% Set variable **img** equal to the image of interest eg lifetime map from ch1 
img=LT4';
img(isnan(img))=0;


imagesc(x,y,img);
colorbar
caxis([0 caxmax]);
axis([0 max(x) 0 max(y)])
title('Channel 4 Lifetime')
set(gca,'Ydir','normal')
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Don't include any zero pixels in the averages
img1=img(m1);
lt_avg1=mean(img1(img1~=0));
lt_std1=std(img1(img1~=0));

img2=img(m2);
lt_avg2=mean(img2(img2~=0));
lt_std2=std(img2(img2~=0));

img3=img(m3);
lt_avg3=mean(img3(img3~=0));
lt_std3=std(img3(img3~=0));

img4=img(m4);
lt_avg4=mean(img4(img4~=0));
lt_std4=std(img4(img4~=0));

%%%%%--------------DRAW CIRCULAR ROIs ON FLIM----------------%%%%%%

col1={'w'};
col2={'w'};
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift1*0.001-(0.5*r) s1.*yshift1*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift2*0.001-(0.5*r) s1.*yshift2*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift3*0.001-(0.5*r) s1.*yshift3*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');

rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
rectangle('Position',[s2.*xshift4*0.001-(0.5*r) s1.*yshift4*0.001-(0.5*r) r r],'Curvature',[1 1],'EdgeColor',char(col1),'LineWidth',3,'LineStyle','-');
hold off

str1 = strcat('LT avg = ',num2str(round(lt_avg1,2)),' +/-',num2str(round(lt_std1,2)),' ns');
dim1 = [0.15 0.15 0.6 0.3];
annotation('textbox',dim1,'String',str1,'FitBoxToText','on','BackgroundColor','white');
str2 = strcat('LT avg = ',num2str(round(lt_avg2,2)),' +/-',num2str(round(lt_std2,2)),' ns');
dim2= [0.31 0.15 0.6 0.3];
annotation('textbox',dim2,'String',str2,'FitBoxToText','on','BackgroundColor','white');
str3 = strcat('LT avg = ',num2str(round(lt_avg3,2)),' +/-',num2str(round(lt_std3,2)),' ns');
dim3= [0.51 0.15 0.6 0.3];
annotation('textbox',dim3,'String',str3,'FitBoxToText','on','BackgroundColor','white');
str4 = strcat('LT avg = ',num2str(round(lt_avg4,2)),' +/-',num2str(round(lt_std4,2)),' ns');
dim4= [0.7 0.15 0.6 0.3];
annotation('textbox',dim4,'String',str4,'FitBoxToText','on','BackgroundColor','white');

savefig(strcat(filename,'_Ch3 LT & Ch4 LT'));
%%

Ch4_LT=[round(lt_avg1,2) round(lt_avg2,2) round(lt_avg3,2) round(lt_avg4,2)];
csvwrite(strcat(filename,'_CH4_LT.dat'),Ch4_LT);

Ch4_LT_STDEV=[round(lt_std1,2) round(lt_std2,2)  round(lt_std3,2) round(lt_std4,2)];
csvwrite(strcat(filename,'_CH4_LT_STDEV.dat'),Ch4_LT_STDEV);