function[mask]=circ_mask(s1,s2,c1,c2,rads)

% Function makes a circular mask to be used with a S1xS2 array
% Mask is centred at c1,c2 and has a radius of rads


[xx,yy] = ndgrid((1:s1)-c1,(1:s2)-c2);
mask = (xx.^2 + yy.^2)<rads^2;