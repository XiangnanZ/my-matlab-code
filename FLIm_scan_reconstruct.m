clear all;
close all;

filefolder = 'C:\Users\XZ\Desktop\Data\20160617 Depleted Cartolage XSection';
[Files,dirpath]=uigetfile('*.mat','Select data file(s)',filefolder, 'MultiSelect','on' ); 
C = strsplit(dirpath,'\')
sampleID = C{end-2};
% load sample frame
load([dirpath Files{1}]);
% define parameters
n_avg = 64;
DAQtime = 47;
basegain = 1800;
n_pix = size(lifet_avg{1},1);
n_lines = length(Files);
s = 0.74; % threshold for saturation

Time = (0:n_pix-1)*DAQtime/1000; % acq time points for a frame

%% initialize
R1 = zeros(n_pix,n_lines);
R2 = zeros(n_pix,n_lines);
R3 = zeros(n_pix,n_lines);
R4 = zeros(n_pix,n_lines);

LT1 = zeros(n_pix,n_lines);
LT2 = zeros(n_pix,n_lines);
LT3 = zeros(n_pix,n_lines);
LT4 = zeros(n_pix,n_lines);

INT1 = zeros(n_pix,n_lines);
INT2 = zeros(n_pix,n_lines);
INT3 = zeros(n_pix,n_lines);
INT4 = zeros(n_pix,n_lines);

SNR1 = zeros(n_pix,n_lines);
SNR2 = zeros(n_pix,n_lines);
SNR3 = zeros(n_pix,n_lines);
SNR4 = zeros(n_pix,n_lines);

% all zero matrix for SNR mask
M1 = zeros(n_pix,n_lines);
M2 = zeros(n_pix,n_lines);
M3 = zeros(n_pix,n_lines);
M4 = zeros(n_pix,n_lines);
% creat SNR mask
% M_SNR1 = zeros(n_pix,n_lines);
% M_SNR2 = zeros(n_pix,n_lines);
% M_SNR3 = zeros(n_pix,n_lines);
% M_SNR4 = zeros(n_pix,n_lines);

%% load each frame 
for i = 1:n_lines
    i
    filename = [dirpath Files{i}];
    load(filename);
    raw1 = out{1}{4};
    raw2 = out{2}{4};
    raw3 = out{3}{4};
    raw4 = out{4}{4};
    
%     R1(:,i) = lifet_avg{1};
%     R2(:,i) = lifet_avg{2};
%     R3(:,i) = lifet_avg{3};
%     R4(:,i) = lifet_avg{4};
%     
    % mask for saturation
    mask1 = prod(~double(raw1>=s)); 
    mask2 = prod(~double(raw2>=s)); 
    mask3 = prod(~double(raw3>=s)); 
    mask4 = prod(~double(raw4>=s)); 
    
    % time stamps
    T = timeStamp;
    % interpolate values and time
    INT1(:,i) = interp1(T,spec_int{1},Time);
    INT2(:,i) = interp1(T,spec_int{2},Time);
    INT3(:,i) = interp1(T,spec_int{3},Time);
    INT4(:,i) = interp1(T,spec_int{4},Time);
    
    LT1(:,i) = interp1(T,lifet_avg{1},Time);
    LT2(:,i) = interp1(T,lifet_avg{2},Time);
    LT3(:,i) = interp1(T,lifet_avg{3},Time);
    LT4(:,i) = interp1(T,lifet_avg{4},Time);
      
    SNR1(:,i) = interp1(T,SNR{1},Time);
    SNR2(:,i) = interp1(T,SNR{2},Time);
    SNR3(:,i) = interp1(T,SNR{3},Time);
    SNR4(:,i) = interp1(T,SNR{4},Time);
 %non interplation    
%     INT1(:,i) = spec_int{1};
%     INT2(:,i) = spec_int{2};
%     INT3(:,i) = spec_int{3};
%     INT4(:,i) = spec_int{4};
%     
%     LT1(:,i) = lifet_avg{1};
%     LT2(:,i) = lifet_avg{2};
%     LT3(:,i) = lifet_avg{3};
%     LT4(:,i) = lifet_avg{4};
%       
%     SNR1(:,i) = SNR{1};
%     SNR2(:,i) = SNR{2};
%     SNR3(:,i) = SNR{3};
%     SNR4(:,i) = SNR{4};
    
    % creat mask from saturation
    M1(:,i) = im2bw(interp1(T,mask1,Time));
    M2(:,i) = im2bw(interp1(T,mask2,Time));
    M3(:,i) = im2bw(interp1(T,mask3,Time));
    M4(:,i) = im2bw(interp1(T,mask4,Time));
    
    
    
    
end
   %SNR mask
   M1(SNR1<25) = 0;
   M2(SNR2<25) = 0;
   M3(SNR3<25) = 0;
   M4(SNR4<25) = 0;
   %total mask
%    M1 = M1.*M_SNR1;
%    M2 = M2.*M_SNR2;
%    M3 = M3.*M_SNR3;
%    M4 = M4.*M_SNR4;
   

   LT1=LT1.*M1;
   LT2=LT2.*M2;
   LT3=LT3.*M3;
   LT4=LT4.*M4;
   INT1=INT1.*M1;
   INT2=INT2.*M2;
   INT3=INT3.*M3;
   INT4=INT4.*M4;
   
   cd(dirpath);
   save(strcat(sampleID,'.mat'),'alpha_out','basegain','BG_scaled','DAQtime','gain_list',...
       'INT1','INT2','INT3','INT4','LT1','LT2','LT3','LT4','M1','M2','M3','M4','s','SNR1','SNR2',...
       'SNR3','SNR4','sampleID');
%    save('map.mat');
   figure
   imagesc(LT1, [5.0 6.5]);
   colorbar;
   figure
   imagesc(INT1);
   colorbar;
 
   