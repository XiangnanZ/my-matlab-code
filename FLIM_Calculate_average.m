close all
clear all
%% load data
%  load('M:\Tissue Engineering Project 2015\Data\20151211 Collagen gel day 0\FLIM\2015_12_11_gel1 6mgperml\Result\map.mat')
[Files,dirpath]=uigetfile('*.mat','Select result file(s)' ); 
dirpath
load([dirpath Files]);

ystep = 0.1;
xstep = 0.1;
x_length = size(LT1,1);
y_length = size(LT1,2);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);
LT_range=[3 7];

figure
imagesc(LT1);

figure
imagsc(LT1);

LT1(INT1<1) = NaN;
LT1(LT1<2) = NaN;

CH1_LT_avg = nanmean(nanmean(LT1));





imagesc(x,y,LT1',LT_range)