function spec_new=spec_align_t(spec,lambda)
%theoretical

fib_length=1.741;

f=load('all_variables.mat');

%
p1=polyfit(f.lambda,f.Nm,10);
Nm=polyval(p1,lambda);

% figure;hold on;
% plot(f.lambda,f.Nm);
% plot(f.lambda,poly_fitted);

Nmp=polyval(p1(1:end-1).*(10:-1:1),lambda);

Ng=Nm./(1+lambda./Nm.*Nmp);

t_new=fib_length*Ng/f.c;
delta_t_new=abs(t_new-t_new(1));
delta_t_new=delta_t_new*10^12;
x1=lambda; 
y1=delta_t_new; 
p1=polyfit(x1,y1,2);

poly_fitted=polyval(p1,x1);
figure;plot(x1,poly_fitted);
%

edge_ind=round(poly_fitted/50);
figure;plot(-edge_ind+edge_ind(1),'o');

shifs=-edge_ind+edge_ind(1);
spec_new=zeros(size(spec));

for i=1:size(spec,2)
    
    spec_new(:,i)=circshift(spec(:,i),-shifs(i));
    
end