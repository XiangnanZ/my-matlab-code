function [out, alpha_ind_out]=spec_decon(spec,laser,K,alpha,shif, alpha_ind, handles, mode)

M=size(spec,1);
% K=8;

if ~isnumeric(alpha)||alpha<=0||alpha>=1
    alpha=alpha_up(M,K);
end

if isnumeric(alpha)||alpha>=0||alpha<=1
    alpha0 = round(1000 * alpha_up(M,K)) / 1000;
    
    if lt(alpha0, alpha)
        
        if eq(alpha_ind, 0)
            
            str = ['The alpha value you provided is not suitable for your truncation length, switching to automated estimation of alpha value. The new alpha is: ' num2str(alpha0)];
            waitfor(msgbox(str));
            alpha = alpha0;
            switch mode
                case 'TRFS'
                    
                    set(handles.edit7, 'string', num2str(alpha));
                    
                case 'Triplex'
                    
                    set(handles.edit10, 'string', num2str(alpha));
                    
            end            
            alpha_ind_out = 1;
            
        else
            
            alpha = alpha0;
            alpha_ind_out = alpha_ind;
            
        end
        
    else
        
        alpha_ind_out = alpha_ind;
        
    end
    
else
    
    alpha_ind_out = alpha_ind;
    
end
    
L=Laguerre(M,K,alpha);

laser_temp=circshift(laser,[shif,0]);
vv=filter(laser_temp,1,L);

%decreasing constraints
% diff_matrix=conv2(eye(size(spec,1)),[-1,1],'same')';
% A_mat=-diff_matrix*L;
% A_mat=conv2(eye(size(spec,1)),[1,-1],'valid')'*L;

%non-negative constraints
% B_mat=-eye(size(spec,1))*L;

%second order derivative non-negative constraints
% C_mat=conv2(eye(size(spec,1)),[-1,2,-1],'valid')'*L;

%third order derivative non-negative constraints
D_mat=conv2(eye(size(spec,1)),[1,-3,3,-1],'valid')'*L;

%higer order constraints
% E_mat=conv2(eye(size(spec,1)),[-1,4,-6,4,-1],'valid')'*L;
% F_mat=conv2(eye(size(spec,1)),[1,-5,10,-10,5,-1],'valid')'*L;
% G_mat=conv2(eye(size(spec,1)),[-1 6 -15 20 -15 6 -1],'valid')'*L;

%constraints matrix
% D=[A_mat;B_mat;C_mat;D_mat];
D=D_mat;%third order is enough

H=vv'*vv;
H_chol=chol(inv(H));
C=H_chol*D';
l1=H_chol*vv';

lam=zeros(size(D,1),size(spec,2));%Lagrangian parameters

parfor i=1:size(spec,2)
        
    d=l1*spec(:,i);
    lam(:,i)=lsqnonneg(C,d);
    
end;

cc=(vv'*vv)\(vv'*spec-D'*lam);
h=L*cc;

out={cc,h,vv*cc,spec,alpha, laser_temp};