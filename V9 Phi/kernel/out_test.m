function stats=out_test(out1,dt,bw,varargin)
% out_test(out1,dt,bw,'lbq',{'lags',min[20,T-1],'alpha',0.05,'dof',20},'chi2',...
% {'var',[],'alpha',0.05,'dof',[]})
% Currently only support 'chi2' and 'lbq' tests
% dt - time resolution (in ns)
% bw - detector bandwidth (in GHz)

spec=out1{4};
spec_fitted=out1{3};

res=spec-spec_fitted;

% Downsampling
dt=dt*1e-9;
bw=bw*1e9;
n_t=round(1/bw/dt);
% n_1=round(size(res,1)*0.3);% Use the last 70% of points to test
[~,n_1]=max(spec_fitted);
n_1=quantile(n_1,0.95)+size(res,1)*0.05;

if n_1>size(res,1)-n_t*20
    
    n_1=1;
    
end

res=res(n_1:n_t:end,:);
    
if nargin==3
    
    test_name={'lbq','chi2'};
    test_param={[],[]};
    
end

if nargin>3
    
    test_name=cell((nargin-3)/2,1);
    test_param=cell((nargin-3)/2,1);
    
    for i=1:(nargin-3)/2
        
        test_name{i}=varargin{i*2-1};
        test_param{i}=varargin{i*2};
        
    end
    
end

for i=1:length(test_name)    
    
    fh=str2func(strcat(test_name{i},'test'));
    [stats.(test_name{i}).h,stats.(test_name{i}).pvalue,...
        stats.(test_name{i}).stat,stats.(test_name{i}).cvalue]=fh(res,test_param{i});
    
end

end

function [h,pvalue,stat,cvalue]=lbqtest(res,param)
% param is the cell of parameters: lags, alpha, dof

T=size(res,1);

lags=min(20,T-1);
alpha=0.05;
dof=lags;

if length(param)/2>=1
    
    for i_param=1:length(param)/2
       
        eval([genvarname(param{i_param*2-1}),'=param{i_param*2};']);
        
    end
        
end

if lags>T-1
    
    lags=min(20,T-1);
    warndlg('Lags must not exceed the length of the data minus one; default value is used.');
    
end

if dof>lags
    
    dof=lags;
    warndlg('Degrees of freedom must not exceed corresponding lag; default value is used.');
    
end

% Autocorrelation
nFFT=2^(nextpow2(size(res,1))+1);
F=fft(bsxfun(@minus,res,mean(res,1)),nFFT);
F=F.*conj(F);
acf=ifft(F);
acf=acf(1:(lags+1),:);%Retain non-negative lags
acf=bsxfun(@rdivide,acf,acf(1,:));%Normalize
acf=real(acf(2:end,:));%Strip off ACF at lag 0

% Compute Q-statistics to the largest lag; keep only those requested:
idx=(T-(1:lags))';
stat=T*(T+2)*cumsum(bsxfun(@rdivide,(acf.^2),idx));
stat=stat(lags,:);

% Compute p-values:
pvalue=1-chi2cdf(stat,dof);

% Compute critical values
cvalue=chi2inv(1-alpha,dof);

% Perform the test:
h=(alpha>=pvalue);

end

function [h,pvalue,stat,cvalue]=chi2test(res,param)

T=size(res,1);
alpha=0.05;

if length(param)/2>=1
    
    for i_param=1:length(param)/2
       
        eval([genvarname(param{i_param*2-1}),'=param{i_param*2};']);
        
    end
        
end

if ~exist('vars','var')||isempty(vars)
    
    vars=var(reshape(res(round(T*0.5):end,:),[],1));% Estimate variance from last 50% of data points
    
end

if ~exist('dof','var')||isempty(dof)
    
    dof=T;
    
end

stat=sum(res.^2)/vars/dof;

pvalue=1-chi2cdf(stat*dof,dof);

cvalue=chi2inv(1-alpha,dof)/dof;

h=(alpha>=pvalue);

end

