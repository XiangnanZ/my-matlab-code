function [lifet,spec_int,cc,alpha,test_out,out1, alpha_ind_out, G, S]=decon_LG(spec,laser,dt,bw,nxy,n,alpha,mode, alpha_ind, handles)
% spec=spec_all{1};
% laser=laser1;

lifet=[];

if isempty(laser)
    
    warndlg('Please load laser profile before proceeding!');
    return;
    
end

% Correct for dispersion in TRFS
switch mode
    case 'TRFS'
        
        spec_new=spec_align(spec(:, 2:end),0.1, spec);
        if isempty(spec_new)
            warndlg('Possible multi-peaks waveform, cannot process!');
            return;
        end
        
    case 'Triplex'
        
        spec_new=spec;
        
end
% Processing

shif=spec_laser_align(spec_new(:, 2:end),laser,8,40,40,[],[]);
[out1, alpha_ind_out]=spec_decon(spec_new,laser,n,alpha,shif, alpha_ind, handles, mode);

laser_shift=circshift(laser,[shif,0]);
[G,S] = estimatephasor(spec_new,laser_shift,0,1,dt*1e-9);

% Analysis
test_out=out_test(out1,dt,bw);

% [~,idx_max]=max(test_out.chi2.stat);
% [~,idx_min]=min(abs(test_out.chi2.stat-1));
% close all;figure('Position',[100,100,900,300]);
% subplot(1,2,1);hold on;
% plot(out1{4}(:,idx_max));plot(out1{3}(:,idx_max),'r');
% title('Worst Fit');
% subplot(1,2,2);hold on;
% plot(out1{4}(:,idx_min));plot(out1{3}(:,idx_min),'r');
% title('Best Fit');

% Post-processing
[lifet,spec_int]=h_lifet(out1{2},dt,'average');
cc=out1{1};

lifet=reshape(lifet,nxy(1),nxy(2));
spec_int=reshape(spec_int,nxy(1),nxy(2));
cc=cellfun(@(x)reshape(x,nxy(1),nxy(2)),mat2cell(cc,ones(1,size(cc,1)),size(cc,2)),'uniformoutput',false);
%****---- Normalized Laguerre Coefficients ----****%
for i = 1:size(cc,1)
    cc{i} = cc{i}./spec_int;
    out1{1}(i,:) = out1{1}(i,:)./spec_int;
end
% - Michael 03/30/2016 - %
alpha=out1{5};