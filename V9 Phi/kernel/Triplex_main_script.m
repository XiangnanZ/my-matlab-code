function [lifet_avg,spec_int,cc,alpha, testout, out, SNR_out, flagvalue, alpha_ind_out, Phasor] = Triplex_main_script(spec_raw, laser, dt, bw, n_tbin, n, alpha, dt_interp, handles, alpha_ind)

% Triplex CLSD-LG deconvolution
alpha_ind_out = alpha_ind;
% Instrumental Parameters
frame_length = size(spec_raw, 2);
nxy = [1 size(spec_raw, 1)];    %nxy(1): # of line scan points
n_tbin0 = n_tbin;
                %nxy(2): # of line scans

try
    
    laser = {laser{1} laser{2} laser{3} laser{4}};
    
catch err
    
    waitfor(msgbox('Your iIRF file was not in the correct format'));
    flagvalue = 13;
    lifet_avg = [];
    spec_int = [];
    cc = [];
    alpha = [];
    testout = [];
    out = [];
    alpha_ind_out = [];
    SNR_out = [];
    Phasor = [];
    return
    
end

% Triplex Loading Parameters

[spec, flagvalue] = Triplex_data_loader(spec_raw', frame_length, n_tbin, handles);

if eq(flagvalue, 17)
    
    lifet_avg = [];
    spec_int = [];
    cc = [];
    alpha = [];
    testout = [];
    out = [];
    alpha_ind_out = [];
    SNR_out = [];
    Phasor = [];
    return
        
end

%============================================================================================================

% Fitting parameters (change the following parameters for your needs)

% Start processing...
% Output is the average lifetime over pixels

if ~isempty(spec{1})
    
    fIRF_noise = spec{1};
    v = round(n_tbin0 * 0.1);
    aa = fIRF_noise(v, :) - [mean(spec_raw(:, (end - 100):end), 2)]';
    bb = std(spec_raw(:, (end - 100):end), 0, 2)';
    SNR1 = 20*log10(aa./bb);
    SNR1(aa < bb) = 0;
    
    clear aa bb fIRF_noise
    
    n_tbin = size(spec{1}, 1);
    n_tbin_laser = size(laser{1}, 1);
    
    if ne(dt, dt_interp)
        
        spec{1} = interp1(0:n_tbin-1,spec{1},0:dt_interp/dt:n_tbin-1);
        laser{1} = interp1(0:n_tbin_laser-1,laser{1},0:dt_interp/dt:n_tbin_laser-1)';
        
        dt1 = dt_interp;
        
    else
        
        dt1 = dt;
        
    end
    
    [lifet1_avg, spec_int1, cc1, alpha1, testout1, out1, alpha_ind_out, G1, S1] = decon_LG(spec{1}, laser{1}, dt1, bw, nxy, n, alpha, 'Triplex', alpha_ind, handles);
    alpha = alpha1;
    
else
    
    lifet1_avg = [];
    spec_int1 = [];
    cc1 = [];
    alpha1 = [];
    testout1 = [];
    out1 = [];
    SNR1 = [];
    G1 = [];
    S1 = [];
    
end

if ~isempty(spec{2})
    
    fIRF_noise = spec{2};
    v = round(n_tbin0 * 0.1);
    aa = fIRF_noise(v, :) - [mean(spec_raw(:, (end - 100):end), 2)]';
    bb = std(spec_raw(:, (end - 100):end), 0, 2)';
    SNR2 = 20*log10(aa./bb);
    SNR2(aa < bb) = 0;
    clear aa bb fIRF_noise
    
    n_tbin = size(spec{2}, 1);
    n_tbin_laser = size(laser{2}, 1);
    
    if ne(dt, dt_interp)
        
        spec{2} = interp1(0:n_tbin-1,spec{2},0:dt_interp/dt:n_tbin-1);
        laser{2} = interp1(0:n_tbin_laser-1,laser{2},0:dt_interp/dt:n_tbin_laser-1)';
        
        dt2 = dt_interp;
        
    else
        
        dt2 = dt;
        
    end
    
    [lifet2_avg, spec_int2, cc2, alpha2, testout2, out2, alpha_ind_out, G2, S2] = decon_LG(spec{2}, laser{2}, dt2, bw, nxy, n, alpha, 'Triplex', alpha_ind_out, handles);
     
else
    
    lifet2_avg = [];
    spec_int2 = [];
    cc2 = [];
    alpha2 = [];
    testout2 = [];
    out2 = [];
    SNR2 = [];
    G2 = [];
    S2 = [];
    
end
    
if ~isempty(spec{3})
    
    fIRF_noise = spec{3};
    v = round(n_tbin0 * 0.1);
    aa = fIRF_noise(v, :) - [mean(spec_raw(:, (end - 100):end), 2)]';
    bb = std(spec_raw(:, (end - 100):end), 0, 2)';
    SNR3 = 20*log10(aa./bb);
    SNR3(aa < bb) = 0;
    clear aa bb fIRF_noise
    
    n_tbin = size(spec{3}, 1);
    n_tbin_laser = size(laser{3}, 1);
    
    if ne(dt, dt_interp)
        
        spec{3} = interp1(0:n_tbin-1,spec{3},0:dt_interp/dt:n_tbin-1);
        laser{3} = interp1(0:n_tbin_laser-1,laser{3},0:dt_interp/dt:n_tbin_laser-1)';
        
        dt3 = dt_interp;
        
    else
        
        dt3 = dt;
        
    end
    
    [lifet3_avg, spec_int3, cc3, alpha3, testout3, out3, alpha_ind_out, G3, S3] = decon_LG(spec{3}, laser{3}, dt3, bw, nxy, n, alpha, 'Triplex', alpha_ind_out, handles);
     
else
    
    lifet3_avg = [];
    spec_int3 = [];
    cc3 = [];
    alpha3 = [];
    testout3 = [];
    out3 = [];
    SNR3 = [];
    G3 = [];
    S3 = [];
    
end

if ~isempty(spec{4})
    
    fIRF_noise = spec{4};
    v = round(n_tbin0 * 0.1);
    aa = fIRF_noise(v, :) - [mean(spec_raw(:, (end - 100):end), 2)]';
    bb = std(spec_raw(:, (end - 100):end), 0, 2)';
    SNR4 = 20*log10(aa./bb);
    SNR4(aa < bb) = 0;
    clear aa bb fIRF_noise
    
    n_tbin = size(spec{4}, 1);
    n_tbin_laser = size(laser{4}, 1);
    
    if ne(dt, dt_interp)
        
        spec{4} = interp1(0:n_tbin-1,spec{4},0:dt_interp/dt:n_tbin-1);
        laser{4} = interp1(0:n_tbin_laser-1,laser{4},0:dt_interp/dt:n_tbin_laser-1)';
        
        dt4 = dt_interp;
        
    else
        
        dt4 = dt;
        
    end
    
    [lifet4_avg, spec_int4, cc4, alpha4, testout4, out4, alpha_ind_out, G4, S4] = decon_LG(spec{4}, laser{4}, dt4, bw, nxy, n, alpha, 'Triplex', alpha_ind_out, handles);
     
else
    
    lifet4_avg = [];
    spec_int4 = [];
    cc4 = [];
    alpha4 = [];
    testout4 = [];
    out4 = [];
    SNR4 = [];
    G4 = [];
    S4 = [];
    
end

lifet_avg = [{lifet1_avg'} {lifet2_avg'} {lifet3_avg'} {lifet4_avg'}];
spec_int = [{spec_int1'} {spec_int2'} {spec_int3'} {spec_int4'}];
cc = [{cc1} {cc2} {cc3} {cc4}];
alpha = [{alpha1} {alpha2} {alpha3} {alpha4}];
testout = [{testout1} {testout2} {testout3} {testout4}];
out = [{out1'} {out2'} {out3'} {out4'}];
SNR_out = [{SNR1'} {SNR2'} {SNR3'} {SNR4'}];
Phasor = [{[G1 S1]} {[G2 S2]} {[G3 S3]} {[G4 S4]}];
flagvalue = 16;