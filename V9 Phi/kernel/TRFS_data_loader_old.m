function out1=TRFS_data_loader(spec_file,lambda,n_tbin,dc_file)

spec_raw=importdata(spec_file);
spec_dc=0;

out1=[];
% out2=[];

if exist('dc_file','var')
    
    spec_dc=importdata(dc_file);
    
end

if isstruct(spec_raw)||isstruct(spec_dc)
    
    warndlg('Expect a matrix from input file!');
    return;
    
end
    
spec_all=spec_raw-spec_dc;

% lambda=lambda_range(1):lambda_range(2):lambda_range(3);

if size(spec_all,2)~=length(lambda)
    
    warndlg('The number of wavelengths is not consistent with the data file!');
    return;
    
end

% n_tbin=800;
peak_left=0.15*n_tbin;

h1=figure;plot(min(spec_all,[],2));
cmax_bin=input('Please enter the location of the peak: \n');

close(h1);

spec=-spec_all(cmax_bin-peak_left:cmax_bin+n_tbin-peak_left-1,:);

figure;plot(spec);

out1=spec;
% out2=lambda;

