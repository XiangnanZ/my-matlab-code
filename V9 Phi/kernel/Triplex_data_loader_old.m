function [out1, flagvalue] = Triplex_data_loader(spec_raw, frame_length, n_tbin, handles)

spec_dc=0;

out1={};

if or(isstruct(spec_raw), isstruct(spec_dc))
    
    warndlg('Expect a matrix from input file!');
    flagvalue = 17;
    return;
    
end

if ne(mod(size(spec_raw, 1), frame_length), 0)
    
    warndlg('Frame length is not consistent with the data file!');
    flagvalue = 17;
    return;
    
end

spec_all = reshape(spec_raw, frame_length, []);

peak_left=round(n_tbin*0.1);

c_max = getappdata(handles.figure1, 'TRIPLEXPeaks');

if isempty(c_max)
    
    BGPos = load('PeaksPos.dat');
    
    h1 = figure;
    set(h1, 'Position',  get(0,'Screensize'));
    spec_ttemp = min(spec_all, [], 2);
    plot(spec_ttemp);
    pos = get(gca,'position');
    pos(2) = pos(2) + 0.1;
    pos(4) = pos(4) - 0.05;
    set(gca,'position',pos);
    
    hcur = mycursors(gca,'r');
    hcur.off(1);
    hcur.add(BGPos(2,1));
    hcur.add(BGPos(2,2));
    hcur.add(BGPos(2,3));
    hcur.add(BGPos(2,4));
    
    pause(0.01);
    
    uicontrol('unit','Normalized','Position',[0.425 0.09 0.15 0.05],'String','Continue','Callback','uiresume(gcbf)');
    uiwait(h1);
    
%     flagvalue = 17;
%     waitfor(msgbox('The peak selection was wrong'));
%     close(h1);
%     return
    c_max = round(hcur.val()');
    BGPos(2, :) = [c_max 0];
    save('PeaksPos.dat', 'BGPos', '-ascii');
    
    close(h1);
    
    setappdata(handles.figure1, 'TRIPLEXPeaks', c_max);
    
end

if ne(c_max(1), 0)
    
    spec1=-spec_all(c_max(1)-peak_left:c_max(1)+n_tbin-peak_left-1,:);
    
else
    
    spec1 = [];
    
end

if ne(c_max(2), 0)
    
    spec2=-spec_all(c_max(2)-peak_left:c_max(2)+n_tbin-peak_left-1,:);
    
else
    
    spec2 = [];
    
end

if ne(c_max(3), 0)
    
    spec3=-spec_all(c_max(3)-peak_left:c_max(3)+n_tbin-peak_left-1,:);
    
else
    
    spec3 = [];
    
end

if ne(c_max(4), 0)
    
    spec4=-spec_all(c_max(4)-peak_left:c_max(4)+n_tbin-peak_left-1,:);
    
else
    
    spec4 = [];
    
end

out1={spec1,spec2,spec3,spec4};

flagvalue = 16;

