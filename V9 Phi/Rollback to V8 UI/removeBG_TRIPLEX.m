function [spec_out1, gainCor, BG] = removeBG_TRIPLEX(filename_BG, foldername_BG, spec, handles)

try
    
    BG = loadBinary_TRFS(fullfile(foldername_BG, filename_BG), 'double', 0)';
    
catch err
    
    try 
        BG = load(fullfile(foldername_BG, filename_BG))';
        
    catch err0
        
        try
            
            BG = loadBinary(fullfile(foldername_BG, filename_BG), 'int16', 0)';
            
        catch err1
            
            try
                
                BG = -LoadBackground(fullfile(foldername_BG, filename_BG))';
                
            catch err2
                
                BG = loadBinary(fullfile(foldername_BG, filename_BG), 'double', 0)';
                
            end            
            
        end
        
    end

end

ssr = mean(BG((end - 100):end, :));
BG = bsxfun(@minus, BG, ssr);
BG = bsxfun(@rdivide, BG, abs(min(BG)));
BG = mean(BG, 2);

BGremove_flag = getappdata(handles.figure1, 'BGrem_flag');

if eq(BGremove_flag, 0)
    
    BGPos = load('PeaksPos.dat');
    
    fig = figure;
    set(fig, 'Position', [755 76 1129 857]);
    subplot(2, 1, 1)
    plot(BG);    
    if ne(BGPos(3, 1), 0)
        
        hold on
        plot([BGPos(3, 1); BGPos(3, 1)], [0; min(BG(:))], 'k-');
        plot([(BGPos(3, 1) - (BGPos(3, 3) / 2)); (BGPos(3, 1) - (BGPos(3, 3) / 2))], [0; min(BG(:))], 'k--');
        plot([(BGPos(3, 1) + (BGPos(3, 3) / 2)); (BGPos(3, 1) + (BGPos(3, 3) / 2))], [0; min(BG(:))], 'k--');
        hold off
        
    end
    
     if ne(BGPos(3, 5), 0)
        
        hold on
        plot([BGPos(3, 5); BGPos(3, 5)], [0; min(BG(:))], 'k--');
        hold off
        
    end
    
    subplot(2, 1, 2)
    plot(spec);
     if ne(BGPos(3, 2), 0)
        
        hold on
        plot([BGPos(3, 2); BGPos(3, 2)], [0; min(spec(:))], 'k-');
        plot([(BGPos(3, 2) - (BGPos(3, 3) / 2)); (BGPos(3, 2) - (BGPos(3, 3) / 2))], [0; min(BG(:))], 'k--');
        plot([(BGPos(3, 2) + (BGPos(3, 3) / 2)); (BGPos(3, 2) + (BGPos(3, 3) / 2))], [0; min(BG(:))], 'k--');
        hold off
        
     end
    
      if ne(BGPos(3, 6), 0)
        
        hold on
        plot([BGPos(3, 6); BGPos(3, 6)], [0; min(spec(:))], 'k--');
        hold off
        
     end
    dlg_title = 'Please enter the points for your background and signal for subtraction';
    options.Resize='on';
    options.WindowStyle='normal';
    def1 = {num2str(BGPos(3, 1)) num2str(BGPos(3, 2)) num2str(BGPos(3, 3)) num2str(BGPos(3, 4)) num2str(BGPos(3, 5)) num2str(BGPos(3, 6))};
    c_max = inputdlg({'Point for Subtraction in BackGround (Black solid line in bacground plot)' 'Point for Subtraction in Signal (Black line in signal plot)' 'Total Number of Subtraction Points' 'Base Gain (leave zero if you want to skip it)' 'Alignment Point of BackGround (leave zero if already aligned)(Black dashed line in bacground plot)' 'Alignment Point of Signal (leave zero if already aligned)(Black dashed line in signal plot)'}, dlg_title, [1, length(dlg_title) + 30], def1, options);
    c_max = str2double(c_max)';
    close(fig)
    
else
    
    c_max = getappdata(handles.figure1, 'c_max');
    
end

if isempty(c_max)
    
    spec_out1 = [];
    gainCor = 0;
    return
    
end

if eq(BGremove_flag, 0)
    
    setappdata(handles.figure1, 'BGrem_flag', 1);
    setappdata(handles.figure1, 'c_max', c_max);
    
    BGPos(3, :) = c_max;
    save('PeaksPos.dat', 'BGPos', '-ascii');
    
end

c_max0 = [c_max(1); c_max(2)];
gain_base = c_max(4);
points_number = c_max(3);
c_max1 = [c_max(5); c_max(6)];

if and(and(eq(c_max0(1), 0), eq(c_max0(2), 0)), eq(gain_base, 0))
    
    spec_out1 = [];
    gainCor = 0;
    return
    
end

if and(ne(c_max0(1), 0), ne(c_max0(2), 0))
    
    if and(ne(c_max1(1), 0), ne(c_max1(2), 0))
        
        [~, BGpeak] = min(BG((min(c_max1) - 30):(c_max1(1) + 30), :));
        [~, run_xpeak] = min(spec((min(c_max1) - 30):(c_max1(2) + 30), :));
        
        shiftindex = run_xpeak - BGpeak;
        [m, n] = size(BG);
        b=mod(bsxfun(@plus, (0:(m - 1))', -shiftindex(:)'), m) + 1;
        b=bsxfun(@plus, b, ((0:(n - 1)) * m));
        BG = BG(b);
        
    end
    
    if eq(points_number, 1)
        
        BG_peak = max(BG(1:(c_max0(1) + 60), :));
        spec_peak = max(spec(1:(c_max0(2) + 60), :));
        scaleFactor = spec_peak./BG_peak;
        BG = repmat(BG, 1, size(spec, 2));
        BG = bsxfun(@times, BG, scaleFactor);
        run1 = spec - BG;
        
    else
        
        BG_peak = mean(BG(round(c_max0(1) - (points_number /  2)):round(c_max0(1) + (points_number /  2)), :));
        spec_peak = mean(spec(round(c_max0(2) - (points_number /  2)):round(c_max0(2) + (points_number /  2)), :));
        scaleFactor = spec_peak./BG_peak;
        if eq(size(BG, 2), 1)
            BG = repmat(BG, 1, size(spec, 2));
        end
        BG = bsxfun(@times, BG, scaleFactor);
        run1 = spec - BG;
        
    end
    
else
    
    run1 = spec;
    
end

if ne(gain_base, 0)
    
    gainCor = 1;

else
    
    gainCor = 0;
    
end
  
spec_out1 = run1;