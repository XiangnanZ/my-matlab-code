In order to roll back to the UI of V8, while keeping other changes in V9, please follow these steps:
1. Copy the "PeaksPos.dat" to the root folder;
2. Copy "removeBG.m" and "removeBG_TRIPLEX.m" to "kernels" folder
3. Copy "Triplex_data_loader.m" to "kernel" folder