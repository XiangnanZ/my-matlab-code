function varargout = TRFS_TRIPLEX_GUI(varargin)
%TRFS_TRIPLEX_GUI M-file for TRFS_TRIPLEX_GUI.fig
%      TRFS_TRIPLEX_GUI, by itself, creates a new TRFS_TRIPLEX_GUI or raises the existing
%      singleton*.
%
%      H = TRFS_TRIPLEX_GUI returns the handle to a new TRFS_TRIPLEX_GUI or the handle to
%      the existing singleton*.
%
%      TRFS_TRIPLEX_GUI('Property','Value',...) creates a new TRFS_TRIPLEX_GUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to TRFS_TRIPLEX_GUI_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      TRFS_TRIPLEX_GUI('CALLBACK') and TRFS_TRIPLEX_GUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in TRFS_TRIPLEX_GUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TRFS_TRIPLEX_GUI

% Last Modified by GUIDE v2.5 14-Mar-2016 17:07:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TRFS_TRIPLEX_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @TRFS_TRIPLEX_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before TRFS_TRIPLEX_GUI is made visible.
function TRFS_TRIPLEX_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

p = path;
path('./Kernels', p);

% Choose default command line output for TRFS_TRIPLEX_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% --------------------------------------------------------------------------------------------------------
% Center Window
%pixels
set(handles.figure1, 'Units', 'pixels');
%get your display size
screenSize = get(0, 'MonitorPositions');
screenSize = screenSize(1, :);
%calculate the center of the display
position = get(handles.figure1, 'Position');
position(1) = round((screenSize(3) - position(3)) / 2);
%center the window
set(handles.figure1, 'Position', position);

% --------------------------------------------------------------------------------------------------------
% Initialize Buttons
onoffButtons(handles, 0);

% --------------------------------------------------------------------------------------------------------
% Setup popupmenus
set(handles.popupmenu1, 'Value', 1);
set(handles.popupmenu2, 'Value', 1);
L_num = str2double(get(handles.edit6, 'String'));

str_popupmenu1 = cell((L_num + 1), 1);
str_popupmenu2 = cell((L_num + 1), 1);
str_popupmenu1{1} = 'Lifetime';
str_popupmenu2{1} = 'Int. Inensity';

for i = 2:(L_num + 1)
    
    str_popupmenu1{i} = ['Laguerre ', num2str(i - 1)];
    str_popupmenu2{i} = ['Laguerre ', num2str(i - 1)];
    
end

set(handles.popupmenu1, 'String', str_popupmenu1);
set(handles.popupmenu2, 'String', str_popupmenu2);

set(handles.popupmenu3, 'Value', 1);
set(handles.popupmenu4, 'Value', 1);
L_num = str2double(get(handles.edit13, 'String'));

str_popupmenu3 = cell((L_num + 1), 1);
str_popupmenu4 = cell((L_num + 1), 1);
str_popupmenu3{1} = 'Lifetime';
str_popupmenu4{1} = 'Int. Inensity';

for i = 2:(L_num + 1)
    
    str_popupmenu3{i} = ['Laguerre ', num2str(i - 1)];
    str_popupmenu4{i} = ['Laguerre ', num2str(i - 1)];
    
end

set(handles.popupmenu3, 'String', str_popupmenu1);
set(handles.popupmenu4, 'String', str_popupmenu2);

path(p)

% UIWAIT makes TRFS_TRIPLEX_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TRFS_TRIPLEX_GUI_OutputFcn(~, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% ResetGUI Button
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(~, ~, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

closeGUI = handles.figure1; %handles.figure1 is the GUI figure
guiPosition = get(handles.figure1, 'Position'); %get the position of the GUI
guiName = get(handles.figure1, 'Name'); %get the name of the GUI
close(closeGUI); %close the old GUI
eval(guiName) %call the GUI again
set(gcf,'Position',guiPosition); %set the position for the new GUI

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% View Multiple Results Button (open new GUI)
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(~, ~, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

closeGUI = handles.figure1; %handles.figure1 is the GUI figure
close(closeGUI); %close the old GUI
TRFS_TRIPLEX_Results_GUI;

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Assign Labels To Data
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(~, ~, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

p = path;
path('./Kernels', p);

set(handles.radiobutton11, 'enable', 'off');
set(handles.radiobutton12, 'enable', 'off');
mes_hist_reg(handles);

path(p)

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% TRFS Preprocess
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Load TRFS Data, Wavelengths and File Names
% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(~, ~, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

p = path;
path('./Kernels', p);

pb14_flag = get(handles.pushbutton14, 'enable');
rb12_flag = get(handles.radiobutton12, 'enable');
onoffButtons(handles, 1);

flagvalue = selectDataType(handles); % flagvalue 1 if fails 2 if succeeds

onoffButtons(handles, flagvalue);

if eq(flagvalue, 2)
    
    set(handles.pushbutton14, 'enable', 'on');
    
    if ~isempty(getappdata(handles.figure1, 'TRIPLEXrawDataNames'))
        
        set(handles.radiobutton12, 'enable', char(rb12_flag));
        set(handles.radiobutton11, 'enable', char(rb12_flag));
        
    elseif isempty(getappdata(handles.figure1, 'TRIPLEXrawDataNames'))
        
        set(handles.radiobutton12, 'enable', 'off');
        set(handles.radiobutton11, 'enable', 'off');
        set(handles.radiobutton12, 'Value', 1);
        
    end
    
elseif eq(flagvalue, 1)
    
    if ~isempty(getappdata(handles.figure1, 'TRIPLEXrawDataNames'))
        
        set(handles.pushbutton14, 'enable', char(pb14_flag));
        set(handles.radiobutton12, 'enable', char(rb12_flag));
        set(handles.radiobutton11, 'enable', char(rb12_flag));
        
    elseif isempty(getappdata(handles.figure1, 'TRIPLEXrawDataNames'))
        
        set(handles.pushbutton14, 'enable', 'off');
        set(handles.radiobutton12, 'enable', 'off');
        set(handles.radiobutton11, 'enable', 'off');
        set(handles.radiobutton12, 'Value', 1);
        
    end
    
end

path(p)

% 2. Load Wavelength Calibration File
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(~, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

p = path;
path('./Kernels', p);

onoffButtons(handles, 3);

flagvalue = loadwaveCal(handles); % flagvalue 3 if fails 4 if succeeds

onoffButtons(handles, flagvalue);

path(p)

% 3. Load iIRF for TRFS
% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(~, ~, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

p = path;
path('./Kernels', p);

onoffButtons(handles, 5);

flagvalue = loadiIRF(handles); % flagvalue 5 if fails 6 if succeeds

onoffButtons(handles, flagvalue);

path(p)

% 4. Perform preprocessing
% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(~, ~, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

p = path;
path('./Kernels', p);

onoffButtons(handles, 7);

flagvalue = runTRFSPreproc_MC_New(handles); % 0 if it fails 8 if succeeds

onoffButtons(handles, flagvalue);

path(p)

% 5. Save preproc data
% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(~, ~, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

foldername = uigetdir('', 'Please Select the Folder to Save your Raw Data');
if ~foldername
    path(p)
    return;
end

rawOutputData = getappdata(handles.figure1, 'rawOutputData');
intIntensity0 = rawOutputData.intIntensity;
wavelengths_list0 = rawOutputData.wavelengths_list;
trunc_Intensity0 = rawOutputData.trunc_Intensity;
raw_Intensity0 = rawOutputData.raw_Intensity;
SNR0 = rawOutputData.SNR;
gain_list0 = rawOutputData.gain_list;
iIRF = getappdata(handles.figure1, 'iIRF');
if gt(size(iIRF, 2), 1)
    iIRF = iIRF';
end
outputNames = getappdata(handles.figure1, 'outputNames');
suffix = outputNames.suffix;
prefixes = outputNames.prefixes;

for i = 1:size(intIntensity0, 1)
    
    try
        
        groupnum = outputNames.Group;
        groupnum00 = num2str(str2double(groupnum{i}), '%02i');
        filename = fullfile(foldername, strcat(groupnum00, '_', prefixes{i}, '_TRFS_PreProc', suffix));
        
    catch err
        
        groupnum00 = num2str(1, '%02i');
        filename = fullfile(foldername, strcat(groupnum00, '_', prefixes{i}, '_TRFS_PreProc', suffix));
        
    end
    
    intIntensity = intIntensity0{i}';
    wavelengths_list = wavelengths_list0{i};
    trunc_Intensity = trunc_Intensity0{i};
    raw_Intensity = raw_Intensity0{i};
    SNR = SNR0{i}';
    gain_list = gain_list0{i};
    
    save(filename, 'intIntensity', 'wavelengths_list', 'trunc_Intensity', 'raw_Intensity', 'iIRF', 'SNR', 'gain_list')
    clear intIntensity wavelengths_list trunc_Intensity raw_Intensity SNR gain_list
    
end

msgbox('The data have been successfully saved')

path(p)

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Edits...do nothing
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

function edit1_Callback(~, ~, ~)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, ~, ~)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(~, ~, ~)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, ~, ~)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Slider Actions
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% --- Executes on slider movement.
function slider2_Callback(hObject, ~, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
p = path;
path('./Kernels', p);

ind = round(get(hObject, 'Value'));
set(hObject, 'Value', ind);

sliderFlag = getappdata(handles.figure1, 'sliderFlag');

if eq(sliderFlag, 1)
    
    axes(handles.axes2);
    delete(legend(gca));
    sliderTRFSPreproc(handles, ind);
    
elseif eq(sliderFlag, 2)
    
    axes(handles.axes2);
    delete(legend(gca));
    sliderTRFSProc(handles, ind);
    
elseif eq(sliderFlag, 3)
    
    axes(handles.axes1);
    delete(legend(gca));
    axes(handles.axes2);
    delete(legend(gca));
    sliderTRIPLEXProc(handles, ind);
    
end

path(p)

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, ~, ~)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

set(hObject, 'Visible', 'off', 'Value', 1, 'Min', 1);

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% TRFS Deconvolution
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Perform the Deconvolution
% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(~, ~, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
q = path('./kernel', p);
path('./Kernels', q);

onoffButtons(handles, 9)

axes(handles.axes2);
delete(legend(gca));
TRFSProc(handles)

onoffButtons(handles, 10)

path(p)

% 2. Saves the Data
% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(~, ~, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

foldername = uigetdir('', 'Please Select the Folder to Save your TRSF Data');

if ~foldername
    
    path(p)
    return;
    
end

TRFS_Out = getappdata(handles.figure1, 'TRFS_Data');
intIntensity0 = TRFS_Out.spec_int;
lifet_avg0 = TRFS_Out.lifet_avg;
cc0 = TRFS_Out.cc;
alpha_out0 = TRFS_Out.alpha_out;
test0 = TRFS_Out.test;
out0 = TRFS_Out.out;
inputData = getappdata(handles.figure1, 'rawOutputData');
raw_Intensity0 = inputData.trunc_Intensity;
lambda0 = inputData.wavelengths_list;
gain_list0 = inputData.gain_list;
SNR0 = TRFS_Out.SNR;
timeDiscr_IntRes = str2double(get(handles.edit4, 'string'));
timeDiscr_Real = str2double(get(handles.edit2, 'string'));
BG_scaled0 = inputData.BG_scaled;
Phasor0 = TRFS_Out.Phasor;

outputNames = getappdata(handles.figure1, 'outputNames');
prefixes = outputNames.prefixes;

for i = 1:size(intIntensity0, 1)
    
    try
        
        groupnum = outputNames.Group;
        groupnum00 = num2str(str2double(groupnum{i}), '%02i');
        filename = fullfile(foldername, strcat(groupnum00, '_', prefixes{i}, '_TRFS_DeCon.mat'));
        
    catch err
        
        groupnum00 = num2str(1, '%02i');
        filename = fullfile(foldername, strcat(groupnum00, '_', prefixes{i}, '_TRFS_DeCon.mat'));
        
    end
        
    spec_int = intIntensity0{i}';
    lifet_avg = lifet_avg0{i}';
    Laguerre_coeffs = cc0{i};
    alpha_out = alpha_out0{i};
    test = test0{i};
    out = out0{i};
    lambda = lambda0{i};
    SNR = SNR0{i}';
    gain = gain_list0{i};
    raw_Intensity = raw_Intensity0{i};
    BG_scaled = BG_scaled0{i};
    Phasor = Phasor0{i};
    
    save(filename, 'spec_int', 'lifet_avg', 'Laguerre_coeffs', 'alpha_out', 'test', 'lambda', 'out', 'SNR', 'timeDiscr_IntRes', 'timeDiscr_Real', 'gain', 'raw_Intensity', 'BG_scaled', 'Phasor')
    clear spec_int lifet_avg Laguerre_coeffs alpha_out test out SNR gain raw_Intensity BG_scaled Phasor
    
end

msgbox('The data have been successfully saved')

path(p)

% 3. Popup Menus for display
% --------------------------------------------------------------------------------------------------------
% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(~, ~, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
p = path;
path('./Kernels', p);

popupmenu1_plot(handles);
popupmenu2_plot(handles);

path(p)

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(~, ~, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
p = path;
path('./Kernels', p);

popupmenu2_plot(handles);
popupmenu1_plot(handles);

path(p)


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% 4. Opens the deconvolution evaluation plots
% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(~, ~, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

TRFS_PlotFitting(handles);

path(p)

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Edits .... do nothing
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
function edit3_Callback(~, ~, ~)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, ~, ~)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit4_Callback(~, ~, ~)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, ~, ~)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit5_Callback(~, ~, ~)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, ~, ~)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit6_Callback(~, ~, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
outputData = getappdata(handles.figure1, 'TRFS_Data');

if isempty(outputData)
    
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);
    L_num = str2double(get(handles.edit6, 'String'));
    
    str_popupmenu1 = cell((L_num + 1), 1);
    str_popupmenu2 = cell((L_num + 1), 1);
    str_popupmenu1{1} = 'Lifetime';
    str_popupmenu2{1} = 'Int. Inensity';
    
    for i = 2:(L_num + 1)
        
        str_popupmenu1{i} = ['Laguerre ', num2str(i - 1)];
        str_popupmenu2{i} = ['Laguerre ', num2str(i - 1)];
        
    end
        
    set(handles.popupmenu1, 'String', str_popupmenu1);
    set(handles.popupmenu2, 'String', str_popupmenu2);
    
else
    
    setappdata(handles.figure1, 'TRFS_Data', []);
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    setappdata(handles.figure1, 'BlackPlot1', []);
    setappdata(handles.figure1, 'BlackPlot2', []);
    setappdata(handles.figure1, 'FlagType', []);
    setappdata(handles.figure1, 'sliderFlag', []);
    setappdata(handles.figure1, 'FlagTypeAxis1', []);
    setappdata(handles.figure1, 'FlagTypeAxis2', []);
    set(handles.popupmenu1, 'enable', 'off');
    set(handles.popupmenu2, 'enable', 'off');
    set(handles.popupmenu1, 'Value', 1);
    set(handles.popupmenu2, 'Value', 1);
    set(handles.pushbutton7, 'enable', 'off');
    set(handles.pushbutton8, 'enable', 'off');
    
    axes(handles.axes1);
    cla reset;
    hold off
    axis on
    box on
    
    axes(handles.axes2);
    cla reset;
    legend off
    hold off
    axis on
    box on
    pause(.01)
    
    set(handles.Title1, 'String', 'Title1');
    set(handles.Title2, 'String', 'Title2');
    
    L_num = str2double(get(handles.edit6, 'String'));
    str_popupmenu1 = cell((L_num + 1), 1);
    str_popupmenu2 = cell((L_num + 1), 1);
    str_popupmenu1{1} = 'Lifetime';
    str_popupmenu2{1} = 'Int. Inensity';
    
    for i = 2:(L_num + 1)
        
        str_popupmenu1{i} = ['Laguerre ', num2str(i - 1)];
        str_popupmenu2{i} = ['Laguerre ', num2str(i - 1)];
        
    end
        
    set(handles.popupmenu1, 'String', str_popupmenu1);
    set(handles.popupmenu2, 'String', str_popupmenu2);
    
end

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, ~, ~)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit7_Callback(~, ~, ~)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, ~, ~)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% TRIPLEX
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Load TRIPLEX Files
% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(~, ~, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

pb14_flag = get(handles.pushbutton14, 'enable');
rb12_flag = get(handles.radiobutton12, 'enable');
onoffButtons(handles, 11);

flagvalue = loadTRIPLEXData(handles); % 11 if it fails 12 if succeeds

onoffButtons(handles, flagvalue);

if eq(flagvalue, 12)
    
    set(handles.pushbutton14, 'enable', 'on');
    
    if ~isempty(getappdata(handles.figure1, 'TRIPLEXrawDataNames'))
        
        set(handles.radiobutton12, 'enable', char(rb12_flag));
        set(handles.radiobutton11, 'enable', char(rb12_flag));
        
    elseif isempty(getappdata(handles.figure1, 'TRIPLEXrawDataNames'))
        
        set(handles.radiobutton12, 'enable', 'off');
        set(handles.radiobutton11, 'enable', 'off');
        set(handles.radiobutton12, 'Value', 1);
        
    end
    
elseif eq(flagvalue, 11)
    
    if ~isempty(getappdata(handles.figure1, 'outputNames'))
        
        set(handles.pushbutton14, 'enable', char(pb14_flag));
        set(handles.radiobutton12, 'enable', char(rb12_flag));
        set(handles.radiobutton11, 'enable', char(rb12_flag));
        
    elseif isempty(getappdata(handles.figure1, 'outputNames'))
        
        set(handles.pushbutton14, 'enable', 'off');
        set(handles.radiobutton12, 'enable', 'off');
        set(handles.radiobutton11, 'enable', 'off');
        set(handles.radiobutton12, 'Value', 1);
        
    end
    
end

path(p)

%. 2. Load TRIPLEX iIRF
% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(~, ~, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

onoffButtons(handles, 13);

flagvalue = loadTRIPLEXiIRF(handles); % 13 if it fails 14 if succeeds

onoffButtons(handles, flagvalue);

path(p)

% 3. TRIPLEX Process
% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(~, ~, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
q = path('./Kernels', p);
path('./kernel', q);

onoffButtons(handles, 15);

axes(handles.axes1);
delete(legend(gca));
axes(handles.axes2);
delete(legend(gca));
flagvalue = TRIPLEXProc(handles); % 16 if succeeds 17 or 13 if fails

onoffButtons(handles, flagvalue);

path(p)

% 4. Save TRIPLEX Results
% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(~, ~, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p)

foldername = uigetdir('', 'Please Select the Folder to Save your TRIPLEX Data');

if ~foldername
    
    path(p)
    return;
    
end

TRIPLEX_Out = getappdata(handles.figure1, 'TRIPLEXresultsData');
intIntensity0 = TRIPLEX_Out.spec_int;
lifet_avg0 = TRIPLEX_Out.lifet_avg;
cc0 = TRIPLEX_Out.cc;
alpha_out0 = TRIPLEX_Out.alpha_out;
test0 = TRIPLEX_Out.test;
out0 = TRIPLEX_Out.out;
SNR0 = TRIPLEX_Out.SNR;
gain_list0 = TRIPLEX_Out.gain_list;
BG_scaled0 = TRIPLEX_Out.BG_scaled;
Phasor0 = TRIPLEX_Out.Phasor;

timeDiscr_IntRes = str2double(get(handles.edit9, 'string'));
timeDiscr_Real = str2double(get(handles.edit8, 'string'));

outputNames = getappdata(handles.figure1, 'TRIPLEXrawDataNames');
time_Stamp0 = outputNames.timeStamp;
fiber_pos0 = outputNames.pos_values;

prefixes = outputNames.prefixes;

% **temporary** TIFF image stack option
choice = questdlg('Do you want to save TIFF stack for phasor analysis?',...
        'Save Image Stack','Yes','No','Yes');
% **temporary** TIFF image stack option END

for i = 1:size(intIntensity0, 1)
    
    try
        
        groupnum = outputNames.Group;
        groupnum00 = num2str(str2double(groupnum{i}), '%02i');
        filename = fullfile(foldername, strcat(groupnum00, '_', prefixes{i}, '_TRIPLEX_DeCon.mat'));
        
    catch err
        
        groupnum00 = num2str(1, '%02i');
        filename = fullfile(foldername, strcat(groupnum00, '_', prefixes{i}, '_TRIPLEX_DeCon.mat'));
        
    end
    
    [~, spec] = DataLoader(outputNames.filenames{i});
    spec = spec';
    noise = mean(spec((end - 100):end, :));
    spec = bsxfun(@minus, spec, noise);
    
        
    spec_int = intIntensity0{i}';
    lifet_avg = lifet_avg0{i}';
    Laguerre_coeffs = cc0{i}';
    alpha_out = alpha_out0{i}';
    test = test0{i}';
    out = out0{i}';
    SNR = SNR0{i}';
    gain_list = gain_list0{i};
    timeStamp = time_Stamp0{i};
    fiberPos = fiber_pos0{i};
    BG_scaled = BG_scaled0{i};
    Phasor = Phasor0{i};
    
    % Save phasor image stack
    % The code is temporarily attached here, since aligned iIRF is not 
    % available before Laguerre deconvolution.
    if strcmp(choice,'Yes')
        SaveImageStack(foldername,prefixes{i},out);
    end
    % Save phasor image stack END
    save(filename, 'spec_int', 'lifet_avg', 'Laguerre_coeffs', ...
        'alpha_out', 'test', 'out', 'SNR', 'gain_list', ...
        'timeDiscr_IntRes', 'timeDiscr_Real', 'spec', ...
        'fiberPos', 'timeStamp', 'BG_scaled','Phasor')
    clear spec_int lifet_avg Laguerre_coeffs alpha_out test out SNR gain_list spec timeStamp fiber_pos BG_scaled Phasor
    
end

msgbox('The data have been successfully saved')

path(p)

% 5. Popup menus
% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(~, ~, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3
p = path;
path('./Kernels', p);

popupmenu3_plot(handles);
popupmenu4_plot(handles);

path(p)

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(~, ~, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4
p = path;
path('./Kernels', p);

popupmenu3_plot(handles);
popupmenu4_plot(handles);

path(p)

% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% 6. Opens the deconvolution evaluation plots
% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(~, ~, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

TRIPLEX_PlotFitting(handles);

path(p)

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% 7. Edits
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

function edit8_Callback(~, ~, ~)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, ~, ~)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit11_Callback(~, ~, ~)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double

% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, ~, ~)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit13_Callback(~, ~, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double
outputData = getappdata(handles.figure1, 'TRIPLEXresultsData');

if isempty(outputData)
    
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    L_num = str2double(get(handles.edit13, 'String'));
    
    str_popupmenu3 = cell((L_num + 1), 1);
    str_popupmenu4 = cell((L_num + 1), 1);
    str_popupmenu3{1} = 'Lifetime';
    str_popupmenu4{1} = 'Int. Inensity';
    
    for i = 2:(L_num + 1)
        
        str_popupmenu3{i} = ['Laguerre ', num2str(i - 1)];
        str_popupmenu4{i} = ['Laguerre ', num2str(i - 1)];
        
    end
        
    set(handles.popupmenu3, 'String', str_popupmenu3);
    set(handles.popupmenu4, 'String', str_popupmenu4);
    
else
    
    setappdata(handles.figure1, 'TRIPLEXresultsData', []);
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'off');
    setappdata(handles.figure1, 'BlackPlot1TRIPLEX', []);
    setappdata(handles.figure1, 'BlackPlot2TRIPLEX', []);
    setappdata(handles.figure1, 'sliderFlag', []);
    set(handles.popupmenu3, 'enable', 'off');
    set(handles.popupmenu4, 'enable', 'off');
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    set(handles.pushbutton12, 'enable', 'off');
    set(handles.pushbutton13, 'enable', 'off');
    
    axes(handles.axes1);
    cla reset;
    hold off
    axis on
    box on
    
    axes(handles.axes2);
    cla reset;
    legend off
    hold off
    axis on
    box on
    pause(.01)
    
    set(handles.Title1, 'String', 'Title1');
    set(handles.Title2, 'String', 'Title2');
    
    L_num = str2double(get(handles.edit13, 'String'));
    str_popupmenu3 = cell((L_num + 1), 1);
    str_popupmenu4 = cell((L_num + 1), 1);
    str_popupmenu3{1} = 'Lifetime';
    str_popupmenu4{1} = 'Int. Inensity';
    
    for i = 2:(L_num + 1)
        
        str_popupmenu3{i} = ['Laguerre ', num2str(i - 1)];
        str_popupmenu4{i} = ['Laguerre ', num2str(i - 1)];
        
    end
        
    set(handles.popupmenu3, 'String', str_popupmenu3);
    set(handles.popupmenu4, 'String', str_popupmenu4);
    
end

% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, ~, ~)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit10_Callback(~, ~, ~)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double

% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, ~, ~)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit12_Callback(~, ~, ~)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double

% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, ~, ~)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit14_Callback(~, ~, ~)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double

% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, ~, ~)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit9_Callback(~, ~, ~)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, ~, ~)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox1.
function checkbox1_Callback(~, ~, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
p = path;
path('./Kernels', p);

popupmenu3_plot(handles);
popupmenu4_plot(handles);

path(p)

% --- Executes on button press in checkbox2.
function checkbox2_Callback(~, ~, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
p = path;
path('./Kernels', p);

popupmenu3_plot(handles);
popupmenu4_plot(handles);

path(p)

% --- Executes on button press in checkbox3.
function checkbox3_Callback(~, ~, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
p = path;
path('./Kernels', p);

popupmenu3_plot(handles);
popupmenu4_plot(handles);

path(p)

% --- Executes on button press in checkbox4.
function checkbox4_Callback(~, ~, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4
p = path;
path('./Kernels', p);

popupmenu3_plot(handles);
popupmenu4_plot(handles);

path(p)

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Axis Setup
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Setup Axis limits
% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(~, ~, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

axes(handles.axes1);
legendAxes1 = get(legend(gca), 'String');
delete(legend(gca));
axes(handles.axes2);
legendAxes2 = get(legend(gca), 'String');
delete(legend(gca));
axisLimits(handles, legendAxes2, legendAxes1);

path(p)

function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double

% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double

% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton24. **Segment Phasor**
function pushbutton24_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

SegmentPhasor(handles);
