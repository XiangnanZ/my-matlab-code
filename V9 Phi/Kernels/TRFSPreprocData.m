function flagvalue = TRFSPreprocData(handles)

quest1 = questdlg('Do you want to load a folder or specific files?',...
    'What to Load?', 'Folder', 'Files', 'Folder');

if strcmp(quest1,'Folder')
    
    foldername = uigetdir('', 'Please Select the Raw Data Folder from TRFS');
    if ~foldername
        
        flagvalue = 1;
        return
        
    end
    
    outputNames = loadRawTRFSFolder(foldername);
    
elseif strcmp(quest1,'Files')
    
    [filename, foldername] = uigetfile('.dat', 'Please Select the Raw Data from TRFS', 'Multiselect', 'on');
    if ~foldername
        
        flagvalue = 1;
        return
        
    end
    
    if ~iscell(filename)
        
        filename = {filename};
        
    end
    
    outputNames = loadRawTRFSFiles(foldername, filename);
    
    
    if ~isempty(outputNames)
        
        if le(size(outputNames.filenames, 1), 1)
            
            warndlg('Please select at least two raw data files, this is a spectroscopic app.');
            flagvalue = 1;
            return
            
        end
        
    elseif isempty(outputNames)
        
        warndlg('Please select TRFS raw data files');
        flagvalue = 1;
        return
        
    end
    
else
    
    flagvalue = 1;
    return
    
end

if isempty(outputNames)
    
    warndlg('The folder does not contain any data...');
    flagvalue = 1;
    return;
    
else
    
    setappdata(handles.figure1, 'outputNames', outputNames)
    set(handles.text3, 'String', strcat(['Number of Spectra: ', num2str(outputNames.wavelengths(end, 2))]));
    flagvalue = 2;
    
end