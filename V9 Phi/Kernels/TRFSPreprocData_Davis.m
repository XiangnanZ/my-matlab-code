function flagvalue = TRFSPreprocData_Davis(handles)

quest1 = questdlg('Do you want to load a folder or specific files?',...
    'What to Load?', 'Folder', 'Files', 'Folder');

if strcmp(quest1,'Folder')
    
    foldername = uigetdir('', 'Please Select the Raw Data Folder from TRFS');
    if ~foldername
        
        flagvalue = 1;
        return
        
    end
    
    outputNames = loadRawTRFSFolder_Davis(foldername, handles);
    
elseif strcmp(quest1,'Files')
    
    [filename, foldername] = uigetfile('.txt', 'Please Select the Raw Data from TRFS', 'Multiselect', 'on');
    if ~foldername
        
        flagvalue = 1;
        return
        
    end
    
    if ~iscell(filename)
        
        filename = {filename};
        
    end
    
    outputNames = loadRawTRFSFiles_Davis(handles, foldername, filename);
    
else
    
    flagvalue = 1;
    return
    
end

if isempty(outputNames)
    
    warndlg('Please select TRFS raw data files');
    flagvalue = 1;
    return
    
else
    
    setappdata(handles.figure1, 'outputNames', outputNames)
    set(handles.text3, 'String', strcat(['Number of Spectra: ', num2str(outputNames.wavelengths(end))]));
    flagvalue = 2;
    
end