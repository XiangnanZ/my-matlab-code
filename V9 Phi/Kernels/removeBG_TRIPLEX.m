function [spec_out1, gainCor, BG] = removeBG_TRIPLEX(filename_BG, foldername_BG, spec, handles)

try
    
    BG = loadBinary_TRFS(fullfile(foldername_BG, filename_BG), 'double', 0)';
    
catch err
    
    try
        BG = load(fullfile(foldername_BG, filename_BG))';
        
    catch err0
        
        try
            
            BG = loadBinary(fullfile(foldername_BG, filename_BG), 'int16', 0)';
            
        catch err1
            
            try
                
                BG = -LoadBackground(fullfile(foldername_BG, filename_BG))';
                
            catch err2
                
                BG = loadBinary(fullfile(foldername_BG, filename_BG), 'double', 0)';
                
            end
            
        end
        
    end
    
end

ssr = mean(BG((end - 100):end, :));
BG = bsxfun(@minus, BG, ssr);
BG = bsxfun(@rdivide, BG, abs(min(BG)));
BG = mean(BG, 2);

BGremove_flag = getappdata(handles.figure1, 'BGrem_flag');

if eq(BGremove_flag, 0)
    
    BGPos = load('PeaksPos.dat');
    
    fig = figure;
    set(fig, 'Position',  get(0,'Screensize'));
    hax1 = subplot(2, 1, 1);
    pos = get(hax1,'position');
    pos(2) = pos(2) + 0.05;
    set(hax1,'position',pos);
    plot(BG);
    
    hcur1 = mycursors(hax1,'r');
    hcur1.off(1);
    hcur1.add(BGPos(3,1));
    hcur1.add(BGPos(3,2));
    
    hax2 = subplot(2, 1, 2);
    pos = get(hax2,'position');
    pos(2) = pos(2) + 0.1;
    set(hax2,'position',pos);
    plot(spec);
    
    pause(0.01);
    
    uicontrol('unit','Normalized','Position',[0.425 0.09 0.15 0.05],'String','Continue','Callback','uiresume(gcbf)');
    uiwait(fig);
    
    c_max = [min([hcur1.val(1) hcur1.val(2)]) max([hcur1.val(1) hcur1.val(2)]) 2000 0 0];
    
    close(fig)
    
else
    
    c_max = getappdata(handles.figure1, 'c_max');
    
end

if isempty(c_max)
    
    spec_out1 = [];
    gainCor = 0;
    return
    
end

if eq(BGremove_flag, 0)
    
    setappdata(handles.figure1, 'BGrem_flag', 1);
    setappdata(handles.figure1, 'c_max', c_max);
    
    BGPos(3, :) = c_max;
    save('PeaksPos.dat', 'BGPos', '-ascii');
    
end

c_max0 = round([c_max(1); c_max(2)]);
gain_base = c_max(3);


BG_peak = mean(BG(c_max0(1):c_max0(2), :));
spec_peak = mean(spec(c_max0(1):c_max0(2), :));
scaleFactor = spec_peak./BG_peak;
if eq(size(BG, 2), 1)
    BG = repmat(BG, 1, size(spec, 2));
end
BG = bsxfun(@times, BG, scaleFactor);
run1 = spec - BG;

gainCor = 1;

spec_out1 = run1;