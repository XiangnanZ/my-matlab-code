function flagvalue = loadTRIPLEXData(handles)

axes(handles.axes1);
cla reset;
axis on
box on
hold off

axes(handles.axes2);
cla reset;
legend off
axis on
box on
hold off

set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');

setappdata(handles.figure1, 'TRIPLEXDataType', 2);

quest1 = questdlg('Do you want to load a folder or specific files?',...
    'What to Load?', 'Folder', 'Files', 'Folder');

if strcmp(quest1,'Folder')
    
    foldername = uigetdir('', 'Please Select the Raw Data Folder from TRIPLEX');
    if ~foldername
        
        flagvalue = 11;
        return
        
    end
    
    outputNames = loadRawTRIPLEXFolder(foldername, handles);
    
elseif strcmp(quest1,'Files')
    
    [filename, foldername] = uigetfile('*.*', 'Please Select the Raw Data from TRIPLEX', 'Multiselect', 'on');
    if ~foldername
        
        flagvalue = 11;
        return
        
    end
    
    if ~iscell(filename)
        
        filename = {filename};
        
    end
    
    outputNames = loadRawTRIPLEXFiles(foldername, filename, handles);
    
    if isempty(outputNames)
        
        warndlg('Please select at least two raw data files');
        flagvalue = 11;
        return
        
    end
    
else
    
    flagvalue = 11;
    return
    
end

if isempty(outputNames)
    
    warndlg('The folder does not contain any data...');
    flagvalue = 11;
    return;
    
else
    
    setappdata(handles.figure1, 'TRIPLEXrawDataNames', outputNames)
    set(handles.text15, 'String', strcat(['Number of Points: ', num2str(size(outputNames.prefixes, 1))]));
    flagvalue = 12;
    
end