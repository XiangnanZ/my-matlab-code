function [spec_out1, spec_out2, gain_list_in, BG] = removeBG(filename_BG, foldername_BG, spec, wavelengths, gain_list_in, handles)

try
    
    BG = -loadBinary_TRFS(fullfile(foldername_BG, filename_BG), 'double', 0)';
    
catch err
    
    try
        
        BG = -load(fullfile(foldername_BG, filename_BG))';
        
    catch err0
        
        BG = -LoadBackground(fullfile(foldername_BG, filename_BG))';
        
    end
    
end

if gt(size(BG, 2), size(BG, 1))
    
    BG = BG';
    
end

ssr = mean(BG((end - 100):end, :));
BG = bsxfun(@minus, BG, ssr);

filename_BG_wavelengths = strcat(filename_BG, '_header.txt');
BG_wavelengths = load(fullfile(foldername_BG, filename_BG_wavelengths))';

[~, corWV] = ismember(wavelengths(:, 1), BG_wavelengths(:, 1));
try
    
    BG = BG(:, corWV);
    
catch err
    
    warndlg('The wavelengths of your signal exceeds the wavelengths of your background file');
    spec_out1 = [];
    spec_out2 = [];
    return
    
end
BGremove_flag = getappdata(handles.figure1, 'BGrem_flag');

if eq(BGremove_flag, 0)
    
    BGPos = load('PeaksPos.dat');
    
    fig = figure;
    set(fig, 'Position',  get(0,'Screensize'));
    hax1 = subplot(2, 1, 1);
    pos = get(hax1,'position');
    pos(2) = pos(2) + 0.05;
    set(hax1,'position',pos);
    plot(BG);
    
    hcur1 = mycursors(hax1,'r');
    hcur1.off(1);
    hcur1.add(BGPos(1,1));
    hcur1.add(BGPos(1,2));
    
    hax2 = subplot(2, 1, 2);
    pos = get(hax2,'position');
    pos(2) = pos(2) + 0.1;
    set(hax2,'position',pos);
    plot(spec);
    
    
    pause(0.01);
    
    uicontrol('unit','Normalized','Position',[0.425 0.09 0.15 0.05],'String','Continue','Callback','uiresume(gcbf)');
    uiwait(fig);
    
    c_max = [min([hcur1.val(1) hcur1.val(2)]) max([hcur1.val(1) hcur1.val(2)]) 2400 0 0];
    
    close(fig)
    
else
    
    c_max = getappdata(handles.figure1, 'c_max');
    
end

if isempty(c_max)
    
    spec_out1 = [];
    spec_out2 = [];
    return
    
end

if eq(BGremove_flag, 0)
    
    setappdata(handles.figure1, 'BGrem_flag', 1);
    setappdata(handles.figure1, 'c_max', c_max);
    
    BGPos(1, :) = c_max;
    save('PeaksPos.dat', 'BGPos', '-ascii');
    
end

c_max0 = round([c_max(1); c_max(2)]);
gain_base = c_max(3);

BG_peak = mean(BG(c_max0(1):c_max0(2), :));
spec_peak = mean(spec(c_max0(1):c_max0(2), :));
scaleFactor = spec_peak./BG_peak;
BG = bsxfun(@times, BG, scaleFactor);
run1 = spec - BG;

if ne(gain_base, 0)
    
    gain_run = wavelengths(:, 3);
    gainfactor = gain_base - gain_run;
    gainfactor = gainfactor / 100;
    gainfactor = 3.^gainfactor;
    
    run11 = bsxfun(@times, run1, gainfactor');
    gain_list_in = [gain_run (gain_base * ones(size(gain_run)))];
    
else
    
    run11 = run1;
    
end

spec_out1 = run11;
spec_out2 = run1;