function ExportTIFF(img, filename)
% writeTIFF(data, filename)
% writes data as a multi-channel TIFF with single prec. float pixels


Z=size(img,3);
data=zeros(size(img,1),size(img,2));

data(:,:)=img(:,:,1);
tagstruct.ImageLength = size(data, 1);
tagstruct.ImageWidth = size(data, 2);
tagstruct.Compression = Tiff.Compression.None;
%tagstruct.Compression = Tiff.Compression.LZW;        % compressed
tagstruct.SampleFormat = Tiff.SampleFormat.IEEEFP;
tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
tagstruct.BitsPerSample =  32;                        % float data
tagstruct.SamplesPerPixel = size(data,3);
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;



t = Tiff(filename, 'w');
t.setTag(tagstruct);
t.write(single(data));
t.close();


for z=2:Z
    data(:,:)=img(:,:,z);
    t = Tiff(filename, 'a');
    t.setTag(tagstruct);
    t.write(single(data));
    t.close();
end