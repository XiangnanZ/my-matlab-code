function [Channels, IRFs] = ConvertData_Phasor(out)

CH1 = out{1}{4};
CH1 = reshape(CH1',size(CH1,2),1,size(CH1,1));
IRF1 = out{1}{6};
IRF1 = reshape(IRF1,1,1,size(IRF1,1));

CH2 = out{2}{4};
CH2 = reshape(CH2',size(CH2,2),1,size(CH2,1));
IRF2 = out{2}{6};
IRF2 = reshape(IRF2,1,1,size(IRF2,1));

CH3 = out{3}{4};
CH3 = reshape(CH3',size(CH3,2),1,size(CH3,1));
IRF3 = out{3}{6};
IRF3 = reshape(IRF3,1,1,size(IRF3,1));

CH4 = out{4}{4};
CH4 = reshape(CH4',size(CH4,2),1,size(CH4,1));
IRF4 = out{4}{6};
IRF4 = reshape(IRF4,1,1,size(IRF4,1));

Channels = {CH1, CH2, CH3, CH4};
IRFs = {IRF1, IRF2, IRF3, IRF4};

end


