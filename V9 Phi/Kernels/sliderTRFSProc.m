function sliderTRFSProc(handles, ind)

inputNames = getappdata(handles.figure1, 'outputNames');
prefixes = inputNames.prefixes;

inputData = getappdata(handles.figure1, 'TRFS_Data');

popupmenu1_Value = get(handles.popupmenu1, 'Value');
if ne(popupmenu1_Value, 1)
    
    L_num1 = get(handles.popupmenu1, 'String');
    L_num1 = L_num1{popupmenu1_Value};
    L_num1 = str2double(regexp(L_num1, '\d+', 'match'));
    
end

popupmenu2_Value = get(handles.popupmenu2, 'Value');
if ne(popupmenu2_Value, 1)
    
    L_num2 = get(handles.popupmenu2, 'String');
    L_num2 = L_num2{popupmenu2_Value};
    L_num2 = str2double(regexp(L_num2, '\d+', 'match'));
    
end

if eq(popupmenu1_Value, 1)
    
    yaxis1_value = inputData.lifet_avg{ind};
%     yaxis1_value = yaxis1_value(isfinite(yaxis1_value));
    
else
    
    yaxis1_value = inputData.cc{ind};
    yaxis1_value = yaxis1_value{L_num1};
%     yaxis1_value = yaxis1_value(isfinite(yaxis1_value));
    if eq(get(handles.radiobutton9, 'Value'), 1)
        
        yaxis1_value = yaxis1_value./norm(yaxis1_value);
        
    end
    
end

if eq(popupmenu2_Value, 1)
    
    yaxis2_value = inputData.spec_int{ind};
    yaxis2_value = yaxis2_value(isfinite(yaxis2_value));
    
else
    
    yaxis2_value = inputData.cc{ind};
    yaxis2_value = yaxis2_value{L_num2};
    yaxis2_value = yaxis2_value(isfinite(yaxis2_value));
    
    if eq(get(handles.radiobutton9, 'Value'), 1)
        
        yaxis2_value = yaxis2_value./norm(yaxis2_value);
        
    end
    
end

rawOutputData = getappdata(handles.figure1, 'rawOutputData');
lambda = rawOutputData.wavelengths_list;
wave_step = min(diff(lambda{ind}));
SNR = inputData.SNR;

if eq(get(handles.radiobutton12, 'Value'), 1)
    
    col = jet(size(inputData.lifet_avg, 1));
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        col0 = jet(size(inputData.lifet_avg, 1));
        cmin = min(groupnum);
        cmax = max(groupnum);
        idx = min(size(inputData.lifet_avg, 1), round((size(inputData.lifet_avg, 1) - 1) * (groupnum - cmin) / (cmax - cmin)) + 1);
        col = col0(idx, :);
        
    catch err
        
        col = jet(size(inputData.lifet_avg, 1));
        
    end
    
end

% flagTypeAxis1 = getappdata(handles.figure1, 'FlagTypeAxis1');
flagTypeAxis2 = getappdata(handles.figure1, 'FlagTypeAxis2');

if eq(flagTypeAxis2, 1)
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        axes(handles.axes1);
        h1 = getappdata(handles.figure1, 'BlackPlot1');
        delete(h1);
        
        hold on
        h1(1) =  plot(lambda{ind}, yaxis1_value, 'k-', 'LineWidth', 3);
        h1(2) = h1(1);
        hold off
        
        setappdata(handles.figure1, 'BlackPlot1', h1);
        
        axes(handles.axes2);
        h2 = getappdata(handles.figure1, 'BlackPlot2');
        delete(h2);
        
        hold on
        h2(1) =  plot(lambda{ind}, yaxis2_value./norm(yaxis2_value), 'k-', 'LineWidth', 3);
        h2(2) = h2(1);
        hold off
        
        setappdata(handles.figure1, 'BlackPlot2', h2);
        
    elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
        
        axes(handles.axes1);
        h1 = getappdata(handles.figure1, 'BlackPlot1');
        delete(h1);
        
        xx = linspace(min(lambda{ind}), max(lambda{ind}), (max(lambda{ind}) - min(lambda{ind})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis1_value);
        yy = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis1_value, pp);
        y1 = fnval(yy, xx);
        
        hold on
        h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        h1(2) = h1(1);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{ind};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h1(2) = scatter(lambda{ind}, yaxis1_value, SNR_1, col(ind, :), 'fill');
            clear SNR_1 SNR_0
            
        end
        
        hold off
        
        setappdata(handles.figure1, 'BlackPlot1', h1);
        
        axes(handles.axes2);
        
        h2 = getappdata(handles.figure1, 'BlackPlot2');
        delete(h2);
        
        xx = linspace(min(lambda{ind}), max(lambda{ind}), (max(lambda{ind}) - min(lambda{ind})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis2_value./norm(yaxis2_value));
        yy = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis2_value./norm(yaxis2_value), pp);
        y1 = fnval(yy, xx);
        
        hold on
        h2(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        h2(2) = h2(1);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{ind};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h2(2) = scatter(lambda{ind}, yaxis2_value./norm(yaxis2_value), SNR_1, col(ind, :), 'fill');
            clear SNR_1 SNR_0
            
        end
        
        hold off
        
        setappdata(handles.figure1, 'BlackPlot2', h2);
        
    end
    
elseif eq(flagTypeAxis2, 2)
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        axes(handles.axes1);
        
        h1 = getappdata(handles.figure1, 'BlackPlot1');
        delete(h1);
        
        hold on
        h1(1) =  plot(lambda{ind}, yaxis1_value, 'k-', 'LineWidth', 3);
        h1(2) = h1(1);
        hold off
        
        setappdata(handles.figure1, 'BlackPlot1', h1);
        
        axes(handles.axes2);
        
        h2 = getappdata(handles.figure1, 'BlackPlot2');
        delete(h2);
        
        hold on
        h2(1) =  plot(lambda{ind}, yaxis2_value, 'k-', 'LineWidth', 3);
        h2(2) = h2(1);
        hold off
        
        setappdata(handles.figure1, 'BlackPlot2', h2);
        
    elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
        
        axes(handles.axes1);
        
        h1 = getappdata(handles.figure1, 'BlackPlot1');
        delete(h1);
        
        xx = linspace(min(lambda{ind}), max(lambda{ind}), (max(lambda{ind}) - min(lambda{ind})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis1_value);
        yy = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis1_value, pp);
        y1 = fnval(yy, xx);
        
        hold on
        h1(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        h1(2) = h1(1);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{ind};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h1(2) = scatter(lambda{ind}, yaxis1_value, SNR_1, col(ind, :), 'fill');
            clear SNR_1 SNR_0
            
        end
        
        hold off
        
        setappdata(handles.figure1, 'BlackPlot1', h1);
        
        axes(handles.axes2);
        
        h2 = getappdata(handles.figure1, 'BlackPlot2');
        delete(h2);
        
        xx = linspace(min(lambda{ind}), max(lambda{ind}), (max(lambda{ind}) - min(lambda{ind})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis2_value);
        yy = csaps(min(lambda{ind}):wave_step:max(lambda{ind}), yaxis2_value, pp);
        y1 = fnval(yy, xx);
        
        hold on
        h2(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        h2(2) = h2(1);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
           
            SNR_0 = SNR{ind};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h2(2) = scatter(lambda{ind}, yaxis2_value, SNR_1, col(ind, :), 'fill');
            clear SNR_1 SNR_0
            
        end
        
        hold off
        
        setappdata(handles.figure1, 'BlackPlot2', h2);
        
    end
    
end

less20dB = find(le(SNR{ind}, 20));
str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 2))];
set(handles.text27, 'String', str_less20dB);

great60dB = find(ge(SNR{ind}, 60));
str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 2))];
set(handles.text29, 'String', str_great60dB);

str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR{ind}, 2) - size(great60dB, 2) - size(less20dB, 2))];
set(handles.text28, 'String', str_between2060dB);

axes(handles.axes2);
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{ind}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        legend({[num2str(groupnum(ind)) '_' prefixes{ind}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{ind}}, 'Interpreter', 'none');
        
    end
    
end