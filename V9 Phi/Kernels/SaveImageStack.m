function status = SaveImageStack(foldername,prefix,outdata)
% status 0 - sucessful
% status -1 - failed
% need to modify ExportTIFF so that error in saving individual files will
% also report to status
[Channels, IRFs] = ConvertData_Phasor(outdata);
ImgStackDir = strcat(prefix,'_Image_Stack');
status = mkdir(fullfile(foldername,ImgStackDir));
if status
    ExportTIFF(Channels{1},fullfile(foldername,ImgStackDir,'TIFF_Stack_CH1'));
    ExportTIFF(Channels{2},fullfile(foldername,ImgStackDir,'TIFF_Stack_CH2'));
    ExportTIFF(Channels{3},fullfile(foldername,ImgStackDir,'TIFF_Stack_CH3'));
    ExportTIFF(Channels{4},fullfile(foldername,ImgStackDir,'TIFF_Stack_CH4'));
    ExportTIFF(IRFs{1},fullfile(foldername,ImgStackDir,'TIFF_Stack_IRF1'));
    ExportTIFF(IRFs{2},fullfile(foldername,ImgStackDir,'TIFF_Stack_IRF2'));
    ExportTIFF(IRFs{3},fullfile(foldername,ImgStackDir,'TIFF_Stack_IRF3'));
    ExportTIFF(IRFs{4},fullfile(foldername,ImgStackDir,'TIFF_Stack_IRF4'));
else
    warning('Cannot create directory for saving image stack!');
end

status = status - 1;

end