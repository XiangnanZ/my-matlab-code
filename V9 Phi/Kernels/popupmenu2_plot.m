function popupmenu2_plot(handles)

axes(handles.axes2);
cla reset;
hold off
axis on
box on

inputData = getappdata(handles.figure1, 'TRFS_Data');
rawOutputData = getappdata(handles.figure1, 'rawOutputData');
lambda = rawOutputData.wavelengths_list;
SNR = inputData.SNR;

xlim([min(unique(cell2mat(lambda))) max(unique(cell2mat(lambda)))]);

popupmenu2_Value = get(handles.popupmenu2, 'Value');
if ne(popupmenu2_Value, 1)
    
    L_num = get(handles.popupmenu2, 'String');
    L_num = L_num{popupmenu2_Value};
    L_num = str2double(regexp(L_num, '\d+', 'match'));
    
end

warning off
flagSlider = getappdata(handles.figure1, 'sliderFlag');

if and(eq(get(handles.radiobutton9, 'Value'), 1), eq(popupmenu2_Value, 1))
    
    flagtype = 1;
    
else
    
    flagtype = 2;
    
end

setappdata(handles.figure1, 'FlagTypeAxis2', flagtype);

if eq(get(handles.radiobutton12, 'Value'), 1)
    
    col = jet(size(inputData.lifet_avg, 1));
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        inputNames = getappdata(handles.figure1, 'outputNames');
        groupnum = str2double(inputNames.Group);
        col0 = jet(size(inputData.lifet_avg, 1));
        cmin = min(groupnum);
        cmax = max(groupnum);
        idx = min(size(inputData.lifet_avg, 1), round((size(inputData.lifet_avg, 1) - 1) * (groupnum - cmin) / (cmax - cmin)) + 1);
        col = col0(idx, :);
        
    catch err
        
        col = jet(size(inputData.lifet_avg, 1));
        
    end
    
end

if gt(size(inputData.lifet_avg, 1), 1)
    
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'on');
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(inputData.lifet_avg, 1), 'SliderStep', [(1 / (size(inputData.lifet_avg, 1) - 1)) (1 / (size(inputData.lifet_avg, 1) - 1))]);
    flagSlider = 2;
    
end

if eq(popupmenu2_Value, 1)
    
    for i = 1:size(inputData.lifet_avg, 1)
        
        wave_step = min(abs(unique(diff(lambda{i}))));
        
        if eq(flagSlider, 2)
            
            set(handles.slider2, 'Value', i);
            
        end
        
        yaxis_values = inputData.spec_int{i};
%         yaxis_values = yaxis_values(isfinite(yaxis_values));
        set(handles.Title2, 'String', 'Integrated Intensity');
        
        if eq(get(handles.radiobutton9, 'Value'), 1)
            
            if eq(get(handles.radiobutton6, 'Value'), 1)
                
                hold on
                plot(lambda{i}, yaxis_values./norm(yaxis_values(:)), 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Normalized Intensity (a.u.)')
                ylim([0 1]);
                hold off
                
            elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
                
                xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
                
                % 2.6 Apply spline interpolation on the plane x-y
                [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values./norm(yaxis_values(:)));
                yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values./norm(yaxis_values(:)), pp);
                y1 = fnval(yy, xx);
                
                hold on
                plot(xx, y1, 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Normalized Intensity (a.u.)')
                ylim([0 1]);
                hold off
                
            end
            
        elseif eq(get(handles.radiobutton10, 'Value'), 1)
            
            if eq(get(handles.radiobutton6, 'Value'), 1)
                
                hold on
                plot(lambda{i}, yaxis_values, 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Absolute Intensity (a.u.)')
                ylim([0 25]);
                hold off
                
            elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
                
                xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
                
                % 2.6 Apply spline interpolation on the plane x-y
                [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values);
                yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values, pp);
                y1 = fnval(yy, xx);
                
                hold on
                plot(xx, y1, 'color', col(i,:))
                xlabel('Wavelengths (nm)');
                ylabel('Absolute Intensity (a.u.)')
                ylim([0 25]);
                hold off
                
            end
            
        end
        
        pause(.01)
        
    end
    
    if eq(get(handles.radiobutton9, 'Value'), 1)
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            hold on
            h2(1) = plot(lambda{i}, inputData.spec_int{i}./norm(inputData.spec_int{i}), 'k-', 'LineWidth', 3);
            h2(2) = h2(1);
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.spec_int{i}./norm(inputData.spec_int{i}));
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.spec_int{i}./norm(inputData.spec_int{i}), pp);
            y1 = fnval(yy, xx);
            
            hold on
            h2(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
            
            if eq(get(handles.radiobutton8, 'Value'), 1)
                
                SNR_0 = SNR{i};
                SNR_1 = 50 * ones(size(SNR_0, 2), 1);
                SNR_1(SNR_0 <= 20) = 20;
                SNR_1(SNR_0 >= 60) = 100;
                h2(2) = scatter(lambda{i}, inputData.spec_int{i}./norm(inputData.spec_int{i}), SNR_1, col(i, :), 'fill');
                clear SNR_1 SNR_0
                
            end
            
            hold off
            ydata01 = get(gca, 'Children');
            ydata1 = get(ydata01, 'YData');
            if iscell(ydata1)
                
                ylim([0 max(cellfun(@nanmax, ydata1))])
                
            else
                
                ylim([0 nanmax(ydata1)])
                
            end
            
        end
        
    elseif eq(get(handles.radiobutton10, 'Value'), 1)
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            hold on
            h2(1) = plot(unique(lambda{i}), inputData.spec_int{i}, 'k-', 'LineWidth', 3);
            h2(2) = h2(1);
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.spec_int{i});
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), inputData.spec_int{i}, pp);
            y1 = fnval(yy, xx);
            
            hold on
            h2(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
            
            if eq(get(handles.radiobutton8, 'Value'), 1)
                
                SNR_0 = SNR{i};
                SNR_1 = 50 * ones(size(SNR_0, 2), 1);
                SNR_1(SNR_0 <= 20) = 20;
                SNR_1(SNR_0 >= 60) = 100;
                h2(2) = scatter(lambda{i}, inputData.spec_int{i}, SNR_1, col(i, :), 'fill');
                clear SNR_0 SNR_1
                                
            end
            
            hold off
            
        end
        
        ylim([0 max(cell2mat(inputData.spec_int'))]);
        
    end
    
    setappdata(handles.figure1, 'BlackPlot2', h2);
    
    if eq(flagSlider, 2)
        
        set(handles.slider2, 'enable', 'on');
        
    end
    
    setappdata(handles.figure1, 'sliderFlag', flagSlider);
    
elseif gt(popupmenu2_Value, 1)
    
    for i = 1:size(inputData.lifet_avg, 1)
        
        wave_step = min(abs(unique(diff(lambda{i}))));
        
        if eq(flagSlider, 2)
            
            set(handles.slider2, 'Value', i);
            
        end
        
        yaxis_values = inputData.cc{i};
        yaxis_values = yaxis_values{L_num};
%         yaxis_values = yaxis_values(isfinite(yaxis_values));
        
         if eq(get(handles.radiobutton9, 'Value'), 1)
            
            yaxis_values = yaxis_values./norm(yaxis_values);
            
        end
        
        set(handles.Title2, 'String', ['Laguerre Coeffs ', num2str(L_num)]);
        
        if eq(get(handles.radiobutton6, 'Value'), 1)
            
            hold on
            plot(lambda{i}, yaxis_values, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Laguerre Coeffs (a.u.)')
            ylim([0 1]);
            hold off
            
        elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
            
            xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values);
            yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values, pp);
            y1 = fnval(yy, xx);
            
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Laguerre Coeffs (a.u.)')            
            hold off
            
        end
        
        pause(.01)
        
    end
    
    if eq(get(handles.radiobutton6, 'Value'), 1)
        
        hold on
        h2(1) = plot(unique(lambda{i}), yaxis_values, 'k-', 'LineWidth', 3);
        h2(2) = h2(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton8, 'Value'), 1), eq(get(handles.radiobutton7, 'Value'), 1))
        
        xx = linspace(min(lambda{i}), max(lambda{i}), (max(lambda{i}) - min(lambda{i})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values);
        yy = csaps(min(lambda{i}):wave_step:max(lambda{i}), yaxis_values, pp);
        y1 = fnval(yy, xx);
        
        hold on
        h2(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton8, 'Value'), 1)
            
            SNR_0 = SNR{i};
            SNR_1 = 50 * ones(size(SNR_0, 2), 1);
            SNR_1(SNR_0 <= 20) = 20;
            SNR_1(SNR_0 >= 60) = 100;
            h2(2) = scatter(lambda{i}, yaxis_values, SNR_1, col(i, :), 'fill');
            clear SNR_1 SNR_0
            
        end
        
        hold off
        
    end
    
    setappdata(handles.figure1, 'BlackPlot2', h2);
    
    if eq(flagSlider, 2)
        
        set(handles.slider2, 'enable', 'on');
        
    end
    
    setappdata(handles.figure1, 'sliderFlag', flagSlider);  
    axis tight    
    
end

ydata01 = get(gca, 'Children');
ydata1 = get(ydata01, 'YData');

if iscell(ydata1)
    
    ylim([min(cellfun(@nanmin, ydata1)) max(cellfun(@nanmax, ydata1))])
    
else
    
    ylim([nanmin(ydata1) nanmax(ydata1)])
    
end

less20dB = find(le(SNR{i}, 20));
str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 2))];
set(handles.text27, 'String', str_less20dB);

great60dB = find(ge(SNR{i}, 60));
str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 2))];
set(handles.text29, 'String', str_great60dB);

str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR{i}, 2) - size(great60dB, 2) - size(less20dB, 2))];
set(handles.text28, 'String', str_between2060dB);

inputNames = getappdata(handles.figure1, 'outputNames');
prefixes = inputNames.prefixes;

if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{i}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        legend({[num2str(groupnum(i)) '_' prefixes{i}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{i}}, 'Interpreter', 'none');
        
    end
    
end