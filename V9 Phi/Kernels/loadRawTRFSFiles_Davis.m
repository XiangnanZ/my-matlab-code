function output = loadRawTRFSFiles_Davis(handles, foldername, nameListing)

% 1. Find the file extenstion
find_txt = strfind(nameListing, '.txt');

if ne(size(nameListing, 2), 0)
    
    % wavelengths = [wavelengths flag]
    wavelengths = cell(size(nameListing, 2), 1);
    filenames = cell(size(nameListing, 2), 1);
    
    for i = 1:size(nameListing, 2)
        
        str = cell2mat(nameListing(i));
        filenames{i, 1} = fullfile(foldername, str);
        
        wavelengths_filename = strcat(str(1:(find_txt{i} - 1)), '_header_value.txt');
        wavelengths_filename = load(fullfile(foldername, wavelengths_filename));
        
        set(handles.edit2, 'string', num2str(round((wavelengths_filename(2)) * 10^12)));
        set(handles.edit1, 'string', num2str(round(wavelengths_filename(3))));
        
        wavelengths{i}(:, 1) = round(wavelengths_filename(13:end))';
        wavelengths{i}(:, 2) = i;
        
    end
    
    wavelengths = cell2mat(wavelengths);
    
    prefixes = cell(size(find_txt, 2), 1);
    
    for i = 1:size(find_txt, 2)
        
        str = cell2mat(nameListing(i));
        prefixes{i, 1} = str(1:(find_txt{i} - 1));
        
    end
    
    suffix = strcat('.mat');
    
    output.wavelengths = wavelengths;
    output.suffix = suffix;
    output.filenames = filenames;
    output.prefixes = prefixes;
    
else
    
    output = [];
    
end