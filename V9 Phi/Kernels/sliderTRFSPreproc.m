function sliderTRFSPreproc(handles, ind)

timeStep = str2double(get(handles.edit2, 'string'));
rawOutputData = getappdata(handles.figure1, 'rawOutputData');
spec = rawOutputData.raw_Intensity;
inputNames = getappdata(handles.figure1, 'outputNames');
prefixes = inputNames.prefixes;

rawOutputData = getappdata(handles.figure1, 'rawOutputData');
intIntensity = rawOutputData.intIntensity;
wavelengths = rawOutputData.wavelengths_list;

wave_step = min(diff(wavelengths{ind}));
flagType = getappdata(handles.figure1, 'FlagType');

if eq(flagType, 1)
    
    axes(handles.axes2);    
    x = (timeStep * [1:size(spec{ind}, 1)]) / 1000;
    spec0 = spec{ind};   
    
    cla
    hold on
    plot(x', spec0)
    hold off
    
    axes(handles.axes1)
    
    if eq(get(handles.radiobutton1, 'Value'), 1)
        
        h = getappdata(handles.figure1, 'BlackPlot');
        delete(h);
        
        hold on
        h(1) =  plot(wavelengths{ind}, intIntensity{ind}./norm(intIntensity{ind}), 'k-', 'LineWidth', 3);
        h(2) = h(1);
        hold off
        
        setappdata(handles.figure1, 'BlackPlot', h);
        
    elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
        
        h = getappdata(handles.figure1, 'BlackPlot');
        delete(h);
        
        xx = linspace(min(wavelengths{ind}), max(wavelengths{ind}), (max(wavelengths{ind}) - min(wavelengths{ind})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(wavelengths{ind}):wave_step:max(wavelengths{ind}), intIntensity{ind}./norm(intIntensity{ind}));
        yy = csaps(min(wavelengths{ind}):wave_step:max(wavelengths{ind}), intIntensity{ind}./norm(intIntensity{ind}), pp/2);
        y1 = fnval(yy, xx);
        
        hold on
        h(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        h(2) = h(1);
        
        if eq(get(handles.radiobutton3, 'Value'), 1)
            
            h(2) = plot(wavelengths{ind}, intIntensity{ind}./norm(intIntensity{ind}), 'k.', 'MarkerSize', 15);
            
        end
        
        hold off
        
        setappdata(handles.figure1, 'BlackPlot', h);
        
    end
    
elseif eq(flagType, 2)
    
    axes(handles.axes2);    
    x = (timeStep * [1:size(spec{ind}, 1)]) / 1000;
    spec0 = spec{ind};
    cla
    hold on
    plot(x', spec0)
    hold off
    
    axes(handles.axes1)
    
    if eq(get(handles.radiobutton1, 'Value'), 1)
        
        h = getappdata(handles.figure1, 'BlackPlot');
        delete(h);
        
        hold on
        h(1) =  plot(wavelengths{ind}, intIntensity{ind}, 'k-', 'LineWidth', 3);
        h(2) = h(1);
        hold off
        
        setappdata(handles.figure1, 'BlackPlot', h);
        
    elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
        
        h = getappdata(handles.figure1, 'BlackPlot');
        delete(h);
        
        xx = linspace(min(wavelengths{ind}), max(wavelengths{ind}), (max(wavelengths{ind}) - min(wavelengths{ind})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(wavelengths{ind}):wave_step:max(wavelengths{ind}), intIntensity{ind});
        yy = csaps(min(wavelengths{ind}):wave_step:max(wavelengths{ind}), intIntensity{ind}, pp/2);
        y1 = fnval(yy, xx);
        
        hold on
        h(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        h(2) = h(1);
        
        if eq(get(handles.radiobutton3, 'Value'), 1)
            
            h(2) = plot(wavelengths{ind}, intIntensity{ind}, 'k.', 'MarkerSize', 15);
            
        end
        
        hold off
        
        setappdata(handles.figure1, 'BlackPlot', h);
        
    end
    
end

axes(handles.axes2);
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{ind}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        legend({[num2str(groupnum(ind)) '_' prefixes{ind}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{ind}}, 'Interpreter', 'none');
        
    end
    
end