function mes_hist_reg(handles)

TRFSNames = getappdata(handles.figure1, 'outputNames');

if ~isempty(TRFSNames)
    
    TRFSprefixes = TRFSNames.prefixes;
        
elseif isempty(TRFSNames)
    
    TRFSprefixes = [];
    
end
    
TRIPLEXNames = getappdata(handles.figure1, 'TRIPLEXrawDataNames');

if ~isempty(TRIPLEXNames)
    
    TRIPLEXprefixes = TRIPLEXNames.prefixes;
    
elseif isempty(TRIPLEXNames)
    
    TRIPLEXprefixes = [];
    
end

if eq(size(TRFSprefixes, 1), size(TRIPLEXprefixes, 1))
    
    quest = questdlg('Do you want to register Both, TRFS or TRIPLEX?',...
        'Measurements/Histology Registration', 'Both', 'TRFS', 'TRIPLEX', 'Both');
    
elseif and(and(ne(size(TRFSprefixes, 1), size(TRIPLEXprefixes, 1)), ~isempty(TRIPLEXprefixes)), ~isempty(TRFSprefixes))
    
    quest = questdlg('Your data are not equal in size. Do you want to register TRFS or TRIPLEX?',...
        'Measurements/Histology Registration', 'TRFS', 'TRIPLEX', 'TRFS');
    
elseif and(~isempty(TRFSprefixes), isempty(TRIPLEXprefixes))
    
    quest = 'TRFS';
    
elseif and(isempty(TRFSprefixes), ~isempty(TRIPLEXprefixes))
    
    quest = 'TRIPLEX';
    
else
    
    waitfor(msgbox('Something is wrong with your filenames. I cannot proceed...'));
    return
    
end

if or(strcmp(quest, 'TRFS'), strcmp(quest, 'Both'))
    
    if gt(size(TRFSprefixes, 1), 10)
        
        TRFSprefixes_num = ceil(size(TRFSprefixes, 1) / 10);
        k = 1;
        l = 10;
        markerPos = [];
        for i = 1:TRFSprefixes_num
            
            TRFSprefixes_ord = TRFSprefixes(k:l);
            k = l + 1;
            l = l + 10;
            
            if ge(l, size(TRFSprefixes, 1))
                
                l = size(TRFSprefixes, 1);
                
            end
            
            def1 = cell(size(TRFSprefixes, 1), 1);
            [def1{:}] = deal(num2str(0));
            
            prompt = TRFSprefixes_ord(:);
            dlg_title = ['Please enter the corresponding label for each point (', num2str(i), '/', num2str(TRFSprefixes_num), ')'];
            options.Resize='on';
            options.WindowStyle='normal';
            markerPos0 = inputdlg(prompt, dlg_title, [1, length(dlg_title) + 30], def1, options);
            
            if isempty(markerPos0)
                
                set(handles.radiobutton11, 'enable', 'off');
                set(handles.radiobutton12, 'enable', 'off');
                return
                
            else
                
                markerPos = [markerPos; markerPos0];
                
            end
            
        end
        
    else
        
        def1 = cell(size(TRFSprefixes, 1), 1);
        [def1{:}] = deal(num2str(0));
        
        prompt = TRFSprefixes(:);
        dlg_title = 'Please enter the corresponding label for each point';
        options.Resize='on';
        options.WindowStyle='normal';
        markerPos = inputdlg(prompt, dlg_title, [1, length(dlg_title) + 30], def1, options);
        
        if isempty(markerPos)
            
            set(handles.radiobutton11, 'enable', 'off');
            set(handles.radiobutton12, 'enable', 'off');
            return
            
        end
        
    end
        
    if le(min(str2double(markerPos)), 0)
        
        set(handles.radiobutton11, 'enable', 'off');
        set(handles.radiobutton12, 'enable', 'off');
        return
        
    else
        
        TRFSNames.Group = markerPos;
        setappdata(handles.figure1, 'outputNames', TRFSNames);
        
        if strcmp(quest, 'Both')
            
            TRIPLEXNames.Group = markerPos;
            setappdata(handles.figure1, 'TRIPLEXrawDataNames', TRIPLEXNames);
          
        end
        
        set(handles.radiobutton11, 'enable', 'on');
        set(handles.radiobutton12, 'enable', 'on');
        
    end
    
elseif strcmp(quest, 'TRIPLEX')
    
    if gt(size(TRIPLEXprefixes, 1), 10)
        
        TRIPLEXprefixes_num = ceil(size(TRIPLEXprefixes, 1) / 10);
        k = 1;
        l = 10;
        markerPos = [];
        for i = 1:TRIPLEXprefixes_num
            
            TRIPLEXprefixes_ord = TRIPLEXprefixes(k:l);
            k = l + 1;
            l = l + 10;
            
            if ge(l, size(TRIPLEXprefixes, 1))
                
                l = size(TRIPLEXprefixes, 1);
                
            end
            
            def1 = cell(size(TRIPLEXprefixes, 1), 1);
            [def1{:}] = deal(num2str(0));
            
            prompt = TRIPLEXprefixes_ord(:);
            dlg_title = ['Please enter the corresponding label for each point (', num2str(i), '/', num2str(TRIPLEXprefixes_num), ')'];
            options.Resize='on';
            options.WindowStyle='normal';
            markerPos0 = inputdlg(prompt, dlg_title, [1, length(dlg_title) + 30], def1, options);
            
            if isempty(markerPos0)
                
                set(handles.radiobutton11, 'enable', 'off');
                set(handles.radiobutton12, 'enable', 'off');
                return
                
            else
                
                markerPos = [markerPos; markerPos0];
                
            end
            
        end
        
    else
        
        def1 = cell(size(TRIPLEXprefixes, 1), 1);
        [def1{:}] = deal(num2str(0));
        
        prompt = TRIPLEXprefixes(:);
        dlg_title = 'Please enter the corresponding label for each point';
        options.Resize='on';
        options.WindowStyle='normal';
        markerPos = inputdlg(prompt, dlg_title, [1, length(dlg_title) + 30], def1, options);
        
        if isempty(markerPos)
            
            set(handles.radiobutton11, 'enable', 'off');
            set(handles.radiobutton12, 'enable', 'off');
            return
            
        end
        
    end
        
    if le(min(str2double(markerPos)), 0)
        
        set(handles.radiobutton11, 'enable', 'off');
        set(handles.radiobutton12, 'enable', 'off');
        return
        
    else
        
        TRIPLEXNames.Group = markerPos;
        setappdata(handles.figure1, 'TRIPLEXrawDataNames', TRIPLEXNames);
        
        set(handles.radiobutton11, 'enable', 'on');
        set(handles.radiobutton12, 'enable', 'on');
        
    end
    
end
