function output = loadRawTRIPLEXFolder(foldername, handles)

% 1. Read the folder contents
dirListing = dir(foldername);
nameListing = {dirListing.name};

% 2. Find and exclude folders within the folder
find_Dirs = [dirListing.isdir];
nameListing(find_Dirs) = [];

% 3. Find and exclude filenames with extensions
find_dots = strfind(nameListing, '.');
nameListing(~cellfun('isempty', find_dots)) = [];

% 4. Find and exclude lifetimes files
find_dots = strfind(nameListing, 'lifetimes');
nameListing(~cellfun('isempty', find_dots)) = [];

% 4. Find and exclude header files
find_dots = strfind(nameListing, 'header');
nameListing(~cellfun('isempty', find_dots)) = [];

if ne(size(nameListing, 2), 0)
    
    filenames = cell(size(nameListing, 2), 1);
    prefixes = cell(size(nameListing, 2), 1);
    gain_values = cell(size(nameListing, 2), 1);
    timeStamp = cell(size(nameListing, 2), 1);
    pos_values = cell(size(nameListing, 2), 1);
    flag_value = 1;
    
    for i = 1:size(nameListing, 2)
        
        str = cell2mat(nameListing(i));
        filenames{i, 1} = fullfile(foldername, str); 
        prefixes{i, 1} = str;
        flag_value = flag_value + 1;
        
        [headerFile, ~] = DataLoader(fullfile(foldername, str));
        set(handles.edit8, 'String', num2str(headerFile.TimeResolution));
        set(handles.edit11, 'String', num2str(headerFile.BW));
        
        try
            gain_values{i, 1} = headerFile.GainVoltage;
        catch err
            gain_values{i, 1} = 0;
        end
        try
            timeStamp{i, 1} = headerFile.TimeStamp;
        catch err
            timeStamp{i, 1} = 0;
        end
        try
            pos_values{i, 1} = headerFile.FiberPos;
        catch err
            pos_values{i, 1} = 0;
        end
       
    end
    
    suffix = '.mat';
    
    output.suffix = suffix;
    output.filenames = filenames;
    output.prefixes = prefixes;
    output.gain_values = gain_values;
    output.pos_values = pos_values;
    output.timeStamp = timeStamp;
    
else
    
    output = [];
    
end