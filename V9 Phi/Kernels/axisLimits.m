function axisLimits(handles, legendAxes2, legendAxes1)

axes(handles.axes1);
axis1_xlim = xlim;
axis1_ylim = ylim;
ydata01 = get(gca, 'Children');
ydata1 = get(ydata01, 'YData');
if ~isempty(ydata1)
    
    y_data1_min = cellfun(@nanmin, ydata1);
    y_data1_max = cellfun(@nanmax, ydata1);
    
else
    
    y_data1_min = NaN;
    y_data1_max = NaN;
    
end

axes(handles.axes2);
axis2_xlim = xlim;
axis2_ylim = ylim;
ydata02 = get(gca, 'Children');
ydata2 = get(ydata02, 'YData');
if ~isempty(ydata2)
    
    y_data2_min = cellfun(@nanmin, ydata2);
    y_data2_max = cellfun(@nanmax, ydata2);
    
else
    
    y_data2_min = NaN;
    y_data2_max = NaN;
    
end

dlg_title = 'Please enter the new axis limitis';
options.Resize='on';
options.WindowStyle='normal';
def1 = {num2str(axis1_xlim(1)) num2str(axis1_xlim(2)) num2str(axis1_ylim(1)) num2str(axis1_ylim(2)) num2str(axis2_xlim(1)) num2str(axis2_xlim(2)) num2str(axis2_ylim(1)) num2str(axis2_ylim(2)) num2str(min(y_data1_min)) num2str(max(y_data1_max)) num2str(min(y_data2_min)) num2str(max(y_data2_max))};
c_max = inputdlg({'Top axis minimum x' 'Top axis maximum x' 'Top axis minimum y' 'Top axis maximum y' 'Bottom axis minimum x' 'Bottom axis maximum x' 'Bottom axis minimum y' 'Bottom axis maximum y' 'Top axis minimum value' 'Top axis maximum value' 'Bottom axis minimum value' 'Bottom axis maximum value'}, dlg_title, [1, length(dlg_title) + 30], def1, options);
c_max = str2double(c_max)';

if or(isempty(c_max), eq(max(c_max), 0))
    
    return
    
end

if lt(c_max(1), c_max(2))
    
    axes(handles.axes1);
    xlim([c_max(1) c_max(2)]);
    
end

if lt(c_max(3), c_max(4))
    
    axes(handles.axes1);
    ylim([c_max(3) c_max(4)]);
    
end

if lt(c_max(5), c_max(6))
    
    axes(handles.axes2);
    xlim([c_max(5) c_max(6)]);
    
end

if lt(c_max(7), c_max(8))
    
    axes(handles.axes2);
    ylim([c_max(7) c_max(8)]);
    
end

if and(lt(c_max(9), c_max(10)), ~isempty(ydata1))
    
    axes(handles.axes1);
    for i = 1:size(ydata1, 1)
       
         ydata1{i}(ydata1{i} < c_max(9)) = NaN;
         ydata1{i}(ydata1{i} > c_max(10)) = NaN;
         
         set(ydata01(i), 'YData', ydata1{i});
        
    end
    
end

if and(lt(c_max(11), c_max(12)), ~isempty(ydata2))
    
    axes(handles.axes2);
    for i = 1:size(ydata2, 1)
       
         ydata2{i}(ydata2{i} < c_max(11)) = NaN;
         ydata2{i}(ydata2{i} > c_max(12)) = NaN;
         
         set(ydata02(i), 'YData', ydata2{i});
        
    end
    
end

if ~isempty(legendAxes2)
    
    axes(handles.axes2);
    legend(legendAxes2, 'Interpreter', 'none');
    
end
    
if ~isempty(legendAxes1)
    
    axes(handles.axes1);    
    legend(legendAxes1, 'Interpreter', 'none');
    
end