function loadTRFSPreprocessResults(handles)

axes(handles.axes1);
cla reset;
axis on
box on
hold off

axes(handles.axes2);
cla reset;
legend off
axis on
box on
hold off

set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');

quest1 = questdlg('Do you want to load a folder or specific files?',...
    'What to Load?', 'Folder', 'Files', 'Folder');

if strcmp(quest1,'Folder')
    
    foldername = uigetdir('', 'Please Select the TRFS Preprocess Results Folder');
    if ~foldername
        
        return
        
    end
    
    [outputNames, outputResults] = loadPreResultsTRFSFolder(foldername);
    
elseif strcmp(quest1,'Files')
    
    [filename, foldername] = uigetfile('.mat', 'Please Select the TRFS Preprocess Results Files', 'Multiselect', 'on');
    if ~foldername
        
        return
        
    end
    
    if ~iscell(filename)
        
        filename = {filename};
        
    end
    
    [outputNames, outputResults] = loadPreResultsTRFSFiles(foldername, filename);
    
    if isempty(outputNames)
        
        warndlg('Please select TRFS preprocess results files');
        return
        
    end
    
else
    
    return
    
end

if isempty(outputNames)
    
    warndlg('The folder does not contain any data...');
    return;
    
else
    
    setappdata(handles.figure1, 'outputNames_TRFS', outputNames)
    setappdata(handles.figure1, 'outputResults_TRFS', outputResults)
    
end