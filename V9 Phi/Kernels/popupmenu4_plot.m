function popupmenu4_plot(handles)

setappdata(handles.figure1, 'BlackPlot2TRIPLEX', []);

axes(handles.axes2);
cla reset;
hold off
axis on
box on

popupmenu4_Value = get(handles.popupmenu4, 'Value');
if ne(popupmenu4_Value, 1)
    
    L_num = get(handles.popupmenu4, 'String');
    L_num = L_num{popupmenu4_Value};
    L_num1 = str2double(regexp(L_num, '\d+', 'match'));
    
end

flagSlider = getappdata(handles.figure1, 'sliderFlag');

inputData = getappdata(handles.figure1, 'TRIPLEXresultsData');
inputNames = getappdata(handles.figure1, 'TRIPLEXrawDataNames');
prefixes = inputNames.prefixes;

if eq(get(handles.radiobutton12, 'Value'), 1)
    
    col = jet(size(inputNames.filenames, 1));
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        col0 = jet(size(inputNames.filenames, 1));
        cmin = min(groupnum);
        cmax = max(groupnum);
        idx = min(size(inputNames.filenames, 1), round((size(inputNames.filenames, 1) - 1) * (groupnum - cmin) / (cmax - cmin)) + 1);
        col = col0(idx, :);
        
    catch err
        
        col = jet(size(inputNames.filenames, 1));
        
    end
    
end

if gt(size(inputNames.filenames, 1), 1)
    
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'on');
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(inputNames.filenames, 1), 'SliderStep', [(1 / (size(inputNames.filenames, 1) - 1)) (1 / (size(inputNames.filenames, 1) - 1))]);
    flagSlider = 3;
    
end

max_ylim_value = zeros(size(inputNames.filenames, 1), 1);
min_ylim_value = zeros(size(inputNames.filenames, 1), 1);

for i = 1:size(inputNames.filenames, 1)
    
    SNR = inputData.SNR{i};
    
    axes(handles.axes2);
    
    if eq(popupmenu4_Value, 1)
        
        yaxis1_value = inputData.spec_int{i};
        
        % Get the nanmean values of intensity
        % -----------------------------------------------------------------------------------------------------
        
        if eq(get(handles.radiobutton13, 'Value'), 1)
            
            if ~isempty(yaxis1_value{1})
                
                y_axis1_mean(1) = nanmean(yaxis1_value{1});
                y_axis1_std(1) = nanstd(yaxis1_value{1});
                
            else
                
                y_axis1_mean(1) = NaN;
                y_axis1_std(1) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_mean(2) = nanmean(yaxis1_value{2});
                y_axis1_std(2) = nanstd(yaxis1_value{2});
                
            else
                
                y_axis1_mean(2) = NaN;
                y_axis1_std(2) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_mean(3) = nanmean(yaxis1_value{3});
                y_axis1_std(3) = nanstd(yaxis1_value{3});
                
            else
                
                y_axis1_mean(3) = NaN;
                y_axis1_std(3) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_mean(4) = nanmean(yaxis1_value{4});
                y_axis1_std(4) = nanstd(yaxis1_value{4});
                
            else
                
                y_axis1_mean(4) = NaN;
                y_axis1_std(4) = NaN;
                
            end
            
            max_ylim_value(i, 1) = max(y_axis1_mean + y_axis1_std);
            min_ylim_value(i, 1) = min(y_axis1_mean - abs(y_axis1_std));
            
            % Else get the points
            % -----------------------------------------------------------------------------------------------------
            
        else
            
            if ~isempty(yaxis1_value{1})
                
                y_axis1_1 = yaxis1_value{1};
                
            else
                
                y_axis1_1 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_2 = yaxis1_value{2};
                
            else
                
                y_axis1_2 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_3 = yaxis1_value{3};
                
            else
                
                y_axis1_3 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_4 = yaxis1_value{4};
                
            else
                
                y_axis1_4 = NaN;
                
            end
            
            if ne(max(size(y_axis1_1)), 1)
                
                x_axis_values = (1:size(y_axis1_1, 1))';
                
            elseif ne(max(size(y_axis1_2)), 1)
                
                x_axis_values = (1:size(y_axis1_2, 1))';
                
            elseif ne(max(size(y_axis1_3)), 1)
                
                x_axis_values = (1:size(y_axis1_3, 1))';
                
            elseif ne(max(size(y_axis1_4)), 1)
                
                x_axis_values = (1:size(y_axis1_4, 1))';
                
            end
            
            x_axis_values = x_axis_values - 1;
            
            if ne(max(size(y_axis1_1)), 1)
                
                SNR_0_1 = SNR{1};
                SNR1 = 50 * ones(size(SNR_0_1, 1), 1);
                SNR1(SNR_0_1 <= 20) = 20;
                SNR1(SNR_0_1 >= 60) = 100;
                
            else
                
                y_axis1_1 = NaN * ones(size(x_axis_values));
                SNR1 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_2)), 1)
                
                SNR_0_2 = SNR{2};
                SNR2 = 50 * ones(size(SNR_0_2, 1), 1);
                SNR2(SNR_0_2 <= 20) = 20;
                SNR2(SNR_0_2 >= 60) = 100;
                
            else
                
                y_axis1_2 = NaN * ones(size(x_axis_values));
                SNR2 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_3)), 1)
                
                SNR_0_3 = SNR{3};
                SNR3 = 50 * ones(size(SNR_0_3, 1), 1);
                SNR3(SNR_0_3 <= 20) = 20;
                SNR3(SNR_0_3 >= 60) = 100;
                
            else
                
                y_axis1_3 = NaN * ones(size(x_axis_values));
                SNR3 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_4)), 1)
                
                SNR_0_4 = SNR{4};
                SNR4 = 50 * ones(size(SNR_0_4, 1), 1);
                SNR4(SNR_0_4 <= 20) = 20;
                SNR4(SNR_0_4 >= 60) = 100;
                
            else
                
                y_axis1_4 = NaN * ones(size(x_axis_values));
                SNR4 = ones(size(x_axis_values));
                
            end
            
            max_ylim_value(i, 1) = nanmax([y_axis1_1; y_axis1_2; y_axis1_3; y_axis1_4]);
            min_ylim_value(i, 1) = nanmin([y_axis1_1; y_axis1_2; y_axis1_3; y_axis1_4]);
        
        end
                
        set(handles.Title2, 'String', 'Integrated Intensity');
        ylabel('Integrated Intensity (a.u.)')
        ylim([0 35]); 
        
    else
        
        yaxis1_value = inputData.cc{i};
        
        % Get Laguerre
        % -----------------------------------------------------------------------------------------------------
        
        if eq(get(handles.radiobutton13, 'Value'), 1)
            
            if ~isempty(yaxis1_value{1})
                
                y_axis1_mean(1) = nanmean(yaxis1_value{1}{L_num1});
                y_axis1_std(1) = nanstd(yaxis1_value{1}{L_num1});
                
            else
                
                y_axis1_mean(1) = NaN;
                y_axis1_std(1) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_mean(2) = nanmean(yaxis1_value{2}{L_num1});
                y_axis1_std(2) = nanstd(yaxis1_value{2}{L_num1});
                
            else
                
                y_axis1_mean(2) = NaN;
                y_axis1_std(2) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_mean(3) = nanmean(yaxis1_value{3}{L_num1});
                y_axis1_std(3) = nanstd(yaxis1_value{3}{L_num1});
                
            else
                
                y_axis1_mean(3) = NaN;
                y_axis1_std(3) = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_mean(4) = nanmean(yaxis1_value{4}{L_num1});
                y_axis1_std(4) = nanstd(yaxis1_value{4}{L_num1});
                
            else
                
                y_axis1_mean(4) = NaN;
                y_axis1_std(4) = NaN;
                
            end
            
            max_ylim_value(i, 1) = max(y_axis1_mean + y_axis1_std);
            min_ylim_value(i, 1) = min(y_axis1_mean - abs(y_axis1_std));
            
            else
            
            % Else get the Laguerres
            % -----------------------------------------------------------------------------------------------------
            
            
            if ~isempty(yaxis1_value{1})
                
                y_axis1_1 = yaxis1_value{1}{L_num1};
                
            else
                
                y_axis1_1 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{2})
                
                y_axis1_2 = yaxis1_value{2}{L_num1};
                
            else
                
                y_axis1_2 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{3})
                
                y_axis1_3 = yaxis1_value{3}{L_num1};
                
            else
                
                y_axis1_3 = NaN;
                
            end
            
            if ~isempty(yaxis1_value{4})
                
                y_axis1_4 = yaxis1_value{4}{L_num1};
                
            else
                
                y_axis1_4 = NaN;
                
            end
            
            if ne(max(size(y_axis1_1)), 1)
                
                x_axis_values = (1:size(y_axis1_1, 2))';
                
            elseif ne(max(size(y_axis1_2)), 1)
                
                x_axis_values = (1:size(y_axis1_2, 2))';
                
            elseif ne(max(size(y_axis1_3)), 1)
                
                x_axis_values = (1:size(y_axis1_3, 2))';
                
            elseif ne(max(size(y_axis1_4)), 1)
                
                x_axis_values = (1:size(y_axis1_4, 2))';
                
            end
            
            x_axis_values = x_axis_values - 1;
            
            if ne(max(size(y_axis1_1)), 1)
                
                SNR_0_1 = SNR{1};
                SNR1 = 50 * ones(size(SNR_0_1, 1), 1);
                SNR1(SNR_0_1 <= 20) = 20;
                SNR1(SNR_0_1 >= 60) = 100;
                
            else
                
                y_axis1_1 = NaN * ones(size(x_axis_values))';
                SNR1 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_2)), 1)
                
                SNR_0_2 = SNR{2};
                SNR2 = 50 * ones(size(SNR_0_2, 1), 1);
                SNR2(SNR_0_2 <= 20) = 20;
                SNR2(SNR_0_2 >= 60) = 100;
                
            else
                
                y_axis1_2 = NaN * ones(size(x_axis_values))';
                SNR2 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_3)), 1)
                
                SNR_0_3 = SNR{3};
                SNR3 = 50 * ones(size(SNR_0_3, 1), 1);
                SNR3(SNR_0_3 <= 20) = 20;
                SNR3(SNR_0_3 >= 60) = 100;
                
            else
                
                y_axis1_3 = NaN * ones(size(x_axis_values))';
                SNR3 = ones(size(x_axis_values));
                
            end
            
            if ne(max(size(y_axis1_4)), 1)
                
                SNR_0_4 = SNR{4};
                SNR4 = 50 * ones(size(SNR_0_4, 1), 1);
                SNR4(SNR_0_4 <= 20) = 20;
                SNR4(SNR_0_4 >= 60) = 100;
                
            else
                
                y_axis1_4 = NaN * ones(size(x_axis_values))';
                SNR4 = ones(size(x_axis_values));
                
            end
            
            max_ylim_value(i, 1) = nanmax([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']);
            min_ylim_value(i, 1) = nanmin([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']);
            
        end
        
        set(handles.Title2, 'String', ['Laguerre Coeffs ', num2str(L_num)]);
        ylabel('Laguerre Coeffs (a.u.)')
        ylim([0 1]); 
        
    end
    
    if eq(flagSlider, 3)
        
        set(handles.slider2, 'Value', i);
        
    end
    
    SNR_index = ones(1, 4);
    
     if eq(get(handles.checkbox1, 'Value'), 0)
         
         if eq(get(handles.radiobutton13, 'Value'), 1)        
             
             y_axis1_mean(1) = NaN;
             y_axis1_std(1) = NaN;
             max_ylim_value(i, 1) = max(y_axis1_mean + y_axis1_std);
             min_ylim_value(i, 1) = min(y_axis1_mean - abs(y_axis1_std));
             
         else
             
             y_axis1_1 = NaN * ones(size(y_axis1_1));
             max_ylim_value(i, 1) = nanmax(nanmax([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
             min_ylim_value(i, 1) = nanmin(nanmin([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
             
         end
         
         SNR_index(1) = 0;
        
    end
    
    if eq(get(handles.checkbox2, 'Value'), 0)
        
        if eq(get(handles.radiobutton13, 'Value'), 1)        
            
            y_axis1_mean(2) = NaN;
            y_axis1_std(2) = NaN;
            max_ylim_value(i, 1) = max(y_axis1_mean + y_axis1_std);
            min_ylim_value(i, 1) = min(y_axis1_mean - abs(y_axis1_std));
            
        else
            
            y_axis1_2 = NaN * ones(size(y_axis1_2));
            max_ylim_value(i, 1) = nanmax(nanmax([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
            min_ylim_value(i, 1) = nanmin(nanmin([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
            
        end
        
        SNR_index(2) = 0;
        
    end
    
    if eq(get(handles.checkbox3, 'Value'), 0)
        
        if eq(get(handles.radiobutton13, 'Value'), 1)        
            
            y_axis1_mean(3) = NaN;
            y_axis1_std(3) = NaN;
            max_ylim_value(i, 1) = max(y_axis1_mean + y_axis1_std);
            min_ylim_value(i, 1) = min(y_axis1_mean - abs(y_axis1_std));
            
        else
            
            y_axis1_3 = NaN * ones(size(y_axis1_3));
            max_ylim_value(i, 1) = nanmax(nanmax([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
            min_ylim_value(i, 1) = nanmin(nanmin([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
            
        end
        
        SNR_index(3) = 0;
        
    end
    
    if eq(get(handles.checkbox4, 'Value'), 0)
        
        if eq(get(handles.radiobutton13, 'Value'), 1)        
            
            y_axis1_mean(4) = NaN;
            y_axis1_std(4) = NaN;
            max_ylim_value(i, 1) = max(y_axis1_mean + y_axis1_std);
            min_ylim_value(i, 1) = min(y_axis1_mean - abs(y_axis1_std));
            
        else
            
            y_axis1_4 = NaN * ones(size(y_axis1_4));
            max_ylim_value(i, 1) = nanmax(nanmax([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
            min_ylim_value(i, 1) = nanmin(nanmin([y_axis1_1'; y_axis1_2'; y_axis1_3'; y_axis1_4']));
            
        end
        
        SNR_index(4) = 0;
        
    end
    
   if eq(get(handles.radiobutton13, 'Value'), 1)
        
        hold on
        errorbar(1:4, [y_axis1_mean(1) y_axis1_mean(2) y_axis1_mean(3) y_axis1_mean(4)], [y_axis1_std(1) y_axis1_std(2) y_axis1_std(3) y_axis1_std(4)], 'k.', 'MarkerSize', 15, 'color', col(i, :));
        plot(1:4, y_axis1_mean, 'k--', 'color', col(i, :));
        hold off
        
    else
        
        hold on
        plot(x_axis_values, y_axis1_1, 'b-', 'color', col(i,:));
        plot(x_axis_values, y_axis1_2, 'b--', 'color', col(i,:));
        plot(x_axis_values, y_axis1_3, 'b-.', 'color', col(i,:));
        plot(x_axis_values, y_axis1_4, 'b-o', 'color', col(i,:));
        hold off
        
    end
    
    if eq(flagSlider, 3)
        
        set(handles.slider2, 'enable', 'on');
        
    end
    
    pause(0.01)
    
end

A = cell(size(inputData.spec_int, 1), 1);
x_num_points = zeros(size(inputData.spec_int, 1), 1);
for j = 1:size(A, 1)
    
    A{j} = cell2mat(inputData.spec_int{j});
    x_num_points(j) = size(A{j}, 1) - 1;
    
end
clear A

SNR01 = inputData.SNR{i};
SNR01 = cell2mat(SNR01(logical(SNR_index)));
less20dB = find(le(SNR01, 20));
str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 1))];
set(handles.text27, 'String', str_less20dB);

great60dB = find(ge(SNR01, 60));
str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 1))];
set(handles.text29, 'String', str_great60dB);

str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR01(:), 1) - size(great60dB, 1) - size(less20dB, 1))];
set(handles.text28, 'String', str_between2060dB);

axes(handles.axes2);
if eq(get(handles.radiobutton13, 'Value'), 1)
    
    hold on
    h2(1) = errorbar(1:4, [y_axis1_mean(1) y_axis1_mean(2) y_axis1_mean(3) y_axis1_mean(4)], [y_axis1_std(1) y_axis1_std(2) y_axis1_std(3) y_axis1_std(4)], 'k.', 'MarkerSize', 20);
    h2(2) = plot(1:4, y_axis1_mean, 'k--', 'LineWidth', 3);
    x_axis_values = {'Ch1' 'Ch2' 'Ch3' 'Ch4'};
    set(gca, 'XTick', [1:4]);
    set(gca, 'XTickLabel', x_axis_values);
    hold off
    
else
    
    hold on
    h2(1) = plot(x_axis_values, y_axis1_1, 'k-');
    h2(2) = plot(x_axis_values, y_axis1_2, 'k--');
    h2(3) = plot(x_axis_values, y_axis1_3, 'k-.');
    h2(4) = plot(x_axis_values, y_axis1_4, 'k-o');
%     legend({'CH1' 'CH2' 'CH3' 'CH4'});
    h2(5) = scatter(x_axis_values, y_axis1_1', SNR1, 'k', 'fill');
    h2(6) = scatter(x_axis_values, y_axis1_2', SNR2, 'k', 'fill');
    h2(7) = scatter(x_axis_values, y_axis1_3', SNR3, 'k', 'fill');
    h2(8) = scatter(x_axis_values, y_axis1_4', SNR4, 'k', 'fill');
    xlim([0 max(x_num_points)]);
    hold off
    
end

if isnan(nanmin(min_ylim_value))
    
    ylim([0 1])
    
else
    
    ylim([nanmin(min_ylim_value) nanmax(max_ylim_value)]);
    
end
 
axes(handles.axes2);
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{i}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        legend({[num2str(groupnum(i)) '_' prefixes{i}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{i}}, 'Interpreter', 'none');
        
    end
    
end
setappdata(handles.figure1, 'BlackPlot2TRIPLEX', h2);

setappdata(handles.figure1, 'sliderFlag', flagSlider);