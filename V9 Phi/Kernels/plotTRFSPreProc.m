function plotTRFSPreProc(handles)

% Initialization
inputNames = getappdata(handles.figure1, 'outputNames_TRFS');
if isempty(inputNames)
    return
end
prefixes = inputNames.prefices;
groupnums = inputNames.groupnums;
waveNum_Step = inputNames.waveNum_Step;

inputTRFSData = getappdata(handles.figure1, 'outputResults_TRFS');
wave_step = waveNum_Step(:, 2);

% Reset Axis
axes(handles.axes1);
cla reset;
hold off
axis on
box on

axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on

% Colormap
if ne(unique(groupnums), 0)
    
    col = colorcube(5 * max(groupnums));
    un_groupnums = groupnums;
    
else
    
    col = jet(size(groupnums, 1));
    un_groupnums = 1:size(groupnums, 1);
    
end

% Axis titles
set(handles.Title1, 'String', 'Integrated Intensity');

% Flags for slider and plot type
flagSlider = 0;

intIntensity = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));
wavelengths = NaN * ones(max(waveNum_Step(:, 1)), size(groupnums, 1));

for i = 1:size(groupnums, 1)
    
    intIntensity(1:size(inputTRFSData{i}.intIntensity, 1), i) = inputTRFSData{i}.intIntensity;
    wavelengths(1:size(inputTRFSData{i}. wavelengths_list, 1), i) = inputTRFSData{i}. wavelengths_list;
        
end

 if eq(get(handles.radiobutton4, 'Value'), 1)
     
     for i = 1:size(intIntensity, 2)
         
         intIntensity(:, i) = intIntensity(:, i)./norm(intIntensity(isfinite(intIntensity(:, i)), i));
         
     end
     
end

if eq(get(handles.checkbox1, 'Value'), 1)
    
    un_groupnums = unique(groupnums);
    
    intIntensity0 = NaN * ones(max(waveNum_Step(:, 1)), size(un_groupnums, 1));
    wavelengths0 = NaN * ones(max(waveNum_Step(:, 1)), size(un_groupnums, 1));
    std_intIntensity = intIntensity0;
    
    for i = 1:size(un_groupnums, 1)
        
        f_un_labels = find(eq(groupnums, un_groupnums(i)));
        
        intIntensity0(:, i) = nanmean(intIntensity(:, f_un_labels), 2);
        wavelengths0(:, i) = nanmean(wavelengths(:, f_un_labels), 2);
        std_intIntensity(:, i) = nanstd(intIntensity(:, f_un_labels), [], 2);
        
    end
    
    intIntensity = intIntensity0;
    wavelengths = wavelengths0;
    
end

% If more than one measurements open slider
if gt(size(intIntensity, 2), 1)
    
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'on');
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(intIntensity, 2), 'SliderStep', [(1 / (size(intIntensity, 2) - 1)) (1 / (size(intIntensity, 2) - 1))]);
    flagSlider = 1;
    
end

axes(handles.axes1);
if eq(get(handles.radiobutton4, 'Value'), 1)
    
    ylim([0 nanmax(intIntensity(:))]);
    
else
    
    if eq(get(handles.checkbox1, 'Value'), 1)        
        
        ylim([0 (nanmax(intIntensity(:) + std_intIntensity(:)))]);
        
    else
        
        ylim([0 (nanmax(intIntensity(:)) + 1)]);
        
    end
    
end

for i = 1:size(intIntensity, 2)
    
    if eq(flagSlider, 1)
        
        set(handles.slider2, 'Value', i);
        
    end
    
    if or(eq(get(handles.radiobutton4, 'Value'), 1), eq(get(handles.radiobutton5, 'Value'), 1))
        
        x_axes1_values = unique(wavelengths(isfinite(wavelengths(:, i)), i));
        y_axes1_values = intIntensity(isfinite(intIntensity(:, i)), i);
        
        if eq(get(handles.radiobutton1, 'Value'), 1)
            
            axes(handles.axes1);
            hold on            
            plot(x_axes1_values, y_axes1_values, 'color', col(un_groupnums(i),:))
            xlabel('Wavelengths (nm)');
            ylabel('Normalized Intensity (a.u.)')
            xlim([nanmin(wavelengths(:)) nanmax(wavelengths(:))]);
            hold off
            
        elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
            
            xx = linspace(nanmin(wavelengths(:, i)), nanmax(wavelengths(:, i)), (nanmax(wavelengths(:, i)) - nanmin(wavelengths(:, i))));
            
            % 2.6 Apply spline interpolation on the plane x-y (use
            % intIntensity(1:size(t, 1), i)./norm(intIntensity(1:size(t, 1),
            % i)) because of the NaNs
            if eq(get(handles.checkbox1, 'Value'), 1)
                
                wave_step_ind = find(eq(groupnums, un_groupnums(i)));
                
            else
                
                wave_step_ind = i;
                
            end
            [~, pp] = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i));
            yy = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i), pp);
            y1 = fnval(yy, xx);
            
            axes(handles.axes1);
            hold on
            plot(xx, y1, 'color', col(un_groupnums(i),:))
            xlabel('Wavelengths (nm)');
            ylabel('Normalized Intensity (a.u.)')            
            xlim([nanmin(wavelengths(:)) nanmax(wavelengths(:))])
            hold off
            
        end
        
    end
    
    pause(.01)
    clear spec spec0 t noise
    
end

if or(eq(get(handles.radiobutton4, 'Value'), 1), eq(get(handles.radiobutton5, 'Value'), 1))
    
    x_axes1_values = unique(wavelengths(isfinite(wavelengths(:, i)), i));
    y_axes1_values = intIntensity(isfinite(intIntensity(:, i)), i);
    axes(handles.axes1);
    xlim([nanmin(wavelengths(:)) nanmax(wavelengths(:))])
    
    if eq(get(handles.radiobutton1, 'Value'), 1)
        
        hold on        
        if eq(get(handles.checkbox1, 'Value'), 1)
            
            h(1) = errorbar(x_axes1_values, y_axes1_values, std_intIntensity(isfinite(std_intIntensity(:, i)), i), 'k-', 'LineWidth', 3);
            
        else
            
            h(1) = plot(x_axes1_values, y_axes1_values, 'k-', 'LineWidth', 3);
            
        end
        h(2) = h(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
        
        xx = linspace(nanmin(wavelengths(:, i)), nanmax(wavelengths(:, i)), (nanmax(wavelengths(:, i)) - nanmin(wavelengths(:, i))));
        
        % 2.6 Apply spline interpolation on the plane x-y
        if eq(get(handles.checkbox1, 'Value'), 1)
            
            wave_step_ind = find(eq(groupnums, un_groupnums(i)));
            
        else
            
            wave_step_ind = i;
            
        end
        [~, pp] = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i));
        yy = csaps(nanmin(wavelengths(:, i)):wave_step(wave_step_ind(1)):nanmax(wavelengths(:, i)), intIntensity(isfinite(intIntensity(:, i)), i), pp);
        y1 = fnval(yy, xx);
        
        axes(handles.axes1);
        hold on
        h(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton3, 'Value'), 1)
            
            if eq(get(handles.checkbox1, 'Value'), 1)
                
                h(2) = errorbar(unique(wavelengths(isfinite(intIntensity(:, i)), i)), intIntensity(isfinite(intIntensity(:, i)), i), std_intIntensity(isfinite(std_intIntensity(:, i)), i), 'k.', 'LineWidth', 3);
                
            else
                
                h(2) = plot(unique(wavelengths(:, i)), intIntensity(:, i), 'k.', 'MarkerSize', 15);
                
            end
            
        else
            
            h(2) = h(1);
            
        end
        
        hold off
        
    end
    

end

if eq(get(handles.checkbox1, 'Value'), 0)
    
    legend({prefixes{i}}, 'Interpreter', 'none');
    
else
    
    legend({num2str(un_groupnums(i))}, 'Interpreter', 'none');
    
end

setappdata(handles.figure1, 'BlackPlot', h);
setappdata(handles.figure1, 'sliderFlag', flagSlider);

if eq(flagSlider, 1)
    
    set(handles.slider2, 'enable', 'on');
    setappdata(handles.figure1, 'PB_flags', [get(handles.radiobutton4, 'Value') get(handles.radiobutton5, 'Value')]);
    
end