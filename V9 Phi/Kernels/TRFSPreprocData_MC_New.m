function flagvalue = TRFSPreprocData_MC_New(handles)

quest1 = questdlg('Do you want to load a folder or specific files?',...
    'What to Load?', 'Folder', 'Files', 'Folder');

if strcmp(quest1,'Folder')
    
    foldername = uigetdir('', 'Please Select the Raw Data Folder from TRFS');
    if ~foldername
        
        flagvalue = 1;
        return
        
    end
    
    outputNames = loadRawTRFSFolder_MC_New(foldername, handles);
    
elseif strcmp(quest1,'Files')
    
    [filename, foldername] = uigetfile('*.*', 'Please Select the TRFS Header Files', 'Multiselect', 'on');
    if ~foldername
        
        flagvalue = 1;
        return
        
    end
    
    if ~iscell(filename)
        
        filename = {filename};
        
    end
    
    outputNames = loadRawTRFSFiles_MC_New(foldername, filename, handles);
    
    if isempty(outputNames)
        
        warndlg('Please select TRFS files');
        flagvalue = 1;
        return
        
    end
    
else
    
    flagvalue = 1;
    return
    
end

if isempty(outputNames)
    
    warndlg('The folder does not contain any data...');
    flagvalue = 1;
    return;
    
else
    
    setappdata(handles.figure1, 'outputNames', outputNames)
    set(handles.text3, 'String', strcat(['Number of Spectra: ', num2str(outputNames.wavelengths(end, 2))]));
    flagvalue = 2;
    
end