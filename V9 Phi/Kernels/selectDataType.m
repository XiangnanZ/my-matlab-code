function flagvalue = selectDataType(handles)

axes(handles.axes1);
cla reset;
axis on
box on
hold off

axes(handles.axes2);
cla reset;
legend off
axis on
box on
hold off

set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');

flagvalue = TRFSPreprocData_MC_New(handles);
    
end