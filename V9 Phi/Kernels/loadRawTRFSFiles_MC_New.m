function output = loadRawTRFSFiles_MC_New(foldername, nameListing, handles)

% 3. Keep only the data files
find_data = strfind(nameListing, '_header');
nameListing(cellfun('isempty', find_data)) = [];
find_data = strfind(nameListing, '_header');

if ne(size(nameListing, 2), 0)
    
    wavelengths = cell(size(nameListing, 2), 1);
    filenames_data = cell(size(nameListing, 2), 1);
    prefixes = cell(size(nameListing, 2), 1);
    timeResolution = cell(size(nameListing, 2), 1);
    bw = cell(size(nameListing, 2), 1);
    SysID = cell(size(nameListing, 2), 1);
    
     for i = 1:size(nameListing, 2)
        
        str_data = cell2mat(nameListing(i));
        str_data = str_data(1:(find_data{i} - 1));
        filenames_data{i, 1} = fullfile(foldername, str_data);
        prefixes{i, 1} = str_data;
        
        [wv_load, ~] = DataLoader(filenames_data{i, 1});
        set(handles.edit2, 'String', num2str(wv_load.TimeResolution));
        set(handles.edit5, 'String', num2str(wv_load.BW));
        
        wavelengths{i}(:, 1) = wv_load.Wavelength;
        wavelengths{i}(:, 2) = i;
        wavelengths{i}(:, 3) = wv_load.GainVoltage;
        
        timeResolution{i} = wv_load.TimeResolution;
        bw{i} = wv_load.BW;
        SysID{i} = wv_load.SysID;
        
    end
         
    suffix = '.mat';
    
    output.wavelengths = cell2mat(wavelengths);
    output.suffix = suffix;
    output.filenames = filenames_data;
    output.prefixes = prefixes;
    output.timeResolution = timeResolution;
    output.bw = bw;
    output.SysID = SysID;
    
else
    
    output = [];
    
end