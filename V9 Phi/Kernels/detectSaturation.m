function dataInd = detectSaturation(handles, spec)

data = - spec;

dataInd = zeros(size(data, 2), 1);
k = 1;

minVal = str2double(get(handles.edit21, 'String'));
maxVal = str2double(get(handles.edit20, 'String'));

for i = 1:size(data, 2)
    
    if ge(max(data(:, i)), maxVal)
        
        warning('off')
        [pks, ~] = find(ge(data(:, i), (maxVal - eps)));
        
        if and(~isempty(pks), ge(size(pks, 1), 2))
            
            if lt(abs(min(diff(pks))), 3)
                
                dataInd(k) = i;
                k = k + 1;
                
            end
         
        end
        
    elseif le(max(data(:, i)), minVal)
        
        dataInd(k) = i;
        k = k + 1;
        
    end

end

dataInd(k:end) = [];