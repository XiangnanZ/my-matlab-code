function sliderTRIPLEXProc(handles, ind)

inputNames = getappdata(handles.figure1, 'TRIPLEXrawDataNames');
prefixes = inputNames.prefixes;

inputData = getappdata(handles.figure1, 'TRIPLEXresultsData');
SNR = inputData.SNR{ind};

popupmenu3_Value = get(handles.popupmenu3, 'Value');
if ne(popupmenu3_Value, 1)
    
    L_num1 = get(handles.popupmenu3, 'String');
    L_num1 = L_num1{popupmenu3_Value};
    L_num1 = str2double(regexp(L_num1, '\d+', 'match'));
    
end

popupmenu4_Value = get(handles.popupmenu4, 'Value');
if ne(popupmenu4_Value, 1)
    
    L_num2 = get(handles.popupmenu4, 'String');
    L_num2 = L_num2{popupmenu4_Value};
    L_num2 = str2double(regexp(L_num2, '\d+', 'match'));
    
end

% Top Axis
% --------------------------------------------------------------------------------------------------------
if eq(popupmenu3_Value, 1)
    
    yaxis1_value = inputData.lifet_avg{ind};
    
    % Get the nanmean values of lifetime
    % -----------------------------------------------------------------------------------------------------
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
    
        if ~isempty(yaxis1_value{1})
            
            y_axis1_mean(1) = nanmean(yaxis1_value{1});
            y_axis1_std(1) = nanstd(yaxis1_value{1});
            
        else
            
            y_axis1_mean(1) = NaN;
            y_axis1_std(1) = NaN;
            
        end
        
        if ~isempty(yaxis1_value{2})
            
            y_axis1_mean(2) = nanmean(yaxis1_value{2});
            y_axis1_std(2) = nanstd(yaxis1_value{2});
            
        else
            
            y_axis1_mean(2) = NaN;
            y_axis1_std(2) = NaN;
            
        end
        
        if ~isempty(yaxis1_value{3})
            
            y_axis1_mean(3) = nanmean(yaxis1_value{3});
            y_axis1_std(3) = nanstd(yaxis1_value{3});
            
        else
            
            y_axis1_mean(3) = NaN;
            y_axis1_std(3) = NaN;
            
        end
        
        if ~isempty(yaxis1_value{4})
            
            y_axis1_mean(4) = nanmean(yaxis1_value{4});
            y_axis1_std(4) = nanstd(yaxis1_value{4});
            
        else
            
            y_axis1_mean(4) = NaN;
            y_axis1_std(4) = NaN;
            
        end
        
    % Else get the points
    % -----------------------------------------------------------------------------------------------------
        
    else
        
        if ~isempty(yaxis1_value{1})
            
            y_axis1_1 = yaxis1_value{1};
            
        else
            
            y_axis1_1 = NaN;
            
        end
        
        if ~isempty(yaxis1_value{2})
            
            y_axis1_2 = yaxis1_value{2};
            
        else
            
            y_axis1_2 = NaN;
            
        end
        
        if ~isempty(yaxis1_value{3})
            
            y_axis1_3 = yaxis1_value{3};
            
        else
            
            y_axis1_3 = NaN;
            
        end
        
        if ~isempty(yaxis1_value{4})
            
            y_axis1_4 = yaxis1_value{4};
            
        else
            
            y_axis1_4 = NaN;
            
        end
        
        if ne(max(size(y_axis1_1)), 1)
            
            x_axis_values = (1:size(y_axis1_1, 1))';
            
        elseif ne(max(size(y_axis1_2)), 1)
            
            x_axis_values = (1:size(y_axis1_2, 1))';
            
        elseif ne(max(size(y_axis1_3)), 1)
            
            x_axis_values = (1:size(y_axis1_3, 1))';
            
        elseif ne(max(size(y_axis1_4)), 1)
            
            x_axis_values = (1:size(y_axis1_4, 1))';
            
        end
        
        x_axis_values = x_axis_values - 1;
        
        if ne(max(size(y_axis1_1)), 1)
            
            SNR_0_1 = SNR{1};
            SNR1 = 50 * ones(size(SNR_0_1, 1), 1);
            SNR1(SNR_0_1 <= 20) = 20;
            SNR1(SNR_0_1 >= 60) = 100;
            
        else
            
            y_axis1_1 = NaN * ones(size(x_axis_values));
            SNR1 = ones(size(x_axis_values));
            
        end
        
        if ne(max(size(y_axis1_2)), 1)
            
            SNR_0_2 = SNR{2};
            SNR2 = 50 * ones(size(SNR_0_2, 1), 1);
            SNR2(SNR_0_2 <= 20) = 20;
            SNR2(SNR_0_2 >= 60) = 100;
            
        else
            
            y_axis1_2 = NaN * ones(size(x_axis_values));
            SNR2 = ones(size(x_axis_values));
            
        end
        
        if ne(max(size(y_axis1_3)), 1)
            
            SNR_0_3 = SNR{3};
            SNR3 = 50 * ones(size(SNR_0_3, 1), 1);
            SNR3(SNR_0_3 <= 20) = 20;
            SNR3(SNR_0_3 >= 60) = 100;
            
        else
            
            y_axis1_3 = NaN * ones(size(x_axis_values));
            SNR3 = ones(size(x_axis_values));
            
        end
        
        if ne(max(size(y_axis1_4)), 1)
            
            SNR_0_4 = SNR{4};
            SNR4 = 50 * ones(size(SNR_0_4, 1), 1);
            SNR4(SNR_0_4 <= 20) = 20;
            SNR4(SNR_0_4 >= 60) = 100;
            
        else
            
            y_axis1_4 = NaN * ones(size(x_axis_values));
            SNR4 = ones(size(x_axis_values));
            
        end        
        
    end
    
else
    
    yaxis1_value = inputData.cc{ind};
    
    % Get Laguerre
    % -----------------------------------------------------------------------------------------------------
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        if ~isempty(yaxis1_value{1})
            
            y_axis1_mean(1) = nanmean(yaxis1_value{1}{L_num1});
            y_axis1_std(1) = nanstd(yaxis1_value{1}{L_num1});
            
        else
            
            y_axis1_mean(1) = NaN;
            y_axis1_std(1) = NaN;
            
        end
        
        if ~isempty(yaxis1_value{2})
            
            y_axis1_mean(2) = nanmean(yaxis1_value{2}{L_num1});
            y_axis1_std(2) = nanstd(yaxis1_value{2}{L_num1});
            
        else
            
            y_axis1_mean(2) = NaN;
            y_axis1_std(2) = NaN;
            
        end
        
        if ~isempty(yaxis1_value{3})
            
            y_axis1_mean(3) = nanmean(yaxis1_value{3}{L_num1});
            y_axis1_std(3) = nanstd(yaxis1_value{3}{L_num1});
            
        else
            
            y_axis1_mean(3) = NaN;
            y_axis1_std(3) = NaN;
            
        end
        
        if ~isempty(yaxis1_value{4})
            
            y_axis1_mean(4) = nanmean(yaxis1_value{4}{L_num1});
            y_axis1_std(4) = nanstd(yaxis1_value{4}{L_num1});
            
        else
            
            y_axis1_mean(4) = NaN;
            y_axis1_std(4) = NaN;
            
        end
        
    % Else get the Laguerres
    % -----------------------------------------------------------------------------------------------------
    
    else
        
        if ~isempty(yaxis1_value{1})
            
            y_axis1_1 = yaxis1_value{1}{L_num1};
            
        else
            
            y_axis1_1 = NaN;
            
        end
        
        if ~isempty(yaxis1_value{2})
            
            y_axis1_2 = yaxis1_value{2}{L_num1};
            
        else
            
            y_axis1_2 = NaN;
            
        end
        
        if ~isempty(yaxis1_value{3})
            
            y_axis1_3 = yaxis1_value{3}{L_num1};
            
        else
            
            y_axis1_3 = NaN;
            
        end
        
        if ~isempty(yaxis1_value{4})
            
            y_axis1_4 = yaxis1_value{4}{L_num1};
            
        else
            
            y_axis1_4 = NaN;
            
        end
        
        if ne(max(size(y_axis1_1)), 1)
            
            x_axis_values = (1:size(y_axis1_1, 2))';
            
        elseif ne(max(size(y_axis1_2)), 1)
            
            x_axis_values = (1:size(y_axis1_2, 2))';
            
        elseif ne(max(size(y_axis1_3)), 1)
            
            x_axis_values = (1:size(y_axis1_3, 2))';
            
        elseif ne(max(size(y_axis1_4)), 1)
            
            x_axis_values = (1:size(y_axis1_4, 2))';
            
        end
        
        x_axis_values = x_axis_values - 1;
        
        if ne(max(size(y_axis1_1)), 1)
            
            SNR_0_1 = SNR{1};
            SNR1 = 50 * ones(size(SNR_0_1, 1), 1);
            SNR1(SNR_0_1 <= 20) = 20;
            SNR1(SNR_0_1 >= 60) = 100;
            
        else
            
            y_axis1_1 = NaN * ones(size(x_axis_values));
            SNR1 = ones(size(x_axis_values));
            
        end
        
        if ne(max(size(y_axis1_2)), 1)
            
            SNR_0_2 = SNR{2};
            SNR2 = 50 * ones(size(SNR_0_2, 1), 1);
            SNR2(SNR_0_2 <= 20) = 20;
            SNR2(SNR_0_2 >= 60) = 100;
            
        else
            
            y_axis1_2 = NaN * ones(size(x_axis_values));
            SNR2 = ones(size(x_axis_values));
            
        end
        
        if ne(max(size(y_axis1_3)), 1)
            
            SNR_0_3 = SNR{3};
            SNR3 = 50 * ones(size(SNR_0_3, 1), 1);
            SNR3(SNR_0_3 <= 20) = 20;
            SNR3(SNR_0_3 >= 60) = 100;
            
        else
            
            y_axis1_3 = NaN * ones(size(x_axis_values));
            SNR3 = ones(size(x_axis_values));
            
        end
        
        if ne(max(size(y_axis1_4)), 1)
            
            SNR_0_4 = SNR{4};
            SNR4 = 50 * ones(size(SNR_0_4, 1), 1);
            SNR4(SNR_0_4 <= 20) = 20;
            SNR4(SNR_0_4 >= 60) = 100;
            
        else
            
            y_axis1_4 = NaN * ones(size(x_axis_values));
            SNR4 = ones(size(x_axis_values));
            
        end
        
    end
    
end

% Bottom Axis
% --------------------------------------------------------------------------------------------------------
if eq(popupmenu4_Value, 1)
    
    yaxis2_value = inputData.spec_int{ind};
    
    % Get the nanmean intensity
    % -----------------------------------------------------------------------------------------------------
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        if ~isempty(yaxis2_value{1})
            
            y_axis2_mean(1) = nanmean(yaxis2_value{1});
            y_axis2_std(1) = nanstd(yaxis2_value{1});
            
        else
            
            y_axis2_mean(1) = NaN;
            y_axis2_std(1) = NaN;
            
        end
        
        if ~isempty(yaxis2_value{2})
            
            y_axis2_mean(2) = nanmean(yaxis2_value{2});
            y_axis2_std(2) = nanstd(yaxis2_value{2});
            
        else
            
            y_axis2_mean(2) = NaN;
            y_axis2_std(2) = NaN;
            
        end
        
        if ~isempty(yaxis2_value{3})
            
            y_axis2_mean(3) = nanmean(yaxis2_value{3});
            y_axis2_std(3) = nanstd(yaxis2_value{3});
            
        else
            
            y_axis2_mean(3) = NaN;
            y_axis2_std(3) = NaN;
            
        end
        
        if ~isempty(yaxis2_value{4})
            
            y_axis2_mean(4) = nanmean(yaxis2_value{4});
            y_axis2_std(4) = nanstd(yaxis2_value{4});
            
        else
            
            y_axis2_mean(4) = NaN;
            y_axis2_std(4) = NaN;
            
        end
        
    % Else get point intensit
    % -----------------------------------------------------------------------------------------------------
    else
        
        if ~isempty(yaxis2_value{1})
            
            y_axis2_1 = yaxis2_value{1};
            
        else
            
            y_axis2_1 = NaN * ones(size(x_axis_values));
            
        end
        
        if ~isempty(yaxis2_value{2})
            
            y_axis2_2 = yaxis2_value{2};
            
        else
            
            y_axis2_2 = NaN * ones(size(x_axis_values));
            
        end
        
        if ~isempty(yaxis2_value{3})
            
            y_axis2_3 = yaxis2_value{3};
            
        else
            
            y_axis2_3 = NaN * ones(size(x_axis_values));
            
        end
        
        if ~isempty(yaxis2_value{4})
            
            y_axis2_4 = yaxis2_value{4};
            
        else
            
            y_axis2_4 = NaN * ones(size(x_axis_values));
            
        end
        
    end
    
else
    
    yaxis2_value = inputData.cc{ind};
    
    % Get the nanmean intensity
    % -----------------------------------------------------------------------------------------------------
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        if ~isempty(yaxis2_value{1})
            
            y_axis2_mean(1) = nanmean(yaxis2_value{1}{L_num2});
            y_axis2_std(1) = nanstd(yaxis2_value{1}{L_num2});
            
        else
            
            y_axis2_mean(1) = NaN;
            y_axis2_std(1) = NaN;
            
        end
        
        if ~isempty(yaxis2_value{2})
            
            y_axis2_mean(2) = nanmean(yaxis2_value{2}{L_num2});
            y_axis2_std(2) = nanstd(yaxis2_value{2}{L_num2});
            
        else
            
            y_axis2_mean(2) = NaN;
            y_axis2_std(2) = NaN;
            
        end
        
        if ~isempty(yaxis2_value{3})
            
            y_axis2_mean(3) = nanmean(yaxis2_value{3}{L_num2});
            y_axis2_std(3) = nanstd(yaxis2_value{3}{L_num2});
            
        else
            
            y_axis2_mean(3) = NaN;
            y_axis2_std(3) = NaN;
            
        end
        
        if ~isempty(yaxis2_value{4})
            
            y_axis2_mean(4) = nanmean(yaxis2_value{4}{L_num2});
            y_axis2_std(4) = nanstd(yaxis2_value{4}{L_num2});
            
        else
            
            y_axis2_mean(4) = NaN;
            y_axis2_std(4) = NaN;
            
        end
        
    % Else get the point Laguerres
    % -----------------------------------------------------------------------------------------------------
    else
        
        if ~isempty(yaxis2_value{1})
            
            y_axis2_1 = yaxis2_value{1}{L_num2};
            
        else
            
            y_axis2_1 = NaN * ones(size(x_axis_values));
            
        end
        
        if ~isempty(yaxis2_value{2})
            
            y_axis2_2 = yaxis2_value{2}{L_num2};
            
        else
            
            y_axis2_2 = NaN * ones(size(x_axis_values));
            
        end
        
        if ~isempty(yaxis2_value{3})
            
            y_axis2_3 = yaxis2_value{3}{L_num2};
            
        else
            
            y_axis2_3 = NaN * ones(size(x_axis_values));
            
        end
        
        if ~isempty(yaxis2_value{4})
            
            y_axis2_4 = yaxis2_value{4}{L_num2};
            
        else
            
            y_axis2_4 = NaN * ones(size(x_axis_values));
            
        end
        
    end
    
end

SNR_index = ones(1, 4);

if eq(get(handles.checkbox1, 'Value'), 0)
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        y_axis1_mean(1) = NaN;
        y_axis1_std(1) = NaN;
        y_axis2_mean(1) = NaN;
        y_axis2_std(1) = NaN;
                
    else
        
        y_axis1_1 = NaN * ones(size(y_axis1_1));        
        y_axis2_1 = NaN * ones(size(y_axis2_1));
        
    end
    
    SNR_index(1) = 0;
    
end

if eq(get(handles.checkbox2, 'Value'), 0)
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        y_axis1_mean(2) = NaN;
        y_axis1_std(2) = NaN;
        y_axis2_mean(2) = NaN;
        y_axis2_std(2) = NaN;
        
    else
        
        y_axis1_2 = NaN * ones(size(y_axis1_2));
        y_axis2_2 = NaN * ones(size(y_axis2_2));
        
    end
    
    SNR_index(2) = 0;
    
end

if eq(get(handles.checkbox3, 'Value'), 0)
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        y_axis1_mean(3) = NaN;
        y_axis1_std(3) = NaN;
        y_axis2_mean(3) = NaN;
        y_axis2_std(3) = NaN;
        
    else
        
        y_axis1_3 = NaN * ones(size(y_axis1_3));
        y_axis2_3 = NaN * ones(size(y_axis2_3));
        
    end
    
    SNR_index(3) = 0;
    
end

if eq(get(handles.checkbox4, 'Value'), 0)
    
    if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
        
        y_axis1_mean(4) = NaN;
        y_axis1_std(4) = NaN;
        y_axis2_mean(4) = NaN;
        y_axis2_std(4) = NaN;
        
    else
        
        y_axis1_4 = NaN * ones(size(y_axis1_4));
        y_axis2_4 = NaN * ones(size(y_axis2_4));
        
    end
    
    SNR_index(4) = 0;
    
end

% Plot for nanmean Values

if eq(getappdata(handles.figure1, 'TypeTRIPLEX'), 1)
    
    axes(handles.axes1);
    h1 = getappdata(handles.figure1, 'BlackPlot1TRIPLEX');
    delete(h1);
    hold on
    h1(1) = errorbar(1:4, [y_axis1_mean(1) y_axis1_mean(2) y_axis1_mean(3) y_axis1_mean(4)], [y_axis1_std(1) y_axis1_std(2) y_axis1_std(3) y_axis1_std(4)], 'k.', 'MarkerSize', 20);
    h1(2) = plot(1:4, y_axis1_mean, 'k--', 'LineWidth', 3);
    hold off
    
    setappdata(handles.figure1, 'BlackPlot1TRIPLEX', h1);
    
    axes(handles.axes2);
    h2 = getappdata(handles.figure1, 'BlackPlot2TRIPLEX');
    delete(h2);
    hold on
    h2(1) = errorbar(1:4, [y_axis2_mean(1) y_axis2_mean(2) y_axis2_mean(3) y_axis2_mean(4)], [y_axis2_std(1) y_axis2_std(2) y_axis2_std(3) y_axis2_std(4)], 'k.', 'MarkerSize', 20);
    h2(2) = plot(1:4, y_axis2_mean, 'k--', 'LineWidth', 3);
    hold off
    
    setappdata(handles.figure1, 'BlackPlot2TRIPLEX', h2);
    
else
    
    axes(handles.axes1);
    h1 = getappdata(handles.figure1, 'BlackPlot1TRIPLEX');
    delete(h1);
    hold on
    h1(1) = plot(x_axis_values, y_axis1_1, 'k-');
    h1(2) = plot(x_axis_values, y_axis1_2, 'k--');
    h1(3) = plot(x_axis_values, y_axis1_3, 'k-.');
    h1(4) = plot(x_axis_values, y_axis1_4, 'k-o');
    legend({'CH1' 'CH2' 'CH3' 'CH4'});
    h1(5) = scatter(x_axis_values, y_axis1_1, SNR1, 'k', 'fill');
    h1(6) = scatter(x_axis_values, y_axis1_2, SNR2, 'k', 'fill');
    h1(7) = scatter(x_axis_values, y_axis1_3, SNR3, 'k', 'fill');
    h1(8) = scatter(x_axis_values, y_axis1_4, SNR4, 'k', 'fill');
    hold off
    
    setappdata(handles.figure1, 'BlackPlot1TRIPLEX', h1);    
    
    axes(handles.axes2);
    h2 = getappdata(handles.figure1, 'BlackPlot2TRIPLEX');
    delete(h2);
    hold on
    h2(1) = plot(x_axis_values, y_axis2_1, 'k-');
    h2(2) = plot(x_axis_values, y_axis2_2, 'k--');
    h2(3) = plot(x_axis_values, y_axis2_3, 'k-.');
    h2(4) = plot(x_axis_values, y_axis2_4, 'k-o');    
    h2(5) = scatter(x_axis_values, y_axis2_1, SNR1, 'k', 'fill');
    h2(6) = scatter(x_axis_values, y_axis2_2, SNR2, 'k', 'fill');
    h2(7) = scatter(x_axis_values, y_axis2_3, SNR3, 'k', 'fill');
    h2(8) = scatter(x_axis_values, y_axis2_4, SNR4, 'k', 'fill');
    hold off
    
    setappdata(handles.figure1, 'BlackPlot2TRIPLEX', h2);
    
end

SNR1 = inputData.SNR{ind};
SNR1 = cell2mat(SNR1(logical(SNR_index)));
less20dB = find(le(SNR1, 20));
str_less20dB = ['SNR<=20dB = ' num2str(size(less20dB, 1))];
set(handles.text27, 'String', str_less20dB);

great60dB = find(ge(SNR1, 60));
str_great60dB = ['SNR>=60dB = ' num2str(size(great60dB, 1))];
set(handles.text29, 'String', str_great60dB);

str_between2060dB = ['20dB<=SNR<=60dB = ' num2str(size(SNR1(:), 1) - size(great60dB, 1) - size(less20dB, 1))];
set(handles.text28, 'String', str_between2060dB);

axes(handles.axes2);
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{ind}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        legend({[num2str(groupnum(ind)) '_' prefixes{ind}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{ind}}, 'Interpreter', 'none');
        
    end
    
end