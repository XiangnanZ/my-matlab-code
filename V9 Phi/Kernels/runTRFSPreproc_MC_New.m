function flagvalue = runTRFSPreproc_MC_New(handles)

% Initialization
inputNames = getappdata(handles.figure1, 'outputNames');
wavelengths = inputNames.wavelengths;
wv = unique(wavelengths(:, 2));
fullfileNames = inputNames.filenames;
timeStep = inputNames.timeResolution;
calibration = getappdata(handles.figure1, 'Wavelength_Calib');
prefixes = inputNames.prefixes;

% Reset Axis
axes(handles.axes1);
cla reset;
hold off
axis on
box on

axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on

% Colormap
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    col = jet(size(wv, 1));
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        col0 = jet(size(wv, 1));
        cmin = min(groupnum);
        cmax = max(groupnum);
        idx = min(size(wv, 1), round((size(wv, 1) - 1) * (groupnum - cmin) / (cmax - cmin)) + 1);
        col = col0(idx, :);
        
    catch err
        
        col = jet(size(wv, 1));
        
    end
    
end

% Axis titles
set(handles.Title1, 'String', 'Integrated Intensity');
set(handles.Title2, 'String', 'Measured Voltage');

% Create output matrices
intIntensity = cell(wv(end), 1);
SNR = cell(wv(end), 1);
trunc_Intensity = cell(wv(end), 1);
raw_Intensity = cell(wv(end), 1);
wavelengths_list = cell(wv(end), 1);
gain_list = cell(wv(end), 1);
BG_scaled = cell(wv(end), 1);

% Flags for slider and plot type
flagSlider = 0;
flagType = 0;

% If more than one measurements open slider
if gt(wv(end), 1)
    
    set(handles.slider2, 'enable', 'off');
    set(handles.slider2, 'Visible', 'on');
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', wv(end), 'SliderStep', [(1 / (wv(end) - 1)) (1 / (wv(end) - 1))]);
    flagSlider = 1;
    
end

kk = 0;
setappdata(handles.figure1, 'BGrem_flag', 0);
for i = 1:wv(end)
    
    if eq(flagSlider, 1)
        
        set(handles.slider2, 'Value', i);
        
    end
    
    [~, spec] = DataLoader(fullfileNames{i});
    spec = -spec';
    
    noise = mean(spec((end - 100):end, :));
    spec = bsxfun(@minus, spec, noise);
    trunc_Intensity{i, 1} = spec;
    
    t = find(eq(wavelengths(:, 2), wv(i)));
    wave_step = min(abs(unique(diff(wavelengths(t, 1)))));
    
    while ~kk
        quest1 = questdlg('Do you want to remove background fluorescence and/or adjust for the gain?',...
            'Well?', 'Remove BG and Gain Adj', 'Gain Adj Only', 'Nothing', 'Remove BG and Gain Adj');
        foldername_BG = [];
        if strcmp(quest1, 'Remove BG and Gain Adj')
            
            [BG_path, ~, ~] = fileparts(fullfileNames{i});
            [filename_BG, foldername_BG] = uigetfile('*.*', 'Please Select the Background Data from TRFS', BG_path);
            if ~foldername_BG
                
                flagvalue = 0;
                return
                
            end
            
            kk = 1;
            
        else
            
            kk = 1;
            
        end
        
    end
    
    if foldername_BG
        
        [spec_gainNorm, spec, gain_list{i}, BG_scaled{i}] = removeBG(filename_BG, foldername_BG, spec, wavelengths(t, :), gain_list{i}, handles);
        
        if isempty(spec_gainNorm)
            
            set(handles.pushbutton4, 'String', 'TRFS Preprocess');
            set(handles.text3, 'String', 'Number of Spectra: 0');
            waitfor(msgbox('Something went wrong'));
            flagvalue = 0;
            return
            
        end
        
    else
        
        if strcmp(quest1, 'Gain Adj Only')
            
            BGPos = load('PeaksPos.dat');
            
            gain_run = wavelengths(t, 3);
            gainfactor = BGPos(1, 4) - gain_run;
            gainfactor = gainfactor / 100;
            gainfactor = 3.^gainfactor;
            gain_list{i} = [gain_run (BGPos(1, 4) * ones(size(gain_run)))];
    
            spec_gainNorm = bsxfun(@times, spec, gainfactor');
            
        else
            
            spec_gainNorm = spec;
            gain_list{i} = [wavelengths(t, 3) wavelengths(t, 3)];
            
        end
        
    end
    
    if ~isempty(calibration)
        
        t00 = ismember(calibration(:, 1), round(wavelengths(t, 1)));        
        spec0 = bsxfun(@rdivide, spec_gainNorm, calibration(t00, 2)');
        
    else
        
        spec0 = spec_gainNorm;
      
    end    
      
    intIntensity{i} = trapz(spec0).*(str2double(get(handles.edit2, 'String')));
    fIRF_noise = spec0;
    aa = max(fIRF_noise) - mean(fIRF_noise((end - 500):end, :));
    bb = std(fIRF_noise((end - 500):end, :));
    SNR{i} = 20*log10(aa./bb);
    SNR{i}(aa < bb) = 0;
        
    raw_Intensity{i, 1} = spec0;
    
    wavelengths_list{i, 1} = wavelengths(t, 1);
    
    if eq(get(handles.radiobutton4, 'Value'), 1)
        
        flagType = 1;
        x_axes1_values = wavelengths_list{i, 1};
        y_axes1_values = intIntensity{i}./norm(intIntensity{i});
        
        x_axes2_values = (timeStep{i} * [1:size(spec, 1)]);
        %         y_axes2_values = bsxfun(@rdivide, spec, max(spec));
        y_axes2_values = spec0;
        
        axes(handles.axes2);
        plot(x_axes2_values', y_axes2_values)
        xlabel('Time (ns)')
        ylabel('Normalized Voltage (a.u.)')
        ylim([0 1])
             
        if eq(get(handles.radiobutton1, 'Value'), 1)
            
            axes(handles.axes1);
            hold on
            plot(x_axes1_values, y_axes1_values, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Normalized Intensity (a.u.)')
            ylim([0 1]);
            xlim([min(unique(wavelengths(:, 1))) max(unique(wavelengths(:, 1)))]);
            hold off
            
        elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
            
            xx = linspace(min(wavelengths_list{i, 1}), max(wavelengths_list{i, 1}), (max(wavelengths_list{i, 1}) - min(wavelengths_list{i, 1})));
            
            % 2.6 Apply spline interpolation on the plane x-y (use
            % intIntensity{i}./norm(intIntensity(1:size(t, 1),
            % i)) because of the NaNs
            [~, pp] = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i}./norm(intIntensity{i}));
            yy = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i}./norm(intIntensity{i}), pp/2);
            y1 = fnval(yy, xx);
            
            axes(handles.axes1);
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Normalized Intensity (a.u.)')
            ylim([0 1]);
            xlim([min(unique(wavelengths(:, 1))) max(unique(wavelengths(:, 1)))])
            hold off
            
        end
        
    elseif eq(get(handles.radiobutton5, 'Value'), 1)
        
        flagType = 2;
        x_axes1_values = wavelengths_list{i, 1};
        y_axes1_values = intIntensity{i};
        
        x_axes2_values = (timeStep{i} * [1:size(spec, 1)]);
        y_axes2_values = spec0;
        
        axes(handles.axes2);
        plot(x_axes2_values', y_axes2_values)
        xlabel('Time (ns)')
        ylabel('Voltage (V)')
        ylim([0 1])
        
        if eq(get(handles.radiobutton1, 'Value'), 1)
            
            axes(handles.axes1);
            hold on
            plot(x_axes1_values, y_axes1_values, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Absolute Intensity (a.u.)')
            ylim([0 25]);
            xlim([min(unique(wavelengths(:, 1))) max(unique(wavelengths(:, 1)))])
            hold off
            
        elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
            
            xx = linspace(min(wavelengths_list{i, 1}), max(wavelengths_list{i, 1}), (max(wavelengths_list{i, 1}) - min(wavelengths_list{i, 1})));
            
            % 2.6 Apply spline interpolation on the plane x-y
            [~, pp] = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i});
            yy = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i}, pp/2);
            y1 = fnval(yy, xx);
            
            axes(handles.axes1);
            hold on
            plot(xx, y1, 'color', col(i,:))
            xlabel('Wavelengths (nm)');
            ylabel('Absolute Intensity (a.u.)')
            ylim([0 25]);
            xlim([min(unique(wavelengths(:, 1))) max(unique(wavelengths(:, 1)))])
            hold off
            
        end
        
    end
    
    pause(.01)
    clear spec t noise
    
end

if eq(get(handles.radiobutton4, 'Value'), 1)
    
    x_axes1_values = wavelengths_list{i, 1};
    y_axes1_values = intIntensity{i}./norm(intIntensity{i});
    axes(handles.axes1);
    ydata01 = get(gca, 'Children');
    ydata1 = get(ydata01, 'YData');
    if iscell(ydata1)
        
        ylim([0 max(cellfun(@nanmax, ydata1))])
        
    else
        
        ylim([0 nanmax(ydata1)])
        
    end
    xlim([min(unique(wavelengths(:, 1))) max(unique(wavelengths(:, 1)))])
    
    if eq(get(handles.radiobutton1, 'Value'), 1)
        
        hold on
        h(1) = plot(x_axes1_values, y_axes1_values, 'k-', 'LineWidth', 3);
        h(2) = h(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
        
       xx = linspace(min(wavelengths_list{i, 1}), max(wavelengths_list{i, 1}), (max(wavelengths_list{i, 1}) - min(wavelengths_list{i, 1})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i}./norm(intIntensity{i}));
        yy = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i}./norm(intIntensity{i}), pp/2);
        y1 = fnval(yy, xx);
        
        axes(handles.axes1);
        hold on
        h(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton3, 'Value'), 1)
            
            h(2) = plot(wavelengths_list{i, 1}, intIntensity{i}./norm(intIntensity{i}), 'k.', 'MarkerSize', 15);
            
        else
            
            h(2) = h(1);
            
        end
        
        hold off
        
    end
    
elseif eq(get(handles.radiobutton5, 'Value'), 1)
    
    axes(handles.axes1);
    ylim([0 max(cellfun(@max, intIntensity))])
    xlim([min(unique(wavelengths(:, 1))) max(unique(wavelengths(:, 1)))])
    
    if eq(get(handles.radiobutton1, 'Value'), 1)
        
        hold on
        h(1) = plot(wavelengths_list{i, 1}, intIntensity{i}, 'k-', 'LineWidth', 3);
        h(2) = h(1);
        hold off
        
    elseif or(eq(get(handles.radiobutton2, 'Value'), 1), eq(get(handles.radiobutton3, 'Value'), 1))
        
        t = find(eq(wavelengths(:, 2), wv(i)));
        xx = linspace(min(wavelengths_list{i, 1}), max(wavelengths_list{i, 1}), (max(wavelengths_list{i, 1}) - min(wavelengths_list{i, 1})));
        
        % 2.6 Apply spline interpolation on the plane x-y
        [~, pp] = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i});
        yy = csaps(min(wavelengths_list{i, 1}):wave_step:max(wavelengths_list{i, 1}), intIntensity{i}, pp/2);
        y1 = fnval(yy, xx);
        
        axes(handles.axes1);
        hold on
        h(1) = plot(xx, y1, 'k-', 'LineWidth', 3);
        
        if eq(get(handles.radiobutton3, 'Value'), 1)
            
            h(2) = plot(wavelengths_list{i, 1}, intIntensity{i}, 'k.', 'MarkerSize', 15);
            
        else
            
            h(2) = h(1);
            
        end
        
        hold off
        
    end
    
end

setappdata(handles.figure1, 'BlackPlot', h);
setappdata(handles.figure1, 'FlagType', flagType);

rawOutputData.intIntensity = intIntensity;
rawOutputData.SNR = SNR;
rawOutputData.wavelengths_list = wavelengths_list;
rawOutputData.trunc_Intensity = trunc_Intensity;
rawOutputData.raw_Intensity = raw_Intensity;
rawOutputData.gain_list = gain_list;
rawOutputData.BG_scaled = BG_scaled;

setappdata(handles.figure1, 'rawOutputData', rawOutputData);

if eq(flagSlider, 1)
    
    set(handles.slider2, 'enable', 'on');
    
end

setappdata(handles.figure1, 'sliderFlag', flagSlider);

axes(handles.axes2);
if eq(get(handles.radiobutton12, 'Value'), 1)
    
    legend({prefixes{i}}, 'Interpreter', 'none');
    
elseif eq(get(handles.radiobutton11, 'Value'), 1)
    
    try
        
        groupnum = str2double(inputNames.Group);
        legend({[num2str(groupnum(i)) '_' prefixes{i}]}, 'Interpreter', 'none');
        
    catch err
        
        legend({prefixes{i}}, 'Interpreter', 'none');
        
    end
    
end

flagvalue = 8;