function flagvalue = loadwaveCal(handles)

axes(handles.axes1);
cla reset;
axis on
box on
hold off

axes(handles.axes2);
cla reset;
axis on
box on
legend off
hold off

set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');

dataInfo = getappdata(handles.figure1, 'outputNames');
[dataPath, ~] = fileparts(dataInfo.filenames{1});

pathparts = strsplit(mfilename('fullpath'),filesep);
apppath = strjoin(pathparts(1:end-2),filesep);
SysID = zeros(size(dataInfo.filenames,1),1);
for i = 1:size(dataInfo.filenames,1)
    SysID(i) = dataInfo.SysID{i};
end
if length(SysID >= 2)
    if any(diff(SysID))
        warning('cannot automatically load calibration for different system IDs!')
        Auto_flag = 0; % still use manual selection
    else
        SysID = SysID(1);
    end
end
switch SysID
    case 3
        Auto_flag = 1; % load Sacramento_HoribaCalibration
    case 6
        Auto_flag = 2; % load Davis_HoribaCalibration
    case 26
        Auto_flag = 2; % load Davis_HoribaCalibration
    case 31
        Auto_flag = 3; % load CHROMEX_calibrationNew
    case 32
        Auto_flag = 3; % load CHROMEX_calibrationNew
    otherwise
        Auto_flag = 0; % still use manual selection
end

if ~Auto_flag
    [filename, foldername] = uigetfile('.mat', 'Please Select the Wavelength Calibration File', dataPath);
else
    foldername = fullfile(apppath,'WvCalibration');
    switch Auto_flag
        case 1
            filename = 'Sacramento_HoribaCalibration.mat';
        case 2
            filename = 'Davis_HoribaCalibration.mat';
        case 3
            filename = 'CHROMEX_calibrationNew.mat';
    end
end

if ~foldername
    
    flagvalue = 3;
    return
    
end

full_path = fullfile(foldername, filename);
% to address issue with network path that starts with '\\' like '\\marcufs'
% this might need to be updated on systems running ubuntun or iOS
if full_path(1) == '\'
    full_path = ['\' full_path];
end
wavelength_calib = load(full_path);
fName = fieldnames(wavelength_calib);
wavelength_calib = getfield(wavelength_calib, char(fName));

setappdata(handles.figure1, 'Wavelength_Calib', wavelength_calib);
flagvalue = 4;