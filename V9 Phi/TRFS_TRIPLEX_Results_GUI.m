function varargout = TRFS_TRIPLEX_Results_GUI(varargin)
%TRFS_TRIPLEX_RESULTS_GUI M-file for TRFS_TRIPLEX_Results_GUI.fig
%      TRFS_TRIPLEX_RESULTS_GUI, by itself, creates a new TRFS_TRIPLEX_RESULTS_GUI or raises the existing
%      singleton*.
%
%      H = TRFS_TRIPLEX_RESULTS_GUI returns the handle to a new TRFS_TRIPLEX_RESULTS_GUI or the handle to
%      the existing singleton*.
%
%      TRFS_TRIPLEX_RESULTS_GUI('Property','Value',...) creates a new TRFS_TRIPLEX_RESULTS_GUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to TRFS_TRIPLEX_Results_GUI_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      TRFS_TRIPLEX_RESULTS_GUI('CALLBACK') and TRFS_TRIPLEX_RESULTS_GUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in TRFS_TRIPLEX_RESULTS_GUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TRFS_TRIPLEX_Results_GUI

% Last Modified by GUIDE v2.5 21-Jul-2014 12:38:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TRFS_TRIPLEX_Results_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @TRFS_TRIPLEX_Results_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TRFS_TRIPLEX_Results_GUI is made visible.
function TRFS_TRIPLEX_Results_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for TRFS_TRIPLEX_Results_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% --------------------------------------------------------------------------------------------------------
% Center Window
%pixels
set(handles.figure1, 'Units', 'pixels');
%get your display size
screenSize = get(0, 'MonitorPositions');
screenSize = screenSize(1, :);
%calculate the center of the display
position = get(handles.figure1, 'Position');
position(1) = round((screenSize(3) - position(3)) / 2);
%center the window
set(handles.figure1, 'Position', position);

% UIWAIT makes TRFS_TRIPLEX_Results_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = TRFS_TRIPLEX_Results_GUI_OutputFcn(~, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Reset GUI
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Reset GUI
% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(~, ~, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
closeGUI = handles.figure1; %handles.figure1 is the GUI figure

guiPosition = get(handles.figure1, 'Position'); %get the position of the GUI
guiName = get(handles.figure1, 'Name'); %get the name of the GUI
close(closeGUI); %close the old GUI
eval(guiName) %call the GUI again
set(gcf,'Position',guiPosition); %set the position for the new GUI

% 2. Close GUI and return to TRFS_TRIPLEX_GUI
% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(~, ~, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
closeGUI = handles.figure1; %handles.figure1 is the GUI figure
close(closeGUI); %close the old GUI
TRFS_TRIPLEX_GUI;

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Slider options
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% --- Executes on slider movement.
function slider2_Callback(hObject, ~, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
p = path;
path('./Kernels', p);

ind = round(get(hObject, 'Value'));
set(hObject, 'Value', ind);

sliderFlag = getappdata(handles.figure1, 'sliderFlag');

if eq(sliderFlag, 1)
    
    axes(handles.axes1);
    delete(legend(gca));
    sliderTRFSPreprocResults(handles, ind);
    
elseif eq(sliderFlag, 2)
    
    axes(handles.axes1);
    delete(legend(gca));
    sliderTRFSProcResults(handles, ind);
    
elseif eq(sliderFlag, 3)
    
    axes(handles.axes1);
    delete(legend(gca));
    axes(handles.axes2);
    delete(legend(gca));    
    sliderTRIPLEXProcResults_axes1(handles, ind);
    sliderTRIPLEXProcResults_axes2(handles, ind)
        
end

path(p)

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, ~, ~)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% TRFS Preprocess
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Load Files
% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(~, ~, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

setappdata(handles.figure1, 'outputNames_TRFS', []);
setappdata(handles.figure1, 'outputResults_TRFS', []);
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
loadTRFSPreprocessResults(handles);

path(p);

% 2. Plot Results
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(~, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRFSPreProc(handles);

path(p);

% 3. CheckBox
% --- Executes on button press in checkbox1.
function checkbox1_Callback(~, ~, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRFSPreProc(handles);

path(p);

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% TRFS Process
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Load Files
% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(~, ~, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

setappdata(handles.figure1, 'outputNames_TRFSproc', []);
setappdata(handles.figure1, 'outputResults_TRFSproc', []);
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
loadTRFSProcessResults(handles);
a = getappdata(handles.figure1, 'outputResults_TRFSproc');

if ~isempty(a)
    
    L_num = size(a{1}.Laguerre_coeffs, 1);
    
    str_popupmenu1 = cell((L_num + 1), 1);
    str_popupmenu2 = cell((L_num + 1), 1);
    str_popupmenu1{1} = 'Lifetime';
    str_popupmenu2{1} = 'Int. Intensity';
    
    for i = 2:(L_num + 1)
        
        str_popupmenu1{i} = ['Laguerre ', num2str(i - 1)];
        str_popupmenu2{i} = ['Laguerre ', num2str(i - 1)];
        
    end
    
    set(handles.popupmenu1, 'String', str_popupmenu1);
    set(handles.popupmenu2, 'String', str_popupmenu2);
    
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    
end

path(p);

% 2. Plot Results
% --- Executes on button press in pushbutton3.
function pushbutton4_Callback(~, ~, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRFSProc(handles);

path(p);

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(~, ~, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

inputTRFSData = getappdata(handles.figure1, 'outputResults_TRFSproc');
inputNames = getappdata(handles.figure1, 'outputNames_TRFSproc');

if ~isempty(inputTRFSData)
    
    setappdata(handles.figure1, 'TRFS_Data', inputTRFSData);
    setappdata(handles.figure1, 'outputNames', inputNames);
    
    TRFS_PlotFitting_Results(handles);
    
end

path(p);

% 3. Check Boxes
% --- Executes on button press in checkbox2.
function checkbox2_Callback(~, ~, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRFSProc(handles);

path(p);

% --- Executes on button press in checkbox3.
function checkbox3_Callback(~, ~, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRFSProc(handles);

path(p);

% 4. Popup Menus
% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(~, ~, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRFSProc(handles);

path(p);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(~, ~, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRFSProc(handles);

path(p);

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% TRIPLEX Results
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% 1. Load Data
% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(~, ~, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

setappdata(handles.figure1, 'outputNames', []);
setappdata(handles.figure1, 'outputResults', []);
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
loadTRIPLEXProcessResults(handles);
a = getappdata(handles.figure1, 'outputResults');

if ~isempty(a)
    
   L_num = size(a{1}.Laguerre_coeffs{1}, 1);
    
    str_popupmenu1 = cell((L_num + 1), 1);
    str_popupmenu2 = cell((L_num + 1), 1);
    str_popupmenu1{1} = 'Lifetime';
    str_popupmenu2{1} = 'Int. Intensity';
    
    for i = 2:(L_num + 1)
        
        str_popupmenu1{i} = ['Laguerre ', num2str(i - 1)];
        str_popupmenu2{i} = ['Laguerre ', num2str(i - 1)];
        
    end
    
    set(handles.popupmenu3, 'String', str_popupmenu1);
    set(handles.popupmenu4, 'String', str_popupmenu2);
    
    set(handles.popupmenu3, 'Value', 1);
    set(handles.popupmenu4, 'Value', 1);
    
end

path(p);

% 2. Plot Results
% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(~, ~, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(~, ~, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

inputTRIPLEXData = getappdata(handles.figure1, 'outputResults');
inputNames = getappdata(handles.figure1, 'outputNames');

if ~isempty(inputTRIPLEXData)
    
    setappdata(handles.figure1, 'TRIPLEXresultsData', inputTRIPLEXData);
    setappdata(handles.figure1, 'TRIPLEXrawDataNames', inputNames);
    
    TRIPLEX_PlotFitting_Results(handles);
    
end

path(p);

path(p)

% 3. Popup Menus
% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(~, ~, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(~, ~, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, ~, ~)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% 4. CheckBoxes
% --- Executes on button press in checkbox4.
function checkbox4_Callback(~, ~, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --- Executes on button press in checkbox5.
function checkbox5_Callback(~, ~, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --- Executes on button press in checkbox6.
function checkbox6_Callback(~, ~, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --- Executes on button press in checkbox7.
function checkbox7_Callback(~, ~, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --- Executes on button press in checkbox8.
function checkbox8_Callback(~, ~, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox8
p = path;
path('./Kernels', p);

axes(handles.axes1);
cla reset;
hold off
axis on
box on
axes(handles.axes2);
cla reset;
legend off
hold off
axis on
box on
set(handles.slider2, 'enable', 'off');
set(handles.slider2, 'Visible', 'off');
set(handles.Title1, 'String', 'Title1');
set(handles.Title2, 'String', 'Title2');
set(handles.text28, 'String', 'SNR<=20dB = 0');
set(handles.text30, 'String', 'SNR>=60dB = 0');
set(handles.text29, 'String', '20dB<=SNR<=60dB = 0');
plotTRIPLEXProc_axes1(handles);
plotTRIPLEXProc_axes2(handles);

path(p);

% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% Axis Setup
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------

% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(~, ~, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

axes(handles.axes1);
legendAxes1 = get(legend(gca), 'String');
delete(legend(gca));
axes(handles.axes2);
legendAxes2 = get(legend(gca), 'String');
delete(legend(gca));
axisLimits(handles, legendAxes2, legendAxes1);

path(p)
