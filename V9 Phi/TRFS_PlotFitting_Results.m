function varargout = TRFS_PlotFitting_Results(varargin)
%TRFS_PLOTFITTING_RESULTS M-file for TRFS_PlotFitting_Results.fig
%      TRFS_PLOTFITTING_RESULTS, by itself, creates a new TRFS_PLOTFITTING_RESULTS or raises the existing
%      singleton*.
%
%      H = TRFS_PLOTFITTING_RESULTS returns the handle to a new TRFS_PLOTFITTING_RESULTS or the handle to
%      the existing singleton*.
%
%      TRFS_PLOTFITTING_RESULTS('Property','Value',...) creates a new TRFS_PLOTFITTING_RESULTS using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to TRFS_PlotFitting_Results_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      TRFS_PLOTFITTING_RESULTS('CALLBACK') and TRFS_PLOTFITTING_RESULTS('CALLBACK',hObject,...) call the
%      local function named CALLBACK in TRFS_PLOTFITTING_RESULTS.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TRFS_PlotFitting_Results

% Last Modified by GUIDE v2.5 21-Jul-2014 10:54:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TRFS_PlotFitting_Results_OpeningFcn, ...
                   'gui_OutputFcn',  @TRFS_PlotFitting_Results_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TRFS_PlotFitting_Results is made visible.
function TRFS_PlotFitting_Results_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for TRFS_PlotFitting_Results
handles.output = hObject;

% Update handles structure
setappdata(handles.figure1, 'PreviousGUI', varargin);
setappdata(handles.figure1, 'Point_Intex', 1);

p = path;
path('./Kernels', p);

TRFS_plot_raw_decon_Results(handles, 1, 1);

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};
out = getappdata(prevHandles.figure1, 'TRFS_Data');
set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', size(out{1}.lifet_avg, 1), 'SliderStep', [(1 / (size(out{1}.lifet_avg, 1) - 1)) (1 / (size(out{1}.lifet_avg, 1) - 1))]);

path(p)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TRFS_PlotFitting_Results wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TRFS_PlotFitting_Results_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(~, ~, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

pointIndex = getappdata(handles.figure1, 'Point_Intex');

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRFS_Data');

if eq(size(out, 1), pointIndex)
    
    pointIndex = 0;
    
end

pointIndex = pointIndex + 1;
setappdata(handles.figure1, 'Point_Intex', pointIndex);

TRFS_plot_raw_decon_Results(handles, 1, pointIndex);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg, 1) - 1))]);

path(p)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(~, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

closeGUI = handles.figure1;
close(closeGUI);

nf = findobj('Name','Signal_Gain'); % all graphical objects

if ~isempty(nf)
    
    close(nf);
    
end

% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
p = path;
path('./Kernels', p);

ind = round(get(hObject, 'Value'));
set(hObject, 'Value', ind);

pointIndex = getappdata(handles.figure1, 'Point_Intex');

TRFS_plot_raw_decon_Results(handles, ind, pointIndex);

path(p)


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

pointIndex = getappdata(handles.figure1, 'Point_Intex');

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRFS_Data');

if eq(pointIndex, 1)
    
    pointIndex = size(out, 1) + 1;
    
end

pointIndex = pointIndex - 1;
setappdata(handles.figure1, 'Point_Intex', pointIndex);

TRFS_plot_raw_decon_Results(handles, 1, pointIndex);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg, 1) - 1))]);

path(p)