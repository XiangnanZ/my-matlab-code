function varargout = TRIPLEX_PlotFitting(varargin)
% TRIPLEX_PLOTFITTING MATLAB code for TRIPLEX_PlotFitting.fig
%      TRIPLEX_PLOTFITTING, by itself, creates a new TRIPLEX_PLOTFITTING or raises the existing
%      singleton*.
%
%      H = TRIPLEX_PLOTFITTING returns the handle to a new TRIPLEX_PLOTFITTING or the handle to
%      the existing singleton*.
%
%      TRIPLEX_PLOTFITTING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRIPLEX_PLOTFITTING.M with the given input arguments.
%
%      TRIPLEX_PLOTFITTING('Property','Value',...) creates a new TRIPLEX_PLOTFITTING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TRIPLEX_PlotFitting_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TRIPLEX_PlotFitting_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TRIPLEX_PlotFitting

% Last Modified by GUIDE v2.5 11-Jun-2014 23:16:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TRIPLEX_PlotFitting_OpeningFcn, ...
                   'gui_OutputFcn',  @TRIPLEX_PlotFitting_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TRIPLEX_PlotFitting is made visible.
function TRIPLEX_PlotFitting_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TRIPLEX_PlotFitting (see VARARGIN)

% Choose default command line output for TRIPLEX_PlotFitting
handles.output = hObject;

% Update handles structure
setappdata(handles.figure1, 'PreviousGUI', varargin);
setappdata(handles.figure1, 'Point_Intex', 1);

p = path;
path('./Kernels', p);

TRIPLEX_plot_raw_decon(handles, 1, 1, 1);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', 4, 'SliderStep', [(1 / 3) (1 / 3)]);

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};
out = getappdata(prevHandles.figure1, 'TRIPLEXresultsData');

if ~isempty(out.lifet_avg{1}{1})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{1}{1}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{1}{1}, 1) - 1)) (1 / (size(out.lifet_avg{1}{1}, 1) - 1))]);
    
elseif ~isempty(out.lifet_avg{1}{2})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{1}{2}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{1}{2}, 1) - 1)) (1 / (size(out.lifet_avg{1}{2}, 1) - 1))]);
    
elseif ~isempty(out.lifet_avg{1}{3})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{1}{3}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{1}{3}, 1) - 1)) (1 / (size(out.lifet_avg{1}{3}, 1) - 1))]);
    
else
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{1}{4}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{1}{4}, 1) - 1)) (1 / (size(out.lifet_avg{1}{4}, 1) - 1))]);
    
end
  
path(p)

guidata(hObject, handles);

% UIWAIT makes TRIPLEX_PlotFitting wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TRIPLEX_PlotFitting_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(~, ~, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

pointIndex = getappdata(handles.figure1, 'Point_Intex');

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRIPLEXresultsData');

if eq(size(out.lifet_avg, 1), pointIndex)
    
    pointIndex = 0;
    
end

pointIndex = pointIndex + 1;
setappdata(handles.figure1, 'Point_Intex', pointIndex);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', 4, 'SliderStep', [(1 / 4) (1 / 4)]);

if ~isempty(out.lifet_avg{1}{1})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{1}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{1}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{1}, 1) - 1))]);
    
elseif ~isempty(out.lifet_avg{1}{2})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{2}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{2}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{2}, 1) - 1))]);
    
elseif ~isempty(out.lifet_avg{1}{3})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{3}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{3}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{3}, 1) - 1))]);
    
else
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{4}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{4}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{4}, 1) - 1))]);
    
end

TRIPLEX_plot_raw_decon(handles, 1, pointIndex, 1);

path(p)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(~, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

closeGUI = handles.figure1;
close(closeGUI);
nf = findobj('Name','Signal_Gain'); % all graphical objects

if ~isempty(nf)
    
    close(nf);
    
end


% --- Executes on slider movement.
function slider1_Callback(hObject, ~, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
p = path;
path('./Kernels', p);

ind = round(get(hObject, 'Value'));
set(hObject, 'Value', ind);

ind2 = round(get(handles.slider2, 'Value'));

pointIndex = getappdata(handles.figure1, 'Point_Intex');

TRIPLEX_plot_raw_decon(handles, ind, pointIndex, ind2);

path(p)


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, ~, ~)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
p = path;
path('./Kernels', p);

ind2 = round(get(hObject, 'Value'));
set(hObject, 'Value', ind2);

ind = round(get(handles.slider1, 'Value'));

pointIndex = getappdata(handles.figure1, 'Point_Intex');

TRIPLEX_plot_raw_decon(handles, ind, pointIndex, ind2);

path(p)

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

pointIndex = getappdata(handles.figure1, 'Point_Intex');

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRIPLEXresultsData');

if eq(pointIndex, 1)
    
    pointIndex = size(out.lifet_avg, 1) + 1;
    
end

pointIndex = pointIndex - 1;
setappdata(handles.figure1, 'Point_Intex', pointIndex);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', 4, 'SliderStep', [(1 / 4) (1 / 4)]);

if ~isempty(out.lifet_avg{1}{1})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{1}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{1}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{1}, 1) - 1))]);
    
elseif ~isempty(out.lifet_avg{1}{2})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{2}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{2}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{2}, 1) - 1))]);
    
elseif ~isempty(out.lifet_avg{1}{3})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{3}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{3}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{3}, 1) - 1))]);
    
else
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out.lifet_avg{pointIndex}{4}, 1), 'SliderStep', [(1 / (size(out.lifet_avg{pointIndex}{4}, 1) - 1)) (1 / (size(out.lifet_avg{pointIndex}{4}, 1) - 1))]);
    
end

TRIPLEX_plot_raw_decon(handles, 1, pointIndex, 1);

path(p)
