function varargout = TRIPLEX_PlotFitting_Results(varargin)
%TRIPLEX_PLOTFITTING_RESULTS M-file for TRIPLEX_PlotFitting_Results.fig
%      TRIPLEX_PLOTFITTING_RESULTS, by itself, creates a new TRIPLEX_PLOTFITTING_RESULTS or raises the existing
%      singleton*.
%
%      H = TRIPLEX_PLOTFITTING_RESULTS returns the handle to a new TRIPLEX_PLOTFITTING_RESULTS or the handle to
%      the existing singleton*.
%
%      TRIPLEX_PLOTFITTING_RESULTS('Property','Value',...) creates a new TRIPLEX_PLOTFITTING_RESULTS using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to TRIPLEX_PlotFitting_Results_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      TRIPLEX_PLOTFITTING_RESULTS('CALLBACK') and TRIPLEX_PLOTFITTING_RESULTS('CALLBACK',hObject,...) call the
%      local function named CALLBACK in TRIPLEX_PLOTFITTING_RESULTS.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TRIPLEX_PlotFitting_Results

% Last Modified by GUIDE v2.5 21-Jul-2014 12:21:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TRIPLEX_PlotFitting_Results_OpeningFcn, ...
                   'gui_OutputFcn',  @TRIPLEX_PlotFitting_Results_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TRIPLEX_PlotFitting_Results is made visible.
function TRIPLEX_PlotFitting_Results_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for TRIPLEX_PlotFitting_Results
handles.output = hObject;

% Update handles structure
setappdata(handles.figure1, 'PreviousGUI', varargin);
setappdata(handles.figure1, 'Point_Intex', 1);

p = path;
path('./Kernels', p);

TRIPLEX_plot_raw_decon_Results(handles, 1, 1, 1);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', 4, 'SliderStep', [(1 / 3) (1 / 3)]);

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};
out = getappdata(prevHandles.figure1, 'TRIPLEXresultsData');

if ~isempty(out{1}.lifet_avg{1})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{1}.lifet_avg{1}, 1), 'SliderStep', [(1 / (size(out{1}.lifet_avg{1}, 1) - 1)) (1 / (size(out{1}.lifet_avg{1}, 1) - 1))]);
    
elseif ~isempty(out{1}.lifet_avg{2})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{1}.lifet_avg{2}, 1), 'SliderStep', [(1 / (size(out{1}.lifet_avg{2}, 1) - 1)) (1 / (size(out{1}.lifet_avg{2}, 1) - 1))]);
    
elseif ~isempty(out{1}.lifet_avg{3})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{1}.lifet_avg{3}, 1), 'SliderStep', [(1 / (size(out{1}.lifet_avg{3}, 1) - 1)) (1 / (size(out{1}.lifet_avg{3}, 1) - 1))]);
    
else
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{1}.lifet_avg{4}, 1), 'SliderStep', [(1 / (size(out{1}.lifet_avg{4}, 1) - 1)) (1 / (size(out{1}.lifet_avg{4}, 1) - 1))]);
    
end
  
path(p)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TRIPLEX_PlotFitting_Results wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TRIPLEX_PlotFitting_Results_OutputFcn(~, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(~, ~, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

pointIndex = getappdata(handles.figure1, 'Point_Intex');

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRIPLEXresultsData');

if eq(size(out, 1), pointIndex)
    
    pointIndex = 0;
    
end

pointIndex = pointIndex + 1;
setappdata(handles.figure1, 'Point_Intex', pointIndex);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', 4, 'SliderStep', [(1 / 4) (1 / 4)]);

if ~isempty(out{pointIndex}.lifet_avg{1})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{1}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{1}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{1}, 1) - 1))]);
    
elseif ~isempty(out{pointIndex}.lifet_avg{2})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{2}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{2}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{2}, 1) - 1))]);
    
elseif ~isempty(out{pointIndex}.lifet_avg{3})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{3}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{3}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{3}, 1) - 1))]);
    
else
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{4}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{4}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{4}, 1) - 1))]);
    
end

TRIPLEX_plot_raw_decon_Results(handles, 1, pointIndex, 1);

path(p)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(~, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

closeGUI = handles.figure1;
close(closeGUI);
nf = findobj('Name','Signal_Gain'); % all graphical objects

if ~isempty(nf)
    
    close(nf);
    
end

% --- Executes on slider movement.
function slider1_Callback(hObject, ~, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
p = path;
path('./Kernels', p);

ind = round(get(hObject, 'Value'));
set(hObject, 'Value', ind);

ind2 = round(get(handles.slider2, 'Value'));

pointIndex = getappdata(handles.figure1, 'Point_Intex');

TRIPLEX_plot_raw_decon_Results(handles, ind, pointIndex, ind2);

path(p)


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, ~, ~)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
p = path;
path('./Kernels', p);

ind2 = round(get(hObject, 'Value'));
set(hObject, 'Value', ind2);

ind = round(get(handles.slider1, 'Value'));

pointIndex = getappdata(handles.figure1, 'Point_Intex');

TRIPLEX_plot_raw_decon_Results(handles, ind, pointIndex, ind2);

path(p)

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
p = path;
path('./Kernels', p);

pointIndex = getappdata(handles.figure1, 'Point_Intex');

prevHandles = getappdata(handles.figure1, 'PreviousGUI');
prevHandles = prevHandles{1};

out = getappdata(prevHandles.figure1, 'TRIPLEXresultsData');

if eq(pointIndex, 1)
    
    pointIndex = size(out, 1) + 1;
    
end

pointIndex = pointIndex - 1;
setappdata(handles.figure1, 'Point_Intex', pointIndex);

set(handles.slider1, 'Value', 1, 'Min', 1, 'Max', 4, 'SliderStep', [(1 / 4) (1 / 4)]);

if ~isempty(out{pointIndex}.lifet_avg{1})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{1}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{1}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{1}, 1) - 1))]);
    
elseif ~isempty(out{pointIndex}.lifet_avg{2})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{2}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{2}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{2}, 1) - 1))]);
    
elseif ~isempty(out{pointIndex}.lifet_avg{3})
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{3}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{3}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{3}, 1) - 1))]);
    
else
    
    set(handles.slider2, 'Value', 1, 'Min', 1, 'Max', size(out{pointIndex}.lifet_avg{4}, 1), 'SliderStep', [(1 / (size(out{pointIndex}.lifet_avg{4}, 1) - 1)) (1 / (size(out{pointIndex}.lifet_avg{4}, 1) - 1))]);
    
end

TRIPLEX_plot_raw_decon_Results(handles, 1, pointIndex, 1);

path(p)