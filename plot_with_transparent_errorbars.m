% INPUTS
% x - x-values of plot, column vector
% y - y-values of plot, column vector
% stdev - standard deviation values of y
% color - specifies color of plot. example value : '--r'
% A - [0 1] value to set transparency. 0.2 is best
% OUTPUTS
% out - the handle value of the plot
% this is useful to specify legend names

% EXAMPLE WITH LEGEND
% x = [1:0.1:10];
% y = sin(x);
% stdy = rand(size(x)); %<- standard deviation of y
% z = cos(x);
% stdz = rand(size(x)); %<- standard deviation of y
%
% out1 = plot_with_transparent_errorbars(x,y,stdy,'--r',0.2);
% hold on
% out2 = plot_with_transparent_errorbars(x,z,stdz,'--b',0.2);
% % legend
% ll=legend([out1 out2],{'sine','cosine'});
% set(ll, 'Box', 'off','location','NorthEast')

% ATTENTION!!!!! can not handle NaN


function [out] = plot_with_transparent_errorbars(x,y,stdev,color,A)

% filter out NaN
nan_position = ~(isnan(x)|isnan(y)|isnan(stdev));
x = x(nan_position);
y = y(nan_position);
stdev = stdev(nan_position);

patch_x = [x' fliplr(x')];%one dimention array, can be col or row vector
patch_y = [(y' + stdev') fliplr((y'-stdev'))];%one dimention array, can be col or row vector
if isempty(find(color=='-',1))==0
    k1 = color(end-1:end);
else
    k1 = color;
end
patch(patch_x,patch_y,k1,'facealpha',A,'edgecolor','none'); % facealpha or transparency value is set to 0.2
hold on
out=plot(x,y,color,'linewidth',0.5);
hold off
end