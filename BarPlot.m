barwitherr([Endo(:,3) Nonendo(:,3)],[Endo(:,2) Nonendo(:,2)])
colormap jet
title('Channel 1 Lifetime')
xlabel('Sample #')
ylabel('Lifetime(ns)')
ylim([5 6.5])
legend('Endo','Nonendo')
ax = gca;
ax.XTickLabel={'1','2','3','4','Scaffold'};

figure
barwitherr([Endo(:,5) Nonendo(:,5)],[Endo(:,4) Nonendo(:,4)])
colormap jet
title('Channel 2 Lifetime')
xlabel('Sample #')
ylabel('Lifetime(ns)')
ylim([5 6.5])
legend('Endo','Nonendo')
ax = gca;
ax.XTickLabel={'1','2','3','4','Scaffold'};