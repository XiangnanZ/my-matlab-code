function varargout = FLIm_Data_GUI(varargin)
% FLIM_DATA_GUI MATLAB code for FLIm_Data_GUI.fig
%      FLIM_DATA_GUI, by itself, creates a new FLIM_DATA_GUI or raises the existing
%      singleton*.
%
%      H = FLIM_DATA_GUI returns the handle to a new FLIM_DATA_GUI or the handle to
%      the existing singleton*.
%
%      FLIM_DATA_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIM_DATA_GUI.M with the given input arguments.
%
%      FLIM_DATA_GUI('Property','Value',...) creates a new FLIM_DATA_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIm_Data_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIm_Data_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIm_Data_GUI

% Last Modified by GUIDE v2.5 07-Jun-2016 13:19:36

% Begin initialization code - DO NOT EDIT


gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIm_Data_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIm_Data_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FLIm_Data_GUI is made visible.
function FLIm_Data_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIm_Data_GUI (see VARARGIN)

% Choose default command line output for FLIm_Data_GUI
global xstep ystep int_range lt_range
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FLIm_Data_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FLIm_Data_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in LoadDataButton.
function LoadDataButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadDataButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[Files,dirpath]=uigetfile('*.mat','Select result file(s)' ); 
load([dirpath Files]);

%transpose the image 
INT1=INT1';
INT2=INT2';
INT3=INT3';
INT4=INT4';
LT1=LT1';
LT2=LT2';
LT3=LT3';
LT4=LT4';

global xstep ystep int_range lt_range x y 

xstep = str2num(get(handles.Xres,'String'));
ystep = str2num(get(handles.Yres,'String'));
int_min = str2num(get(handles.IntMin,'String'));
int_max = str2num(get(handles.IntMax,'String'));
lt_min = str2num(get(handles.LTMin,'String'));
lt_max = str2num(get(handles.LTMax,'String'));

int_range = [int_min,int_max];
lt_range = [lt_min,lt_max];

x_length = size(LT1,2);
y_length = size(LT1,1);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);



axes(handles.axes1)
fig1=imagesc(x,y,INT1);
axis equal
colorbar
title('CH1 Intensity')

axes(handles.axes2)
fig2=histogram(INT1,100);
title('CH1 Int Histogram')

axes(handles.axes3)
fig3=imagesc(x,y,LT1);
axis equal
colorbar
title('CH1 lifetime')

axes(handles.axes4)
fig4=histogram(LT1,100);
title('CH1 LT Histogram')

handles.INT1 = INT1;
handles.LT1 = LT1;
handles.INT2 = INT2;
handles.LT2 = LT2;
handles.INT3 = INT3;
handles.LT3 = LT3;
handles.x = x;
handles.y = y;
handles.xstep = xstep;
handles.ystep = ystep;

% handles.fig1=fig1;
% handles.fig2=fig2;
% handles.fig3=fig3;
% handles.fig4=fig4;

guidata(hObject,handles)









function IntThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to IntThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of IntThreshold as text
%        str2double(get(hObject,'String')) returns contents of IntThreshold as a double


% --- Executes during object creation, after setting all properties.
function IntThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IntThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LTThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to LTThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LTThreshold as text
%        str2double(get(hObject,'String')) returns contents of LTThreshold as a double


% --- Executes during object creation, after setting all properties.
function LTThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LTThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CalculateLTaverageButton.
function CalculateLTaverageButton_Callback(hObject, eventdata, handles)
% hObject    handle to CalculateLTaverageButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xstep ystep int_range lt_range

xstep = str2num(get(handles.Xres,'String'));
ystep = str2num(get(handles.Yres,'String'));
int_min = str2num(get(handles.IntMin,'String'));
int_max = str2num(get(handles.IntMax,'String'));
lt_min = str2num(get(handles.LTMin,'String'));
lt_max = str2num(get(handles.LTMax,'String'));

int_range = [int_min,int_max];
lt_range = [lt_min,lt_max];

int_thres=str2num(get(handles.IntThreshold,'String'));
lt_thres=str2num(get(handles.LTThreshold,'String'));

try
INT1 = handles.INT1_ROI;
catch
     INT1 =handles.INT1;
end

try
LT1 = handles.LT1_ROI;
catch
    LT1 =handles.LT1;
end

try
INT2 = handles.INT2_ROI;
catch
     INT2 =handles.INT2;
end

try
LT2 = handles.LT2_ROI;
catch
    LT2 =handles.LT2;
end

try
INT3 = handles.INT3_ROI;
catch
     INT3 =handles.INT3;
end

try
LT3 = handles.LT3_ROI;
catch
    LT3 =handles.LT3;
end

y = 0:1:size(LT1,1);
x = 0:1:size(LT1,2);
% all channels are thresholded by CH1
LT1(INT1<int_thres) = NaN;
LT1(LT1<lt_thres) = NaN;

LT2(isnan(LT1)) = NaN;

LT3(isnan(LT1)) = NaN;

axes(handles.axes4)
imagesc(x,y,LT2,lt_range);
axis equal
colorbar
CH1_lt_avg = nanmean(LT1(:));
CH1_lt_std = nanstd(LT1(:));
CH2_lt_avg = nanmean(LT2(:));
CH2_lt_std = nanstd(LT2(:));
CH3_lt_avg = nanmean(LT3(:));
CH3_lt_std = nanstd(LT3(:));

set(handles.CH1LTAvg,'String',num2str(CH1_lt_avg));
set(handles.CH1LTStd,'String',num2str(CH1_lt_std));
set(handles.CH2LTAvg,'String',num2str(CH2_lt_avg));
set(handles.CH2LTStd,'String',num2str(CH2_lt_std));
set(handles.CH3LTAvg,'String',num2str(CH3_lt_avg));
set(handles.CH3LTStd,'String',num2str(CH3_lt_std));

handles.CH1_lt_avg = CH1_lt_avg;
handles.CH1_lt_std = CH1_lt_std;
handles.CH2_lt_avg = CH2_lt_avg;
handles.CH2_lt_std = CH2_lt_std;
handles.CH3_lt_avg = CH3_lt_avg;
handles.CH3_lt_std = CH3_lt_std;


guidata(hObject,handles)



function CH1LTAvg_Callback(hObject, eventdata, handles)
% hObject    handle to CH1LTAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CH1LTAvg as text
%        str2double(get(hObject,'String')) returns contents of CH1LTAvg as a double


% --- Executes during object creation, after setting all properties.
function CH1LTAvg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CH1LTAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1
% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1



function Xres_Callback(hObject, eventdata, handles)
% hObject    handle to Xres (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Xres as text
%        str2double(get(hObject,'String')) returns contents of Xres as a double


% --- Executes during object creation, after setting all properties.
function Xres_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Xres (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Yres_Callback(hObject, eventdata, handles)
% hObject    handle to Yres (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Yres as text
%        str2double(get(hObject,'String')) returns contents of Yres as a double


% --- Executes during object creation, after setting all properties.
function Yres_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Yres (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function X1_Callback(hObject, eventdata, handles)
% hObject    handle to X1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of X1 as text
%        str2double(get(hObject,'String')) returns contents of X1 as a double


% --- Executes during object creation, after setting all properties.
function X1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to X1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Y1_Callback(hObject, eventdata, handles)
% hObject    handle to Y1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Y1 as text
%        str2double(get(hObject,'String')) returns contents of Y1 as a double


% --- Executes during object creation, after setting all properties.
function Y1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Y1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function X2_Callback(hObject, eventdata, handles)
% hObject    handle to X2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of X2 as text
%        str2double(get(hObject,'String')) returns contents of X2 as a double


% --- Executes during object creation, after setting all properties.
function X2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to X2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Y2_Callback(hObject, eventdata, handles)
% hObject    handle to Y2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Y2 as text
%        str2double(get(hObject,'String')) returns contents of Y2 as a double


% --- Executes during object creation, after setting all properties.
function Y2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Y2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CropButton.
function CropButton_Callback(hObject, eventdata, handles)
% hObject    handle to CropButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global lt_range
xstep = handles.xstep;
ystep = handles.ystep;
LT1 = handles.LT1;
INT1 = handles.INT1;
LT2 = handles.LT2;
INT2 = handles.INT2;
LT3 = handles.LT3;
INT3 = handles.INT3;
y1 = str2num(get(handles.X1,'String'))/xstep;
y2 = str2num(get(handles.X2,'String'))/xstep;
x1 = str2num(get(handles.Y1,'String'))/ystep;
x2 = str2num(get(handles.Y2,'String'))/ystep;
x1 =ceil(x1);
x2 =floor(x2);
y1 = ceil(y1);
y2 = floor(y2);
if x1==0
    x1=1;
end
if y1==0
    y1=1;
end
LT1_ROI = LT1(x1:x2,y1:y2);
INT1_ROI = INT1(x1:x2,y1:y2);
LT2_ROI = LT2(x1:x2,y1:y2);
INT2_ROI = INT2(x1:x2,y1:y2);
LT3_ROI = LT3(x1:x2,y1:y2);
INT3_ROI = INT3(x1:x2,y1:y2);
y=0:1:size(LT1_ROI,1);
x=0:1:size(LT1_ROI,2);

axes(handles.axes2);
imagesc(x,y,LT1_ROI, lt_range);
axis equal
colorbar

handles.LT1_ROI = LT1_ROI;
handles.INT1_ROI = INT1_ROI;
handles.LT2_ROI = LT2_ROI;
handles.INT2_ROI = INT2_ROI;
handles.LT3_ROI = LT3_ROI;
handles.INT3_ROI = INT3_ROI;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes3


% --- Executes during object creation, after setting all properties.
function axes4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes4



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function IntMin_Callback(hObject, eventdata, handles)
% hObject    handle to IntMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of IntMin as text
%        str2double(get(hObject,'String')) returns contents of IntMin as a double


% --- Executes during object creation, after setting all properties.
function IntMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IntMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function IntMax_Callback(hObject, eventdata, handles)
% hObject    handle to IntMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of IntMax as text
%        str2double(get(hObject,'String')) returns contents of IntMax as a double


% --- Executes during object creation, after setting all properties.
function IntMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IntMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LTMin_Callback(hObject, eventdata, handles)
% hObject    handle to LTMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LTMin as text
%        str2double(get(hObject,'String')) returns contents of LTMin as a double


% --- Executes during object creation, after setting all properties.
function LTMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LTMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LTMax_Callback(hObject, eventdata, handles)
% hObject    handle to LTMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LTMax as text
%        str2double(get(hObject,'String')) returns contents of LTMax as a double


% --- Executes during object creation, after setting all properties.
function LTMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LTMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CH1LTStd_Callback(hObject, eventdata, handles)
% hObject    handle to CH1LTStd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CH1LTStd as text
%        str2double(get(hObject,'String')) returns contents of CH1LTStd as a double


% --- Executes during object creation, after setting all properties.
function CH1LTStd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CH1LTStd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ReplotButton.
function ReplotButton_Callback(hObject, eventdata, handles)
% hObject    handle to ReplotButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xstep ystep int_range lt_range


LT1 = handles.LT1;
INT1 = handles.INT1;
LT2 = handles.LT2;
INT2 = handles.INT2;
LT3 = handles.LT3;
INT3 = handles.INT3;

xstep = str2num(get(handles.Xres,'String'));
ystep = str2num(get(handles.Yres,'String'));
int_min = str2num(get(handles.IntMin,'String'));
int_max = str2num(get(handles.IntMax,'String'));
lt_min = str2num(get(handles.LTMin,'String'));
lt_max = str2num(get(handles.LTMax,'String'));

int_range = [int_min,int_max];
lt_range = [lt_min,lt_max];

x_length = size(LT1,2);
y_length = size(LT1,1);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);

axes(handles.axes1);
fig1=imagesc(x,y,INT1);
axis equal
colorbar
title('CH1 Intensity')
caxis(int_range);

axes(handles.axes3);
fig3=imagesc(x,y,LT1);
axis equal
colorbar
title('CH1 lifetime')
caxis(lt_range);

% drawnow

guidata(hObject,handles)







function CH2LTAvg_Callback(hObject, eventdata, handles)
% hObject    handle to CH2LTAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CH2LTAvg as text
%        str2double(get(hObject,'String')) returns contents of CH2LTAvg as a double


% --- Executes during object creation, after setting all properties.
function CH2LTAvg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CH2LTAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CH2LTStd_Callback(hObject, eventdata, handles)
% hObject    handle to CH2LTStd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CH2LTStd as text
%        str2double(get(hObject,'String')) returns contents of CH2LTStd as a double


% --- Executes during object creation, after setting all properties.
function CH2LTStd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CH2LTStd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CH3LTAvg_Callback(hObject, eventdata, handles)
% hObject    handle to CH3LTAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CH3LTAvg as text
%        str2double(get(hObject,'String')) returns contents of CH3LTAvg as a double


% --- Executes during object creation, after setting all properties.
function CH3LTAvg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CH3LTAvg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CH3LTStd_Callback(hObject, eventdata, handles)
% hObject    handle to CH3LTStd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CH3LTStd as text
%        str2double(get(hObject,'String')) returns contents of CH3LTStd as a double


% --- Executes during object creation, after setting all properties.
function CH3LTStd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CH3LTStd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object deletion, before destroying properties.
function CropButton_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to CropButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function CropButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CropButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sample_ID = get(handles.edit20,'String');
file_name = get(handles.edit21,'String');
fileID = fopen(strcat(file_name,'.txt'),'a+');
Data = [handles.CH1_lt_avg handles.CH1_lt_std handles.CH2_lt_avg handles.CH2_lt_std handles.CH2_lt_avg handles.CH3_lt_std];
fprintf(fileID,'%6s %6.4f %6.4f %6.4f %6.4f %6.4f %6.4f\n',sample_ID, Data);
fclose(fileID);




function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
closeGUI = handles.figure1; %handles.figure1 is the GUI figure
guiName = get(handles.figure1, 'Name'); %get the name of the GUI
close(closeGUI); %close the old GUI
eval(guiName) %call the GUI again
