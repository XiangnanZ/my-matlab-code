% script to process OCT data
clear all
close all

Data= csvread('C:\Users\XZ\Desktop\20160525OCT\123_Ch2.csv',0,4);
data = Data(:,1);
data = data(250:750);
plot(data);
k_h=hilbert(data);
phase = angle(k_h);
plot(phase);
Q = unwrap(phase);
hold on
plot(Q);
