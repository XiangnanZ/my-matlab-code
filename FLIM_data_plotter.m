
close all
clear all
%% load data
%  load('M:\Tissue Engineering Project 2015\Data\20151211 Collagen gel day 0\FLIM\2015_12_11_gel1 6mgperml\Result\map.mat')
filefolder = 'C:\Users\XZ\Desktop\Data\20160617 Depleted Cartolage XSection\Reconstructed Image All';
[imagefile,filepath]=uigetfile('*.mat','Select result file(s)',filefolder,'MultiSelect', 'on'); 
filepath
try
load(fullfile(filepath,imagefile{1}));
NumberOfFiles = length(imagefile);
catch
    load(fullfile(filepath,imagefile));
    NumberOfFiles = 1;
end




for j = 1:NumberOfFiles
if NumberOfFiles==1
        load(fullfile(filepath,imagefile));
        [pathstr,name,ext] = fileparts(imagefile);
    else
    load(fullfile(filepath,imagefile{j}));
    [pathstr,name,ext] = fileparts(imagefile{j});
    end

%% step up plotting axis
ystep = 0.05;
xstep = 0.05;
x_length = size(LT1,1);
y_length = size(LT1,2);
x = linspace(0,x_length*xstep,x_length);
y = linspace(0,y_length*ystep,y_length);

CH1_int_range = [0 max(INT1(:))];
CH2_int_range = [0 max(INT2(:))];
CH3_int_range = [0 max(INT3(:))];
CH4_int_range = [0 max(INT4(:))];
LT_range = [5.5 6];
%% 
%set pixels that has a negative intensity to 0 and normalize intensity 
%so that it can be used with intensity weighted lifetime calculation
%intensity up limit to filter out plastic background
% int_high = 0.5;
INT1(INT1<0)=0;
% INT1(INT1>int_high)=0;
INT1_norm = INT1/max(INT1(:));
INT2(INT2<0)=0;
% INT2(INT2>int_high)=0;
INT2_norm = INT2/max(INT2(:));
INT3(INT3<0)=0;
% INT3(INT3>int_high)=0;
INT3_norm = INT3/max(INT3(:));

INT4(INT4<0)=0;
INT4_norm = INT4/max(INT4(:));
% use mask
% M1 = zeros(size(INT1));
% M2 = zeros(size(INT2));
% M3 = zeros(size(INT3));
% M4 = zeros(size(INT4));
% M1(SNR1>25) = 1;
% M2(SNR2>25) = 1;
% M3(SNR3>25) = 1;
% M4(SNR4>25) = 1;



% INT1 = INT1.*M1;
% INT2 = INT2.*M2;
% INT3 = INT3.*M3;
% INT4 = INT4.*M4;
% LT1 = LT1.*M1;
% LT2 = LT2.*M2;
% LT3 = LT3.*M3;
% LT4 = LT4.*M4;



%% plot CH1 and CH2
h1=figure;

colormap jet
subplot(3,2,1);
imagesc(x,y,INT1', CH1_int_range);
colorbar
axis equal
% Ytick=[0 2.5 5];
title('Ch 1 Intensity')
xlabel('mm')
ylabel('mm')

subplot(3,2,2);
imagesc(x,y,INT2', CH2_int_range);
colorbar
axis equal
title('Ch 2 Intensity')
xlabel('mm')
ylabel('mm')

subplot(3,2,3);
imagesc(x,y,LT1', LT_range);
colorbar
axis equal
title('Ch 1 Lifetime')
xlabel('mm')
ylabel('mm')
subplot(3,2,4);
imagesc(x,y,LT2', LT_range);
colorbar
axis equal
title('Ch 2 Lifetime')
xlabel('mm')
ylabel('mm')

subplot(3,2,5);
image_lifet(x,y,LT1',INT1_norm',1);
caxis(LT_range);
colorbar
axis equal
title('Ch 1 Intensity weighted Lifetime')
xlabel('mm')
ylabel('mm')
subplot(3,2,6);
image_lifet(x,y,LT2',INT2_norm',1);
caxis(LT_range);
colorbar
axis equal
title('Ch 2 Intensity weighted Lifetime')
xlabel('mm')
ylabel('mm')

set(gcf,'NextPlot','add');
axes;
h = title(name);
set(gca,'Visible','off');
set(h,'Visible','on');
%% plot CH3 and CH4
% h2=figure;
% 
% subplot(3,2,1);
% imagesc(x,y,INT3', CH3_int_range);
% colorbar
% axis equal
% title('Ch 3 Intensity')
% xlabel('mm')
% ylabel('mm')
% 
% subplot(3,2,2);
% imagesc(x,y,INT4', CH4_int_range);
% colorbar
% axis equal
% title('Ch 4 Intensity')
% xlabel('mm')
% ylabel('mm')
% 
% subplot(3,2,3);
% imagesc(x,y,LT3', LT_range);
% colorbar
% axis equal
% title('Ch 3 Lifetime')
% xlabel('mm')
% ylabel('mm')
% 
% subplot(3,2,4);
% imagesc(x,y,LT4', LT_range);
% colorbar
% axis equal
% title('Ch 4 Lifetime')
% xlabel('mm')
% ylabel('mm')
% 
% subplot(3,2,5);
% image_lifet(x,y,LT3',INT3_norm',1);
% caxis(LT_range);
% colorbar
% axis equal
% title('Ch 3 Intensity weighted Lifetime')
% xlabel('mm')
% ylabel('mm')
% 
% subplot(3,2,6);
% image_lifet(x,y,LT4',INT4_norm',1);
% caxis(LT_range);
% colorbar
% axis equal
% title('Ch 4 Intensity weighted Lifetime')
% xlabel('mm')
% ylabel('mm')


%% cd to directory and save fig file
olddir = cd(filepath);
savefig(h1,strcat(name,'_CH1&2.fig'));
% savefig(h2,strcat(name,'_CH3&4.fig'));

cd(olddir)
%% Intensity ratio plot
% int_ratio_ch1 = INT1./(INT1+INT2+INT3);
% int_ratio_ch2 = INT2./(INT1+INT2+INT3);
% int_ratio_ch3 = INT3./(INT1+INT2+INT3);
% figure(2)
% % subplot(1,3,1);
% imagesc(x,y,int_ratio_ch1',[0 1]);
% axis equal
% title('CH1/CH123')
% % subplot(1,3,2);
% figure
% imagesc(x,y,int_ratio_ch2',[0 1]);
% axis equal
% title('CH2/CH123')
% % subplot(1,3,3);
% figure
% imagesc(x,y,int_ratio_ch3',[0 1]);
% axis equal
% title('CH3/CH123')
% cd(dirpath)
% savefig('Slide3.fig')
end

disp('Finished');