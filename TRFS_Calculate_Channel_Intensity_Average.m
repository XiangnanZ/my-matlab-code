%script to calculate the equvilant intensity averaged lifetime value from TRFS data.
%% 
close all
clear all

%% load data
folderpath = 'C:\Users\XZ\Desktop\Data\20160516 Cartilage construct HA_LOX_LINK TRFS _all\DeCon alpha0.9';
[TRFSFiles,DataFolder] = uigetfile('*_all_*.mat*','Please select FLIm data file',...
    folderpath,'MultiSelect', 'on');

try
load(fullfile(DataFolder,TRFSFiles{1}));
NumOfFiles = length(TRFSFiles);
catch
    load(fullfile(DataFolder,TRFSFiles));
    NumOfFiles = 1;
end
%% setup channel wavelength
CH1_range = [376 402];
CH2_range = [426 479];
CH3_range = [512 573];
CH4_range = [499 611];


cd(folderpath)
file_name = '20160516 Cartilage construct HA_LOX_LINK TRFS Summary';
fileID = fopen(strcat(file_name,'.txt'),'at+');
%% start loop
for i = 1:NumOfFiles
    if NumOfFiles==1
        load(fullfile(DataFolder,TRFSFiles));
        [pathstr,name,ext] = fileparts(TRFSFiles);
    else
    load(fullfile(DataFolder,TRFSFiles{i}));
    [pathstr,name,ext] = fileparts(TRFSFiles{i});
    end
    
sample_name = name(1:9);
%% setup variables


alpha_tau_square = norm_spec_int.*avg_lt.^2;
alpha_tau = norm_spec_int.*avg_lt;
%% find indexes

CH1_id = CH1_range(1)<=lambda&lambda<=CH1_range(2);
CH2_id = CH2_range(1)<=lambda&lambda<=CH2_range(2);
CH3_id = CH3_range(1)<=lambda&lambda<=CH3_range(2);
CH4_id = CH4_range(1)<=lambda&lambda<=CH4_range(2);

%% calculate average intensity

CH1=sum(alpha_tau_square(CH1_id))/sum(alpha_tau(CH1_id));
CH2=sum(alpha_tau_square(CH2_id))/sum(alpha_tau(CH2_id));
CH3=sum(alpha_tau_square(CH3_id))/sum(alpha_tau(CH3_id));
CH4=sum(alpha_tau_square(CH4_id))/sum(alpha_tau(CH4_id));



sample_ID = sample_name;

Data = [CH1 CH2 CH3 CH4];
if i==1
    header=['Sample_ID  CH1  CH2  CH3  CH4'];
    fprintf(fileID,'%-20s\n',header);
    end
fprintf(fileID,'%-10s %2.4f %2.4f %2.4f %2.4f\n',sample_ID, Data);


end
fclose(fileID);