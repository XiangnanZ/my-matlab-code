% code to convert all fig to jpeg files
close all
clear all

% d=dir('*.fig'); % capture everything in the directory with FIG extension
% allNames={d.name}; % extract names of all FIG-files
[FigFiles,DataFolder] = uigetfile('*.fig*','Please select FLIm data file',...
    'C:\Users\XZ\Desktop\20160408_collagen_GAG_Study\DeCon','MultiSelect', 'on');
close all; % close any open figures
cd(DataFolder)
for i=1:length(FigFiles)
      open(FigFiles{i}); % open the FIG-file
%       set(gca,'FontSize',14);
      
      base=strtok(FigFiles{i},'.'); % chop off the extension (".fig")
      print(base,'-djpeg','-r300'); % export to JPEG as usual
      close(gcf); % close it ("gcf" returns the handle to the current figure)
end