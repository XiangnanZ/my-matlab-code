%% FLIm Part

clear all
close all

% copy default path in 3rd argument
DataFolder = uigetdir('M:\Tissue Engineering Project\Data','Please select folder');
cd(DataFolder)
FLImFiles = dir('*_TimeStamp*');
FLImFileNames = {FLImFiles.name};
%% do convert
for i = 1:length(FLImFileNames)
    
    FLImFile = FLImFileNames{i};
    FLImFile = regexprep(FLImFile,'_TimeStamp.txt','');
    % FLIm Data
    SysID = 35;
    InputRange = 1;
    Offset = -0.25;
    TimeResolution = 0.08;
    BW = 3; % Hamamatsu
    numAvg = 64; % 4 should be default
    
    % load time stamp file
    ts = load(fullfile(DataFolder,strcat(FLImFile,'_TimeStamp.txt')));
    
    % Save FLIm Header
    fid = fopen(fullfile(DataFolder,strcat(FLImFile,'_header')),'w+');
    fwrite(fid,SysID,'int32','l');
    fwrite(fid,InputRange,'double','l');
    fwrite(fid,Offset,'double','l');
    fwrite(fid,TimeResolution,'double','l');
    fwrite(fid,BW,'double','l');
    fwrite(fid,numAvg,'int32','l');
    [n,m] = size(ts);
    fwrite(fid,m,'int32','l');
    fwrite(fid,n,'int32','l');
    fwrite(fid,ts,'double','l');
    fclose(fid);
    
    
end